## Requirements

- `node '^7.6.0'`
- `npm '^4.1.2'`

### Development

```shell
$ npm run dev
```

Serves your app at `localip:3000`. HMR will be enabled in development. A proxy is used for when you  request http://localip:3000/, it will fetch http://localip:3001/ and return.

```shell
$ npm start
```

Runs `npm run dev` script.

```shell
$ npm run start:server
```

Starts the `dev` server with nodemon to serve your app at `localip:3000`.

```shell
$ npm run start:client:server
```

Starts the `webpack dev server` to serve your `webpack bundle` at `localip:3001` and enable HMR in development.


### Production

```shell
$ npm run build:client
```

It does some optimizations and compiles the client app, for the production, to disk (`~/readyToDeploy/static/dist`).

```shell
$ npm run build:server
```

It does some optimizations and compiles the server app, for the production, to disk (`~/readyToDeploy/server.js`).

```shell
$ npm run deploy
```

Runs `npm run build:client` and `npm run build:server` scripts.

```
   ERROR in ./src/static/images/*
    Module build failed: Error: dyld: Library not loaded: /usr/local/opt/libpng/lib/libpng16.16.dylib
```
If you get this sort of error run `brew install libpng`

Run this command to integrate remote-redux-devtools with iOS simulator. Devtools will be available on remotedev.io
```shell
$ ./node_modules/bin/remotedev --port=3001
```

```shell
$ cd readyToDeploy
$ npm install or yarn
$ npm start
```

Starts the `prod` server to serve your app at `localip:3000`.


### Linter

```shell
$ npm run lint
```

Lint all files in `~/src` and `~/__tests__`.
