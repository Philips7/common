import {SIGN_IN_ATTEMPT} from 'redux/modules/User'
import signInSaga from 'sagas/signIn'
import SagaTester from 'redux-saga-tester'

const action = {
  type: SIGN_IN_ATTEMPT,
  values: {
    client_id: '',
    client_secret: '',
    grant_type: '',
    username: '',
    password: ''
  },
  responseSuccess: () => ({}),
  responseFailure: () => ({}),
  resolve: () => ({}),
  reject: () => ({})
}

describe('(Sagas)', () => {
  it(`Should take ${SIGN_IN_ATTEMPT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(signInSaga().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
