import {SIGN_UP_NEWSLETTER_ATTEMPT} from 'redux/modules/User'
import signUpNewsletter from 'sagas/signUpNewsletter'
import SagaTester from 'redux-saga-tester'

const action = {
  type: SIGN_UP_NEWSLETTER_ATTEMPT,
  responseSuccess: () => ({}),
  responseFailure: () => ({})
}

describe('(Sagas)', () => {
  it(`Should take ${SIGN_UP_NEWSLETTER_ATTEMPT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(signUpNewsletter().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
