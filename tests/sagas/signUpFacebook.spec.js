import {SIGN_UP_FACEBOOK_ATTEMPT} from 'redux/modules/User'
import signUpFacebookSaga from 'sagas/signUpFacebook'
import SagaTester from 'redux-saga-tester'

const action = {
  type: SIGN_UP_FACEBOOK_ATTEMPT,
  values: {
    client_id: '',
    client_secret: '',
    grant_type: '',
    backend: '',
    token: ''
  },
  responseSuccess: () => ({}),
  responseFailure: () => ({}),
  resolve: () => ({}),
  reject: () => ({})
}

describe('(Sagas)', () => {
  it(`Should take ${SIGN_UP_FACEBOOK_ATTEMPT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(signUpFacebookSaga().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
