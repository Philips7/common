import {REFRESH_TOKEN_ATTEMPT} from 'redux/modules/User'
import refreshTokenSaga from 'sagas/refreshToken'
import SagaTester from 'redux-saga-tester'

const action = {
  type: REFRESH_TOKEN_ATTEMPT,
  values: {
    client_id: '',
    client_secret: '',
    grant_type: '',
    refresh_token: ''
  },
  responseSuccess: () => ({}),
  responseFailure: () => ({}),
  resolve: () => ({}),
  reject: () => ({})
}

describe('(Sagas)', () => {
  it(`Should take ${REFRESH_TOKEN_ATTEMPT}`, async () => {
    const sagaTester = new SagaTester({})
    sagaTester.start(refreshTokenSaga().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
