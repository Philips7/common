import {USER_UPDATE_ATTEMPT} from 'redux/modules/User'
import userUpdateSaga from 'sagas/userUpdate'
import SagaTester from 'redux-saga-tester'

const action = {
  type: USER_UPDATE_ATTEMPT,
  values: {
    email: ''
  },
  responseSuccess: () => ({}),
  responseFailure: () => ({}),
  resolve: () => ({}),
  reject: () => ({})
}

describe('(Sagas)', () => {
  it(`Should take ${USER_UPDATE_ATTEMPT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(userUpdateSaga().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
