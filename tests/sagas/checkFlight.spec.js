import {CHECK_FLIGHT_CONNECT} from 'redux/modules/Ticket'
import checkFlightSaga from 'sagas/checkFlight'
import SagaTester from 'redux-saga-tester'

const action = {
  type: CHECK_FLIGHT_CONNECT
}

describe('(Sagas)', () => {
  it(`Should take ${CHECK_FLIGHT_CONNECT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(checkFlightSaga().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
