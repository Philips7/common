import {SIGN_UP_ATTEMPT} from 'redux/modules/User'
import signUpSaga from 'sagas/signUp'
import SagaTester from 'redux-saga-tester'

const action = {
  type: SIGN_UP_ATTEMPT,
  values: {
    username: '',
    password: ''
  },
  responseSuccess: () => ({}),
  responseFailure: () => ({}),
  resolve: () => ({}),
  reject: () => ({})
}

describe('(Sagas)', () => {
  it(`Should take ${SIGN_UP_ATTEMPT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(signUpSaga().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
