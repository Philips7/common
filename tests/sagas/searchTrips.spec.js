import {SEARCH_TRIPS_CONNECT} from 'redux/modules/Trips'
import searchTripsSaga from 'sagas/searchTrips'
import SagaTester from 'redux-saga-tester'


const action = {
  type: SEARCH_TRIPS_CONNECT
}

describe('(Sagas)', () => {
  it(`Should take ${SEARCH_TRIPS_CONNECT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(searchTripsSaga().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
