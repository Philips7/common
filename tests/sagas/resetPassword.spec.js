import {RESET_PASSWORD_ATTEMPT} from 'redux/modules/ResetPassword'
import resetPasswordSaga from 'sagas/resetPassword'
import SagaTester from 'redux-saga-tester'

const action = {
  type: RESET_PASSWORD_ATTEMPT,
  values: {
    email: '',
    frontend_domain: '',
    reset_url: ''
  },
  responseSuccess: () => ({}),
  responseFailure: () => ({}),
  resolve: () => ({}),
  reject: () => ({})
}

describe('(Sagas)', () => {
  it(`Should take ${RESET_PASSWORD_ATTEMPT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(resetPasswordSaga().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
