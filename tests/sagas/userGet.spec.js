import {USER_GET_ATTEMPT} from 'redux/modules/User'
import userGetSaga from 'sagas/userGet'
import SagaTester from 'redux-saga-tester'

const action = {
  type: USER_GET_ATTEMPT,
  responseSuccess: () => ({}),
  responseFailure: () => ({}),
  resolve: () => ({}),
  reject: () => ({})
}

describe('(Sagas)', () => {
  it(`Should take ${USER_GET_ATTEMPT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(userGetSaga().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
