import {EMAIL_CONFIRM_ATTEMPT} from 'routes/EmailConfirm/modules/EmailConfirm.js'
import EmailChangeSaga from 'routes/EmailConfirm/modules/EmailChangeSaga.js'
import SagaTester from 'redux-saga-tester'

const action = {
  type: EMAIL_CONFIRM_ATTEMPT,
  values: {
    email: ''
  },
  responseSuccess: () => ({}),
  responseFailure: () => ({})
}

describe('(Sagas)', () => {
  it(`Should take ${EMAIL_CONFIRM_ATTEMPT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(EmailChangeSaga().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
