import reducer from 'routes/EmailConfirm/modules/EmailConfirm'
import {
  emailConfirmAttempt,
  emailConfirmSuccess,
  emailConfirmFailure
} from 'routes/EmailConfirm/modules/EmailConfirm.js'

describe('(Route) EmailConfirm', () => {
  describe('(Modules) EmailConfirm', () => {
    describe('Actions', () => {
      it('emailConfirmAttempt should return correct type', () => {
        expect(typeof emailConfirmAttempt()).to.eql('function')
      })
      it('emailConfirmSuccess should return correct type', () => {
        expect(typeof emailConfirmSuccess()).to.eql('function')
      })
      it('emailConfirmFailure should return correct type', () => {
        expect(typeof emailConfirmFailure()).to.eql(('function'))
      })
    })
    describe('Reducer', () => {

    })
  })
})
