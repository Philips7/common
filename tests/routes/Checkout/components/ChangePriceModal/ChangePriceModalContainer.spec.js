import React from 'react'
import ChangePriceModalContainer from 'routes/Checkout/components/ChangePriceModal/ChangePriceModalContainer'

describe('(Containers)', () => {
  describe('ChangePriceModalContainer', () => {
    it('Should be a function', () => {
      expect(typeof ChangePriceModalContainer).to.equal('function')
    })
  })
})
