import React from 'react'
import SelectBankModalContainer from 'routes/Checkout/components/SelectBankModal/SelectBankModalContainer'

describe('(Containers)', () => {
  describe('SelectBankModalContainer', () => {
    it('Should be a function', () => {
      expect(typeof SelectBankModalContainer).to.equal('function')
    })
  })
})
