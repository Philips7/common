import React from 'react'
import Checkout from 'routes/Checkout/components/Checkout'
import PaybackUnavailableModal from '../../../../src/components/modals/PaybackUnavailableModal/PaybackUnavailableModal'
import SelectBankModal from 'routes/Checkout/components/SelectBankModal/SelectBankModal'
import UnavailableTicketModal from 'routes/Checkout/components/UnavailableTicketModal/UnavailableTicketModal'
import {shallow} from 'enzyme'
import createStore from 'store/createStore'

const props = {
  checkFlightConnect: () => ({}),
  checkIfPaybackIsAvailable: () => ({})
}

describe('(Route) Checkout', () => {
  describe('(Components) Checkout', () => {
    it('Should render', () => {
      shallow(<Checkout {...props}/>)
    })
    describe('Modals', () => {
      const store = createStore({})
      it('PaybackUnavailableModal should render', () => {
        shallow(<PaybackUnavailableModal store={store}/>)
      })
      it('SelectBankModal should render', () => {
        shallow(<SelectBankModal store={store}/>)
      })
      it('UnavailableTicketModal should render', () => {
        shallow(<UnavailableTicketModal store={store}/>)
      })
    })
  })
})
