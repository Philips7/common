import React from 'react'
import UnavailableTicketModalContainer from 'routes/Checkout/components/UnavailableTicketModal/UnavailableTicketModalContainer'

describe('(Containers)', () => {
  describe('UnavailableTicketModalContainer', () => {
    it('Should be a function', () => {
      expect(typeof UnavailableTicketModalContainer).to.equal('function')
    })
  })
})
