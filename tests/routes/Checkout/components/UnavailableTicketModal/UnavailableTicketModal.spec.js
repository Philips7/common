import React from 'react'
import UnavailableTicketModal from 'routes/Checkout/components/UnavailableTicketModal/UnavailableTicketModal'
import {shallow} from 'enzyme'
import createStore from 'store/createStore'

describe('(Route) Checkout', () => {
  describe('(Components) UnavailableTicketModal', () => {
    it('Should render', () => {
      const store = createStore({})
      shallow(<UnavailableTicketModal store={store}/>)
    })
  })
})
