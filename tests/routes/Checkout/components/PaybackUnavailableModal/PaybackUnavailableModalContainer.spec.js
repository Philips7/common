import React from 'react'
import PaybackUnavailableModalContainer from '../../../../../src/components/modals/PaybackUnavailableModal/PaybackUnavailableModalContainer'

describe('(Containers)', () => {
  describe('PaybackUnavailableModalContainer', () => {
    it('Should be a function', () => {
      expect(typeof PaybackUnavailableModalContainer).to.equal('function')
    })
  })
})
