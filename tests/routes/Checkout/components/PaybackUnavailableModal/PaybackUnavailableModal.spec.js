import React from 'react'
import PaybackUnavailableModal from '../../../../../src/components/modals/PaybackUnavailableModal/PaybackUnavailableModal'
import {shallow} from 'enzyme'
import createStore from 'store/createStore'

describe('(Route) Checkout', () => {
  describe('(Components) PaybackUnavailableModal', () => {
    it('Should render', () => {
      const store = createStore({})
      shallow(<PaybackUnavailableModal store={store}/>)
    })
  })
})
