import reducer, {
  setPaybackUnavailable,
  setPaybackAvailable,
  checkIfPaybackIsAvailable,
  SET_PAYBACK_UNAVAILABLE,
  SET_PAYBACK_AVAILABLE
} from 'routes/Checkout/modules/Checkout'
import {setCookie} from 'redux-cookie'

describe('(Route) Checkout', () => {
  describe('(Modules) Checkout', () => {
    describe('Actions', () => {
      it('setPaybackUnavailable should set payback months to 1 and dispatch SET_PAYBACK_UNAVAILABLE', () => {
        let dispatch = sinon.spy(() => ({}))
        setPaybackUnavailable()(dispatch)

        expect(dispatch.getCall(0).calledWith(setCookie('payback', JSON.stringify({value: 1}))))
        expect(dispatch.getCall(1).calledWith({type: SET_PAYBACK_UNAVAILABLE}))
      })
      it('setPaybackAvailable should dispatch SET_PAYBACK_AVAILABLE', () => {
				const obj = {type: SET_PAYBACK_AVAILABLE}
				expect(setPaybackAvailable()).to.eql(obj)
      })
      //   ToDo: test cookies
    })
    describe('Reducer', () => {

    })
  })
})
