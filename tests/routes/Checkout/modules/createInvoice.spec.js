import {CREATE_INVOICE_CONNECT} from 'routes/Checkout/modules/Checkout.js'
import createInvoice from 'routes/Checkout/modules/createInvoice'
import SagaTester from 'redux-saga-tester'

const action = {
  type: CREATE_INVOICE_CONNECT,
}

describe('(Sagas)', () => {
  it(`Should take ${CREATE_INVOICE_CONNECT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(createInvoice().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
