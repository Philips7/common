import reducer from 'routes/Dashboard/modules/Dashboard'
import {
  GET_DASHBOARD_DATA_ATTEMPT,
  GET_DASHBOARD_DATA_SUCCESS,
  GET_DASHBOARD_DATA_FAILURE,
  GET_TICKETS_ATTEMPT,
  GET_TICKETS_SUCCESS,
  GET_TICKETS_FAILURE,
  REMOVE_TICKETS,
  getDashboardDataAttempt,
  getDashboardDataSuccess,
  getDashboardDataFailure,
  getTicketsAttempt,
  getTicketsSuccess,
  getTicketsFailure,
  showFailedModal,
  clearTickets,
  initialState
} from 'routes/Dashboard/modules/Dashboard.js'
import {LOGOUT} from 'redux/modules/User'

describe('(Route) Dashboard', () => {
  describe('(Modules) Dashboard', () => {
    describe('Actions', () => {
      it('getDashboardDataAttempt should attempt dashboard data', () => {
        const obj = {
          type: GET_DASHBOARD_DATA_ATTEMPT,
          responseSuccess: getDashboardDataSuccess,
          responseFailure: getDashboardDataFailure
        }
        expect(getDashboardDataAttempt()).to.eql(obj)
      })

      it('getDashboardDataSuccess should return dashboard data', () => {
        const obj = {
          type: GET_DASHBOARD_DATA_SUCCESS,
          data: 'a'
        }
        expect(getDashboardDataSuccess('a')).to.eql(obj)
      })

      it('getDashboardDataFailure should return error', () => {
        const obj = {
          type: GET_DASHBOARD_DATA_FAILURE
        }
        expect(getDashboardDataFailure()).to.eql(obj)
      })

      it('getTicketsAttempt should return propper type', () => {
        expect(getTicketsAttempt()).to.be.function
      })

      it('getTicketsSuccess should return propper type', () => {
        expect(typeof getTicketsSuccess).to.eql('function')
      })

      it('getTicketsFailure should return error', () => {
        const obj = {
          type: GET_TICKETS_FAILURE
        }
        expect(getTicketsFailure()).to.eql(obj)
      })

      it('clearTickets should return error', () => {
        const obj = {type: REMOVE_TICKETS}
        expect(clearTickets()).to.eql(obj)
      })

    })
    describe('Reducer', () => {
      it(`${GET_DASHBOARD_DATA_ATTEMPT} should attempt dashboard data`, () => {
        const expectedResult = {
          ...initialState,
          getDashboardDataLoading: true
        }

        const action = {
          type: GET_DASHBOARD_DATA_ATTEMPT
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${GET_DASHBOARD_DATA_SUCCESS} should return dashboard data`, () => {
        const expectedResult = {
          ...initialState,
          getDashboardDataLoading: false,
          statistics: undefined
        }

        const action = {
          type: GET_DASHBOARD_DATA_SUCCESS
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${GET_DASHBOARD_DATA_FAILURE} should return error`, () => {
        const expectedResult = {
          ...initialState,
          getDashboardDataLoading: false
        }

        const action = {
          type: GET_DASHBOARD_DATA_FAILURE
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${GET_TICKETS_ATTEMPT} should attempt tickets`, () => {
        const expectedResult = {
          ...initialState,
          getTicketsLoading: true
        }

        const action = {
          type: GET_TICKETS_ATTEMPT
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${GET_TICKETS_SUCCESS} should return tickets`, () => {
        const expectedResult = {
          ...initialState,
          getTicketsLoading: false,
          ticketsPage: 1,
          tickets: {
            0: {},
            results: {}
          }
        }

        const action = {
          type: GET_TICKETS_SUCCESS,
          data: {results: {}}
        }
        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${GET_TICKETS_FAILURE} should return error`, () => {
        const expectedResult = {
          ...initialState,
          getTicketsLoading: false
        }

        const action = {
          type: GET_TICKETS_FAILURE,
        }
        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${REMOVE_TICKETS} should remove tickets`, () => {
        const expectedResult = {
          ...initialState,
          tickets: initialState.tickets
        }

        const action = {
          type: REMOVE_TICKETS
        }
        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${LOGOUT} should return error`, () => {
        const action = {
          type: LOGOUT
        }
        const results = reducer(undefined, action)
        expect(results).to.eql(initialState)
      })

    })
  })
})
