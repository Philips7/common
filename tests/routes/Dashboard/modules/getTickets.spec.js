import {GET_TICKETS_ATTEMPT} from 'routes/Dashboard/modules/Dashboard'
import getTickets from 'routes/Dashboard/modules/getTickets'
import SagaTester from 'redux-saga-tester'

const action = {
  type: GET_TICKETS_ATTEMPT,
  responseSuccess: () => ({}),
  responseFailure: () => ({})
}

describe('(Sagas)', () => {
  it(`Should take ${GET_TICKETS_ATTEMPT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(getTickets().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
