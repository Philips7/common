import {GET_DASHBOARD_DATA_ATTEMPT} from 'routes/Dashboard/modules/Dashboard'
import getDashboardData from 'routes/Dashboard/modules/getDashboardData'
import SagaTester from 'redux-saga-tester'

const action = {
  type: GET_DASHBOARD_DATA_ATTEMPT,
  responseSuccess: () => ({}),
  responseFailure: () => ({})
}

describe('(Sagas)', () => {
  it(`Should take ${GET_DASHBOARD_DATA_ATTEMPT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(getDashboardData().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
