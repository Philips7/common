import {SEND_SUPPORT_MESSAGE_ATTEMPT} from 'routes/Support/modules/Support'
import sendSupportMessagge from 'routes/Support/modules/sendSupportMessagge'
import SagaTester from 'redux-saga-tester'

const action = {
  type: SEND_SUPPORT_MESSAGE_ATTEMPT,
  responseSuccess: () => ({}),
  responseFailure: () => ({})
}

describe('(Sagas)', () => {
  it(`Should take ${SEND_SUPPORT_MESSAGE_ATTEMPT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(sendSupportMessagge().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
