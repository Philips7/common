import {GET_PROBLEM_TYPES_ATTEMPT} from 'routes/Support/modules/Support'
import getProblemTypes from 'routes/Support/modules/getProblemTypes'
import SagaTester from 'redux-saga-tester'

const action = {
  type: GET_PROBLEM_TYPES_ATTEMPT,
  responseSuccess: () => ({}),
  responseFailure: () => ({})
}

describe('(Sagas)', () => {
  it(`Should take ${GET_PROBLEM_TYPES_ATTEMPT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(getProblemTypes().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
