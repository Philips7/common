import reducer,{
  GET_PROBLEM_TYPES_ATTEMPT,
  GET_PROBLEM_TYPES_SUCCESS,
  GET_PROBLEM_TYPES_FAILURE,
  SEND_SUPPORT_MESSAGE_ATTEMPT,
  SEND_SUPPORT_MESSAGE_SUCCESS,
  SEND_SUPPORT_MESSAGE_FAILURE,
  getProblemTypes,
  getProblemTypesSuccess,
  getProblemTypesFailure,
  sendSupportMessage,
  sendSupportMessageSuccess,
  sendSupportMessageFailure,
  openLoginModal,
  openPreLoginModal,
  hideLoginModal,
  initialState
} from 'routes/Support/modules/Support'


describe('(Route) Support', () => {
  describe('(Modules) Support', () => {
    describe('Actions', () => {
      it('getProblemTypes should attempt problem types', () => {
        const obj = {
          type: GET_PROBLEM_TYPES_ATTEMPT,
          responseSuccess: getProblemTypesSuccess,
          responseFailure: getProblemTypesFailure
        }
        expect(getProblemTypes()).to.eql(obj)
      })

      it('getProblemTypesSuccess should return problem types', () => {
        const obj = {type: GET_PROBLEM_TYPES_SUCCESS, types: undefined}
        expect(getProblemTypesSuccess()).to.eql(obj)
      })

      it('getProblemTypesFailure should return error', () => {
        expect(typeof getProblemTypesFailure()).to.eql.function
      })

      it('sendSupportMessage should attempt sending message', () => {
        const result = sendSupportMessage()(() => ({}))
        expect(result instanceof Promise).to.be.true
      })

      it('sendSupportMessageSuccess should send message', () => {
        const obj = {type: SEND_SUPPORT_MESSAGE_SUCCESS}
        expect(sendSupportMessageSuccess()).to.eql(obj)
      })

      it('sendSupportMessageFailure should return error', () => {
        expect(typeof sendSupportMessageFailure()).to.eql.function
      })

      it('openLoginModal should return error', () => {
        expect(typeof openLoginModal()).to.eql.function
      })

      it('openPreLoginModal should return error', () => {
        expect(typeof openPreLoginModal()).to.eql.function
      })

      it('hideLoginModal should return error', () => {
        expect(typeof hideLoginModal()).to.eql.function
      })

    })
    describe('Reducer', () => {
      it(`${GET_PROBLEM_TYPES_ATTEMPT} should attempt problme types`, () => {
        const expectedResult = {
          ...initialState,
          isProblemTypesLoading: true,
          showMessage: false
        }

        const action = {
          type: GET_PROBLEM_TYPES_ATTEMPT
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${GET_PROBLEM_TYPES_SUCCESS} should return problem types`, () => {
        const expectedResult = {
          ...initialState,
          isProblemTypesLoading: false,
          types: undefined
        }

        const action = {
          type: GET_PROBLEM_TYPES_SUCCESS
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${GET_PROBLEM_TYPES_FAILURE} should return error`, () => {
        const expectedResult = {
          ...initialState,
          isProblemTypesLoading: false
        }

        const action = {
          type: GET_PROBLEM_TYPES_FAILURE
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${SEND_SUPPORT_MESSAGE_ATTEMPT} should attempt support message`, () => {
        const expectedResult = {
          ...initialState,
          isSupportMessageLoading: true
        }

        const action = {
          type: SEND_SUPPORT_MESSAGE_ATTEMPT
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${SEND_SUPPORT_MESSAGE_SUCCESS} should return support message`, () => {
        const expectedResult = {
          ...initialState,
          isSupportMessageLoading: false,
          showMessage: true
        }

        const action = {
          type: SEND_SUPPORT_MESSAGE_SUCCESS
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${SEND_SUPPORT_MESSAGE_FAILURE} should return support message`, () => {
        const expectedResult = {
          ...initialState,
          isSupportMessageLoading: false
      }

        const action = {
          type: SEND_SUPPORT_MESSAGE_FAILURE
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })
    })
  })
})
