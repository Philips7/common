import {resendEmailAttempt, RESEND_EMAIL_ATTEMPT} from 'routes/EditInfo/modules/EditInfo.js'

describe('(Route) EditInfo', () => {
  describe('(Modules) EditInfo', () => {
    describe('Actions', () => {
      it('resendEmailAttempt should return correct type', () => {
        const action = {
          type: RESEND_EMAIL_ATTEMPT
        }
        expect(resendEmailAttempt()).to.eql(action)
      })
    })
  })
  describe('Reducer', () => {

  })
})
