import {RESEND_EMAIL_ATTEMPT} from 'routes/EditInfo/modules/EditInfo.js'
import ResendEmailSaga from 'routes/EditInfo/modules/resendEmail.js'
import SagaTester from 'redux-saga-tester'

const action = {
  type: RESEND_EMAIL_ATTEMPT,
}

describe('(Sagas)', () => {
  it(`Should take ${RESEND_EMAIL_ATTEMPT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(ResendEmailSaga().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
