import {ACTIVATE_USER_ATTEMPT} from 'routes/ActivateUser/modules/ActivateUser'
import ActivateUserSaga from 'routes/ActivateUser/modules/ActivateUserSaga'
import SagaTester from 'redux-saga-tester'

const action = {
  type: ACTIVATE_USER_ATTEMPT,
}

describe('(Sagas)', () => {
  it(`Should take ${ACTIVATE_USER_ATTEMPT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(ActivateUserSaga().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
