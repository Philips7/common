import {
  activateUserAttempt,
  activateUserSuccess,
  activateUserFailure
} from 'routes/ActivateUser/modules/ActivateUser'

describe('(Route) ActivateUser', () => {
  describe('(Modules) ActivateUser', () => {
    describe('Actions', () => {
      it('activateUserAttempt should attempt dashboard data', () => {
        expect(activateUserAttempt()).to.eql.object
      })

      it('activateUserSuccess should return dashboard data', () => {
        expect(typeof activateUserSuccess()).to.eql.function
      })

      it('activateUserFailure should return error', () => {
        expect(typeof activateUserFailure()).to.eql.function
      })
    })
  })
})
