import React from 'react'
import SearchContainer from 'routes/Landing/routes/Search/containers/SearchContainer'

describe('(Containers)', () => {
  describe('SearchContainer', () => {
    it('Should be a function', () => {
      expect(typeof SearchContainer).to.equal('function')
    })
  })
})
