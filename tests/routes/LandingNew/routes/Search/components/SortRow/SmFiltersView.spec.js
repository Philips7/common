import React from 'react'
import SmFiltersView from 'routes/Landing/routes/Search/components/SortRow/SmFiltersView'
import {shallow} from 'enzyme'
import {defaultFilters} from 'redux/modules/Trips'

const props = {
  filters: defaultFilters,
	dropClosed: true
}

describe('(Components)', () => {
  describe('SmFiltersView', () => {
    it('Should render', () => {
      shallow(<SmFiltersView {...props}/>)
    })
  })
})
