import searchFlightsSaga from 'sagas/checkFlight'
import {SEARCH_AIRPORTS_DISCONNECT} from 'routes/Landing/modules/Landing'
import SagaTester from 'redux-saga-tester'

const action = {
  type: SEARCH_AIRPORTS_DISCONNECT
}

describe('(Sagas)', () => {
  it(`Should take ${SEARCH_AIRPORTS_DISCONNECT}`, async() => {
    const sagaTester = new SagaTester({})
    sagaTester.start(searchFlightsSaga().watcher)

    sagaTester.dispatch(action)
    await sagaTester.waitFor(action.type)

    expect(sagaTester.getCalledActions()[0]).to.eql(action)
  })
})
