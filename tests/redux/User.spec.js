import reducer, {
  validateUser,
  signUpFacebookAttempt,
  singUpFacebookSuccess,
  singUpFacebookFailure,
  signUpAttempt,
  signUpSuccess,
  signUpFailure,
  userUpdateAttempt,
  userUpdateSuccess,
  userUpdateFailure,
  signInAttempt,
  signInSuccess,
  signInFailure,
  userGetAttempt,
  userGetSuccess,
  userGetFailure,
  refreshToken,
  refreshTokenSuccess,
  refreshTokenFailure,
  SIGN_UP_FACEBOOK_ATTEMPT,
  SIGN_UP_FACEBOOK_SUCCESS,
  SIGN_UP_FACEBOOK_FAILURE,
  SIGN_UP_ATTEMPT,
  SIGN_UP_SUCCESS,
  SIGN_UP_FAILURE,
  USER_GET_ATTEMPT,
  USER_GET_SUCCESS,
  USER_GET_FAILURE,
  USER_UPDATE_ATTEMPT,
  USER_UPDATE_SUCCESS,
  USER_UPDATE_FAILURE,
  SIGN_IN_ATTEMPT,
  SIGN_IN_SUCCESS,
  SIGN_IN_FAILURE,
  REFRESH_TOKEN_SUCCESS,
  LOGOUT,
  initialState
} from 'redux/modules/User'

describe('(Modules)', () => {
  describe('User', () => {
    describe('Actions', () => {
      it('signUpFacebookAttempt should return promise', () => {
        const result = signUpFacebookAttempt()(() => ({}))
        expect(result instanceof Promise).to.be.true
      })
      it('singUpFacebookSuccess should return correct type', () => {
        expect(typeof singUpFacebookSuccess()).to.eql('function')
      })
      it('singUpFacebookFailure should return correct type', () => {
        expect(typeof singUpFacebookFailure()).to.eql(('function'))
      })

      it('signUpAttempt should return promise', () => {
        const result = signUpAttempt()(() => ({}))
        expect(result instanceof Promise).to.be.true
      })
      it('signUpSuccess should return correct type', () => {
        expect(typeof signUpSuccess()).to.eql('function')
      })
      it('signUpFailure should return correct type', () => {
        expect(typeof signUpFailure()).to.eql(('function'))
      })

      it('userUpdateAttempt should return promise', () => {
        const result = userUpdateAttempt()(() => ({}))
        expect(result instanceof Promise).to.be.true
      })
      it('userUpdateSuccess should return correct type', () => {
        expect(typeof userUpdateSuccess()).to.eql('function')
      })
      it('userUpdateFailure should return correct type', () => {
        expect(typeof userUpdateFailure()).to.eql(('function'))
      })

      it('signInAttempt should return promise', () => {
        const result = signInAttempt()(() => ({}))
        expect(result instanceof Promise).to.be.true
      })
      it('signInSuccess should return correct type', () => {
        expect(typeof signInSuccess()).to.eql('function')
      })
      it('signInFailure should return correct type', () => {
        expect(typeof signInFailure()).to.eql(('object'))
      })

      it('userGetAttempt should return correct type', () => {
        expect(typeof userGetAttempt()).to.eql('object')
      })
      it('userGetSuccess should return correct type', () => {
        expect(typeof userGetSuccess({user: {
          birth_day: '12-12-1999'
        }})).to.eql(('object'))
      })
      it('userGetFailure should return correct type', () => {
        expect(typeof userGetFailure()).to.eql(('function'))
      })

      it('refreshToken should return promise', () => {
        expect(typeof refreshToken()).to.eql('function')
      })
      it('refreshTokenSuccess should return correct type', () => {
        expect(typeof refreshTokenSuccess()).to.eql('function')
      })
      it('refreshTokenFailure should return correct type', () => {
        expect(typeof refreshTokenFailure()).to.eql(('function'))
      })
    })

    describe('Reducers', () => {
      it(`${SIGN_UP_FACEBOOK_ATTEMPT} should attempt sign up to facebook`, () => {
        const expectedResult = {
          ...initialState,
          signUpLoading: true,
          isVerifiedMessage: ''
        }

        const action = {
          type: SIGN_UP_FACEBOOK_ATTEMPT
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${SIGN_UP_FACEBOOK_SUCCESS} should sign up to facebook successfully`, () => {
        const expectedResult = {
          ...initialState,
          signUpLoading: false
        }
        const results = reducer(undefined, {type: SIGN_UP_FACEBOOK_SUCCESS})
        expect(results).to.eql(expectedResult)
      })

      it(`${SIGN_UP_FACEBOOK_FAILURE} should not sign up to facebook`, () => {
        const error = 'error'
        const expectedResult = {
          ...initialState,
          signUpLoading: false
        }
        const action = {
          type: SIGN_UP_FACEBOOK_FAILURE,
          error
        }
        const results = reducer(undefined, {type: SIGN_UP_FACEBOOK_FAILURE})
        expect(results).to.eql(expectedResult)
      })

      it(`${SIGN_UP_ATTEMPT} should attempt sign up`, () => {
        const expectedResult = {
          ...initialState,
          signUpLoading: true
        }
        const results = reducer(undefined, {type: SIGN_UP_ATTEMPT})
        expect(results).to.eql(expectedResult)
      })

      it(`${SIGN_UP_SUCCESS} should sign up  successfully`, () => {
        const expectedResult = {
          ...initialState,
          signUpLoading: false
        }
        const results = reducer(undefined, {type: SIGN_UP_SUCCESS})
        expect(results).to.eql(expectedResult)
      })

      it(`${SIGN_UP_FAILURE} should not sign up`, () => {
        const error = 'error'
        const expectedResult = {
          ...initialState,
          signUpLoading: false,
          signUpSuccess: true
        }
        const action = {
          type: SIGN_UP_FAILURE,
          error
        }
        const results = reducer(undefined, {type: SIGN_UP_FAILURE})
        expect(results).to.eql(expectedResult)
      })

      it(`${USER_GET_ATTEMPT} should attempt to get user`, () => {
        const expectedResult = {
          ...initialState,
          userGetLoading: true
        }
        const results = reducer(undefined, {type: USER_GET_ATTEMPT})
        expect(results).to.eql(expectedResult)
      })

      it(`${USER_GET_SUCCESS} should get user`, () => {
        const expectedResult = {
          ...initialState,
          userGetLoading: false,
          loggedIn: true,
          user: {
            birth_day: '12-12-1999'
          }
        }

        const action = {
          type: USER_GET_SUCCESS,
          user: {
            birth_day: '12-12-1999'
          },
          loggedIn: true
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${USER_GET_FAILURE} should not get user`, () => {
        const expectedResult = {
          ...initialState,
          userGetLoading: false
        }
        const results = reducer(undefined, {type: USER_GET_FAILURE})
        expect(results).to.eql(expectedResult)
      })

      it(`${USER_UPDATE_ATTEMPT} should attempt to update user`, () => {
        const expectedResult = {
          ...initialState,
          userUpdateLoading: true
        }

        const results = reducer(undefined, {type: USER_UPDATE_ATTEMPT})
        expect(results).to.eql(expectedResult)
      })

      it(`${USER_UPDATE_SUCCESS} should update user`, () => {
        const expectedResult = {
          ...initialState,
          userUpdateLoading: false,
          user: {}
        }

        const action = {
          type: USER_UPDATE_SUCCESS,
          user: {}
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${USER_UPDATE_FAILURE} should not update user`, () => {
        const expectedResult = {
          ...initialState,
          userUpdateLoading: false
        }
        const results = reducer(undefined, {type: USER_UPDATE_FAILURE})
        expect(results).to.eql(expectedResult)
      })

      it(`${SIGN_IN_ATTEMPT} should attempt to sign in`, () => {
        const expectedResult = {
          ...initialState,
          signInLoading: true,
          rememberMe: true
        }
        const action = {
          type: SIGN_IN_ATTEMPT,
          values: {
            rememberMe: true
          }
        }
        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${SIGN_IN_SUCCESS} should sign in`, () => {
        const expectedResult = {
          ...initialState,
          signInLoading: false,
          loggedIn: true
        }
        const results = reducer(undefined, {type: SIGN_IN_SUCCESS})
        expect(results).to.eql(expectedResult)
      })

      it(`${SIGN_IN_FAILURE} should not sign in`, () => {
        const expectedResult = {
          ...initialState,
          signInLoading: false
        }
        const results = reducer(undefined, {type: SIGN_IN_FAILURE})
        expect(results).to.eql(expectedResult)
      })

      it(`${REFRESH_TOKEN_SUCCESS} should refresh token`, () => {
        const expectedResult = {
          ...initialState,
          loggedIn: true
        }
        const results = reducer(undefined, {type: REFRESH_TOKEN_SUCCESS})
        expect(results).to.eql(expectedResult)
      })

      it(`${LOGOUT} should logout`, () => {
        const expectedResult = {
          ...initialState,
          loggedIn: false,
          user: null
        }
        const results = reducer(undefined, {type: LOGOUT})
        expect(results).to.eql(expectedResult)
      })
    })
  })
})
