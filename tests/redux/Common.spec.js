import {STARTUP, startup, changeLanguage} from '../../src/redux/modules/Common'
import {getCookie} from 'redux-cookie'
import {loadTranslations, setLocale} from 'react-redux-i18n'
import common_en from 'static/i18n/en.json'

describe('(Modules)', () => {
	describe('Common', () => {
		describe('Actions', () => {
			it('startup should run translations actions', () => {
				const dispatch = sinon.spy(() => ({}))
				startup()(dispatch)

				expect(dispatch.getCall(0).calledWith(getCookie('lang')))
				expect(dispatch.getCall(1).calledWith(loadTranslations(common_en)))
				expect(dispatch.getCall(1).calledWith(loadTranslations({en: common_en})))
				expect(dispatch.getCall(2).calledWith(setLocale('en')))
				expect(dispatch.getCall(3).calledWith({type: STARTUP}))
			})

			it('changeLanguage should dispatch REDUX_COOKIES_GET', () => {
				const lang = 'pl'
				const dispatch = sinon.spy(() => ({}))
				const exceptedResult = {type: 'REDUX_COOKIES_SET', name: 'lang', value: 'pl', options: {}}

				changeLanguage(lang)(dispatch)
				expect(dispatch.getCall(0).args[0]).to.eql(exceptedResult)
			})
		})
	})
})
