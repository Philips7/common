import reducer, {
  CHECK_FLIGHT_CONNECT,
  CHECK_FLIGHT_DISCONNECT,
  CHECK_FLIGHT_SEND,
  CHECK_FLIGHT_EMIT,
  CHECK_FLIGHT_EMIT_ERROR,
  REMOVE_TICKET_FROM_CACHE,
  saveTicketToCache,
  removeTicketFromCache,
  checkFlightConnect,
  checkFlightDisconnect,
  checkFlight,
  checkFlightSend,
  checkFlightEmit,
  checkTrip,
  initialState
} from '../../src/redux/modules/Ticket'

describe('(Modules)', () => {
  describe('Ticket', () => {
    describe('Actions', () => {
      it('checkFlight should set correct data', () => {
        const data = 123
        expect(checkFlight(data)).to.eql({type: CHECK_FLIGHT_SEND, values: {data}})
      })
    })

    describe('Reducer', () => {
      it(`${REMOVE_TICKET_FROM_CACHE} should set ticked as null`, () => {
        const results = reducer(undefined, {type: REMOVE_TICKET_FROM_CACHE})
        expect(results).to.eql({...initialState, ticket: null})
      })

      it(`${CHECK_FLIGHT_CONNECT} should set websocketConnected as true`, () => {
        const results = reducer(undefined, {type: CHECK_FLIGHT_CONNECT})
        expect(results).to.eql({...initialState, websocketConnected: true})
      })

      it(`${CHECK_FLIGHT_DISCONNECT} should set websocketConnected as false`, () => {
        const results = reducer(undefined, {type: CHECK_FLIGHT_DISCONNECT})
        expect(results).to.eql({...initialState, websocketConnected: false})
      })

      it(`${CHECK_FLIGHT_EMIT} should set correct data`, () => {
        const ticket = 123
        const exceptedResult = {
          ...initialState,
          checkFlightLoading: false,
          ticket,
          needShowLoader: false
        }
        const action = {
          type: CHECK_FLIGHT_EMIT,
          ticket
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(exceptedResult)
      })

      it(`${CHECK_FLIGHT_EMIT_ERROR} should set correct data`, () => {
        const exceptedResult = {
          ...initialState,
          checkFlightLoading: false,
          needShowLoader: false
        }
        const action = {type: CHECK_FLIGHT_EMIT_ERROR,}

        const results = reducer(undefined, action)
        expect(results).to.eql(exceptedResult)
      })

      it(`${CHECK_FLIGHT_SEND} should set correct data`, () => {
        const action = {
          type: CHECK_FLIGHT_SEND,
          values: {
            data: {
              booking_token: 123
            }
          }

        }

        const exceptedResult = {
          ...initialState,
          checkFlightLoading: true,
          needShowLoader: true
        }

        const results = reducer(undefined, action)
        expect(results).to.eql(exceptedResult)

        expect(results).to.eql(exceptedResult)
      })

      it(`${CHECK_FLIGHT_SEND} should set correct data if ticket isn\'t null`, () => {
        const action = {
          type: CHECK_FLIGHT_SEND,
          values: {
            data: {
              booking_token: 321
            }
          }
        }

        const initialReducerState = {
          ...initialState,
          ticket: {
            booking_token: 321
          }
        }

        const exceptedResult = {
          ...initialReducerState,
          checkFlightLoading: true,
          needShowLoader: false
        }

        const results = reducer(initialReducerState, action)
        expect(results).to.eql(exceptedResult)
      })
    })
  })
})
