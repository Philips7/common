import 'babel-polyfill'
import reducer, {
  SEARCH_TRIPS_CONNECT,
  SEARCH_TRIPS_DISCONNECT,
  SEARCH_TRIPS_SEND,
  SEARCH_TRIPS_EMIT,
  SEARCH_TRIPS_EMIT_ERROR,
  SEARCH_FEATURE_FLIGHTS_EMIT,
  CHANGE_ON_CONNECT_BEHAVIOUR,
  SEARCH_TRIPS_STOP_TIMEOUT,
  SAVE_SEARCH_CRITERIA,
  RESET_SEARCH_CRITERIA,
  UPDATE_FROM_URL,
  CLEAN_RESULTS,
  SAVE_FILTERS,
  SEARCH_TRIPS_RECONNECT,
  searchTripsConnect,
  searchTripsDisconnect,
  searchTripStopTimeout,
  searchTripReconnect,
  searchTrips,
  saveFilters,
  submitFilters,
  resetFilter,
  getDefaultFilters,
  defaultFilters,
  durationFilters,
  timeFilters,
  searchFromUrl,
  createPushQuery,
  checkChanges,
  updateFromUrl,
  convertToNumber,
  serializeRange,
  searchTripsEmit,
  scrollToSearchResults,
  serializeTickets,
  searchTripsPagination,
  displayCorrectPlace,
  updateFilters,
  prepareSearch,
  serializeFilters,
  searchTripsModalPagination,
  serializeDesinationPlace,
  searchFromTicketSend,
  saveSearchCriteria,
  cleanResults,
  searchTripsSend,
  dateYearFirst,
  serializeTicket,
  serializeFeaturePlace,
  searchTripsWithFiltersSend,
  initialState
} from 'redux/modules/Trips'
import {anywhereObj} from 'utils/helpers'
import moment from 'moment'
import createStore from 'store/createStore'

describe('(Modules)', () => {
  describe('Trips', () => {
    describe('Actions', () => {
      it('searchTripsConnect should connect to search trips', () => {
        const store = createStore({})
        const dispatch = store.dispatch
        const expectedAction = {
          type: SEARCH_TRIPS_CONNECT,
          dispatch
        }
        expect(searchTripsConnect()(dispatch)).to.eql(expectedAction)
      })

      it('searchTripsDisconnect should disconnect from search trips', () => {
        const expectedAction = {
          type: SEARCH_TRIPS_DISCONNECT
        }
        expect(searchTripsDisconnect()).to.eql(expectedAction)
      })

      it('should reconnect search trips', () => {
        const store = createStore({})
        const dispatch = store.dispatch
        const expectedAction1 = {
          type: SEARCH_TRIPS_CONNECT,
          dispatch
        }
        const expectedAction2 = {
          type: SEARCH_TRIPS_DISCONNECT
        }
        expect(searchTripsConnect()(dispatch)).to.eql(expectedAction1)
        expect(searchTripsDisconnect()).to.eql(expectedAction2)
      })

      it('searchTripStopTimeout should stop timeout on reconnect search trips', () => {
        const expectedAction = {
          type: SEARCH_TRIPS_STOP_TIMEOUT
        }
        expect(searchTripStopTimeout()).to.eql(expectedAction)
      })

      it('searchTripReconnect should should reconnect after timeout', () => {
        expect(searchTripReconnect()).to.be.function
      })

      it('serializeFeaturePlace should serialize feature place', () => {
        const obj = {
          airport_name: 'Name',
          airport_IATA: 'IATA',
          city: 'Rzeszow',
          country: 'Poland'
        }
        const exp_obj = {
          name: 'Name',
          code: 'IATA',
          city: 'Rzeszow',
          country: 'Poland'
        }
        expect(serializeFeaturePlace(obj)).to.eql(exp_obj)
      })

      it('dateYearFirst should set date format to year first', () => {
        const date = moment('1970-01-01')
        const date2 = '1970-01-01'
        expect(dateYearFirst(date)).to.eql(date2)
      })

      it('serializeTicket should serialize ticket', () => {
        const obj1 = {
          outbound: {
            segments: [{
              origin: {
                date: '1970-01-01',
                airport_name: 'Name',
                airport_IATA: 'IATA',
                city: 'Rzeszow',
                country: 'Poland'
              }
            }]
          },
          returnn: {
            segments: [{
              origin: {
                date: '1970-01-01',
                airport_name: 'Name',
                airport_IATA: 'IATA',
                city: 'Rzeszow',
                country: 'Poland'
              }
            }]
          }
        }
        const date = moment('1970-01-01')
        const long_ticket = {
          outbound_place: {
            name: 'Name',
            code: 'IATA',
            city: 'Rzeszow',
            country: 'Poland'
          },
          destination_place: {
            name: 'Name',
            code: 'IATA',
            city: 'Rzeszow',
            country: 'Poland'
          },
          outbound_date: moment.parseZone(date),
          return_date: moment.parseZone(date)
        }
        const short_ticket = {
          outbound_place: 'IATA',
          destination_place: 'IATA',
          outbound_date: '1970-01-01',
          return_date: '1970-01-01'
        }
        expect(serializeTicket(obj1)).to.eql(long_ticket)
        expect(serializeTicket(obj1, true)).to.eql(short_ticket)
      })

      it('cleanResults should dispatch action', () => {
        const action = {type: CLEAN_RESULTS}
        expect(cleanResults()).to.eql(action)
      })

      it('saveSearchCriteria should set proper value', () => {
        const searchCriteria = {}
        const action = ({
          type: SAVE_SEARCH_CRITERIA,
          searchCriteria
        })
        expect(saveSearchCriteria(searchCriteria)).to.eql(action)
      })

      it('saveFilters should set proper value', () => {
        const action = ({
          type: SAVE_FILTERS,
          filters: defaultFilters,
          name: 'filterki'
        })
        expect(saveFilters(defaultFilters, 'filterki')).to.eql(action)
      })

      it('getDefaultFilters should return proper value', () => {
        const expected_duration_filters = {
          totalDuration: 60,
          stopover: {min: 2, max: 25}
        }
        const expected_time_filters = {
          departureTime: {min: 0, max: 1439},
          returnTime: {min: 0, max: 1439},
					departureTakeOff: true,
					returnTakeOff: true,
					departureTimeLanding: {min: 0, max: 1439},
					returnTimeLanding: {min: 0, max: 1439},
        }
        expect(getDefaultFilters(durationFilters)).to.eql(expected_duration_filters)
        expect(getDefaultFilters(timeFilters)).to.eql(expected_time_filters)
      })

      it('resetFilter should return proper value', () => {
        const expected_duration_filters = {
          totalDuration: 60,
          stopover: {min: 2, max: 25},
        }
				const expected_time_filters = {
					departureTime: {min: 0, max: 1439},
					returnTime: {min: 0, max: 1439},
					departureTakeOff: true,
					returnTakeOff: true,
					departureTimeLanding: {min: 0, max: 1439},
					returnTimeLanding: {min: 0, max: 1439},
				}
        const action1 = ({
          type: RESET_SEARCH_CRITERIA,
          filters: expected_duration_filters,
          name: 'duration'
        })
        const action2 = ({
          type: RESET_SEARCH_CRITERIA,
          filters: expected_time_filters,
          name: 'time'
        })
        expect(resetFilter('duration')).to.deep.eql(action1)
        expect(resetFilter('time')).to.deep.eql(action2)
      })

      it('prepareSearch should be function', () => {
        expect(prepareSearch()).to.be.function
      })

      it('searchFromTicketSend should be function', () => {
        const obj1 = {
          outbound: {
            segments: [{
              origin: {
                date: '1970-01-01',
                airport_name: 'Name',
                airport_IATA: 'IATA',
                city: 'Rzeszow',
                country: 'Poland'
              }
            }]
          },
          returnn: {
            segments: [{
              origin: {
                date: '1970-01-01',
                airport_name: 'Name',
                airport_IATA: 'IATA',
                city: 'Rzeszow',
                country: 'Poland'
              }
            }]
          }
        }
        expect(searchFromTicketSend(obj1)).to.be.function
      })

      it('serializeDesinationPlace should return propper value', () => {
        const obj1 = {one_for_city: true}
        const obj2 = {destination_place: 'qwe'}

        expect(serializeDesinationPlace(anywhereObj)).to.deep.equal(obj1)
        expect(serializeDesinationPlace({code: 'qwe'})).to.deep.equal(obj2)
      })

      it('searchTripsSend should return propper value', () => {
        const trip = {
          outbound_place: 'Rzeszow',
          destination_place: 'Kijow',
          range_date: moment('1970-01-01'),
          way: '1'
        }
        expect(searchTripsSend(trip)).to.be.function
      })

      it('searchTripsWithFiltersSend should return propper value', () => {
        expect(searchTripsWithFiltersSend(defaultFilters)).to.be.function
      })

      it('updateFilters should return propper value', () => {
        expect(updateFilters('filterki')).to.equal.function
      })

      it('submitFilters should return propper value', () => {
        expect(submitFilters()).to.equal.function
      })

      it('searchTripsModalPagination should return propper value', () => {
        expect(searchTripsModalPagination()).to.equal.function
      })

      it('searchTripsPagination should return propper value', () => {
        expect(searchTripsPagination()).to.equal.function
      })

      it('displayCorrectPlace should return propper value', () => {
        const val1 = {code: [], name: 'name'}
        const val2 = {code: 'code', name: 'name'}
        const val3 = 'val'
        expect(displayCorrectPlace(val1)).to.equal(val1.name)
        expect(displayCorrectPlace(val2)).to.equal(val2.code)
        expect(displayCorrectPlace(val3)).to.equal(val3)
      })

      it('createPushQuery should return propper value', () => {
        expect(typeof createPushQuery).to.equal.function
      })

      it('serializeTickets should return propper value', () => {
        expect(typeof serializeTickets).to.equal.function
      })

      it('scrollToSearchResults should return propper value', () => {
        expect(scrollToSearchResults()).to.equal.function
      })

      it('searchTripsEmit should return propper value', () => {
        expect(searchTripsEmit()).to.equal.function
      })

      it('serializeRange should return propper value', () => {
        expect(typeof serializeRange).to.equal.function
      })

      it('checkChanges should return propper value', () => {
        expect(typeof serializeRange).to.equal.function
      })

      it('convertToNumber should return propper value', () => {
        expect(typeof convertToNumber).to.equal.function
      })

      it('serializeFilters should return propper value', () => {
        expect(typeof serializeFilters).to.equal.function
      })

      it('updateFromUrl should return propper value', () => {
        expect(typeof updateFromUrl).to.equal.function
      })

      it('searchFromUrl should return propper value', () => {
        expect(typeof searchFromUrl).to.equal.function
      })

      it('changeOnConnectBehaviour should return propper value', () => {
        expect(typeof changeOnConnectBehaviour).to.equal.function
      })

    })

    describe('Reducers', () => {
      it(`${SEARCH_TRIPS_SEND} should emit error`, () => {
        const expectedResult = {
          ...initialState,
          showError: false,
          reconnect: false,
          searchTripsLoading: false,
          loadingFeatureFlights: false
        }

        const action = {
          type: SEARCH_TRIPS_SEND,
          values: {
            data: {
              ordering_by_duration: false,
              page_num: 1
            }
          },
          searchTripsLoading: false,
          loadingFeatureFlights: false
        }
        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${SEARCH_TRIPS_EMIT_ERROR} should emit error`, () => {
        const expectedResult = {
          ...initialState,
          searchTripsLoading: false,
          showError: true
        }
        const results = reducer(undefined, {type: SEARCH_TRIPS_EMIT_ERROR})
        expect(results).to.eql(expectedResult)
      })

      it(`${UPDATE_FROM_URL} should update from url`, () => {
        const expectedResult = {
          ...initialState,
          onConnectBehaviour: 1,
          searchCriteria: {
            ordering_by_duration: true
          }
        }
        const action = {
          type: UPDATE_FROM_URL,
          onConnectBehaviour: 1,
          searchCriteria: {
            ordering_by_duration: true
          }
        }
        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${SEARCH_TRIPS_EMIT} should emit search trip`, () => {
        const expectedResult = {
          ...initialState,
          searchTripsLoading: false,
          showError: false,
          nextPage: false,
          num_results: 20,
          onConnectBehaviour: '0',
          searchResults: ['tickets']
        }
        const action = {
          type: SEARCH_TRIPS_EMIT,
          searchResults: ['tickets'],
          nextPage: false,
          num_results: 20
        }
        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${SEARCH_FEATURE_FLIGHTS_EMIT} should emit feature flights`, () => {
        const expectedResult = {
          ...initialState,
          featureFlights: ['flights'],
          loadingFeatureFlights: false,
          onConnectBehaviour: '0',
          searchTripsLoading: false
        }
        const action = {
          type: SEARCH_FEATURE_FLIGHTS_EMIT,
          featureFlights: ['flights']
        }
        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${CLEAN_RESULTS} should clean search results`, () => {
        const expectedResult = {
          ...initialState,
          searchResults: [],
          nextPage: false
        }
        const results = reducer(undefined, {type: CLEAN_RESULTS})
        expect(results).to.eql(expectedResult)
      })

      it(`${CHANGE_ON_CONNECT_BEHAVIOUR} should change variable`, () => {
        const expectedResult = {
          ...initialState,
          onConnectBehaviour: '1'
        }
        const action = {
          type: CHANGE_ON_CONNECT_BEHAVIOUR,
          onConnectBehaviour: '1'
        }
        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${SAVE_SEARCH_CRITERIA} should save search criteria`, () => {
        const expectedResult = {
          ...initialState,
          searchCriteria: {
            ordering_by_duration: false,
            page_num: 1
          }
        }
        const action = {
          type: SAVE_SEARCH_CRITERIA,
          searchCriteria: {
            ordering_by_duration: false,
            page_num: 1
          }
        }
        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })

      it(`${SEARCH_TRIPS_CONNECT} should connect for search trips`, () => {
        const expectedResult = {
          ...initialState,
          searchTripsLoading: true
        }
        const dispatch = sinon.spy()
        const action = {
          type: SEARCH_TRIPS_CONNECT,
          dispatch
        }
        const results = reducer(undefined, action)
        expect(results).to.eql(expectedResult)
      })
    })
  })
})

