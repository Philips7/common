/*
import 'babel-polyfill'
import reducer, {
GET_COUNTRIES_ATTEMPT,
GET_COUNTRIES_SUCCESS,
GET_COUNTRIES_FAILURE,
getCountries,
getCountriesSuccess,
getCountriesFailure,
initialState
} from '../../src/redux/modules/Countries'

describe('(Modules)', () => {
  describe('Countries', () => {
    describe('Actions', () => {
      it('getCountries should return correct object', () => {
        const exceptedResult = {
          type: GET_COUNTRIES_ATTEMPT,
          responseSuccess: getCountriesSuccess,
          responseFailure: getCountriesFailure
        }

        expect(getCountries()).to.eql(exceptedResult)
      })

      it('getCountriesSuccess should set Netherlands as first element in array', () => {
        const countries = [
          {name: 'Poland', pk: 'PL'},
          {name: 'Ukraine', pk: 'UA'},
          {name: 'Netherlands', pk: 'NL'},
          {name: 'Germany', pk: 'GR'}
        ]
        const exceptedResult = 'Netherlands'

        expect(getCountriesSuccess(countries).countries[0].name).to.eql(exceptedResult)
      })

      it('getCountriesFailure should return correct type', () => {
        expect(getCountriesFailure()).to.eql(({type: GET_COUNTRIES_FAILURE}))
      })
    })

    describe('Reducer', () => {
      it(`${GET_COUNTRIES_SUCCESS} should set countries correctly`, () => {
        const countries = 'countriesData'
        const exceptedResult = {
          ...initialState,
          countries
        }
        const results = reducer(undefined, {type: GET_COUNTRIES_SUCCESS, countries})
        expect(results).to.eql(exceptedResult)
      })
    })
  })
})
*/
