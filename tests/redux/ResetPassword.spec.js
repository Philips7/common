import 'babel-polyfill'
import reducer, {
  RESET_PASSWORD_ATTEMPT,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAILURE,
  resetPasswordAttempt,
  resetPasswordSuccess,
  resetPasswordFailure
} from '../../src/redux/modules/ResetPassword'

describe('(Modules)', () => {
  describe('ResetPassword', () => {
    describe('Actions', () => {
      it('resetPasswordAttempt should return promise', () => {
        const result = resetPasswordAttempt()(() => ({}))
        expect(result instanceof Promise).to.be.true
      })
      it('resetPasswordSuccess should return correct type', () => {
        expect(typeof resetPasswordSuccess()).to.eql('function')
      })
      it('resetPasswordFailure should return correct type', () => {
        expect(typeof resetPasswordFailure()).to.eql(('function'))
      })
    })
    describe('Reducer', () => {
      it(`${RESET_PASSWORD_ATTEMPT} should set resetPasswordLoading flag as true`, () => {
        const results = reducer(undefined, {type: RESET_PASSWORD_ATTEMPT})
        expect(results).to.eql({resetPasswordLoading: true})
      })
      it(`${RESET_PASSWORD_SUCCESS} should set resetPasswordLoading flag as false`, () => {
        const results = reducer(undefined, {type: RESET_PASSWORD_SUCCESS})
        expect(results).to.eql({resetPasswordLoading: false})
      })
      it(`${RESET_PASSWORD_FAILURE} should set resetPasswordLoading flag as false`, () => {
        const results = reducer(undefined, {type: RESET_PASSWORD_FAILURE})
        expect(results).to.eql({resetPasswordLoading: false})
      })
    })
  })
})
