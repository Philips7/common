import React from 'react'
import CoreLayout from 'layouts/CoreLayout/CoreLayout'
import {shallow} from 'enzyme'

const props = {
  children: <h1>Children</h1>,
  location: {
    pathname: ''
  },
  store: {
		subscribe: () => {},
		getState: () => {},
		dispatch: () => {}
  }
}

describe('(Layouts)', () => {
  describe('CoreLayout', () => {
    it('Should render', () => {
      shallow(<CoreLayout {...props}/>)
    })
  })
})
