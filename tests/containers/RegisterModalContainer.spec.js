import React from 'react'
import RegisterModalContainer from 'containers/RegisterModalContainer'

describe('(Containers)', () => {
  describe('RegisterModalContainer', () => {
    it('Should be a function', () => {
      expect(typeof RegisterModalContainer).to.equal('function')
    })
  })
})
