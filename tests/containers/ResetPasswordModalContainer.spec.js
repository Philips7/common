import React from 'react'
import ResetPasswordModalContainer from 'containers/ResetPasswordModalContainer'

describe('(Containers)', () => {
  describe('ResetPasswordModalContainer', () => {
    it('Should be a function', () => {
      expect(typeof ResetPasswordModalContainer).to.equal('function')
    })
  })
})
