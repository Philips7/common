import React from 'react'
import AutosuggestContainer from 'containers/AutosuggestContainer'

describe('(Containers)', () => {
  describe('AutosuggestContainer', () => {
    it('Should be a function', () => {
      expect(typeof AutosuggestContainer).to.equal('function')
    })
  })
})
