import React from 'react'
import InputFieldGroup from 'components/FieldGroup/InputFieldGroup'
import {shallow} from 'enzyme'

const props = {
  input: '',
  label: '',
  type: '',
  meta: {
    touched: '',
    error: '',
  },
  disabled: '',
  labelStyle: '',
  placeholder: '',
  inputStyle: '',
  customStyle: '',
  customStyleSm: '',
  showError: '',
  validate: ''
}

describe('(Components)', () => {
  describe('FieldGroup', () => {
    describe('InputFieldGroup', () => {
      it('Should render', () => {
        shallow(<InputFieldGroup {...props}/>)
      })
    })
  })
})
