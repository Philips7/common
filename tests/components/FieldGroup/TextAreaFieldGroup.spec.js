import React from 'react'
import TextAreaFieldGroup from 'components/FieldGroup/TextAreaFieldGroup'
import {shallow} from 'enzyme'

const props = {
  input: '',
  label: '',
  type: '',
  meta: {
    touched: '',
    error: '',
  },
  disabled: '',
  labelStyle: '',
  placeholder: '',
  inputStyle: '',
  customStyle: '',
  customStyleSm: '',
  showError: '',
  rows: '',
}

describe('(Components)', () => {
  describe('FieldGroup', () => {
    describe('TextAreaFieldGroup', () => {
      it('Should render', () => {
        shallow(<TextAreaFieldGroup {...props}/>)
      })
    })
  })
})
