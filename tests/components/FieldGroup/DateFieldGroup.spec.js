import React from 'react'
import DateFieldGroup from 'components/FieldGroup/DateFieldGroup'
import {shallow} from 'enzyme'

const props = {
  input: '',
  label: '',
  type: '',
  meta: {
    touched: '',
    error: '',
    active: ''
  },
  disabled: '',
  labelStyle: '',
  placeholder: '',
  inputStyle: '',
  customStyle: '',
  customStyleSm: '',
  showError: '',
}

describe('(Components)', () => {
  describe('FieldGroup', () => {
    describe('DateFieldGroup', () => {
      it('Should render', () => {
        shallow(<DateFieldGroup {...props}/>)
      })
    })
  })
})
