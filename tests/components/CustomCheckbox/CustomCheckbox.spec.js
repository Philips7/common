import React from 'react'
import CustomCheckbox from 'components/CustomCheckbox/CustomCheckbox'
import {shallow} from 'enzyme'

const props = {
  label: '',
  name: '',
  id: '',
  checkboxType: '',
  meta: {
    touched: '',
    error: '',
  },
  value: '',
  disabled: '',
  input: '',
  onChange: '',
  small: '',
  showError: ''
}

describe('(Components)', () => {
  describe('CustomCheckbox', () => {
    it('Should render', () => {
      shallow(<CustomCheckbox {...props}/>)
    })
  })
})
