import React from 'react'
import Header from 'components/Header'
import {shallow} from 'enzyme'
import {Translate} from 'react-redux-i18n'

const props = {
  fillHeader: false,
  show: () => ({}),
  hide: () => ({}),
  logout: () => ({}),
  loggedIn: false,
  router: {}
}

describe('(Components)', () => {
  describe('Header', () => {
    it('Should display signIn text if user isn\'t logged in', () => {
      const component = shallow(<Header {...props}/>)

      expect(component.containsMatchingElement(<Translate value='Header.signIn'/>)).to.equal(true)
    })

    it('Should display links to Settings/dashboard/logout if user is logged in', () => {
      const component = shallow(<Header {...props} loggedIn/>)

      expect(component.containsMatchingElement(<Translate value='Header.settings'/>)).to.equal(true)
      expect(component.containsMatchingElement(<Translate value='Header.dashboard'/>)).to.equal(true)
      expect(component.containsMatchingElement(<Translate value='Header.logout'/>)).to.equal(true)
    })

    //it('Click on signIn text should call showSignInModal', () => {
    //  // TODO refactor render method
    //  const showSignInModal = sinon.spy(props, 'showSignInModal')
    //  const component = shallow(<Header {...props} loggedIn/>)
    //
    //  component.find(<Translate value='Header.signIn'/>).parent().simulate('click')
    //
    //  expect(showSignInModal.called).to.equal(true)
    //  props.showSignInModal.restore()
    //})
  })
})
