import React from 'react'
import TripDisplay from 'components/ShortDisplayFlight/TripDisplay/TripDisplay'
import {shallow} from 'enzyme'

const props = {
  outbound: false,
  trip: {
    companiesMap: [],
    segments: [{
      origin: {
        date: '01-01-2016'
      },
      destination: {
        date: ''
      }
    }],
    duration: '',
    total_layover_time: ''
  },
  compact: false
}

describe('(Components)', () => {
  describe('TripDisplay', () => {
    it('Should render', () => {
      shallow(<TripDisplay {...props}/>)
    })
  })
})
