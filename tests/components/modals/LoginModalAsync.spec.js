import React from 'react'
import AsyncComponent from 'components/modals/LoginModal/AsyncComponent'

describe('(Modals)', () => {
  describe('LoginModalAsyncComponent', () => {
    it('Should be a function', () => {
      expect(typeof AsyncComponent).to.equal('function')
    })
  })
})
