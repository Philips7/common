import React from 'react'
import AsyncComponent from 'components/modals/ResetPasswordModal/AsyncComponent'

describe('(Modals)', () => {
  describe('ResetPasswordModalAsyncComponent', () => {
    it('Should be a function', () => {
      expect(typeof AsyncComponent).to.equal('function')
    })
  })
})
