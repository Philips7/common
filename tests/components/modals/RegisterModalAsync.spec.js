import React from 'react'
import AsyncComponent from 'components/modals/RegisterModal/AsyncComponent'

describe('(Modals)', () => {
  describe('RegisterModalAsyncComponent', () => {
    it('Should be a function', () => {
      expect(typeof AsyncComponent).to.equal('function')
    })
  })
})
