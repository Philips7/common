import {
  anywhereCheck,
  anywhere,
  isEmpty,
  convertTitle,
  oneWay,
  serializeUser,
  serializeDateAccordingToBackend,
  scrollToTop,
  scrollTo,
  addCompaniesMap,
	isDefaultFilter,
  parseTimeToHHMM
} from 'utils/helpers'

describe('(Helpers)', () => {
  describe('Utils', () => {
    describe('anywhereCheck function', () => {
      it('should return  function', () => {
        expect(typeof anywhereCheck).to.equal('function')
      })
      it('should return true', () => {
        expect(anywhereCheck(anywhere)).to.be.true
      })
    })
		describe('isDefaultFilter function', () => {
			it('should return  function', () => {
				expect(typeof isDefaultFilter).to.equal('function')
			})
			it('should return propper value', () => {
				expect(isDefaultFilter(60,'totalDuration')).to.be.true
				expect(isDefaultFilter(70,'totalDuration')).to.be.false
				expect(isDefaultFilter({min: 2, max: 25},'stopover')).to.be.true
				expect(isDefaultFilter({min: 3, max: 25},'stopover')).to.be.false
				expect(isDefaultFilter({min: 3, max: 25},'departureTakeOff')).to.be.false
				expect(isDefaultFilter(true,'departureTakeOff')).to.be.true
				expect(isDefaultFilter(false,'departureTakeOff')).to.be.false
			})
		})
    describe('isEmpty function', () => {
      it('should return function', () => {
        expect(typeof isEmpty).to.equal('function')
      })
      it('should return true', () => {
        const obj = {}
        expect(isEmpty(obj)).to.be.true
      })
    })
    describe('convertTitle function', () => {
      it('should return function', () => {
        expect(typeof convertTitle).to.equal('function')
      })
      it('should return null', () => {
        const title = {title: true}
        expect(convertTitle(title)).to.be.null
      })
    })
    describe('oneWay function', () => {
      it('should return function', () => {
        expect(typeof oneWay).to.equal('function')
      })
      it('should return true', () => {
        const way = '1'
        expect(oneWay(way)).to.be.true
      })
    })
    describe('serializeUser function', () => {
      it('should return function', () => {
        expect(typeof serializeUser).to.equal('function')
      })
      it('should return object', () => {
        const user = {
          birth_date: '12-12-1991',
          title: true,
          phone: '+48 531 531 531',
          document_expiration: '12-12-1991'
        }
        expect(serializeUser(user)).to.be.object
      })
    })
    describe('serializeDateAccordingToBackend function', () => {
      it('should return function', () => {
        expect(typeof serializeDateAccordingToBackend).to.equal('function')
      })
      it('should return string', () => {
        const date = '12-12-12'
        expect(serializeDateAccordingToBackend(date)).to.be.string
      })
    })
    describe('scrollToTop function', () => {
      it('should return function', () => {
        expect(typeof scrollToTop).to.equal('function')
      })
    })
    describe('scrollTo function', () => {
      it('should return function', () => {
        expect(typeof scrollTo).to.equal('function')
      })
    })
    describe('addCompaniesMap function', () => {
      it('should return function', () => {
        expect(typeof addCompaniesMap).to.equal('function')
      })
      it('should return object', () => {
        const trip = {
          type: 2,
          outbound: {
            segments: [{
              carrier_iata: 'foo',
              carrier_icon: 'bar',
              carrier: 'baz'
            }
            ]
          },
          returnn: {
            segments: [{
              carrier_iata: 'foo',
              carrier_icon: 'bar',
              carrier: 'baz'
            }
            ]
          }
        }
        expect(addCompaniesMap(trip)).to.be.object
      })
    })
    describe('parseTimeToHHMM function', () => {
      it('should return function', () => {
        expect(typeof parseTimeToHHMM).to.equal('function')
      })
      it('should return propper value', () => {
        const minutes = 188
        const expected = '03:08'
        expect(parseTimeToHHMM(minutes)).to.equal(expected)
      })
    })
  })
})
