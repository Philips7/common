import {
  countTotalPrice,
  countPerMonth,
  breakDownPrices,
  priceParser,
  countFeatureTripPrice
} from 'utils/price'

describe('(Helpers)', () => {
  describe('Price', () => {
    describe('priceParser function', () => {
      it('should return function', () => {
        expect(typeof priceParser).to.equal('function')
      })
      it('should return proper value', () => {
        let value = '2.000'
        expect(priceParser(value)).to.equal('2')
        value = 2.4432123
        expect(priceParser(value)).to.equal('2.44')
        value = 2.0000001
        expect(priceParser(value)).to.equal('2')
        value = 2.004
        expect(priceParser(value)).to.equal('2')
        value = 2.009
        expect(priceParser(value)).to.equal('2.01')
      })
    })
    describe('countTotalPrice function', () => {
      it('should return function', () => {
        expect(typeof countTotalPrice).to.equal('function')
      })
      it('should return proper value', () => {
        let airfare = 1
        let fee = 2
        let rate = 3
        expect(countTotalPrice(airfare, fee, rate)).to.equal('3.09')
      })
    })
    describe('countPerMonth function', () => {
      it('should return function', () => {
        expect(typeof countPerMonth).to.equal('function')
      })
      it('should return proper value', () => {
        let totalPrice = 20.909
        let value = 10
        expect(countPerMonth(totalPrice, value)).to.equal('2.09')
        totalPrice = 20
        expect(countPerMonth(totalPrice, value)).to.equal('2')
      })
    })
    describe('countFeatureTripPrice function', () => {
      it('should return function', () => {
        expect(typeof countFeatureTripPrice).to.equal('function')
      })
      it('should return proper value', () => {
        let airfare = 100
        let fee = 2
        let rate = 3
        let months = 4
        expect(countFeatureTripPrice(airfare, fee, rate, months)).to.equal('26.27')
      })
    })
    describe('breakDownPrices function', () => {
      it('should return function', () => {
        expect(typeof breakDownPrices).to.equal('function')
      })
      it('should return proper value', () => {
        let airfare = 1
        let fake_booking_fee_percent = 2
        let rate = 3
        let value = 4
        let obj = {
          interestRate: 3,
          totalPrice: '3.09',
          perMonth: '0.77',
          creditPoint: 4,
        }

        expect(breakDownPrices({airfare, fake_booking_fee_percent}, {rate, value})).to.be.object
        expect(breakDownPrices({airfare, fake_booking_fee_percent}, {rate, value})).to.deep.equal(obj)
      })
    })
  })
})
