import {errorNotification, successNotification} from 'utils/notifications'

describe('(Helpers)', () => {
  describe('Notifications', () => {
    describe('errorNotification function', () => {
      it('should return function', () => {
        expect(typeof errorNotification).to.equal('function')
      })
      it('should return object', () => {
        expect(errorNotification()).to.be.object
      })
    })
    describe('successNotification function', () => {
      it('should return function', () => {
        expect(typeof successNotification).to.equal('function')
      })
      it('should return object', () => {
        expect(successNotification()).to.be.object
      })
    })
  })
})
