import React from 'react'
import <%= pascalEntityName %>Container from 'containers/<%= pascalEntityName %>Container'

describe('(Containers)', () => {
  describe('<%= pascalEntityName %>Container', () => {
    it('Should be a function', () => {
      expect(typeof <%= pascalEntityName %>Container).to.equal('function')
    })
  })
})
