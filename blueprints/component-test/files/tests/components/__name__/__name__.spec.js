import React from 'react'
import <%= pascalEntityName %> from 'components/<%= pascalEntityName %>'
import {shallow} from 'enzyme'

describe('(Components)', () => {
  describe('<%= pascalEntityName %>', () => {
    it('Should render', () => {
      shallow(<<%= pascalEntityName %>/>)
    })
  })
})
