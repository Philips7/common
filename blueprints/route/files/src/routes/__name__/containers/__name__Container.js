import {connect} from 'react-redux'
import {} from '../modules/<%= pascalEntityName %>'
import <%= pascalEntityName %> from '../components/<%= pascalEntityName %>'
import {UserIsAuthenticated} from 'utils/auth'

const mapActionCreators = {}

const mapStateToProps = state => ({

})

export default UserIsAuthenticated(
  connect(mapStateToProps, mapActionCreators)(<%= pascalEntityName %>)
)
