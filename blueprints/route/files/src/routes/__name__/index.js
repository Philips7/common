import {injectReducer} from 'store/reducers'
import {injectSagas} from 'store/sagas'

export default (store) => ({
  path: '<%= dashesEntityName %>',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      const <%= pascalEntityName %> = require('./containers/<%= pascalEntityName %>Container').default
      const reducer = require('./modules/<%= pascalEntityName %>').default

      injectReducer(store, { key: '<%= pascalEntityName %>', reducer })
      injectSagas(store, {key: '<%= pascalEntityName %>', sagas: []})

      cb(null, <%= pascalEntityName %>)
    }, '<%= pascalEntityName %>')
  }
})
