import <%= pascalEntityName %> from 'routes/<%= pascalEntityName %>'

describe('(Route) <%= pascalEntityName %>', () => {
  let _route

  beforeEach(() => {
    _route = <%= pascalEntityName %>({
      dispatch: () => ({}),
      injectTranslations: () => ({}),
      asyncReducers: [],
      asyncSagas: [],
      runSaga: function*() {},
      replaceReducer: () => ({})
    })
  })

  it('getComponent shouldn\' throw error', () => {
    route.getComponent({}, () => ({}))
  })

  it('Should return a route configuration object', () => {
    expect(typeof _route).to.equal('object')
    _route.getComponent()
  })

  it('Configuration should contain path `<%= dashesEntityName %>`', () => {
    expect(_route.path).to.equal('<%= dashesEntityName %>')
  })
})
