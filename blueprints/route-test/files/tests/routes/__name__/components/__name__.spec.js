import React from 'react'
import <%= pascalEntityName %> from 'routes/<%= pascalEntityName %>/components/<%= pascalEntityName %>'
import {shallow} from 'enzyme'

describe('(Route) <%= pascalEntityName %>', () => {
  describe('(Components) <%= pascalEntityName %>', () => {
    it('Should render', () => {
      shallow(<<%= pascalEntityName %>/>)
    })
  })
})
