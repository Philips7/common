import React from 'react'
import <%= pascalEntityName %>Container from 'routes/<%= pascalEntityName %>/containers/<%= pascalEntityName %>Container'

describe('(Route) <%= pascalEntityName %>', () => {
  describe('(Containers) <%= pascalEntityName %>Container', () => {
    it('Should be a function', () => {
      expect(typeof <%= pascalEntityName %>Container).to.equal('function')
    })
  })
})
