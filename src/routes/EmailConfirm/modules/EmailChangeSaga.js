import {take, call, put} from 'redux-saga/effects'
import request from 'sagas/api'
import {EMAIL_CONFIRM_ATTEMPT} from './EmailConfirm'

export default () => {
  function * worker({values, responseSuccess, responseFailure}) {

    const body = {
      method: 'POST',
      body: values
    }

    try {
      const {res, err} = yield call(request, 'users/user/email-confirm', body, false)

      if (res) {
        yield put(responseSuccess(res))
      } else {
        yield put(responseFailure(err))
      }
    } catch (error) {
      yield put(responseFailure())
    }
  }

  function * watcher() {
    while (true) {
      const data = yield take(EMAIL_CONFIRM_ATTEMPT)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
