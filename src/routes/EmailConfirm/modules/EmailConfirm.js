import createReducer from '../../../utils/createReducer'
import {push} from 'react-router-redux/lib/actions'
import {I18n} from 'react-redux-i18n'
import {userGetAttempt} from '../../../redux/modules/User'
import {errorNotification, successNotification} from '../../../utils/notifications'

// ------------------------------------
// Constants
// ------------------------------------
export const EMAIL_CONFIRM_ATTEMPT = 'ActivateUser.EMAIL_CONFIRM_ATTEMPT'
export const EMAIL_CONFIRM_SUCCESS = 'ActivateUser.EMAIL_CONFIRM_SUCCESS'
export const EMAIL_CONFIRM_FAILURE = 'ActivateUser.EMAIL_CONFIRM_FAILURE'
// ------------------------------------
// Actions
// ------------------------------------
export const emailConfirmAttempt = values => dispatch => {
  (values && values.uid && values.token) ?
    dispatch({
      type: EMAIL_CONFIRM_ATTEMPT,
      values,
      responseSuccess: emailConfirmSuccess,
      responseFailure: emailConfirmFailure
    })
    :
    dispatch(emailConfirmFailure())
}

export const emailConfirmSuccess = () => dispatch => {
  dispatch({type: EMAIL_CONFIRM_SUCCESS})
  dispatch(userGetAttempt())
  dispatch(push('/settings'))
  dispatch(successNotification(I18n.t('Notify.emailChangeSuccess')))
}

export const emailConfirmFailure = (err = {}) => dispatch => {
  dispatch({type: EMAIL_CONFIRM_FAILURE})
  dispatch(push('/settings'))
  !err.detail && dispatch(errorNotification(I18n.t('Notify.emailChangeAccountFail')))
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {}

export default createReducer(initialState, {})
