import {injectSagas} from 'store/sagas'

export default (store) => ({
  path: 'email-confirm',
  getComponent (nextState, cb) {
      const EmailConfirm = require('./containers/EmailConfirmContainer').default
      const EmailChangeSaga = require('./modules/EmailChangeSaga').default

      injectSagas(store, {key: 'EmailChangeSaga', sagas: [EmailChangeSaga]})


      cb(null, EmailConfirm)
  }
})
