import React, {Component} from 'react'
import PreloaderOverlay from 'components/PreloaderOverlay'

class EmailConfirm extends Component {
  componentWillMount() {
    const {emailConfirmAttempt, location:{query}} = this.props
    emailConfirmAttempt(query)
  }

  render() {
    return <PreloaderOverlay/>
  }
}

export default EmailConfirm

