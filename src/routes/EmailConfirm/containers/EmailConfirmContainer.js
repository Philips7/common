import {connect} from 'react-redux'
import { pure, compose } from 'recompose'
import {emailConfirmAttempt} from '../modules/EmailConfirm'

import EmailConfirm from '../components/EmailConfirm'

const mapActionCreators = {emailConfirmAttempt}

const mapStateToProps = (state) => ({})

const enhance = compose(
	connect(mapStateToProps, mapActionCreators),
	pure
)

export default enhance(EmailConfirm)
