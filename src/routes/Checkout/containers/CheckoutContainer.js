import { connect } from 'react-redux'
import { pure, compose } from 'recompose'
import {
  checkFlightConnect,
  checkFlightDisconnect,
  checkTrip,
  stopTimer,
  changeBaggageNumber,
  changePassengersNumber,
  removePassenger,
  onCheckFlightOpen,
  cleanTicket,
} from '../../../redux/modules/Ticket'
import { searchTripsConnect, searchTripsDisconnect } from '../../../redux/modules/Trips'
import { signUpNewsletterAttempt } from '../../../redux/modules/User'
import {
  createFirstPayment,
  changeTabNumber,
  createInvoiceDisconnect,
  setPaybackAvailable,
  unsetEmailRegistered,
  checkIfPaybackIsAvailable,
  preparingCreditAttempt,
  stopPreparingCredit,
  handleSubmitUserInfo,
  createFNPLPaymentSend,
  createFNPLPaymentConnect,
  createFNPLPaymentDisconnect,
  resetPaymentButton,
  changeNumberValue,
  showExtraBaggageModal,
  showErrorNotification,
  showFNPLTermsModal,
  checkoutEmails,
} from '../modules/Checkout'

import {
  getGuideStatus,
  setGuideStatus,
} from '../../../redux/modules/Common'

import Checkout from '../components/Checkout'

const mapActionCreators = {
  checkTrip,
  changeNumberValue,
  createFirstPayment,
  checkFlightConnect,
  checkFlightDisconnect,
  searchTripsConnect,
  searchTripsDisconnect,
  showErrorNotification,
  createInvoiceDisconnect,
  changeTabNumber,
  signUpNewsletterAttempt,
  setPaybackAvailable,
  unsetEmailRegistered,
  checkIfPaybackIsAvailable,
  preparingCreditAttempt,
  stopPreparingCredit,
  handleSubmitUserInfo,
  createFNPLPaymentSend,
  createFNPLPaymentConnect,
  createFNPLPaymentDisconnect,
  resetPaymentButton,
  showFNPLTermsModal,
  getGuideStatus,
  setGuideStatus,
  stopChecking: stopTimer,
  changeBaggageNumber,
  changePassengersNumber,
  removePassenger,
  onCheckFlightOpen,
  showExtraBaggageModal,
  cleanTicket,
  checkoutEmails
}

const mapStateToProps = ({
  Ticket: {
    ticket,
    checkFlightLoading,
    needShowLoader,
    passengersNumber,
    baggageNumber,
  },
  Trips: {
    paybackSelected,
  },
  Checkout: {
    isCreatingInvoice,
    isRedirecting,
    tabNumber,
    paybackUnavailable,
    preparingCredit,
    showPaymentButton,
    isCreatingFNPLInvoice,
    smsCodeValid,
    checkingCode,
    codaValidationError,
    phoneError,
    emailError
  },
  User: {
    user,
    userGetLoading
  },
  Common: {
    guideStatus,
  },
  form: {
    PassengerInfoForm
  }
}) => ({
  ticket,
  user,
  baggageNumber,
  guideStatus,
  userGetLoading,
  checkFlightLoading,
  needShowLoader,
  isCreatingInvoice,
  isRedirecting,
  tabNumber,
  paybackUnavailable,
  preparingCredit,
  showPaymentButton,
  isCreatingFNPLInvoice,
  PassengerInfoForm,
  smsCodeValid,
  passengersNumber,
  checkingCode,
  codaValidationError,
  paybackSelected,
  phoneError,
  emailError
})

const enhance = compose(
  connect(mapStateToProps, mapActionCreators),
  pure
)

export default enhance(Checkout)
