import {connect} from 'react-redux'
import SelectBankModal from './SelectBankModal'
import {confirmBankModal, createInvoiceDisconnect} from 'routes/Checkout/modules/Checkout'

const mapActionCreators = {
  createInvoiceDisconnect,
  onSubmit: confirmBankModal
}

const mapStateToProps = ({Checkout: {banks, paymentLink, isRedirecting}}) => ({banks, paymentLink, isRedirecting})

export default connect(mapStateToProps, mapActionCreators)(SelectBankModal)