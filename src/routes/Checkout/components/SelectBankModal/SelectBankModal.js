import React from 'react'
import Modal from 'react-bootstrap/lib/Modal'
import {Field, reduxForm} from 'redux-form'
import {connectModal} from 'redux-modal'
import {pure} from 'recompose'
import {I18n} from 'react-redux-i18n'
import s from './SelectBankModal.scss'

const SelectBankModal = pure(({
    show,
    handleHide,
    banks,
    submitting,
    submitFailed,
    handleSubmit,
    paymentLink,
    isRedirecting
  }) =>
    <Modal
      show={show}
      className={s.modal}
    >
      <Modal.Body className={s.modalBody}>
        {paymentLink && !isRedirecting ?
          <div>
            <a href={paymentLink} className={s.submitButton}>{I18n.t('passengerInfo.proceedToPayment')}</a>
          </div>
          :
          <form onSubmit={handleSubmit}>
            <div>
              <h2 className={s.title}>{I18n.t('Checkout.choosePayment')}</h2>

              <div className={s.contentWrap}>
                <ul className={s.paymentsList}>
                  {
                    banks.map(({id, name}, i) =>
                      <li key={i}>
                        <div className={s.customRadio}>
                          <Field name='issuer_id' component='input' type='radio' value={id} id={id}/>
                          <label htmlFor={id}><span className={s.checkBlock}><span className={s.circle}></span></span>
                            <span className={s.name}>{name}</span></label>
                        </div>
                      </li>
                    )
                  }
                </ul>

                {
                  submitFailed &&
                  <div className={s.error}>{I18n.t('Checkout.chooseOneOfTheBank')}</div>
                }

                <button type='submit' className={s.submitButton}>{I18n.t('Checkout.continueOperation')}</button>

              </div>

            </div>
          </form>}
      </Modal.Body>
    </Modal>
)

const validate = values => !values.issuer_id ? {issuer_id: I18n.t('form.required')} : {}

export default
connectModal({name: 'selectBankModal'})(
  reduxForm({
    form: 'SelectBank',
    validate
  })(SelectBankModal)
)
