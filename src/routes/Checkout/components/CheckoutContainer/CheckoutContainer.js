import React from 'react'
import PropTypes from 'prop-types'
import { pure, withHandlers, compose } from 'recompose'
import InlineSVG from 'svg-inline-react'
import closeIcon from 'static/images/close.svg'
import cn from 'classnames'
import s from './CheckoutContainer.scss'

const composedHOC = compose(
  withHandlers({
    clickHandler: ({ passengersNumber, minPassengerNo, showExtraBaggageModal, checkFlightLoading, removePassenger, passengerIndex }) => () => {
      if (passengersNumber <= minPassengerNo) {
        showExtraBaggageModal({ noRemove: true })
      } else {
        !checkFlightLoading && removePassenger(passengerIndex)
      }
    }
  }),
  pure
)

const CheckoutContainer = composedHOC(({
  children, title,
  icon, isWrapped,
  clickHandler,
  passengerIndex,
  fromPromo,
}) =>
  <div className={s.checkoutContainer}>
    <div className={s.titleWrap}>

      <h2 className={s.title}>
        <span>{title}</span>
        <span className={s.iconWrap}>
          <InlineSVG src={icon} />
        </span>
        {passengerIndex === 0 &&
          <span className={s.booker}>
            Booker
          </span>
        }
      </h2>

      {!!passengerIndex &&
        <a onClick={clickHandler} className={s.removeBtn}>
          <InlineSVG src={closeIcon} className={s.icon} />
        </a>
      }
    </div>
    <div className={cn(passengerIndex === 0 && s.border, isWrapped && s.contentWrap, fromPromo && s.promoPadding)}>{children}</div>
  </div>
)

CheckoutContainer.defaultProps = {
  isWrapped: true
}

CheckoutContainer.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired,
  icon: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  isWrapped: PropTypes.bool
}

export default CheckoutContainer
