import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import { I18n } from 'react-redux-i18n'
import validator from 'validator'
import moment from 'moment'
import { compose, withHandlers } from 'recompose'
import { scrollToFirstError } from 'utils/helpers'
import times from 'ramda/src/times'
import { isTabletScreen } from 'utils/responsive'
import airplaneIcon from 'static/images/checkout/airplane-icon.svg'
import TripDescription from 'components/TripDescription'
import CheckoutContainer from '../CheckoutContainer/CheckoutContainer'
import PassengerInfoForm from '../PassengerInfoForm/PassengerInfoForm'
import PassengerInfo from '../PassengerInfo/PassengerInfo'
import s from './CheckoutConfirmComponent.scss'

let isNotifShowed = false

const onsubmitFail = (errors, dispatch, submitError, props) => {
  if (!isNotifShowed) { isNotifShowed = true }
  setTimeout(() => {
    if (isNotifShowed) {
      props.showErrorNotification('Please make sure that all information is filled in correctly.')
      scrollToFirstError(errors)
    }
    isNotifShowed = false
  }, 10)
}

const composedHOC = compose(
  withHandlers({
    checkAndConfirm: ({ onCheckFlightOpen, handleSubmit }) => () => {
      onCheckFlightOpen(handleSubmit)
    }
  })
)

const CheckoutConfirmComponent = composedHOC(({
  confirm, trip, handleSubmit, editInfo, showExtraBaggageModal,
  paybackUnavailable, showPaymentButton, user, form, showFNPLTermsModal,
  createFNPLPaymentSend, stopChecking, checkAndConfirm,
  resetPaymentButton, smsCodeValid, changeBaggageNumber, baggageNumber,
  changePassengersNumber, passengersNumber, removePassenger, showErrorNotification,
  checkFlightLoading, changeNumberValue, formData, checkingCode,
  codaValidationError, interestRate, phoneError, emailError, paybackSelected, checkoutEmails
}) =>
  <section className={s.checkoutConfirmComponent}>
    <div className={isTabletScreen ? s.reversed : s.container}>
      <div className={s.leftCol}>
        {
          trip &&
          <CheckoutContainer
            title='Your flight ticket'
            icon={airplaneIcon}
            isWrapped={false}
          >
            <TripDescription trip={trip} paybackSelected={paybackSelected} noSelectSection panelView checkout_flag />
          </CheckoutContainer>
        }
        <PassengerInfoForm
          confirm={confirm}
          initialValues={user}
          needPassportData={trip.document_need}
          creditAmount={trip.credit_amount}
          trip={trip}
          showExtraBaggageModal={showExtraBaggageModal}
          changeNumberValue={changeNumberValue}
          changeBaggageNumber={changeBaggageNumber}
          handleSubmit={handleSubmit}
          email={user && user.email}
          editInfo={editInfo}
          confirmInfo={confirm}
          showPaymentButton={showPaymentButton}
          user={user}
          stopChecking={stopChecking}
          createFNPLPaymentSend={createFNPLPaymentSend}
          form={form}
          resetPaymentButton={resetPaymentButton}
          smsCodeValid={smsCodeValid}
          removePassenger={removePassenger}
          checkFlightLoading={checkFlightLoading}
          changePassengersNumber={changePassengersNumber}
          passengersNumber={passengersNumber}
          formData={formData}
          paybackSelected={paybackSelected}
          interestRate={interestRate}
          checkingCode={checkingCode}
          showErrorNotification={showErrorNotification}
          codaValidationError={codaValidationError}
          phoneError={phoneError}
          showFNPLTermsModal={showFNPLTermsModal}
          emailError={emailError}
          baggageNumber={baggageNumber}
          minPassengerNo={trip.min_number_of_passengers}
          maxPassengerNo={trip.max_number_of_passengers}
          checkoutEmails={checkoutEmails}
        />
      </div>
      <div className={s.rightCol} id={'passengerInfo'}>
        <PassengerInfo
          confirm={confirm}
          trip={trip}
          passengersNumber={passengersNumber}
          cabinBaggage={trip.baggage_info && trip.baggage_info.cabin}
          additionalBaggages={trip.baggage_info && trip.baggage_info.additional}
          paybackSelected={paybackSelected}
          baggageNumber={baggageNumber}
          paybackUnavailable={paybackUnavailable}
        />
      </div>
    </div>
    <div className={s.container}>
      <div className={s.col}>
        <div className={s.buttonWrap}>
          <button
            className={s.confirmBtn}
            disabled={!smsCodeValid}
            onClick={checkAndConfirm}
          >
            {
              confirm
                ? ' Confirm & Book Now'
                : ' Proceed to confirmation'
            }
          </button>
        </div>
      </div>
    </div>
  </section>
)

const validate = (values, props) => {
  const errors = {}

  const requiredFields = [
    'address_full',
    'residental_status',
    'annual_salary',
    'county',
    'city',
    'house_number',
    'street',
    'post_code',
    'employment_status'
  ]
  const requiredPassengerField = [
    'first_name',
    'last_name',
    'birth_date',
    'email',
    'document_number',
  ]
  const today = moment()
  const past = moment(new Date(1900, 1, 1))

  requiredFields.forEach((field) => {
    if (!values[field]) {
      errors[field] = I18n.t('form.required')
    }
  })

  const passengersArray = []

  values.passengers.forEach((passenger, passIndex) => {
    const passengerError = {}
    requiredPassengerField.forEach((field) => {
      if (!passenger[field]) {
        passengerError[field] = I18n.t('form.required')
      }
    })

    if (passenger.email && !validator.isEmail(passenger.email)) {
      passengerError.email = I18n.t('form.invalidEmailError')
    }

    if (passenger.first_name && passenger.first_name.length > 30) {
      passengerError.first_name = I18n.t('form.firstNameLessThan', { max: 30 })
    }

    if (passenger.last_name && passenger.last_name.length > 30) {
      passengerError.last_name = I18n.t('form.lastNameLessThan', { max: 30 })
    }

    const isBirthDate = passenger.birth_date
    if (isBirthDate) {
      if (/^((?!_).)*$/.test(isBirthDate)) {
        const birthDate = moment(isBirthDate, 'DD-MM-YYYY')
        if (birthDate.isAfter(today)) {
          passengerError.birth_date = I18n.t('form.yearMustBeRange')
        } else if (birthDate.isBefore(past)) {
          passengerError.birth_date = I18n.t('form.yearMustBeRange')
        } else if (today.diff(birthDate, 'years') < 18 && passIndex === 0) {
          passengerError.birth_date = I18n.t('form.userToYoung')
        }
      } else {
        passengerError.birth_date = I18n.t('form.wrongDateFormat')
      }
    }

    passengersArray[passIndex] = passengerError
  })

  if (values.moreThenYear === '2') {
    errors.moreThenYear = 'You are not allowed to get credit'
  }

  if (passengersArray.length) {
    errors.passengers = passengersArray
  }

  if (!values.phone || values.phone.length < 3) {
    errors.phone = I18n.t('form.requiredPhoneNumber')
  } else if (values.phone.length < 9) {
    errors.phone = I18n.t('form.invalidPhoneNumber')
  }

  if (values.contact_phone && values.contact_phone.length < 9) {
    errors.contact_phone = I18n.t('form.invalidPhoneNumber')
  }

  if (!values.termsFlymble) {
    errors.termsFlymble = I18n.t('form.needAcceptTerms')
  }

  if (values.annual_salary <= 6000) {
    errors.annual_salary = I18n.t('form.needExceed6000')
  }

  if (values.house_number && values.house_number.match(/[^a-zA-Z0-9']\s?/g)) {
    errors.house_number = I18n.t('form.cannotIntoWhitespace')
  }

  return errors
}

export default connect(
  state => ({
    formData: state.form.PassengerInfoForm,
    initialValues: {
      promoCode: '',
      passengers:
      times(() => ({}), state.Ticket.passengersNumber)
        .map(() => ({ title: '17' })),
    }
  })
)(reduxForm({
  form: 'PassengerInfoForm',
  validate,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  onSubmitFail: onsubmitFail
})(CheckoutConfirmComponent))
