import React from 'react'
import { pure } from 'recompose'
import moment from 'moment'
import paymentsImage from 'static/images/paymentsList.png'
import { I18n } from 'react-redux-i18n'
import { priceParser } from 'utils/price'
import { getValues } from 'utils/helpers'
import config from 'config'
import s from './PassengerInfo.scss'

const PassengerInfo = pure(({
  trip: { outbound, returnn, all_months, airfare }, paybackSelected, trip,
  cabinBaggage = [], additionalBaggages = {}, baggageNumber, passengersNumber }
) => {
  const getDay = moment().format('DD')
  const { per_month, total_price, price_per_person, service_fee_percentage, paid_today } = getValues(trip, all_months, paybackSelected, true)
  const parsedPerMonth = priceParser(per_month)
  const perPerson = priceParser(price_per_person)
  const totalAirfare = priceParser(airfare / passengersNumber)
  const serviceFee = service_fee_percentage
  const total = priceParser(total_price)

  const generateArray = Array(...{
    length: paybackSelected
  }).map(Number.call, Number)
  const getMonth = i => moment().add(i, 'month').format('MMMM')
  return (
    <section className={s.passengerInfo}>
      <div className={s.passengerInfoWrap}>
        <h3 className={s.title}>Your payment plan</h3>

        <div className={s.flightInfo}>
          <div className={s.route}>
            {`
            ${outbound.segments[0].origin.airport_IATA} - ${outbound.segments[outbound.segments.length - 1].destination.airport_IATA} -
            ${outbound.segments[outbound.segments.length - 1].destination.airport_IATA === returnn.segments[0].origin.airport_IATA
                ? ''
                : `${returnn.segments[0].origin.airport_IATA} - `}
            ${returnn.segments[returnn.segments.length - 1].destination.airport_IATA}
          `}
          </div>
          <div className={s.time}>
            {moment(outbound.segments[0].origin.date).format('DD MMM')} - {moment(returnn.segments[returnn.segments.length - 1].destination.date).format('DD MMM')}
          </div>
        </div>

        <div className={s.passengerInfo}>
          <div className={s.passengerCount}>
            {passengersNumber}x passenger,
          </div>
          {
            !!cabinBaggage.length && cabinBaggage.map((data, index) =>
              <div className={s.baggageCount} key={index}>
                {passengersNumber} baggage, {data.length} x {data.width} x {data.height} cm, {data.weight}kg
              </div>)
          }
          {
            baggageNumber > 0 && additionalBaggages && additionalBaggages.params &&
            <div>
              <span>{baggageNumber} baggage, </span>
              <span>
                {additionalBaggages.params.length} x {additionalBaggages.params.width} x {additionalBaggages.params.height} cm, {additionalBaggages.params.weight}kg
                </span>
            </div>
          }
          <div className={s.line} />
          <li className={s.listItem}>
            <span>{I18n.t('passengerInfo.airfare')}</span>
            <span>{config.currency} {totalAirfare}</span>
          </li>
          <li className={s.listItem}>
            <span>{I18n.t('passengerInfo.serviceFee')}</span>
            {serviceFee !== 'NaN' && <span>{serviceFee}%</span>}
          </li>
          <li className={s.listItem}>
            <span>{I18n.t('passengerInfo.perPerson')}</span>
            <span>{config.currency} {perPerson}</span>
          </li>
          <li className={s.listItem}>
            <span>{I18n.t('passengerInfo.numberPassengers')}</span>
            <span>{passengersNumber}</span>
          </li>
          <li className={s.listItem}>
            <span>{I18n.t('passengerInfo.totalPrice')}</span>
            <span>{config.currency} {total}</span>
          </li>
        </div>

        <div className={s.paymenets}>
          <h4 className={s.paymentTitle}><span>Payments for all passengers</span></h4>
          <ul className={s.creditPointList}>
            <li className={s.listItem}>
              <span className={s.greenText}>Pay today <span className={s.greyText}>(upfront)</span></span>
              <span className={s.darkBlueText}>£ {priceParser(paid_today)}</span>
            </li>
            {generateArray.map((e, i) =>
              <li key={e} className={s.listItem}>
                <span>{`${getDay} ${getMonth(i + 1)}`}</span>
                <span>{`£ ${parsedPerMonth}`}</span>
              </li>
            )}
          </ul>
        </div>

        <div className={s.totalPrice}>Total £{total}</div>
        <div className={s.priceInfo}>(Inc. VAT and service fee)</div>

        <div className={s.paymentSummary}>
          <span className={s.label}>Price per month</span>
          <span className={s.summaryPrice}>£ {parsedPerMonth}</span>
        </div>

        <div className={s.paymentsAccepted}>
          <div className={s.label}>Payments accepted:</div>
          <img src={paymentsImage} alt='payments' className={s.paymentsImg} />
        </div>
      </div>
    </section>
  )
})

export default PassengerInfo
