import { connect } from 'react-redux'
import { searchTripsModalPagination } from '../../../../redux/modules/Trips'
import ChangePriceModal from './ChangePriceModal'
import { selectTrip, goToSearch, stillBookThisFlight } from '../../modules/Checkout'

const mapActionCreators = {
  searchTripsModalPagination,
  selectTrip: trip => selectTrip(trip, 'changePriceModal'),
  stillBookThisFlight,
  goToSearch
}

const mapStateToProps = ({
  Trips: { searchResults, searchTripsLoading, nextPage, num_results, paybackSelected },
  Ticket: { ticket, passengersNumber } }) =>
  ({
    originalPrice: ticket ? ticket.original_price : null,
    searchResults,
    searchTripsLoading,
    nextPage,
    num_results,
    paybackSelected,
    creditPoint: ticket && ticket.creditPoint,
    interestRate: ticket && ticket.interestRate,
    passengersNumber,
  })

export default connect(mapStateToProps, mapActionCreators)(ChangePriceModal)
