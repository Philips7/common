import React from 'react'
import Modal from 'react-bootstrap/lib/Modal'
import { connectModal } from 'redux-modal'
import PreloaderOverlay from 'components/PreloaderOverlay'
import TripDescription from 'components/TripDescription'
import { pure } from 'recompose'
import ChangePriceImg from 'static/images/price-has-changed-img.svg'
import cn from 'classnames'
import ArrowRight from 'static/images/arrow-pointing-to-right.svg'
import { Link } from 'react-router'
import InlineSVG from 'svg-inline-react'
import { I18n } from 'react-redux-i18n'
import { priceParser } from 'utils/price'
import s from './ChangePriceModal.scss'
import config from '../../../../config'

const ChangePriceModal = pure(({
    show, newPrice, nextPage, num_results, passengersNumber,
    searchResults, searchTripsLoading, searchTripsModalPagination,
    selectTrip, creditPoint, interestRate, stillBookThisFlight, paybackSelected
  }) =>
    <Modal
      show={show}
      className={cn(s.modal, s.checkoutModal)}
      bsSize='lg'
    >
      <Modal.Body className={s.modalBody}>
        <div className={s.contentWrap}>
          <h1 className={s.title}>{I18n.t('Checkout.priceHasChanged', { amount: priceParser(newPrice), currency: config.currency })}</h1>
          <h3 className={s.subTitle}>{I18n.t('Checkout.dueToFewerSeats')}</h3>
        </div>

        <div className={s.imgWrap}>
          <InlineSVG src={ChangePriceImg} />
        </div>

        <span className={s.button} onClick={stillBookThisFlight}>
          {I18n.t('form.proceed')}
        </span>

        <div className={s.otherTicketsList}>
          <h4 className={s.listTitle}><span>{num_results}</span>{I18n.t('Checkout.alternativeFlightsFound')}</h4>

          <div className={s.ticketsWrap}>

            {num_results === 0
              ?
              <div className={s.noTickets}>
                <span>{I18n.t('Checkout.noAlternativeFlights')}</span>
                <Link to='/'><span>{I18n.t('Checkout.searchAnotherDestination')}</span> <InlineSVG src={ArrowRight} /> </Link>
              </div>
              :
              <div className={s.ticketsWrap}>
                {searchResults.map((trip, index) =>
                  <div className={s.tripWrap} key={index}>
                    <TripDescription
                      trip={trip}
                      showMonth={false}
                      passengersNumber={passengersNumber}
                      paybackSelected={paybackSelected}
                      creditPoint={creditPoint}
                      interestRate={interestRate}
                      selectTrip={selectTrip}
                      compact
                    />
                  </div>
                )}
                {nextPage && <button onClick={searchTripsModalPagination} className={s.showMoreBtn}>{I18n.t('Checkout.showMore')}</button>}
              </div>
            }

          </div>
        </div>

        {searchTripsLoading && <PreloaderOverlay />}

      </Modal.Body>
    </Modal>
)

export default
connectModal({ name: 'changePriceModal' })(ChangePriceModal)
