import React from 'react'
import InlineSVG from 'svg-inline-react'
import { pure, compose, withHandlers } from 'recompose'
import connectModal from 'redux-modal/lib/connectModal'
import Modal from 'react-bootstrap/lib/Modal'
import cn from 'classnames'
import { I18n } from 'react-redux-i18n'
import closeIcon from 'static/images/close.svg'
import s from './ExtraBaggageModal.scss'

const composedHOC = compose(
  withHandlers({
    clickHandler: ({ extraBaggage, changeBaggageNumber, handleHide, fromError, changePassengersNumber }) => () => {
      if (extraBaggage) {
        changeBaggageNumber(1)
        handleHide()
      } else if (fromError) {
        changePassengersNumber(1, true)
        handleHide()
      } else handleHide()
    }
  }),
  pure
)

const ExtraBaggageModal = composedHOC(({
  show,
  handleHide,
  extraBaggage,
  noPassenger,
  noBaggage,
  noRemove,
  clickHandler,
}) =>
  <Modal
    show={show}
    onHide={handleHide}
    className={cn(s.modal, s.baggageModal)}
    bsSize='lg'
  >
    <Modal.Body className={s.modalBody}>
      <a onClick={handleHide} className={s.closeModalBtn}>
        <InlineSVG src={closeIcon} />
      </a>
      <div className={s.contentWrap}>
        <h1 className={s.title}>
          {extraBaggage && I18n.t('Checkout.addingExtraBaggage')}
          {noPassenger && I18n.t('Checkout.addingExtraPassenger')}
          {noBaggage && I18n.t('Checkout.addingExtraBaggage')}
          {noRemove && I18n.t('Checkout.removingPassengerTitle')}
        </h1>
        <h3 className={s.subTitle}>
          {extraBaggage && I18n.t('Checkout.extraBaggageText')}
          {noPassenger && I18n.t('Checkout.cannotAddPassenger')}
          {noBaggage && I18n.t('Checkout.cannotAddBaggage')}
          {noRemove && I18n.t('Checkout.removingPassenger')}
        </h3>
        <button className={s.button} onClick={clickHandler}>{I18n.t('Checkout.baggageButtton')}</button>
      </div>
    </Modal.Body>
  </Modal>
)

export default connectModal({ name: 'extraBaggageModal', destroyOnHide: true })(ExtraBaggageModal)
