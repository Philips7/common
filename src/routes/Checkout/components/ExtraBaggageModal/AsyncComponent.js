import React, { Component } from 'react'
import { connect } from 'react-redux'
import path from 'ramda/src/path'
import { changeBaggageNumber, removePassenger, changePassengersNumber } from '../../../../redux/modules/Ticket'

class ExtraBaggageModal extends Component {
  state = {
    loaded: false
  }

  async componentWillMount() {
    const AsyncComponent = await System.import(/* webpackChunkName: "ExtraBaggageModal" */ './ExtraBaggageModal')

    if (!this.unmounted) {
      this.AsyncComponent = AsyncComponent.default
      this.setState({ loaded: true })
    }
  }

  componentWillUnmount() {
    this.unmounted = true
  }

  render() {
    const { state: { loaded }, AsyncComponent, props } = this
    return (
      loaded ? <AsyncComponent {...props} /> : null
    )
  }
}

const mapActionCreators = {
  changeBaggageNumber,
  removePassenger,
  changePassengersNumber,
}

const mapStateToProps = ({ form }) => ({
  passengersLength: path(['PassengerInfoForm', 'values', 'passengers', 'length'], form),
})

export default connect(mapStateToProps, mapActionCreators)(ExtraBaggageModal)
