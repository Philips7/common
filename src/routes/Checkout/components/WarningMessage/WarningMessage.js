import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { pure } from 'recompose'
import s from './WarningMessage.scss'
import InlineSVG from 'svg-inline-react'
import WarningIcon from './assets/warningIcon.svg'


const WarningMessage = pure(({ title, description, withIcon, className }) =>
  <div className={cn(s.warningMessage, withIcon && s.withIcon, className)}>
    {
      title && (
        <h4 className={s.title}>{title}</h4>
      )
    }
    {
      description && (
        <p className={s.description}>{description}</p>
      )
    }
    {
      withIcon && (
       <span className={s.iconWrap}><InlineSVG src={WarningIcon}/></span>
      )
    }
  </div>
)

WarningMessage.defaultProps = {
  withIcon: true
};

WarningMessage.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  withIcon: PropTypes.bool
}

export default WarningMessage
