import React from 'react'
import InlineSVG from 'svg-inline-react'
import { compose, pure, withHandlers } from 'recompose'
import connectModal from 'redux-modal/lib/connectModal'
import Modal from 'react-bootstrap/lib/Modal'
import cn from 'classnames'
import closeIcon from 'static/images/close.svg'
import s from './FNPLTerms.scss'

const composedHOC = compose(
  withHandlers({
    clickHandler: ({ extraBaggage, changeBaggageNumber, handleHide, fromError, changePassengersNumber }) => () => {
      if (extraBaggage) {
        changeBaggageNumber(1)
        handleHide()
      } else if (fromError) {
        changePassengersNumber(1, true)
        handleHide()
      } else handleHide()
    }
  }),
  pure
)

const FNPLTerms = composedHOC(({
                                 show,
                                 handleHide,
                               }) =>
  <Modal
    show={show}
    onHide={handleHide}
    dialogClassName={s.baggageModal}
    className={cn(s.baggageModal)}
    bsSize='lg'
  >
    <Modal.Body>
      <div className={s.contentWrap}>
        <a onClick={handleHide} className={s.closeModalBtn}>
          <InlineSVG src={closeIcon}/>
        </a>
        <div className="mobile-scrollable">
          <div className="header">
            <div className={s.logo}></div>
            <div className={s.title}>Terms &amp; Conditions</div>
            <div className={s.headline}>Read the following information carefully to ensure this is suitable for your
              needs.
            </div>
          </div>
          <div className={s.container}>
            <div className="body">
              <div className="row">
                <div className="col-12">
                  <div className="row">
                    <div className="col-12">
                      <div className="block-text">
                        <div className="flex/terms-and-conditions"><p className={s.blueTitle}>Is flex suitable for the
                          type of credit you are looking for?</p><p className={s.paragraph}>Flex is a flexible
                          instalment credit product that may only be used for the purchase of the travel products and
                          services you’ve selected from the travel provider. The loan isn’t suitable for any other
                          purpose. The loan is repayable by pre-agreed instalments over a fixed period specified in the
                          agreement. If you want flexibility in how you use the loan monies, this credit product won’t
                          be suitable for your requirements.</p><p className={s.blueTitle}>How much will it cost
                          me?</p><p className={s.paragraph}>The amount of your loan is shown in the Agreement. It’s
                          described as the 'amount of credit.' This is equivalent to the total cash price for the travel
                          products and services minus any deposit payable. The transaction fee you’ll need to pay when
                          you enter into the agreement is also shown in the Agreement under 'Total charge for credit.’
                          The total amount you’ll have to repay is shown in the Agreement under 'Total amount payable'
                          and includes the amount of credit, transaction fee and deposit. You must repay the total
                          amount payable by paying the transaction fee and deposit upfront, then the monthly instalments
                          on the promise dates set out in your Agreement. It’s important you only enter into the
                          Agreement if you can comfortably afford the payments and you’re not aware of any potential
                          changes to your circumstances that could affect your ability to make payments in the future
                          that you’ve not told us about.</p><p className={s.blueTitle}>Which other features of the
                          Agreement should I be aware of?</p><p className={s.paragraph}>We will cancel your booking or
                          travel arrangement with the travel provider if you fail to make payments on the promise date.
                          We will serve a notice of cancellation and make at least 2 attempts to contact you prior to
                          cancelling your booking or travel arrangement.</p><p className={s.paragraph}>We’ll collect
                          your repayments on each promise date using the debit card details you’ve provided to us. We
                          use continuous payment authority to collect repayments. You can cancel your continuous payment
                          authority by contacting us at <a href="mailto:support@flynowpaylater.com" target="_blank">support@flynowpaylater.com</a>
                          or by calling us on 020 3322 2996. If you cancel, you’ll still need to make your repayment on
                          each promise date and provide an alternative payment method to avoid going into default.
                          Continuous payment authorities aren’t covered by the direct debit guarantee scheme.</p><p
                          className={s.blueTitle}>What are the consequences for me if I do not keep up with
                          repayments?</p><p className={s.paragraph}>If we’re unsuccessful in collecting full payment on
                          a promise date or we have not received payment from you by other means we’ll charge you a
                          £12.00 missed payment fee. We may report any missed payment to credit reference agencies. This
                          may affect your ability to get credit in the future, both from us and other lenders. We may
                          sell your debt and the debt purchaser may take legal action to recover it.</p><p
                          className={s.paragraph}>We may also cancel your booking or travel arrangement with the travel
                          provider if you fail to make payments on the promise date. We will always make at least 1
                          attempt to contact you prior to cancelling your booking or travel arrangement.</p><p
                          className={s.blueTitle}>What rights do I have to withdraw from the Credit Agreement once I
                          have entered into it?</p><p className={s.paragraph}>If you decide that you no longer want
                          your new Fly Now Pay Later account, you have 14 days from the day after you receive
                          confirmation of your Fly Now Pay Later account to tell us you want to withdraw from the Credit
                          Agreement. If you want to withdraw, you must notify us by either calling us on 020 3322 2996
                          or by writing to us. If you wish to notify us in writing, please e-mail us at <a
                            href="mailto:support@flynowpaylater.com" target="_blank">support@flynowpaylater.com</a>
                          before the end of the withdrawal period set out above.</p><p className={s.blueTitle}>Are
                          there any particular features of this credit product that I should be aware of?</p><p
                          className={s.paragraph}>Before accepting a Fly Now Pay Later agreement, you should read the
                          SECCI (pre-contractual information) and the Credit Agreement provided to you carefully, to
                          ensure that this is the best product for you and that you understand the features of this
                          product.</p><p className={s.blueTitle}>How do you ask us for further explanation or
                          information?</p><p className={s.paragraph}>We hope that this explanation and the other
                          information provided contains everything you need to know about the credit agreement to allow
                          you to make an informed decision about taking the credit. It is very important that you
                          understand your obligations under the credit agreement. If you have any questions regarding
                          the credit agreement or would like to ask us for more information, please contact us on either
                          020 3322 2996 or <a href="mailto:support@flynowpaylater.com" target="_blank">support@flynowpaylater.com</a>.
                          Calls may be monitored and recorded.</p><p className={s.blueTitle}>How We Use Your
                          Information (Privacy Policy)</p><p className={s.paragraph}>By ‘your information’ we mean
                          personal and financial information about you we collect, use, share and store. This includes
                          information we:</p><p className={s.paragraph}><span>(a)</span> obtain from you or from third
                          parties, such as employers, joint account holders, credit reference agencies (who may search
                          the Electoral Register and any other public or private database they have access to), fraud
                          prevention agencies or other organisations when you apply for an account (or any other product
                          or service) or which you or they give us at any other time, through any type of communication
                          or means; verbal, written or electronic, including through letters, calls, emails, websites
                          (e.g. internet protocol (IP) address, cookies), applications, telephone systems (we may
                          capture automatically and use any telephone number you may call us from), registrations,
                          researches, promotions and competitions or through accounts or products you have or previously
                          had with us;</p><p className={s.paragraph}><span>(b)</span> learn from the way you use and
                          manage your account(s), from the transactions you make such as the date, amount, currency and
                          the name and type of supplier you use, from the purchases you make and from the payments which
                          are made to your account. </p><p className={s.paragraph}>When you give us someone else’s
                          personal information in connection with any credit, credit related or other, products and
                          services, on behalf of someone else (such as an additional account holder), you confirm that
                          you have obtained his or her consent for his or her information to be disclosed to us and
                          other companies in the Fly Now Pay Later Group or others as described below, and used as
                          described in this privacy statement. Any information you give us and/or other companies of the
                          Fly Now Pay Later Group about someone else, or someone else discloses a link with you, may be
                          recorded and considered with your information. </p><p className={s.paragraph}>We and other
                          companies in the Fly Now Pay Later Group and/or other organisations as described below may
                          collect, use, share and store your information: </p><p className={s.paragraph}>
                          <span>(a)</span> to check your eligibility when you apply for any credit and credit-related or
                          other products, check your details and verify your identity and the details and identities of
                          your spouse, partner or other directors/partners if you are an owner, director or partner in a
                          small business, which means, an organisation which might be a sole trader, partnership or a
                          limited company that has three or less partners or directors; </p><p className={s.paragraph}>
                          <span>(b)</span> to check your personal accounts, your financial associate’s personal accounts
                          if you have one, and may also check on your business accounts if you are an owner, director or
                          partner in a small business. A “financial associate” is someone living at the same address as
                          you with whom you have a personal relationship and manage your finances jointly in a similar
                          way to a spouse or life partner. This does not include temporary arrangements such as students
                          or flatmates or business relationships, but will include joint account holders, anyone you
                          have told us is a financial associate of yours, and anyone identified by the credit reference
                          agencies’ records as your financial associate (you can apply to the credit reference agencies
                          to ask them to de-link someone who is one of your financial associates). </p><p
                          className={s.paragraph}><span>(c)</span> to administer and manage your application,
                          account(s), and any payments you make using your account , give you statements and provide you
                          with products and services, inform you about changes to the features of those products or
                          services or their operation; </p><p className={s.paragraph}><span>(d)</span> for assessment,
                          testing (including systems tests) and analysis, including credit and/or behaviour scoring,
                          statistical, market and product analysis; </p><p className={s.paragraph}><span>(e)</span> to
                          prevent, detect and prosecute money laundering, fraud and other crimes; </p><p
                          className={s.paragraph}><span>(f)</span> to improve the accuracy of our records; </p><p
                          className={s.paragraph}><span>(g)</span> to develop and improve our services to you and other
                          customers; </p><p className={s.paragraph}><span>(h)</span> to respond to your enquiries or
                          complaints; </p><p className={s.paragraph}><span>(i)</span> to carry out regulatory checks or
                          other work to meet our obligations to any regulatory authority; </p><p
                          className={s.paragraph}><span>(j)</span> to protect our interests, including locate you and
                          recover any debts you owe, cross-check details on proposals or claims for all types of
                          insurance, to process and collect charges; </p><p className={s.paragraph}><span>(k)</span> to
                          identify and inform you by letter, telephone (including automated dialling, digital
                          television),text messages or electronically about products and services (including those of
                          others) which may be of interest to you, unless you tell us not to contact you about these or
                          other available promotions; </p><p className={s.paragraph}><span>(l)</span> to manage and
                          provide any rewards and offers and administer any promotions and competitions; and </p><p
                          className={s.paragraph}><span>(m)</span> in any other ways described below and in the Fly Now
                          Pay Later Conditions. </p><p className={s.paragraph}>We will keep information about you and
                          how you manage your account(s) private and confidential, but may share it as follows: </p><p
                          className={s.paragraph}><span>(a)</span> with credit reference agencies and fraud prevention
                          agencies in the ways described below; </p><p className={s.paragraph}><span>(b)</span> with
                          other companies within the Fly Now Pay Later Group; </p><p className={s.paragraph}>
                          <span>(c)</span> with people who provide a service to you (when you use your account to make
                          payments), with people who provide a service to us, or who are acting as our agents, on the
                          understanding that they will keep the information confidential; </p><p
                          className={s.paragraph}><span>(d)</span> with any company that we are providing products or
                          services in conjunction with; </p><p className={s.paragraph}><span>(e)</span> with anyone to
                          whom we transfer or may transfer our rights and duties under this agreement; </p><p
                          className={s.paragraph}><span>(f)</span> with any third party as a result of any restructure,
                          sale, merger or acquisition of any company within the Fly Now Pay Later Group, provided that
                          any recipient uses your information for the same purposes as it was originally supplied to us
                          and/or used by us; </p><p className={s.paragraph}><span>(g)</span> if you have consented;
                        </p><p className={s.paragraph}><span>(i)</span> if we have a duty to do so or if the law,
                          public interest, or this agreement allows us to do so. </p><p className={s.paragraph}>We may
                          use automated processes when we use your information for any of the purposes listed in How We
                          Use Your Information and/or elsewhere in the Fly Now Pay Later Conditions. </p><p
                          className={s.paragraph}>You must write to us at, Fly Now Pay Later, 3rd Floor, 12 East
                          Passage, London, EC1A 7LP, United Kingdom. You can also call us on 020 3322 2996 if you don’t
                          want us to tell you or allow other members of the Fly Now Pay Later Group to tell you about
                          other products and services. </p><p className={s.paragraph}>You can also manage your
                          marketing preferences by logging on to your Fly Now Pay Later account at <a
                            href="https://www.flynowpaylater.com" target="_blank">https://www.flynowpaylater.com</a> and
                          selecting “Your Details”.</p><p className={s.paragraph}>If you are not eligible for this
                          credit product at this time, unless you write to the address above you agree that we may use
                          your information to send you details of alternative products (including those of other
                          companies). </p><p className={s.paragraph}>We will retain information about you after the
                          closure of your account for as long as permitted for legal, regulatory, fraud prevention and
                          legitimate business purposes. </p><p className={s.paragraph}>We may give you further details,
                          from time to time, about how we may use your personal information. You can always find the
                          most up-to-date Privacy Policy at <a href="https://www.flynowpaylater.com" target="_blank">https://www.flynowpaylater.com</a>.
                        </p><p className={s.paragraph}>CREDIT REFERENCE AND FRAUD PREVENTION AGENCIES</p><p
                          className={s.paragraph}>We use credit reference agencies and fraud prevention agencies: </p>
                          <p className={s.paragraph}><span>(a)</span> to help us make decisions when you apply for and
                            after you have obtained any credit and credit related or other products, make enquires,
                            check your details, check your credit history, verify your identity or to assist us in
                            managing your account(s), for example if we wish to consider changing your credit limit, or
                            offering you other products, now or in the future, or trace your whereabouts, recover debts
                            you owe or as otherwise described above in How We Use Your Information and in the Fly Now
                            Pay Later Conditions; </p><p className={s.paragraph}><span>(b)</span> to share information
                            about you and how you manage your account(s) and </p><p className={s.paragraph}>
                            <span>(c)</span> if you give us false or inaccurate information or we suspect or identify
                            fraud, money laundering and/or other crimes. </p><p className={s.paragraph}>When making
                            enquiries we may search at:</p><p className={s.paragraph}><span>(a)</span> Credit reference
                            agencies records for information on: </p><p className={s.paragraph}><span>(i)</span>
                            previous applications and the conduct of the accounts in your and your associate(s) name,
                            public available information such as County Court Judgments (CCJs) and bankruptcies,
                            Electoral Register information and fraud prevention information; </p><p
                            className={s.paragraph}><span>(ii)</span> if you are making a joint application now or have
                            ever previously made joint applications, have joint accounts or are financially linked or
                            tell us that you have a spouse or financial associate, we will link your records together
                            and we will check your financial associates’ personal accounts as well. So you must be sure
                            that you have their agreement to disclose information about them. Credit reference agencies
                            also link your records together and these links will remain on your and their files until
                            such time as you or your partner successfully files for a disassociation with the credit
                            reference agencies to break that link. </p><p className={s.paragraph}><span>(b)</span>
                            Fraud prevention agencies for information on you and any addresses at which you have lived
                            and on your business (if you have one). </p><p className={s.paragraph}>Any enquiry we make
                            at a credit reference agency may be assessed with reference to any ‘financially associated’
                            records.</p><p className={s.paragraph}>Credit reference agencies will not use information
                            about you to create a blacklist or make a decision. </p><p className={s.paragraph}>Credit
                            reference agencies keep a record of our enquiries/searches (“footprint”) and may record, use
                            and give out information we give them to other lenders, insurers and other organisations,
                            including any previous and subsequent names that have been used by the accountholders and
                            how you/they manage it/them, even if your application does not proceed or is unsuccessful.
                            This also applies to fraud prevention agencies and other organisations involved in crime and
                            fraud prevention. </p><p className={s.paragraph}>If the search was for a credit
                            application, the record of that search (but not the name of the organisation that carried it
                            out) may be seen by other organisations when you apply for credit in the future. </p><p
                            className={s.paragraph}>Credit reference agencies record the amount of any outstanding
                            debts we (or others) inform them about, if you have borrowed from us (or them) and not
                            repaid in full and on time. Records shared with credit reference agencies remain on file for
                            6 years after they are closed, whether settled by you or defaulted. </p><p
                            className={s.paragraph}>The information recorded by credit reference agencies and fraud
                            prevention agencies may be used by us and other organisations to make assessments for credit
                            from you and your financial associates and to help make decisions about you and your
                            financial associates on credit and credit related services, motor, household, life, and
                            other insurance facilities (including handling claims), trace your whereabouts, recover
                            debts that you owe, prevent crime, fraud and money laundering, verify your identity and
                            manage your accounts or insurance policies including to update and maintain the accuracy of
                            your records. </p><p className={s.paragraph}>The information recorded by fraud prevention
                            agencies may be accessed and used by us and other organisations in the UK and in other
                            countries. </p><p className={s.paragraph}>By proceeding with an application, you agree to
                            allow us to use your personal information to verify your identity and carry out a credit
                            check.</p><p className={s.paragraph}>INTERNATIONAL TRANSFERS</p><p
                            className={s.paragraph}>If we transfer your information to a person, office, branch,
                            organisation, service provider or agent in another country, we will make sure that they
                            agree to apply the same levels of protection as we are required to apply to information held
                            in the UK and to use your information only for the purposes that we have permitted. </p><p
                            className={s.paragraph}>In order to make or receive some payments, the details of the
                            payment (including information relating to those involved in the payment) may be received
                            from or sent abroad, where it could be accessible by overseas regulators and authorities in
                            connection with their legitimate duties (e.g. the prevention of crime). By using your
                            account to make payments you agree to this on behalf of yourselves and others involved in
                            your payments. </p><p className={s.paragraph}>FURTHER INFORMATION</p><p
                            className={s.paragraph}>You can ask us for a copy of the information we keep about you. A
                            fee will be charged for this service. </p><p className={s.paragraph}>If you believe that
                            any information we hold about you is incorrect or incomplete, you should write to us
                            immediately. If we find that any information is incorrect or incomplete we will correct it
                            promptly. </p><p className={s.paragraph}>For information about the data the credit
                            reference agencies hold about you, you can contact the credit reference agencies directly.
                            The information they each hold about you may not be the same, so you should consider
                            contacting them all. They will charge you a fee for this service. </p><p
                            className={s.paragraph}><span>(a)</span> CallCredit, Consumer Services Team, PO Box 491,
                            Leeds, LS3 1WZ or call 0870 0601414 (Personal credit data only) or log on to
                            www.callcredit.co.uk </p><p className={s.paragraph}><span>(b)</span> Equifax PLC, Credit
                            File Advice Centre, PO Box 1140, Bradford, BD1 5US or call 08443 350 550 or log on to
                            www.myequifax.co.uk </p><p className={s.paragraph}><span>(c)</span> Experian, Consumer Help
                            Service, PO Box 8000, Nottingham NG80 7WF or call 0844 481 8000 or log on to
                            www.experian.co.uk </p><p className={s.paragraph}>Please contact us if you want to receive
                            details of the fraud prevention agencies. </p><p className={s.paragraph}>You agree that
                            calls between us may be recorded and/or monitored in the interest of security, for quality
                            control and to ensure better customer servicing, staff training and account operation. </p>
                          <p className={s.blueTitle}>SECURITY POLICY</p><p className={s.paragraph}><span>•</span> Our
                            systems operate on secure encryption. Look for the yellow padlock symbol in your browser
                            status bar</p><p className={s.paragraph}><span>•</span> A team of independent security
                            experts regularly tests our website</p><p className={s.paragraph}><span>•</span> We’ll
                            verify your identity before disclosing confidential informationover the phone or resetting
                            your password</p><p className={s.paragraph}><span>•</span> Despite our best endeavours, we
                            can’t always guarantee the website will be fault free and don’t accept liability fr any
                            errors or omissions</p><p className={s.paragraph}>All forms and online account pages, i.e.
                            those pages that show your information, use encryption. Encryption makes your information
                            unreadable to anyne who might intercept it. In addition, a Secure Sockets Layer (SSL) is
                            used to connect your browser to our secure servers.</p><p className={s.blueTitle}>SECURE
                            SOCKET LAYER (SSL)</p><p className={s.paragraph}>A “Secure Socket Layer” is acommonly used
                            method of managing the security of messages transmitted across the Internet and is used by
                            us to connect your computer to our secure server. You can tell that SSL is in use if the URL
                            in the search bar starts with https:// and/or a lock icon is displayed on your browser. If
                            you have problems getting to secure mode, install one of the latest browsers and try the
                            website again.</p><p className={s.blueTitle}>USE OF EMAIL</p><p className={s.paragraph}>
                            Please note tha ordinary email is not secure.</p><p className={s.paragraph}>Please don’t
                            send us any confidential informationvia email. We’ll only use email to send you account
                            information, such as your account balance and transaction information or special offers, if
                            you’ve given your consent. We won’t use e-mail to send other confidential personal
                            information to you. We can’t accept any responsibility for the unauthorised access by a
                            third party and/or the corruption of data being sent by individuals to Fly Now Pay Later.
                            It’s our policy that if any of our customers are victims of unauthorised access to their
                            accounts, and provided that you’ve not breached our security procedures, acted fraudulently
                            or without reasonable care, Fly Now Pay Later will cover any direct financial loss which you
                            may have suffered.</p><p className={s.blueTitle}>PHISHING</p><p className={s.paragraph}>
                            If you thin you’ve received a fraudulent email that looks like it is from Fly Now Pay Later
                            forward the entire email including the header and footer to <a
                            href="mailto:support@flynowpaylater.com" target="_blank">support@flynowpaylater.com</a> and
                            then delete it from your email account.</p><p className={s.blueTitle}>WHAT IS A COOKIE</p>
                          <p className={s.paragraph}>A cookie is a tiny ile that stores information on your computer
                            when you visit a website. You can find out more about cookies by visiting
                            www.allaboutcookies.org. Cookies enable us to improve our service to you, estimate our
                            audience size and usage patterns, store information about your preferences and recognise you
                            when you return to our site. We will set a cookie on the landing page of&nbsp;<a
                              href="https://www.flynowpaylater.com" target="_blank">https://www.flynowpaylater.com</a>,
                            but as soon as possible we will provide you with information about our use of cookies. By
                            using our site, you agree to our use of cookies. You can set your web browser to refuse
                            cookies (or just to refuse third party cookies), but if you do this you may not be able to
                            enjoy full use of the site and you may not be able to take advantage of certain promotions
                            we may run from time to time. </p><p className={s.blueTitle}>WHY DO WE USE COOKIES?</p><p
                            className={s.paragraph}>We use cookies to providean enhanced experience for our customers
                            and to provide our service to you; this personal information is only used by us and is not
                            shared with third parties. Cookies allow us to remember your preferences and personalise our
                            site, including the personalised welcome message that you see when you visit the site as a
                            returning customer. Cookies also help us understand what customers like to look at on our
                            site, for example, we use cookies to count the number of visits to particular pages or how
                            many customers complete an application. This information does not allow us to identify any
                            particular customer but helps us to understand our customers’ needs and improve our service
                            to you. We also set cookies to manage the loan application and account management process,
                            which are critical to providing our service to you.</p><p className={s.blueTitle}>WHAT
                            COOKIES ARE SET WHEN USING Fly Now Pay Later</p><p className={s.paragraph}>The table below
                            shows all of the cookies that might e set by us when you visit our site and why we use
                            them.</p><p className={s.paragraph}>Cookie Description</p><p className={s.blueTitle}>
                            Purpose</p><p className={s.blueTitle}>Session ID</p><p className={s.paragraph}>Enables us
                            tomaintain the state of your current browsing session as you move across pages on our site,
                            including during the application process.</p><p className={s.blueTitle}>User ID</p><p
                            className={s.paragraph}>Allows youto access personal information about your account.</p><p
                            className={s.blueTitle}>Logged in state</p><p className={s.paragraph}>Tracks whether
                            youhave successfully provided your credentials so that we can provide secure information
                            about your account.</p><p className={s.blueTitle}>Affiliate tracking</p><p
                            className={s.paragraph}>Tells us that you reahed Fly Now Pay Later via one of our
                            affiliates, so that we can tailor the application experience.&nbsp;This is also achieved
                            using a unique URL identifier in the description URL when you initially visit our site.</p>
                          <p className={s.paragraph}>The following cookies are set by our third party partner suppliers
                            who provide the following services:</p><p className={s.blueTitle}>Cookie Description</p><p
                            className={s.blueTitle}>Purpose</p><p className={s.blueTitle}>Website Aalytics</p><p
                            className={s.paragraph}>This website uses Goole Analytics Premium, a web analytics service
                            provided by Google Ireland Limited ("Google"). Google Analytics Premium uses "cookies",
                            which are text files placed on your computer, to help the website analyze how users use the
                            site. The information generated by the cookies about your use of the website and your
                            current IP-address will be transmitted by your browser to and will be stored by Google on
                            servers in the United States and other countries. On behalf of the operator of this website
                            Google will use this information for the purpose of evaluating your use of the website,
                            compiling reports on website activity and providing other services relating to website
                            activity and internet usage to the website operator. The IP address collected through Google
                            Analytics will not be associated with any other data held by Google. You may refuse the use
                            of cookies by selecting the appropriate settings on your browser, however please note that
                            if you do this you may not be able to use the full functionality of this website. You may
                            also stop the transmission of information generated by the cookies about your use of the
                            website and of your IP address to Google, by downloading and installing the Google Analytics
                            Opt-out Browser Add-on available here:&nbsp;tools.google.com/dlpage/gaoptout</p><p
                            className={s.blueTitle}>Advertisement tracking</p><p className={s.paragraph}>These
                            cookies are used tocollect non-personal information about how visitors interact with
                            advertising served on other websites before they arrive at&nbsp;<a
                              href="https://www.flynowpaylater.com" target="_blank">https://www.flynowpaylater.com</a>
                            and which advertisements our customers prefer. This information helps us deliver ads that
                            are relevant to your interests, control the number of times you see a given ad, and measure
                            the effectiveness of advertising campaigns. If you don't wish to have this information used
                            for the purpose of serving you targeted ads, you may opt-out by clicking&nbsp;here.&nbsp;
                            Please note this does not opt you out of being served advertising, you will continue to
                            receive generic ads.</p><p className={s.blueTitle}>Mobile Redirect</p><p
                            className={s.paragraph}>This cookie allowsus to remember your preference to view either our
                            full or mobile websites when visiting <a href="https://www.flynowpaylater.com"
                                                                     target="_blank">https://www.flynowpaylater.com</a>
                            via a mobile device.</p><p className={s.blueTitle}>Website testing</p><p
                            className={s.paragraph}>These allow us to sow different versions of the same page or
                            feature on the site and then track to see which version performs best. We use a cookie to
                            manage which version to display when you visit&nbsp;<a href="https://www.flynowpaylater.com"
                                                                                   target="_blank">https://www.flynowpaylater.com</a>
                            The specific variation that you will see is randomly chosen and no personal information is
                            stored or tracked through this cookie.</p><p className={s.blueTitle}>Clickstream</p><p
                            className={s.paragraph}>Allows us to ientify the specific pages viewed during the current
                            session.</p><p className={s.paragraph}>Fly Now Pay Later Limited acts as a credit
                            intermediary and not a lender and is authorised and regulated by the Financial Conduct
                            Authority under registration number 726937. Registered office: 3rd Floor, 12 East Passage,
                            London, EC1A 7LP, United Kingdom. Registered in England &amp; Wales. Registered No:
                            06936813.</p><p className={s.paragraph}>Loan facilities are provided by Travelfund Limited
                            who are authorised and regulated by the Financial Conduct Authority under registration
                            number 672306. Registered office: 3rd Floor, 12 East Passage, London, EC1A 7LP, United
                            Kingdom. Registered in England &amp; Wales. Registered No: 09020100.</p><p
                            className={s.paragraph}>Fly Now Pay Later ® &amp; Travelfund ® are registered trademarks of
                            Travelfund.co.uk Limited.</p></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Modal.Body>
  </Modal>
)

export default connectModal({ name: 'fnplTermsModal', destroyOnHide: true })(FNPLTerms)
