import React, { Component } from 'react'
import { connect } from 'react-redux'

class FNPLTerms extends Component {
  state = {
    loaded: false
  }

  async componentWillMount() {
    const AsyncComponent = await System.import(/* webpackChunkName: "FNPLTermsModal" */ './FNPLTerms')

    if (!this.unmounted) {
      this.AsyncComponent = AsyncComponent.default
      this.setState({ loaded: true })
    }
  }

  componentWillUnmount() {
    this.unmounted = true
  }

  render() {
    const { state: { loaded }, AsyncComponent, props } = this
    return (
      loaded ? <AsyncComponent {...props} /> : null
    )
  }
}

const mapActionCreators = {
}

const mapStateToProps = () => ({
})

export default connect(mapStateToProps, mapActionCreators)(FNPLTerms)
