import React from 'react'
import Form from 'react-bootstrap/lib/Form'
import pure from 'recompose/pure'
import s from './PassengerInfoForm.scss'
import Checkout from './Checkout/Checkout'
import Confirmation from './Confirmation/Confirmation'
import FNPLBtn from './FNPLBtn'

const PassengerInfoForm = pure(({
  confirm, handleSubmit, checkoutEmails,
  trip = { baggage_info: {} }, email, form, minPassengerNo, showFNPLTermsModal,
  editInfo, confirmInfo, showPaymentButton, passengersNumber, maxPassengerNo,
  user, createFNPLPaymentSend, stopChecking, formData, resetPaymentButton,
  smsCodeValid, changeBaggageNumber, changePassengersNumber, removePassenger, creditAmount,
  checkFlightLoading, changeNumberValue, checkingCode, codaValidationError, phoneError, emailError,
  interestRate, paybackSelected, showExtraBaggageModal, baggageNumber,
}) =>
  <Form
    onSubmit={handleSubmit}
    className={s.passengerInfoFormWrap}
  >
    {!confirmInfo
      ? <Checkout
        confirm={confirm}
        handleSubmit={handleSubmit}
        email={email}
        cabinBaggage={trip.baggage_info && trip.baggage_info.cabin}
        additionalBaggages={trip.baggage_info && trip.baggage_info.additional}
        baggageFees={trip.baggage_fees}
        changePassengersNumber={changePassengersNumber}
        changeBaggageNumber={changeBaggageNumber}
        editInfo={editInfo}
        showExtraBaggageModal={showExtraBaggageModal}
        removePassenger={removePassenger}
        confirmInfo={confirmInfo}
        showPaymentButton={showPaymentButton}
        smsCodeValid={smsCodeValid}
        changeNumberValue={changeNumberValue}
        checkFlightLoading={checkFlightLoading}
        checkingCode={checkingCode}
        formData={formData}
        creditAmount={creditAmount}
        codaValidationError={codaValidationError}
        passengersNumber={passengersNumber}
        minPassengerNo={minPassengerNo}
        phoneError={phoneError}
        emailError={emailError}
        maxPassengerNo={maxPassengerNo}
        checkoutEmails={checkoutEmails}
      />
      : <Confirmation
        baggage_info={trip.baggage_info}
        confirm={confirm}
        formData={formData}
        baggageNumber={baggageNumber}
        resetPaymentButton={resetPaymentButton}
        passengersNumber={passengersNumber}
        interestRate={interestRate}
        showFNPLTermsModal={showFNPLTermsModal}
      />
    }
    {__CLIENT__ && showPaymentButton &&
      <FNPLBtn
        ticket={trip}
        user={user}
        form={form}
        formData={formData}
        createFNPLPaymentSend={createFNPLPaymentSend}
        stopChecking={stopChecking}
        resetPaymentButton={resetPaymentButton}
      />
    }
  </Form>
)

export default PassengerInfoForm
