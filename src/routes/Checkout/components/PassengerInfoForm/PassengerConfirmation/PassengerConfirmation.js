import React from 'react'
import { pure } from 'recompose'
import cn from 'classnames'
import s from './PassengerConfirmation.scss'

import CheckoutHeader from './../CheckoutHeader/CheckoutHeader'

const PassengerConfirmation = pure(({
  formData,
  cabinBaggage,
  baggageNumber,
  passengersNumber,
  resetPaymentButton,
  additionalBaggages,
 }
) =>
  <div className={s.passengerConfirmation}>
    {
      formData.values.passengers.map(({
        first_name,
        last_name,
        title,
        birth_date,
        email
      }, index) =>
        <div key={index}>
          <CheckoutHeader title={`Passenger ${index + 1}`} buttonOnClick={resetPaymentButton} buttonText={'Edit'} />
          <div className={s.infoBlock}>
            <h4 className={s.title}>{first_name} {last_name}</h4>
            <div className={s.content}>{title === '17' ? 'Male' : 'Female'}, {birth_date}</div>
          </div>

          {index === 0 &&
          <div>
            <div className={s.infoBlock}>
              <h4 className={s.title}>Contact information</h4>
              <div className={cn(s.content, s.contentWrap)}>
                <div className={s.address}>
                  <div>{formData.values.house_number} {formData.values.street}</div>
                  <div>{formData.values.post_code} {formData.values.city}</div>
                  <div>{formData.values.county}</div>
                </div>
                <div className={s.contactWrap}>
                  <div>{formData.values.passengers[0].email}</div>
                  <div>{formData.values.phone}</div>
                </div>
              </div>
            </div>

            <div className={s.infoBlock}>
              <h4 className={s.title}>Baggage information</h4>
              {
                !!cabinBaggage.length && cabinBaggage.map((data, i) =>
                  <div className={s.content} key={i}>
                    {passengersNumber} baggage, {data.width} x {data.height} x {data.length} cm, max. {data.weight}kg
                  </div>)
              }
              {
                baggageNumber > 0 && additionalBaggages && additionalBaggages.params &&
                <div className={s.content}>
                  <span>
                    {baggageNumber} baggage, {additionalBaggages.params.length} x {additionalBaggages.params.width} x {additionalBaggages.params.height} cm, max. {additionalBaggages.params.weight}kg
                  </span>
                </div>
              }
            </div>
          </div>
          }
        </div>
      )
    }
  </div>
)

export default PassengerConfirmation
