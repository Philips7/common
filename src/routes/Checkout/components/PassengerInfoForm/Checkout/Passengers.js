import React from 'react'
import { pure, compose, withHandlers } from 'recompose'
import Passenger from './Passenger'
import s from './Checkout.scss'

const composedHOC = compose(
  withHandlers({
    clickHandler: ({ passengersNumber, maxPassengerNo, showExtraBaggageModal, checkFlightLoading, changePassengersNumber }) => () => {
      if (passengersNumber >= maxPassengerNo) {
        showExtraBaggageModal({ noPassenger: true })
      } else {
        !checkFlightLoading && changePassengersNumber(1, true)
      }
    }
  }),
  pure
)

export default composedHOC(({
  confirm,
  email,
  formData,
  changeBaggageNumber,
  baggageFees,
  cabinBaggage,
  additionalBaggages, fields,
  removePassenger,
  creditAmount,
  showExtraBaggageModal,
  checkFlightLoading,
  passengersNumber,
  minPassengerNo,
  clickHandler,
  emailError,
  checkoutEmails
}) =>
  <div>
    {fields.map((filedName, index) => <Passenger
      confirm={confirm}
      email={email}
      formData={formData}
      changeBaggageNumber={changeBaggageNumber}
      baggageFees={baggageFees}
      cabinBaggage={cabinBaggage}
      additionalBaggages={additionalBaggages}
      filedName={filedName}
      removePassenger={removePassenger}
      passengerIndex={index}
      key={index}
      passengersNumber={passengersNumber}
      minPassengerNo={minPassengerNo}
      creditAmount={creditAmount}
      showExtraBaggageModal={showExtraBaggageModal}
      checkFlightLoading={checkFlightLoading}
      emailError={emailError}
      checkoutEmails={checkoutEmails}
    />)}
    {fields.length < 9 &&
    <div className={s.addPasssengerWrap}>
      <a
        className={s.addPassengerBtn}
        onClick={clickHandler}
      >
        + Add new passenger
      </a>
      {checkFlightLoading && <div className={s.loaderSpinner}/>}
    </div>
    }
  </div>
)
