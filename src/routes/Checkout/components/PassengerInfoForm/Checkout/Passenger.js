import React from 'react'
import { pure, compose, withHandlers, lifecycle } from 'recompose'
import Field from 'redux-form/lib/Field'
import { formValueSelector } from 'redux-form'
import { I18n } from 'react-redux-i18n'
import s from './Checkout.scss'
import cn from 'classnames'
import { DateFieldGroup, InputFieldGroup, } from 'components/FieldGroup'
import InlineSVG from 'svg-inline-react'
import RadioButton from 'components/RadioButton/RadioButton'
import CheckoutContainer from './../../CheckoutContainer/CheckoutContainer'
import CheckoutHeader from './../CheckoutHeader/CheckoutHeader'
import WarningMessage from './../../WarningMessage/WarningMessage'
import FNLPoptions from '../../../../../utils/FNLPoptions'
import passengerIcon from 'static/images/checkout/passenger-icon.svg'
import baggageSm from 'static/images/checkout/baggage-sm.svg'
import baggageLg from 'static/images/checkout/baggage-lg.svg'
import path from 'ramda/src/path'
import config from '../../../../../config'

// const NormalizeInput = value => value && value.replace(/[^a-z^A-Z]/g, '')
const NormalizeInputWithSpaces = value => value && value.replace(/[^a-z^A-Z ]/g, '')
const NormalizeInputWithNumbers = value => value && value.replace(/[^a-z^A-Z^0-9]/g, '')
const NormalizeInputEmail = value => value && value.replace(/[^a-z^A-Z^0-9!@#$%^&*.]/g, '')

const composedHOC = compose(
  withHandlers({
    clickHandler: ({ creditAmount, showExtraBaggageModal, passengerIndex, changeBaggageNumber }) => (price) => {
      if (price + creditAmount > 2000) {
        showExtraBaggageModal({ noBaggage: true })
      } else if (passengerIndex > 0) {
        showExtraBaggageModal({ extraBaggage: true })
      } else changeBaggageNumber(1)
    }
  }),
  lifecycle({
    componentWillReceiveProps(nextProps) {
      const { additionalBaggages, changeBaggageNumber } = nextProps
      if (!this.checked && JSON.stringify(nextProps) !== JSON.stringify(this.props) && additionalBaggages) {
        this.checked = true
        if (additionalBaggages.prices && additionalBaggages.prices.length && additionalBaggages.prices[0] === 0) {
          setTimeout(() => changeBaggageNumber(1), 0)
        }
      }
    }
  }),
  pure
)

export default composedHOC(({
  confirm, email, formData, changeBaggageNumber, baggageFees = [],
  cabinBaggage = [], additionalBaggages, filedName, passengerIndex,
  removePassenger, checkFlightLoading, showExtraBaggageModal,
  passengersNumber, minPassengerNo, clickHandler, emailError, checkoutEmails, touched, meta, payload
}) => {
  const baggageFeesArray = (baggageFees && baggageFees[passengerIndex] && Object.values(baggageFees[passengerIndex])) || []
  return (<CheckoutContainer
    passengerIndex={passengerIndex}
    removePassenger={removePassenger}
    title={`${passengerIndex + 1}. Passenger information ${!passengerIndex ? '- ' : ''}`}
    icon={passengerIcon}
    checkFlightLoading={checkFlightLoading}
    passengersNumber={passengersNumber}
    minPassengerNo={minPassengerNo}
    showExtraBaggageModal={showExtraBaggageModal}
  >

    <WarningMessage
      title={!passengerIndex ? I18n.t('form.firstPassengerTitle') : I18n.t('form.otherPassengersTitle')}
      description={`${!passengerIndex ? `${I18n.t('form.otherPassengersTitle')}. ` : ''}${I18n.t('form.passengerDescription')}`}
    />

    <div className={s.formTable}>
      {/* gender */}
      <div className={s.fieldRow}>
        <div className={s.labelCol} />
        <div className={s.fieldCol}>
          <Field
            component={RadioButton}
            name={`${filedName}.title`}
            disabled={confirm}
            placeholder='Title'
            options={FNLPoptions.title}
            checked={path(['values', 'passengers', [`${passengerIndex}`], 'title'], formData) || '17'}
          />
        </div>
      </div>

      {/* first name */}
      <div className={s.fieldRow}>
        <div className={s.labelCol}>
          <label htmlFor="first_name">First name</label>
        </div>
        <div className={s.fieldCol}>
          <Field
            id='first_name'
            name={`${filedName}.first_name`}
            type='text'
            component={InputFieldGroup}
            disabled={confirm}
            placeholder={I18n.t('form.firstName')}
            normalize={NormalizeInputWithSpaces}
            customStyleSm
          />
        </div>
      </div>


      {/* last name */}
      <div className={s.fieldRow}>
        <div className={s.labelCol}>
          <label htmlFor="last_name">Last name</label>
        </div>
        <div className={s.fieldCol}>
          <Field
            id='last_name'
            name={`${filedName}.last_name`}
            type='text'
            component={InputFieldGroup}
            disabled={confirm}
            placeholder={I18n.t('form.lastName')}
            normalize={NormalizeInputWithSpaces}
            customStyleSm
          />
        </div>
      </div>


      {/* birth date */}
      <div className={s.fieldRow}>
        <div className={s.labelCol}>
          <label htmlFor="birth_date">Date of birth</label>
        </div>
        <div className={s.fieldCol}>
          <Field
            name={`${filedName}.birth_date`}
            component={DateFieldGroup}
            placeholder={I18n.t('form.dateOfBirth')}
            inputStyle={s.input}
            disabled={confirm}
            customStyleSm
          />
        </div>
      </div>


      {/* email address */}
      <div className={s.fieldRow}>
        <div className={s.labelCol}>
          <label htmlFor="email">E-mail address</label>
        </div>
        <div className={s.fieldCol}>
          <Field
            id='email'
            name={`${filedName}.email`}
            customError={filedName === 'passengers[0]' && emailError && 'The email address you have provided does not appear to be valid.'}
            type='email'
            component={InputFieldGroup}
            disabled={confirm || email}
            placeholder='Email'
            normalize={NormalizeInputEmail}
            customStyleSm
            onBlur={() => checkoutEmails(`${filedName}.email`)}
          />
        </div>
      </div>


      {/* document number */}
      <div className={s.fieldRow}>
        <div className={s.labelCol}>
          <label htmlFor="document_number">Document number</label>
        </div>
        <div className={s.fieldCol}>
          <Field
            id='document_number'
            component={InputFieldGroup}
            name={`${filedName}.document_number`}
            disabled={confirm}
            placeholder={I18n.t('form.passportIdNumber')}
            type='text'
            normalize={NormalizeInputWithNumbers}
            customStyleSm
          />
        </div>
      </div>

    </div>

    <CheckoutHeader title='Baggage' className={s.baggageTitle} />

    {/* Baggage */}
    {
      checkFlightLoading && !cabinBaggage.length &&
      <div className={s.loaderSpinner}>
        Loading...
      </div>
    }
    {
      !!cabinBaggage.length && cabinBaggage.map((data, index) =>
        <div className={s.baggage} key={index}>
          <span className={s.baggageLabel}>
            <span>Cabin Baggage</span>
            <InlineSVG src={baggageSm} className={s.iconWrap} />
          </span>
          <span className={s.info}>
            <span className={s.baggageSize}>
              {data.length} x {data.width} x {data.height} cm, {data.weight}kg
            </span>
            <span className={cn(s.baggagePrice, s.greenText)}>FREE</span>
          </span>
        </div>
      )}
    {
      additionalBaggages && additionalBaggages.prices && additionalBaggages.params && <div>
        {
          !!baggageFees.length && baggageFeesArray.map((bagagePrice, index) => (
            <div
              key={index}
              className={cn(s.baggage, s.checked)}
            >
              <span className={s.baggageLabel}>
                <span>Checked Baggage</span>
                <InlineSVG src={baggageLg} className={s.iconWrap} />
              </span>
              <span className={s.info}>
                <span className={s.baggageSize}>
                  {additionalBaggages.params.length} x {additionalBaggages.params.width} x {additionalBaggages.params.height}
                  cm, {additionalBaggages.params.weight}kg
                </span>
                <div>
                  <span className={cn(s.baggagePrice, bagagePrice > 0 ? s.blueText : s.greenText)}>
                    {bagagePrice > 0 ? `${config.currency} ${bagagePrice}` : 'FREE'}
                  </span>
                  { bagagePrice > 0 &&
                  <div
                    onClick={() => changeBaggageNumber(-1)}
                    className={s.remove}
                  >remove</div>
                  }
                </div>
              </span>
            </div>
          ))
        }
        {
          additionalBaggages && baggageFeesArray.length < additionalBaggages.prices.length && (
            <div
              className={cn(s.baggage, s.pointer)}
              onClick={() => clickHandler(additionalBaggages.prices[baggageFeesArray.length])}
            >
              <span className={s.baggageLabel}>
                <span>+Add Checked Baggage</span>
                <InlineSVG src={baggageLg} className={s.iconWrap} />
              </span>
              <span className={s.info}>
                <span className={s.baggageSize}>
                  {additionalBaggages.params.length} x {additionalBaggages.params.width} x {additionalBaggages.params.height}
                  cm, {additionalBaggages.params.weight}kg
                </span>
                {additionalBaggages.prices[baggageFeesArray.length] > 0
                  ? <span className={cn(s.baggagePrice, s.blueText)}>
                    + {config.currency} {additionalBaggages.prices[baggageFeesArray.length]}
                  </span>
                  : <span className={cn(s.baggagePrice, s.greenText)}>
                    + FREE
                  </span>}
              </span>
            </div>
          )
        }
      </div>
    }
  </CheckoutContainer>)
})
