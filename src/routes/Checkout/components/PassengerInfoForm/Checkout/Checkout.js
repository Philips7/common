import React from 'react'
import { pure } from 'recompose'
import Field from 'redux-form/lib/Field'
import FieldArray from 'redux-form/lib/FieldArray'
import InlineSVG from 'svg-inline-react'
import listIcon from 'static/images/checkout/info-list-icon.svg'
import checkemarkIcon from 'static/images/checkmark.svg'
import phoneIcon from 'static/images/checkout/phone-icon.svg'
import { InputFieldGroup, PhoneFieldGroup, SelectFieldGroup, } from 'components/FieldGroup'
import Passengers from './Passengers'
import PromoCode from './PromoCode'
import s from './Checkout.scss'

import CheckoutContainer from '../../CheckoutContainer/CheckoutContainer'
import FNLPoptions from '../../../../../utils/FNLPoptions'

const NormalizeInput = value => value && value.replace(/[^a-z^A-Z ]/g, '')
const NormalizeInputOnlyNumbers = value => value && value.replace(/[^0-9]/g, '')
const NormalizeInputWithNumbers = value => value && value.replace(/[^a-zA-Z0-9']\s?/g, '')
const NormalizeInputWithNumbersandSpaces = value => value && value.replace(/[^a-z^A-Z^0-9 ]/g, '')

const Checkout = pure(({
                         confirm, handleSubmit, email, showPaymentButton, showExtraBaggageModal,
                         smsCodeValid, formData, changeBaggageNumber, baggageFees = [], checkAndConfirm,
                         cabinBaggage = [], additionalBaggages, changePassengersNumber, removePassenger, creditAmount,
                         checkFlightLoading, changeNumberValue, checkingCode, codaValidationError, phoneError, emailError,
                         minPassengerNo, passengersNumber, maxPassengerNo, checkoutEmails
                       }) =>
  <div className={s.checkoutForm}>
    <FieldArray
      name='passengers'
      component={Passengers}
      confirm={confirm}
      email={email}
      minPassengerNo={minPassengerNo}
      formData={formData}
      passengersNumber={passengersNumber}
      creditAmount={creditAmount}
      changeBaggageNumber={changeBaggageNumber}
      baggageFees={baggageFees}
      maxPassengerNo={maxPassengerNo}
      cabinBaggage={cabinBaggage}
      changePassengersNumber={changePassengersNumber}
      removePassenger={removePassenger}
      checkFlightLoading={checkFlightLoading}
      additionalBaggages={additionalBaggages}
      showExtraBaggageModal={showExtraBaggageModal}
      emailError={emailError}
      checkoutEmails={checkoutEmails}
    />

    <CheckoutContainer title='Contact information - ' icon={listIcon} passengerIndex={0}>
      <div className={s.descContainer}>
        <span className={s.bookerInfo}>
          Please fill in the contact information belonging to the booker.
        </span>
      </div>
      <div className={s.formTable}>

        {/* UK mobile number */}
        <div className={s.fieldRow}>
          <div className={s.labelCol}>
            <label htmlFor='contact_phone'>UK mobile number</label>
          </div>
          <div className={s.fieldCol}>
            <Field
              component={PhoneFieldGroup}
              name='contact_phone'
              id='contact_phone'
              disabled={confirm}
              placeholder='0XXXXXXXXXX'
              type='text'
              customError={phoneError && 'The mobile phone you have provided does not appear to be a valid UK mobile number. Example of correct input format: 07925717068'}
              onChange={e => changeNumberValue(e.target.value)}
              normalize={NormalizeInputOnlyNumbers}
              customStyleSm
            />
          </div>
        </div>

        {/* county */}
        <div className={s.fieldRow}>
          <div className={s.labelCol}>
            <label htmlFor='county'>County</label>
          </div>
          <div className={s.fieldCol}>
            <Field
              component={InputFieldGroup}
              name='county'
              id='county'
              disabled={confirm}
              placeholder='West Midlands'
              type='text'
              normalize={NormalizeInput}
              customStyleSm
            />
          </div>
        </div>

        {/* city */}
        <div className={s.fieldRow}>
          <div className={s.labelCol}>
            <label htmlFor='city'>City</label>
          </div>
          <div className={s.fieldCol}>
            <Field
              component={InputFieldGroup}
              name='city'
              id='city'
              disabled={confirm}
              placeholder='Birmingham'
              type='text'
              normalize={NormalizeInput}
              customStyleSm
            />
          </div>
        </div>


        {/* post code */}
        <div className={s.fieldRow}>
          <div className={s.labelCol}>
            <label htmlFor='post_code'>Postcode</label>
          </div>
          <div className={s.fieldCol}>
            <Field
              component={InputFieldGroup}
              name='post_code'
              id='post_code'
              disabled={confirm}
              placeholder='B4 7DA'
              type='text'
              normalize={NormalizeInputWithNumbersandSpaces}
              customStyleSm
            />
          </div>
        </div>


        {/* Street */}
        <div className={s.fieldRow}>
          <div className={s.labelCol}>
            <label htmlFor='street'>Street</label>
          </div>
          <div className={s.fieldCol}>
            <Field
              component={InputFieldGroup}
              name='street'
              id='street'
              disabled={confirm}
              placeholder='Aston St'
              type='text'
              normalize={NormalizeInputWithNumbersandSpaces}
              customStyleSm
            />
          </div>
        </div>

        {/* house number */}
        <div className={s.fieldRow}>
          <div className={s.labelCol}>
            <label htmlFor='house_number'>House number</label>
          </div>
          <div className={s.fieldCol}>
            <Field
              component={InputFieldGroup}
              name='house_number'
              id='house_number'
              disabled={confirm}
              placeholder='83'
              type='text'
              normalize={NormalizeInputWithNumbers}
              customStyleSm
            />
          </div>
        </div>

        {/* residential status */}
        <div className={s.fieldRow}>
          <div className={s.labelCol}>
            <label htmlFor='residental_status'>Residental status</label>
          </div>
          <div className={s.fieldCol}>
            <Field
              component={SelectFieldGroup}
              name='residental_status'
              id='residental_status'
              options={FNLPoptions.residental_status}
              disabled={confirm}
              valueKey='value'
              placeholder='Residential Status'
              customStyleSm
              searchable={false}
            />
          </div>
        </div>
      </div>

    </CheckoutContainer>

    <PromoCode promoValue={formData && formData.values && formData.values.promoCode} />

    <CheckoutContainer
      title='Mobile Phone Authentication Required - '
      icon={phoneIcon}
      passengerIndex={0}
    >
      <div className={s.descContainer}>
        <span className={s.bookerInfo}>
          Confirm your identity by entering the 4-digit code sent to your provided phone number.
        </span>
      </div>
      <div className={s.formTable}>

        {/* mobile number */}
        <div className={s.fieldRow}>
          <div className={s.labelCol}>
            <label htmlFor='phone'>UK Mobile number</label>
          </div>
          <div className={s.fieldCol}>

            <Field
              component={PhoneFieldGroup}
              name='phone'
              id='phone'
              disabled={confirm}
              placeholder='0XXXXXXXXXX'
              customStyleSm
              customError={phoneError && 'The mobile phone you have provided does not appear to be a valid UK mobile number. Example of correct input format: 07925717068'}
              normalize={NormalizeInputOnlyNumbers}
              button={
                <button
                  onClick={checkAndConfirm}
                  disabled={confirm}
                  type='submit'
                >
                  Send SMS
                </button>
              }
            />
          </div>
        </div>

        {/* verification code */}
        <div className={s.fieldRow}>
          <div className={s.labelCol}>
            <label htmlFor='verification_code'>Verification Code</label>
          </div>
          <div className={s.fieldCol}>
            <Field
              component={InputFieldGroup}
              name='verification_code'
              id='verification_code'
              placeholder='XXXX'
              disabled={
                checkingCode
                || smsCodeValid
                || phoneError
                || emailError
                || (!confirm && !showPaymentButton)
              }
              type='text'
              showError
              customError={codaValidationError && !checkingCode && 'Error. Incorrect code'}
              customStyleSm
              button={
                <button
                  onClick={handleSubmit}
                  disabled={smsCodeValid || (!confirm && !showPaymentButton)}
                >
                  {checkingCode ? 'Processing' : 'Verify'}
                </button>
              }
            />
          </div>
        </div>

        {/* verified message */}
        {smsCodeValid && !phoneError && !emailError &&
        <div className={s.fieldRow}>
          <div className={s.labelCol}/>
          <div className={s.fieldCol}>
              <span className={s.verifiedMessage}>
                <span>Code verified</span>
                <InlineSVG src={checkemarkIcon} className={s.icon}/>
              </span>
          </div>
        </div>
        }
      </div>

      <div className={s.infoText}>
        Code will be sent by our partner FlyNowPayLater.
      </div>

    </CheckoutContainer>
  </div>
)

export default Checkout
