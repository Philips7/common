import React, { Component } from 'react'
import Field from 'redux-form/lib/Field'
import phoneIcon from 'static/images/checkout/phone-icon.svg'
import { InputFieldGroup } from 'components/FieldGroup'
import CheckoutContainer from '../../CheckoutContainer/CheckoutContainer'
import s from './Checkout.scss'

class PromoCode extends Component {
  state = {
    verified: false,
    checked: false,
  }

  checkCode = () => this.setState({
    verified: this.props.promoValue === 'FLYMBLE40',
    checked: true,
  })

  render() {
    const { verified, checked } = this.state
    return (
      <CheckoutContainer
        title='Do you have a promo code?'
        icon={phoneIcon}
        fromPromo
      >
        <div className={s.formTable}>
          <div className={s.fieldRow}>
            <Field
              component={InputFieldGroup}
              name='promoCode'
              id='promoCode'
              placeholder='Enter code or leave blank'
              type='text'
              showError
              customError={checked && !verified && 'Code is not valid.'}
              customStyleSm
              greyButton
              normalize={v => v && v.toUpperCase()}
              button={
                <button
                  onClick={this.checkCode}
                  disabled={verified}
                  type='button'
                >{verified ? 'Verified' : 'Apply'}</button>
              }
            />
          </div>
          {verified && checked &&
          <span className={s.infoSuccessText}>Code accepted! Please expect a £40 cash-back to your bank account within one week from your transaction.</span>}
        </div>
      </CheckoutContainer>
    )
  }
}

export default PromoCode
