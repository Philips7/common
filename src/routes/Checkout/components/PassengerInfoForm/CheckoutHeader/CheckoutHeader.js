import React from 'react'
import { pure } from 'recompose'
import cn from 'classnames'
import s from './CheckoutHeader.scss'

const CheckoutHeader = pure(({ title, buttonOnClick, buttonText, className }) =>
  <div className={cn(s.checkoutHeader, className)}>
    <h3 className={s.title}>{title}</h3>
    <div className={s.divider} />
    {
      buttonText && buttonOnClick && (
        <button className={s.editButton} type='button' onClick={buttonOnClick}>{buttonText}</button>
      )
    }
  </div>
)

export default CheckoutHeader
