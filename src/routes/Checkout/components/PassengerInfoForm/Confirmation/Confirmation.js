import React from 'react'
import { pure } from 'recompose'
import Field from 'redux-form/lib/Field'
import { Translate } from 'react-redux-i18n'
import s from './Confirmation.scss'
import Tooltip from 'react-bootstrap/lib/Tooltip'
import {
  SelectFieldGroup,
  InputFieldGroup,
} from 'components/FieldGroup'
import { compose, withState } from 'recompose'
import CustomCheckbox from 'components/CustomCheckbox/CustomCheckbox'
import FNLPoptions from '../../../../../utils/FNLPoptions'
import CheckoutContainer from './../../CheckoutContainer/CheckoutContainer'
import WarningMessage from './../../WarningMessage/WarningMessage'
import PassengerConfirmation from './../PassengerConfirmation/PassengerConfirmation'
import RadioButton from 'components/RadioButton/RadioButton'

import checkedIcon from 'static/images/checkout/checked-icon.svg'
import passengerIcon from 'static/images/checkout/passenger-icon.svg'
import cardIcon from 'static/images/checkout/credit-card-icon.svg'
import FNLPlogo from 'static/images/checkout/FNLP.png'

const composedHOC = compose(
  withState('isEnter', 'changeIsEnter', false)
)


const Confirmation = composedHOC(({
  confirm,
  formData,
  baggage_info,
  baggageNumber,
  resetPaymentButton,
  passengersNumber,
  isEnter,
  changeIsEnter,
  showFNPLTermsModal,
  }) => <div className={s.conformationForm}>
    <CheckoutContainer title='Passenger information' icon={passengerIcon}>
      <WarningMessage
        title='Double check your personal information'
        description='If the information is incorrect, it could be possible that
        you cannot join the flight. Often you will have to buy a new
                  (more expensive) ticket or pay extra for a flight change.'
      />
      <PassengerConfirmation
        formData={formData}
        resetPaymentButton={resetPaymentButton}
        cabinBaggage={baggage_info && baggage_info.cabin}
        passengersNumber={passengersNumber}
        additionalBaggages={baggage_info && baggage_info.additional}
        baggageNumber={baggageNumber}
      />
    </CheckoutContainer>

    <CheckoutContainer title='Payment' icon={cardIcon}>

      <h3 className={s.title}>
        Confirm your status and salary to estimate eligibility for credit.
    </h3>

      <div className={s.listTitle}>
        <span>Pay with </span>
        <a href="" className={s.link}>
          <span>Fly Now Pay Later</span>
          <img src={FNLPlogo} alt="FNLP logo" className={s.logo} />
        </a>
      </div>

      <ul className={s.featuresList}>
        <li className={s.listItem}>
          Checking your eligibility for financing will not affect your credit score.
        </li>
        <li className={s.listItem}>
          Please assure your information is as accurate as possible. Our soft check is based on accuracy and truth, not on salary height.
        </li>
      </ul>

      <div className={s.formTable}>
        <div className={s.fieldRow}>
          <div className={s.labelCol}>Employment status</div>
          <div className={s.fieldCol}>
            <Field
              component={SelectFieldGroup}
              name='employment_status'
              id='employment_status'
              disabled={!confirm}
              valueKey='value'
              options={FNLPoptions.employment_status}
              placeholder='Employment status'
              customStyleSm
            />
          </div>
        </div>
        <div className={s.fieldRow}>
          <div className={s.labelCol}>Annual Salary</div>
          <div
            className={s.fieldCol}
            onMouseEnter={() => changeIsEnter(isEnter => isEnter = true)}
            onMouseLeave={() => changeIsEnter(isEnter => isEnter = false)}
          >
            <Field
              component={InputFieldGroup}
              name='annual_salary'
              disabled={!confirm}
              placeholder='Annual salary in GBP'
              type='text'
              normalize={value => value && value.replace(/[^0-9.]/g, '')}
              customStyleSm
            />
          </div>
        </div>
        {isEnter
          && <div className={s.tooltipWraper}>
            <Tooltip className='in' placement='bottom' id='tooltip-top'>
              The minimum annual salary is £6000
          </Tooltip>
          </div>}
      </div>

      <h4 className={s.subtitle}>
        {`Have you lived in ${formData.values.city}, United Kingdom for more than a year?`}
      </h4>

      <div className={s.checkboxWrap}>
        <Field
          component={RadioButton}
          name='moreThenYear'
          withError
          disabled={!confirm}
          options={FNLPoptions.permanent_residence}
          checked={(formData && formData.values.moreThenYear) || 1}
        />
      </div>

      <WarningMessage
        title='Important!'
        description='After confirmation you will access Fly Now Pay Later’s
        payment environment to securely complete your payment.'
        className={s.warning}
      />

  </CheckoutContainer>

    <CheckoutContainer title='Confirmation' icon={checkedIcon}>
      <Field
        component={CustomCheckbox}
        label={
          <span>
            <Translate value='form.iAccept' />&nbsp;
      	    <a href='/terms-conditions' target='_blank' className={s.termsLink}>Flymble</a>&nbsp;
             <Translate value='form.and' />&nbsp;
            <a onClick={showFNPLTermsModal} className={s.termsLink}>
              Fly Now Pay Later
            </a>&nbsp;
            <Translate value='form.termsAndConditions' />
          </span>
        }
        name='termsFlymble'
        id='accepttermsFlymble'
        showError
        checkboxType='block' />
    </CheckoutContainer>
  </div>
)

export default pure(Confirmation)
