import React, {Component} from 'react'
import R from 'ramda'
import {fnpl_integration_key} from 'config.json'
import {serializeDateAccordingToBackend} from 'utils/helpers'
import {serializeTicket} from '../../../../redux/modules/Trips'
import PreloaderOverlay from 'components/PreloaderOverlay'
import { I18n } from 'react-redux-i18n'

class FNPLBtn extends Component {
  constructor(props) {
    super(props)
    this.state = {
      scriptLoaded: false
    }
  }

  handleScriptCreate = () => {
    this.setState({scriptLoaded: false})
  }

  handleScriptError = () => {
    this.setState({scriptError: true})
  }

  handleScriptLoad = () => {
    this.setState({scriptLoaded: true})
  }

  HandleIntegrationError = (errorIntegrating) => {
    if (errorIntegrating) {
      console.warn('Itinerary Error:', errorIntegrating)
    } else {
      console.log('Application integrated, customer can now see our application button integrated.')
    }
  }

  componentDidMount() {
    if (__CLIENT__) {
      window['callBackFNPL'] = this.callBackFNPL
      const script = document.createElement('script')
      script.src = 'https://merchants.flynowpaylater.com/app.service/' + fnpl_integration_key
      script.async = 1

      script.onload = () => {
        this.handleScriptLoad()
        document.body.removeChild(script)
      }

      script.onerror = () => {
        this.handleScriptError()
      }
      document.body.appendChild(script)
    }
  }

  componentWillReceiveProps(nextProps) {
    const {ticket, formData, resetPaymentButton} = nextProps
    if (ticket.total_price !== this.props.ticket.total_price ||
      ticket.booking_token !== this.props.ticket.booking_token) {
      const serializedTicket = serializeTicket(ticket, true)
      this.Integration && this.Integration.setDepartureDate(serializedTicket.outbound_date)
        .setReturnDate(serializedTicket.return_date)
        .setLoanAmount(ticket.total_price)
        .setMerchantReference(ticket.booking_token)
        .done(this.HandleIntegrationError)
    }

    if (formData.values.passengers[0].title !== this.props.formData.values.passengers[0].title
      || formData.values.passengers[0].first_name !== this.props.formData.values.passengers[0].first_name
      || formData.values.passengers[0].last_name !== this.props.formData.values.passengers[0].last_name
      || formData.values.passengers[0].birth_date !== this.props.formData.values.passengers[0].birth_date
      || formData.values.passengers[0].email !== this.props.formData.values.passengers[0].email
      || formData.values.phone !== this.props.formData.values.phone) {
      resetPaymentButton()
    }
  }

  phoneNumberOptimization = (number) => {
    let correctNumber = number
    if (number.length > 11) {
      const difference = number.length - 11
      correctNumber = number.slice(difference, number.length)
    }
    if (correctNumber.charAt(0) !== '0') {
      correctNumber = `0${correctNumber}`
    }
    return correctNumber
  }

  callBackFNPL = (Integration, newValues) => {
    const {ticket, user, createFNPLPaymentSend, formData} = this.props
    const serializedTicket = serializeTicket(ticket, true)
    const values = newValues || formData.values
    this.Integration = Integration

    console.log('callBackFNPL')

    Integration
      .setTitle(values.passengers[0].title)
      .setFirstName(values.passengers[0].first_name)
      .setLastName(values.passengers[0].last_name)
      .setDateOfBirth(serializeDateAccordingToBackend(values.passengers[0].birth_date))
      .setEmail(values.passengers[0].email)
      .setMobilePhone(this.phoneNumberOptimization(values.phone))
      .setPassengerCount(1)
      .setDepartureDate(serializedTicket.outbound_date)
      .setReturnDate(serializedTicket.return_date)
      // .setAirlineName('test')
      // .setCountryCode('test')
      .setLoanAmount(ticket.total_price)
      .setMerchantReference(ticket.booking_token)
      .setApplicationDeclined(() => {
        console.log('Sorry, the application for credit was declined.')
      })
      .setCheckoutCallback((checkoutReference) => {
        createFNPLPaymentSend(checkoutReference.api.reference)
      }
      )
      .done(this.HandleIntegrationError)
  }

  render() {

    const shortTime = 500
    const labels = [
      { text: `${I18n.t('Preloader.LoadingScripts')}`, time: shortTime },
      { text: `${I18n.t('Preloader.LoadingScripts')}.`, time: shortTime },
      { text: `${I18n.t('Preloader.LoadingScripts')}..`, time: shortTime },
      { text: `${I18n.t('Preloader.LoadingScripts')}...`, time: shortTime },
      { text: `${I18n.t('Preloader.LoadingScripts')}`, time: shortTime },
      { text: `${I18n.t('Preloader.LoadingScripts')}.`, time: shortTime },
      { text: `${I18n.t('Preloader.LoadingScripts')}..`, time: shortTime },
      { text: `${I18n.t('Preloader.LoadingScripts')}...`, time: shortTime },
    ]

    return <div className="text-center">
      {this.state.scriptLoaded
        ? <div
          data-integration={fnpl_integration_key}
          data-integration-call={'callBackFNPL'}
          //onClick={this.props.stopChecking}
          style={{
            margin: '0 -40px',
            display: 'none'
          }}/>
        : <PreloaderOverlay labels={labels} />
      }
    </div>
  }
}

export default FNPLBtn
