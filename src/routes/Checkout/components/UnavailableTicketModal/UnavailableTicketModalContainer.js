import { connect } from 'react-redux'
import { searchTripsModalPagination } from '../../../../redux/modules/Trips'
import UnavailableTicketModal from './UnavailableTicketModal'
import { selectTrip, goToSearch } from '../../modules/Checkout'

const mapActionCreators = {
  searchTripsModalPagination,
  selectTrip: trip => selectTrip(trip, 'unavailableTicketModal'),
  goToSearch
}

const mapStateToProps = ({ Trips: {
  searchResults,
  searchTripsLoading,
  nextPage,
  paybackSelected,
  searchCriteria: { number_of_passengers },
},
  Ticket: { ticket, passengersNumber } }) =>
  ({
    searchResults,
    searchTripsLoading,
    nextPage,
    paybackSelected,
    passengersNumber: passengersNumber || number_of_passengers || 1,
    creditPoint: (ticket && ticket.creditPoint) || paybackSelected,
    interestRate: ticket && ticket.interestRate
  })

export default connect(mapStateToProps, mapActionCreators)(UnavailableTicketModal)
