import React from 'react'
import Modal from 'react-bootstrap/lib/Modal'
import { connectModal } from 'redux-modal'
import PreloaderOverlay from 'components/PreloaderOverlay'
import TripDescription from 'components/TripDescription'
import { pure } from 'recompose'
import NoTicketsPriceImg from 'static/images/plain-big-image.svg'
import ArrowRight from 'static/images/arrow-pointing-to-right.svg'
import cn from 'classnames'
import { Link } from 'react-router'
import InlineSVG from 'svg-inline-react'
import { I18n } from 'react-redux-i18n'
import { sortByMonth } from 'utils/filters'
import s from './UnavailableTicketModal.scss'

const UnavailableTicketModal = pure(({
    show,
  nextPage,
  goToSearch,
  selectTrip,
  creditPoint,
  paybackSelected,
  searchResults,
  passengersNumber,
  searchTripsLoading,
  searchTripsModalPagination,
  }) => <Modal
    show={show}
    className={cn(s.modal, s.checkoutModal)}
    bsSize='lg'
  >
    <Modal.Body className={s.modalBody}>

      <div className={s.contentWrap}>
        <h1 className={s.title}>{I18n.t('Checkout.noMoreTicketsLeft')}</h1>
        <h3 className={s.subTitle}>{I18n.t('Checkout.checkAlternative')}</h3>
      </div>

      <div className={s.imgWrap}>
        <InlineSVG src={NoTicketsPriceImg} />
      </div>

      <span className={s.button} onClick={goToSearch}>{I18n.t('Checkout.backToSearch')}</span>

      <div className={s.otherTicketsList}>
        <h4 className={s.listTitle}>
          <span>{searchResults.length}</span>
          {I18n.t('Checkout.alternativeFlightsFound')}
        </h4>
        {(searchResults.length === 0)
          ? <div className={s.noTickets}>
            <span>{I18n.t('Checkout.noAlternativeFlights')}</span>
            <Link to='/'>
              <span>
                {I18n.t('Checkout.searchAnotherDestination')}
              </span>
              <InlineSVG src={ArrowRight} />
            </Link>
          </div>
          :
          <div className={s.ticketsWrap}>
            {searchResults.map((trip, index) =>
              <div className={s.tripWrap} key={index}>
                <TripDescription
                  trip={trip}
                  apr={sortByMonth.find(({ value }) => value === creditPoint).apr}
                  showMonth={false}
                  creditPoint={creditPoint}
                  selectTrip={selectTrip}
                  paybackSelected={paybackSelected}
                  interestRate={sortByMonth.find(({ value }) => value === creditPoint).rate}
                  passengersNumber={passengersNumber}
                  compact
                />
              </div>
            )}
            {nextPage
              && <button
                onClick={searchTripsModalPagination}
                className={s.showMoreBtn}
              >
                {I18n.t('Checkout.showMore')}
              </button>}
          </div>
        }
      </div>

      {searchTripsLoading && <PreloaderOverlay />}
    </Modal.Body>
  </Modal>
)

export default
  connectModal({ name: 'unavailableTicketModal' })(UnavailableTicketModal)
