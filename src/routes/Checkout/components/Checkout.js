import React, { Component } from 'react'
// import InlineSVG from 'svg-inline-react'
import ApprpovalIcon from 'static/images/apprpoval_1.svg'
import { I18n } from 'react-redux-i18n'
import Tabs from 'components/Tabs/Tabs'
import PreloaderOverlay, { SpinnerLoader } from 'components/PreloaderOverlay'
import Guide from 'components/Guide'
import { scrollTo } from 'utils/helpers'
import { sortByMonth } from 'utils/filters'
import { isTabletScreen, isSmallScreen } from 'utils/responsive'
import PaybackUnavailableModal from 'components/modals/PaybackUnavailableModal/PaybackUnavailableModalContainer'
import CheckoutConfirmComponent from './CheckoutConfirmComponent/CheckoutConfirmComponent'
import SelectBankModal from './SelectBankModal/SelectBankModalContainer'
import ChangePriceModal from './ChangePriceModal/ChangePriceModalContainer'
import UnavailableTicketModal from './UnavailableTicketModal/UnavailableTicketModalContainer'
import ExtraBaggageModal from './ExtraBaggageModal'
import FNPLTermsModal from './FNPLTermsModal'

// import AfterPayIcon from 'static/images/afterpay.svg'

const shortTime = 500

const labels = [
  { text: '', time: 2000 },
  { text: I18n.t('Preloader.SearchTickets'), time: shortTime },
  { text: I18n.t('Preloader.SearchTickets'), time: shortTime },
  { text: `${I18n.t('Preloader.SearchTickets')}`, time: shortTime },
  { text: `${I18n.t('Preloader.SearchTickets')}.`, time: shortTime },
  { text: `${I18n.t('Preloader.SearchTickets')}..`, time: shortTime },
  { text: `${I18n.t('Preloader.SearchTickets')}...`, time: shortTime },
  { text: `${I18n.t('Preloader.SearchTickets')}`, time: shortTime },
  { text: `${I18n.t('Preloader.SearchTickets')}.`, time: shortTime },
  { text: `${I18n.t('Preloader.SearchTickets')}..`, time: shortTime },
  { text: `${I18n.t('Preloader.SearchTickets')}...`, time: shortTime },
]

const preparingCreditLabels = [
  { text: 'A credit check is being performed', time: 5000 }
]

export default class Checkout extends Component {

  constructor(props) {
    super(props)
    this.state = { tabNumber: 1 }
  }

  componentDidMount() {
    const { checkFlightConnect, createFNPLPaymentConnect, getGuideStatus } = this.props
    checkFlightConnect()
    createFNPLPaymentConnect()
    getGuideStatus()
  }

  componentWillUnmount() {
    const {
      checkFlightDisconnect,
      unsetEmailRegistered,
      createFNPLPaymentDisconnect,
      resetPaymentButton,
      stopChecking,
      cleanTicket,
    } = this.props
    checkFlightDisconnect()
    createFNPLPaymentDisconnect()
    unsetEmailRegistered()
    resetPaymentButton()
    cleanTicket()
    //stopChecking()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.guideStatus === 1 && !this.scrolled && (document.getElementById('passengerInfo') || !isTabletScreen) && !this.once) {
      this.props.setGuideStatus(2)
      this.once = true
      if (isTabletScreen && !isSmallScreen) {
        scrollTo(document.getElementById('passengerInfo').offsetTop, { duration: 1 })
        setTimeout(() => { this.scrolled = true }, 10)
      }
    }
  }

  resetOnce = () => {
    this.once = false
  }

  editInfo = () => this.props.changeTabNumber(1)
  endAction = () => this.props.changeTabNumber(2)

  render() {
    const {
      ticket, user, userGetLoading, needShowLoader, handleSubmitUserInfo, stopChecking, isCreatingInvoice,
      isRedirecting, tabNumber, paybackUnavailable, signUpNewsletterAttempt, checkFlightLoading, onCheckFlightOpen,
      preparingCredit, showPaymentButton, isCreatingFNPLInvoice, createFNPLPaymentSend, resetPaymentButton,
      smsCodeValid, changeBaggageNumber, changePassengersNumber, passengersNumber, removePassenger,
      changeNumberValue, PassengerInfoForm, checkingCode, codaValidationError, phoneError, emailError, paybackSelected,
      showExtraBaggageModal, baggageNumber, checkFlightConnect, showErrorNotification, showFNPLTermsModal, checkoutEmails
    } = this.props
    return (
      <div>
        {!(isCreatingInvoice
          || userGetLoading
          || isRedirecting
          || needShowLoader
          || isCreatingFNPLInvoice)
          && <Guide run={this.once} resetOnce={this.resetOnce} checkout={this.scrolled || !isTabletScreen} />}
        <section>
          <Tabs tabNumber={tabNumber} />
        </section>
        {(isCreatingInvoice
          || userGetLoading
          || isRedirecting
          || needShowLoader
          || isCreatingFNPLInvoice) &&
          <PreloaderOverlay labels={labels} />
        }
        <SpinnerLoader
          labels={preparingCreditLabels}
          show={preparingCredit}
          beforeCloseIcon={ApprpovalIcon}
          endAction={this.endAction}
        />
        {ticket &&
          <CheckoutConfirmComponent
            trip={ticket}
            user={user}
            showErrorNotification={showErrorNotification}
            changeNumberValue={changeNumberValue}
            passengersNumber={passengersNumber}
            removePassenger={removePassenger}
            changeBaggageNumber={changeBaggageNumber}
            changePassengersNumber={changePassengersNumber}
            onSubmit={handleSubmitUserInfo}
            checkFlightLoading={checkFlightLoading}
            showPaymentButton={showPaymentButton}
            editInfo={this.editInfo}
            confirm={tabNumber === 2}
            showExtraBaggageModal={showExtraBaggageModal}
            paybackUnavailable={paybackUnavailable}
            createFNPLPaymentSend={createFNPLPaymentSend}
            stopChecking={stopChecking}
            resetPaymentButton={resetPaymentButton}
            smsCodeValid={smsCodeValid}
            formData={PassengerInfoForm}
            checkingCode={checkingCode}
            paybackSelected={paybackSelected}
            interestRate={paybackSelected
              && sortByMonth.find(el => el.value === paybackSelected).rate
            }
            codaValidationError={codaValidationError}
            phoneError={phoneError}
            emailError={emailError}
            onCheckFlightOpen={checkFlightConnect}
            baggageNumber={baggageNumber}
            showFNPLTermsModal={showFNPLTermsModal}
            checkoutEmails={checkoutEmails}
          />}
        <SelectBankModal />
        <ChangePriceModal />
        <UnavailableTicketModal />
        <PaybackUnavailableModal onSubmit={signUpNewsletterAttempt} />
        <ExtraBaggageModal />
        <FNPLTermsModal />
      </div>
    )
  }
}
