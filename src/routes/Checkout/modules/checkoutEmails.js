import { take, call } from 'redux-saga/effects'
import request from 'sagas/api'
import { CHECKOUT_EMAILS_ATTEMPT } from './Checkout'

function* watcher() {
  while (true) {
    const { value } = yield take(CHECKOUT_EMAILS_ATTEMPT)
    const body = {
      method: 'POST',
      body: {
        email: value
      },
    }
    yield call(request, 'newsletter/checkout-emails/', body)
  }
}

export default () => ({ watcher })
