import {take, call, put, fork, cancel} from 'redux-saga/effects'
import {eventChannel, END} from 'redux-saga'
import {ws} from 'services/api'

import {
  CREATE_FNPL_PAYMENT_CONNECT,
  CREATE_FNPL_PAYMENT_DISCONNECT,
  CREATE_FNPL_PAYMENT_SEND,
  createFNPLPaymentReconnect,
  createFNPLPaymentEmit
} from './Checkout'

export default () => {
  function* connect(dispatch) {
    const socket = ws('payments/fnpl/credit/create ')

    socket.onclose = () => dispatch(createFNPLPaymentReconnect())

    return socket
  }

  const subscribe = socket => {
    return eventChannel(emit => {
      socket.onmessage = response => {
        emit(createFNPLPaymentEmit(response))
      }
      return () => {
        emit(END)
      }
    })
  }

  function* emit(socket) {
    const channel = yield call(subscribe, socket)
    while (true) {
      const action = yield take(channel)
      yield put(action)
    }
  }

  function* send(socket) {
    // Workaround http://stackoverflow.com/questions/23369368/how-to-get-the-current-status-of-a-javascript-websocket-connection
    while (true) {
      const { values } = yield take(CREATE_FNPL_PAYMENT_SEND)
      if (socket.readyState === socket.OPEN) {
        socket.send(JSON.stringify(values))
      }
    }
  }

  function* handleIO(socket) {
    yield fork(emit, socket)
    yield fork(send, socket)
  }

  function* watcher() {
    while (true) {
      const {dispatch} = yield take(CREATE_FNPL_PAYMENT_CONNECT)

      const socket = yield call(connect, dispatch)
      const io = yield fork(handleIO, socket)

      yield take(CREATE_FNPL_PAYMENT_DISCONNECT)
      socket.close()
      yield cancel(io)
    }
  }

  return {
    watcher
  }
}
