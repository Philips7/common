import {take, call, put, fork, cancel} from 'redux-saga/effects'
import {eventChannel, END} from 'redux-saga'
import {ws} from 'services/api'

import {
  CREATE_INVOICE_CONNECT,
  CREATE_INVOICE_DISCONNECT,
  CREATE_INVOICE_SEND,
  createInvoiceEmit,
  createInvoiceReconnect
} from './Checkout'

export default () => {
  function* connect(dispatch) {
    const socket = ws('payments/credit/create')

    socket.onclose = () => dispatch(createInvoiceReconnect())

    return socket
  }

  const subscribe = socket => {
    return eventChannel(emit => {
      socket.onmessage = response => {
        emit(createInvoiceEmit(response))
      }
      return () => {
        emit(END)
      }
    })
  }

  function* emit(socket) {
    const channel = yield call(subscribe, socket)
    while (true) {
      const action = yield take(channel)
      yield put(action)
    }
  }

  function* send(socket) {
    // Workaround http://stackoverflow.com/questions/23369368/how-to-get-the-current-status-of-a-javascript-websocket-connection
    while (true) {
      const {values} = yield take(CREATE_INVOICE_SEND)
      if (socket.readyState === socket.OPEN) {
        socket.send(JSON.stringify(values))
      }
    }
  }

  function* handleIO(socket) {
    yield fork(emit, socket)
    yield fork(send, socket)
  }

  function* watcher() {
    while (true) {
      const {dispatch} = yield take(CREATE_INVOICE_CONNECT)

      const socket = yield call(connect, dispatch)
      const io = yield fork(handleIO, socket)

      yield take(CREATE_INVOICE_DISCONNECT)
      socket.close()
      yield cancel(io)
    }
  }

  return {
    watcher
  }
}
