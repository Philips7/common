import Raven from 'raven-js'
import { replace } from 'react-router-redux'
import { hide, show } from 'redux-modal'
import { I18n } from 'react-redux-i18n'
import { push } from 'react-router-redux/lib/actions'
import { change, stopSubmit, formValueSelector } from 'redux-form'
import { getCookie, setCookie } from 'redux-cookie'
import validator from 'validator'
import config from 'config.json'
import path from 'ramda/src/path'
import createReducer from 'utils/createReducer'
import { errorNotification } from 'utils/notifications'
import { scrollTo, serializeDateAccordingToBackend, scrollToFirstError, serializeUser } from 'utils/helpers'
import { changeBaggageNumber, checkTrip, removeTicketFromCache, sendLastCheck } from '../../../redux/modules/Ticket'
import { createPushQuery, serializeTicket } from '../../../redux/modules/Trips'
import { SIGN_UP_ATTEMPT, SIGN_UP_FAILURE, userGetSuccess, userUpdateSuccess } from '../../../redux/modules/User'

// ------------------------------------
// Constants
// ------------------------------------
export const CREATE_INVOICE_CONNECT = 'Checkout.CREATE_INVOICE_CONNECT'
export const CREATE_INVOICE_DISCONNECT = 'Checkout.CREATE_INVOICE_DISCONNECT'
export const CREATE_INVOICE_SEND = 'Checkout.CREATE_INVOICE_SEND'
export const CREATE_INVOICE_EMIT = 'Checkout.CREATE_INVOICE_EMIT'
export const CREATE_INVOICE_EMIT_ERROR = 'Checkout.CREATE_INVOICE_EMIT_ERROR'

export const CREATE_FIRST_PAYMENT_FAILURE = 'Checkout.CREATE_FIRST_PAYMENT_FAILURE'

export const CHANGE_TAB_NUMBER = 'Checkout.CHANGE_TAB_NUMBER'
export const SET_EMAIL_REGISTERED = 'Checkout.SET_EMAIL_REGISTERED'
export const UNSET_EMAIL_REGISTERED = 'Checkout.UNSET_EMAIL_REGISTERED'

export const SET_BANKS = 'Checkout.SET_BANKS'
export const SET_PAYBACK_UNAVAILABLE = 'Checkout.SET_PAYBACK_UNAVAILABLE'
export const SET_PAYBACK_AVAILABLE = 'Checkout.SET_PAYBACK_AVAILABLE'
export const VALIDATE_SMS_CODE = 'Checkout.VALIDATE_SMS_CODE'
export const CODE_VALIDATION = 'Checkout.CODE_VALIDATION'

export const PREPARING_CREDIT_ATTEMPT = 'Checkout.PREPARING_CREDIT_ATTEMPT'
export const PREPARING_CREDIT_STOP = 'Checkout.PREPARING_CREDIT_STOP'
export const SHOW_PAYMENT_BUTTON = 'Checkout.SHOW_PAYMENT_BUTTON'
export const HIDE_PAYMENT_BUTTON = 'Checkout.HIDE_PAYMENT_BUTTON'
export const SET_USER_EMAIL = 'Checkout.SET_USER_EMAIL'

export const CREATE_FNPL_PAYMENT_CONNECT = 'Checkout.CREATE_FNPL_PAYMENT_CONNECT'
export const CREATE_FNPL_PAYMENT_DISCONNECT = 'Checkout.CREATE_FNPL_PAYMENT_DISCONNECT'
export const CREATE_FNPL_PAYMENT_SEND = 'Checkout.CREATE_FNPL_PAYMENT_SEND'
export const CREATE_FNPL_PAYMENT_EMIT = 'Checkout.CREATE_FNPL_PAYMENT_EMIT'
export const CREATE_FNPL_PAYMENT_EMIT_ERROR = 'Checkout.CREATE_FNPL_PAYMENT_EMIT_ERROR'

export const CHECKOUT_EMAILS_ATTEMPT = 'Checkout.CHECKOUT_EMAILS_ATTEMPT'

// ------------------------------------
// Actions
// ------------------------------------

export const preparingCreditAttempt = () => dispatch => {
  dispatch({ type: PREPARING_CREDIT_ATTEMPT })
  setTimeout(() => {
    dispatch(stopPreparingCredit())
  }, 4000)
}

export const stopPreparingCredit = () => dispatch => {
  dispatch({ type: PREPARING_CREDIT_STOP })
  dispatch(show('successApprovedModal'))
}

export const checkUserEmail = () => (dispatch, getState) => {
  const { form: { PassengerInfoForm: { values } } } = getState()

  dispatch({
    type: SIGN_UP_ATTEMPT,
    values: { ...serializeUser(values) },
    responseSuccess: tempSignUpUser,
    responseFailure: checkoutLoginUser
  })
}

const checkoutUpdateUser = user => dispatch => {
  dispatch(userUpdateSuccess(user))
  dispatch(sendLastCheck())
}

const tempSignUpUser = ({ access_token, ...user }) => dispatch => {
  dispatch(userGetSuccess(user))
  dispatch(sendLastCheck())
}

const checkoutLoginUser = error => (dispatch) => {
  if (error && Object.keys(error).length === 1 && error.email) {
    dispatch({ type: SIGN_UP_FAILURE })
    dispatch({ type: SET_EMAIL_REGISTERED })
    dispatch(show('loginModal'))
  } else {
    dispatch(stopSubmit('PassengerInfoForm', { ...error, email: null }))
    dispatch(changeTabNumber(1))
    dispatch(closeFNLPA())
  }
}

export const unsetEmailRegistered = () => ({
  type: UNSET_EMAIL_REGISTERED
})

const checkoutUpdateUserFailer = error => dispatch => {
  dispatch(stopSubmit('PassengerInfoForm', { ...error, email: error.email && I18n.t('form.unIniqueEmail') }))
  dispatch(changeTabNumber(1))
  dispatch(closeFNLPA())
}

export const createFirstPayment = () => (dispatch, getState) => {
  const {
    // User: {loggedIn},
    form: { PassengerInfoForm: { values } },
    Ticket: { ticket: { total_price, creditPoint }, ticket },
    // Checkout: {paybackUnavailable}
  } = getState()
  const { first_name, last_name, email, phone, birth_date } = values

  Raven.captureMessage('User clicked purchase',
    {
      extra: {
        user: {
          first_name,
          last_name,
          email,
          phone,
          birth_date
        },
        ticket: {
          ...serializeTicket(ticket),
          price: total_price,
          payback_months: creditPoint
        }
      }
    })

  // if (loggedIn) {
  //   dispatch({
  //     type: USER_UPDATE_ATTEMPT,
  //     values: {...serializeUser(values)},
  //     responseSuccess: checkoutUpdateUser,
  //     responseFailure: checkoutUpdateUserFailer
  //   })
  // } else {
  //   dispatch(checkUserEmail())
  // }
}

export const createFirstInvoice = () => dispatch => {
  dispatch(createInvoiceDisconnect())
  dispatch(createInvoiceConnect())
}

export const createFirstPaymentFailure = ({ error_description }) => dispatch => {
  dispatch(errorNotification(error_description))

  dispatch({ type: CREATE_FIRST_PAYMENT_FAILURE })
  Raven.captureMessage('createFirstPaymentFailure', {
    extra: {
      message: error_description,
    },
  })
}

export const createInvoiceConnect = () => dispatch => dispatch({
  type: CREATE_INVOICE_CONNECT,
  dispatch
})

export const createInvoiceDisconnect = () => ({
  type: CREATE_INVOICE_DISCONNECT
})
export const validateSMSCode = valid => ({
  type: VALIDATE_SMS_CODE,
  smsCodeValid: valid,
})

export const createInvoiceSend = data => ({
  type: CREATE_INVOICE_SEND,
  values: { data }
})
export const createInvoiceReconnect = () => (dispatch, getState) => {
  const { websocketConnected } = getState().Checkout
  dispatch(createInvoiceDisconnect())
  websocketConnected && dispatch(createInvoiceConnect())
}

export const createInvoiceEmit = response => dispatch => {
  const message = JSON.parse(response.data)

  if (message.status === 'error') {
    dispatch(errorNotification())
    dispatch({
      type: CREATE_INVOICE_EMIT_ERROR,
      message: message.message
    })
    Raven.captureMessage('createInvoiceEmit', {
      extra: {
        message,
      },
    })
  } else if (message.status === 'success') {
    const data = message.data

    if (data && Array.isArray(data)) {
      dispatch({ type: SET_BANKS, banks: data })
      dispatch(show('selectBankModal'))
      return
    }

    if (data && data.payment) {
      dispatch(removeTicketFromCache())
      if (window) {
        window.location.href = data.payment.payment_url
      }
      dispatch({
        type: CREATE_INVOICE_EMIT,
        isRedirecting: !!window,
        paymentLink: data.payment.payment_url
      })
    } else {
      dispatch({ type: CREATE_INVOICE_EMIT, isRedirecting: false })
    }
  }
}

export const changeTabNumber = tabNumber => ({
  type: CHANGE_TAB_NUMBER,
  tabNumber
})

export const confirmBankModal = ({ issuer_id }) => (dispatch, getState) => {
  const { Ticket: { ticket } } = getState()
  const months = JSON.parse(dispatch(getCookie('payback')))
  const access_token = dispatch(getCookie('token'))

  if (months && access_token) {
    const { site: { domain, protocol } } = config

    dispatch(createInvoiceSend({
      booking_token: ticket.booking_token,
      months: months.value,
      access_token,
      issuer_id,
      url_success: `${protocol}://${domain}/thank-you`,
      url_pending: `${protocol}://${domain}/thank-you`,
      url_failure: `${protocol}://${domain}/thank-you`
    }))
  } else {
    dispatch(createFirstPaymentFailure({ error_description: 'Something go wrong try again' }))
  }
}

export const selectTrip = (booking_token, modal) => (dispatch, getState) => {
  const { Ticket: { passengersNumber, ticket } } = getState()
  const searchQuery = path(['baggage_info', 'additional'], ticket) || ''
  const updatedBaggagesNumber = searchQuery && searchQuery.prices[0] === 0 ? passengersNumber : 0
  const modalType = modal !== 'unavailableTicketModal'
  dispatch(checkTrip(booking_token, true))
  dispatch(changeBaggageNumber(updatedBaggagesNumber, modalType))
  dispatch(hide(modal))
}

const parseCookies = (name) => dispatch => {
  const _cookie = dispatch(getCookie(name))
  return _cookie ? JSON.parse(_cookie) : {}
}

export const goToSearch = () => dispatch => {
  const searchCriteria = dispatch(parseCookies('search_hash'))
  const filters = dispatch(parseCookies('filters'))
  const paybackSelected = dispatch(parseCookies('payback'))
  dispatch(push({
    pathname: '/search',
    query: createPushQuery({ ...searchCriteria, paybackSelected }, filters)
  }))
}

export const stillBookThisFlight = () => (dispatch, getState) => {
  const { lastCheck } = getState().Ticket
  dispatch(hide('changePriceModal'))
  lastCheck && dispatch(createFirstInvoice())
}

export const setPaybackUnavailable = () => dispatch => {
  const months = JSON.stringify({ value: 1 })
  dispatch(setCookie('payback', months))
  dispatch({ type: SET_PAYBACK_UNAVAILABLE })
  dispatch(changeTabNumber(1))
  dispatch(closeFNLPA())
  dispatch(hide('paybackUnavailableModal'))
}

export const setPaybackAvailable = () => ({ type: SET_PAYBACK_AVAILABLE })

export const checkIfPaybackIsAvailable = () => (dispatch, getState) => {
  const payback = __CLIENT__ ? JSON.parse(dispatch(getCookie('payback'))) : getState().Common.payback
  if (payback && payback.value === 1) dispatch(setPaybackUnavailable())
}

export const closeFNLPA = () => {
  const closeApp = document.querySelector('[data-jeek-close-application=message-buttons]')
  if (closeApp) {
    const yesClose = document.querySelector('[data-jeek-popover=question-buttons] > [data-jeek-block-button=primary]')
    yesClose.click()
  }
}

export const handleSubmitUserInfo = () => (dispatch, getState) => {
  const { paybackUnavailable, tabNumber, smsCodeValid } = getState().Checkout
  const {
    Trips: { paybackSelected }, form: {
      PassengerInfoForm: {
        values: {
          verification_code,
          passengers,
          residental_status,
          employment_status,
          annual_salary,
          city,
          house_number,
          street,
          county,
          post_code
        }
      }
    }
  } = getState()

  const FNLPStep2 = () => {
    const startNewApplication = setInterval(() => {
      const submitBtn = document.querySelector('[data-jeek-button-identifier=submit]')
      if (submitBtn) {
        console.log('FNLPStep2')
        const monthCount = document.querySelectorAll('[data-jeek-dynamic-selector=month-selection]>span')
        monthCount.forEach(span => parseInt(span.innerText) === paybackSelected && span.click())
        document.getElementsByTagName('body')[0].style.overflow = 'visible'
        const checkPhoneError = setInterval(() => {
          console.log('checkPhoneError')
          const error = document.querySelector('[data-jeek-popover=message-buttons] > [data-jeek-block-button=primary]')
          const smsCodeWindow = document.querySelector('[data-jeek=smscode1]')
          if (error) {
            const errorMessage = document.querySelector('[data-jeek-popover=message-text]')
            const isEmailError = /[Mm]ail/g.test(errorMessage.textContent)
            if (isEmailError) {
              dispatch({
                type: SHOW_PAYMENT_BUTTON,
                emailError: true
              })
              scrollToFirstError('passengers[0].email')
            } else {
              dispatch({
                type: SHOW_PAYMENT_BUTTON,
                phoneError: true
              })
            }
            error.click()
            closeFNLPA()
            clearInterval(checkPhoneError)
          } else if (smsCodeWindow) {
            clearInterval(checkPhoneError)
          }
        }, 1000)
        submitBtn.click()
        clearInterval(startNewApplication)
      }
    }, 1000)
  }

  const FNLPStep3 = () => {
    const code = verification_code.trim().split('')
    dispatch({
      type: CODE_VALIDATION,
      checkingCode: true,
      codaValidationError: false,
      phoneError: false,
      emailError: false
    })
    const codePass = setInterval(() => {
      console.log('FNLPStep3')
      const smsCodeWindow = document.querySelector('[data-jeek=smscode1]')
      const checkoutPending = document.querySelector('[data-jeek-overlay-item=auth-summary]')
      const closedWindow = document.querySelector('[data-jeek-tfobject-btn=apply]')

      if (smsCodeWindow) {
        smsCodeWindow.value = code[0]
        document.querySelector('[data-jeek=smscode2]').value = code[1]
        document.querySelector('[data-jeek=smscode3]').value = code[2]
        document.querySelector('[data-jeek=smscode4]').value = code[3]
        document.querySelector('[data-jeek-block-button=primary]').click()
        clearInterval(codePass)
        setTimeout(() => {
          checkCode()
        }, 3000)
      } else if (checkoutPending) {
        dispatch({
          type: CODE_VALIDATION,
          checkingCode: false,
          codaValidationError: false
        })
        clearInterval(codePass)
        showFNLPModal()
      } else if (closedWindow) {
        document.querySelector('[data-jeek-tfobject-btn=apply]').click()
      }
    }, 1000)
  }

  const checkCode = () => {
    const selfInformation = setInterval(() => {
      const infoReady = document.querySelector('input[data-jeek=annualsalary]')
      const userExist = document.querySelector('[data-jeek-overlay-item=flex-change-credit-agreement]')
      const userExistAgreementChanged = document.querySelector('[data-jeek=block-footer]>[data-jeek-button-identifier=submit]')
      const authorise = document.querySelector('[data-jeek-overlay-item=auth-ask-to-checkout]')
      const smsCodeError = document.querySelector('[data-jeek-popover=bottom-error-display]')
      const checkoutPending = document.querySelector('[data-jeek-overlay-item=auth-summary]')
      if (smsCodeError) {
        console.log('Code error')
        dispatch({
          type: CODE_VALIDATION,
          checkingCode: false,
          codaValidationError: true
        })
        clearInterval(selfInformation)
      } else if (infoReady) {
        dispatch(validateSMSCode(true))
        console.log('Code Valid')
        dispatch({
          type: CODE_VALIDATION,
          checkingCode: false,
          codaValidationError: false
        })
        clearInterval(selfInformation)
      } else if (userExist || userExistAgreementChanged || authorise || checkoutPending) {
        console.log('userExist')
        dispatch({
          type: CODE_VALIDATION,
          checkingCode: false,
          codaValidationError: false
        })
        clearInterval(selfInformation)
        showFNLPModal()
      }
    }, 1000)
  }

  const goToCheckout = () => {
    dispatch(changeTabNumber(2))
    scrollTo(0, 0)
  }

  const FNLPStep4 = () => {
    console.log('FNLPStep4')
    const isFNLPNotCanceled = document.querySelector('[data-loan-application]')
    if (!isFNLPNotCanceled) {
      dispatch(resetPaymentButton())
    } else {
      const selfInformation = setInterval(() => {
        const infoReady = document.querySelector('input[data-jeek=annualsalary]')
        if (infoReady) {
          document.querySelector('select[data-jeek=title]').value = passengers[0].title
          document.querySelector('select[data-jeek=employmentstatus]').value = employment_status
          document.querySelector('select[data-jeek=residentialstatus]').value = residental_status
          infoReady.value = annual_salary
          document.querySelector('[data-jeek-button-identifier=submit]').click()
          clearInterval(selfInformation)
          FNLPStep5()
        }
      }, 1000)
    }
  }

  const FNLPStep5 = () => {
    console.log('FNLPStep5')
    const addressInformation = setInterval(() => {
      const location = document.querySelector('input[data-jeek=address-autocomplete-1]')
      if (location) {
        location.value = city
        document.querySelector('input[data-jeek=housenumber-1]').value = house_number
        document.querySelector('input[data-jeek=street-1]').value = street
        document.querySelector('input[data-jeek=city-1]').value = city
        document.querySelector('input[data-jeek=county-1]').value = county
        document.querySelector('input[data-jeek=postalcode-1]').value = post_code
        document.querySelector('[data-jeek-button-identifier=submit-applicant-address-1]').click()
        clearInterval(addressInformation)
        FNLPStep6()
      }
    }, 1000)
  }
  const FNLPStep6 = () => {
    console.log('FNLPStep6')
    const congrats = setInterval(() => {
      const moreThenYearBtn = document.querySelector('div[data-jeek-block-button=primary]')
      if (moreThenYearBtn) {
        moreThenYearBtn.click()
        clearInterval(congrats)
        showFNLPModal()
      }
    }, 1000)
  }

  const showFNLPModal = (hideFNLP) => {
    const body = document.getElementsByTagName('body')[0]
    if (hideFNLP) {
      body.classList.add('hideFNLP')
    } else {
      body.classList.remove('hideFNLP')
    }
  }

  if (tabNumber === 2) {
    FNLPStep4()
  } else if (smsCodeValid) {
    goToCheckout()
  } else if (verification_code) {
    FNLPStep3()
  } else {
    showFNLPModal(true)
    dispatch({
      type: SHOW_PAYMENT_BUTTON
    })
    dispatch({
      type: SET_USER_EMAIL,
      email: passengers[0].email
    })
    const applyFNLP = setInterval(() => {
      console.log('applyFNLP')
      const applyBtn = document.querySelector('[data-jeek-tfobject-btn=apply]')
      if (applyBtn) {
        applyBtn.click()
        clearInterval(applyFNLP)
        FNLPStep2()
      }
    }, 1000)
  }
}

// ------------------------------------
// Fly Now Pay Later Integration
// ------------------------------------

export const createFNPLPaymentConnect = () => dispatch => dispatch({
  type: CREATE_FNPL_PAYMENT_CONNECT,
  dispatch
})

export const createFNPLPaymentDisconnect = () => ({
  type: CREATE_FNPL_PAYMENT_DISCONNECT
})

export const createFNPLPaymentSend = reference => (dispatch, getState) => {
  const {
    Ticket: { ticket, baggageNumber },
    form: { PassengerInfoForm: { values: { passengers, contact_phone } } },
    Trips: { paybackSelected },
  } = getState()
  const psngrs = passengers.map(p => ({ ...p, title: p.title === '17' ? 'mr' : 'ms', birth_date: serializeDateAccordingToBackend(p.birth_date) }))
  const parsedPhoneNumber = '+44' + contact_phone.split('').slice(1, 11).join('')
  dispatch({
    type: CREATE_FNPL_PAYMENT_SEND,
    values: {
      data: {
        booking_token: ticket.booking_token,
        number_of_baggages: baggageNumber,
        number_of_months: paybackSelected,
        contact_phone: parsedPhoneNumber,
        passengers: psngrs,
        reference
      }
    }
  })
}
export const createFNPLPaymentReconnect = () => (dispatch, getState) => {
  const { websocketFNPLConnected } = getState().Checkout
  dispatch(createFNPLPaymentDisconnect())
  websocketFNPLConnected && dispatch(createFNPLPaymentConnect())
}

export const showErrorNotification = message => errorNotification(message)

export const createFNPLPaymentEmit = response => (dispatch, getState) => {
  const message = JSON.parse(response.data)
  if (message.status === 'error') {
    const { form: { PassengerInfoForm: { values } } } = getState()
    dispatch(errorNotification())
    dispatch({
      type: CREATE_FNPL_PAYMENT_EMIT_ERROR,
      message: message.message
    })
    Raven.captureMessage('createFNPLPaymentEmit', {
      extra: {
        message: message.message,
        userInput: values
      },
    })
  } else if (message.status === 'success') {
    if (message.data === 'Credit has been accepted') {
      dispatch({ type: CREATE_FNPL_PAYMENT_EMIT })
      dispatch(replace('thank-you'))
    }
  }
}

export const resetPaymentButton = () => (dispatch) => {
  dispatch({ type: HIDE_PAYMENT_BUTTON })
  closeFNLPA()
  dispatch(change('PassengerInfoForm', 'verification_code', ''))
  dispatch(stopSubmit('PassengerInfoForm', { verification_code: 'You need to verify your mobile number again and enter the new verification code' }))
  const FNLPNode = document.querySelector('[data-loan-application]')
  if (FNLPNode) {
    FNLPNode.remove()
  }
}

export const checkoutEmails = value => (dispatch, getState) => {
  const state = getState()
  const selector = formValueSelector('PassengerInfoForm')
  const emailValue = selector(state, value)
  if (emailValue && validator.isEmail(emailValue)) {
    dispatch({
      type: CHECKOUT_EMAILS_ATTEMPT,
      value: emailValue,
    })
  }
}

export const changeNumberValue = value => change('PassengerInfoForm', 'phone', value.replace(/[^0-9]/g, ''))

export const showExtraBaggageModal = props => show('extraBaggageModal', props)

export const showFNPLTermsModal = () => show('fnplTermsModal')

// ------------------------------------
// Reducer
// ------------------------------------

const initialState = {
  tabNumber: 1,
  websocketConnected: false,
  websocketFNPLConnected: false,

  document_expiration: null,
  document_number: null,

  isRedirecting: false,
  isCreatingInvoice: false,
  showError: false,
  emailRegistered: false,
  paymentLink: null,
  paybackUnavailable: false,

  banks: [],
  preparingCredit: false,
  showPaymentButton: false,
  isCreatingFNPLInvoice: false,
  smsCodeValid: false,
  checkingCode: false,
  codaValidationError: false
}

export default createReducer(initialState, {
  [SET_BANKS]: (state, { banks }) => ({
    banks
  }),
  [CREATE_INVOICE_SEND]: (state, action) => ({
    isCreatingInvoice: true,
    isRedirecting: false,
    showError: false,
    data: action.data
  }),
  [CREATE_INVOICE_EMIT_ERROR]: (state, action) => ({
    isCreatingInvoice: false,
    showError: true,
    error: action.message
  }),
  [CREATE_INVOICE_EMIT]: (state, action) => ({
    isCreatingInvoice: false,
    showError: false,
    isRedirecting: action.isRedirecting,
    emailRegistered: false,
    paymentLink: action.paymentLink
  }),
  [CHANGE_TAB_NUMBER]: (state, { tabNumber }) => ({ tabNumber, showPaymentButton: true }),
  [SET_EMAIL_REGISTERED]: () => ({ emailRegistered: true }),
  [UNSET_EMAIL_REGISTERED]: () => ({ emailRegistered: false }),
  ['@@router/LOCATION_CHANGE']: () => ({ tabNumber: 1 }),
  [CREATE_INVOICE_CONNECT]: () => ({ websocketConnected: true }),
  [CREATE_INVOICE_DISCONNECT]: () => ({ websocketConnected: false }),
  [SET_PAYBACK_UNAVAILABLE]: () => ({ paybackUnavailable: true }),
  [SET_PAYBACK_AVAILABLE]: () => ({ paybackUnavailable: false }),

  [PREPARING_CREDIT_ATTEMPT]: () => ({ preparingCredit: true }),
  [PREPARING_CREDIT_STOP]: () => ({ preparingCredit: false }),
  [SHOW_PAYMENT_BUTTON]: (state, { phoneError = false, emailError = false }) => ({ showPaymentButton: true, phoneError, emailError }),
  [HIDE_PAYMENT_BUTTON]: () => ({
    tabNumber: 1,
    showPaymentButton: false,
    smsCodeValid: false
  }),
  [SET_USER_EMAIL]: (state, { email }) => ({ email }),
  [VALIDATE_SMS_CODE]: (state, { smsCodeValid }) => ({ smsCodeValid, checkingCode: false }),
  [CODE_VALIDATION]: (state, {
    checkingCode,
    codaValidationError = false,
    phoneError = false,
    emailError = false
  }) => ({ checkingCode, codaValidationError, phoneError, emailError }),
  [CREATE_FNPL_PAYMENT_SEND]: (state, action) => ({
    isCreatingFNPLInvoice: true,
    showError: false,
    data: action.values.data
  }),
  [CREATE_FNPL_PAYMENT_EMIT_ERROR]: (state, action) => ({
    isCreatingFNPLInvoice: false,
    showError: true,
    error: action.message
  }),
  [CREATE_FNPL_PAYMENT_EMIT]: (state, action) => ({
    isCreatingFNPLInvoice: false,
    showError: false,
    emailRegistered: false,
  }),
  [CREATE_FNPL_PAYMENT_CONNECT]: () => ({ websocketFNPLConnected: true }),
  [CREATE_FNPL_PAYMENT_DISCONNECT]: () => ({ websocketFNPLConnected: false }),
  [CHECKOUT_EMAILS_ATTEMPT]: () => ({ checkoutEmails: true }),
})
