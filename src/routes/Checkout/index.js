import {injectSagas} from '../../store/sagas'

export default (store) => ({
  path: 'checkout',
  getComponent (nextState, cb) {
      const Checkout = require('./containers/CheckoutContainer').default
      const createInvoice = require('./modules/createFNPLInvoice').default
      const checkoutEmails = require('./modules/checkoutEmails').default

      injectSagas(store, {key: 'Checkout', sagas: [createInvoice, checkoutEmails]})

      cb(null, Checkout)
  }
})
