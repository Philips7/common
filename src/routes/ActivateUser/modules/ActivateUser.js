import createReducer from '../../../utils/createReducer'
import {push} from 'react-router-redux/lib/actions'
import {I18n} from 'react-redux-i18n'
import {errorNotification, successNotification} from '../../../utils/notifications'

// ------------------------------------
// Constants
// ------------------------------------
export const ACTIVATE_USER_ATTEMPT = 'ActivateUser.ACTIVATE_USER_ATTEMPT'
export const ACTIVATE_USER_SUCCESS = 'ActivateUser.ACTIVATE_USER_SUCCESS'
export const ACTIVATE_USER_FAILURE = 'ActivateUser.ACTIVATE_USER_FAILURE'
// ------------------------------------
// Actions
// ------------------------------------
export const activateUserAttempt = values => dispatch => {
  (values && values.uid && values.token) ?
    dispatch({
      type: ACTIVATE_USER_ATTEMPT,
      values,
      responseSuccess: activateUserSuccess,
      responseFailure: activateUserFailure
    })
    :
    dispatch(activateUserFailure())
}

export const activateUserSuccess = () => dispatch => {
  dispatch({type: ACTIVATE_USER_SUCCESS})
  dispatch(push('/'))
  dispatch(successNotification(I18n.t('Notify.activateAccountSuccess')))
}

export const activateUserFailure = () => dispatch => {
  dispatch({type: ACTIVATE_USER_FAILURE})
  dispatch(push('/'))
  dispatch(errorNotification(I18n.t('Notify.activateAccountFail')))
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {}

export default createReducer(initialState, {})
