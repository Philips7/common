import {take, call, put} from 'redux-saga/effects'
import request from 'sagas/api'
import {ACTIVATE_USER_ATTEMPT} from './ActivateUser'

export default () => {
  function * worker({values, responseSuccess, responseFailure}) {

    const body = {
      method: 'POST',
      body: values
    }

    try {
      const {res, err} = yield call(request, 'users/user/activate', body, false)

      if (res) {
        yield put(responseSuccess(res))
      } else {
        yield put(responseFailure(err))
      }
    } catch (error) {
      yield put(responseFailure())
    }
  }

  function * watcher() {
    while (true) {
      const data = yield take(ACTIVATE_USER_ATTEMPT)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
