import React, {Component} from 'react'
import PreloaderOverlay from 'components/PreloaderOverlay'

class ActivateUser extends Component {
  componentWillMount() {
    const {activateUserAttempt, location:{query}} = this.props
    activateUserAttempt(query)
  }

  render() {
    return <PreloaderOverlay/>
  }
}

export default ActivateUser
