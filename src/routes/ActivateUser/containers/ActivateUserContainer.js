import {connect} from 'react-redux'
import { pure, compose } from 'recompose'
import {activateUserAttempt} from '../modules/ActivateUser'
import ActivateUser from '../components/ActivateUser'

const mapActionCreators = {
  activateUserAttempt
}

const mapStateToProps = (state) => ({})

const enhance = compose(
	connect(mapStateToProps, mapActionCreators),
	pure
)

export default enhance(ActivateUser)
