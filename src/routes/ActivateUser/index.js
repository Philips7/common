import {injectSagas} from 'store/sagas'

export default (store) => ({
  path: 'activate-user',
  getComponent (nextState, cb) {
      const ActivateUser = require('./containers/ActivateUserContainer').default
      const ActivateUserSaga = require('./modules/ActivateUserSaga').default

      injectSagas(store, {key: 'ActivateUserSaga', sagas: [ActivateUserSaga]})

      cb(null, ActivateUser)
  }
})
