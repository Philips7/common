import {take, call, put} from 'redux-saga/effects'
import {RESEND_EMAIL_ATTEMPT} from './EditInfo'
import {I18n} from 'react-redux-i18n'
import {errorNotification, successNotification} from '../../../utils/notifications'
import request from 'sagas/api'

export default () => {
  function *watcher() {
    while (true) {
      yield take(RESEND_EMAIL_ATTEMPT)
      try {
        const {res, err} = yield call(request, 'users/user/resend-activation-email', {
          method: 'GET'
        })
        if (res) {
          yield put(successNotification(I18n.t('Notify.emailSendSuccess')))
        }
        if (err) {
          yield put(errorNotification(err.error))
        }
      } catch (e) {
        yield put(errorNotification(I18n.t('Notify.emailSendFail')))
      }
    }
  }
  return {watcher}
}
