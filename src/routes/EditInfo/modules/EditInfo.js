import createReducer from '../../../utils/createReducer'

// ------------------------------------
// Constants
// ------------------------------------
export const RESEND_EMAIL_ATTEMPT = 'RESEND_EMAIL_ATTEMPT'
// ------------------------------------
// Actions
// ------------------------------------

export const resendEmailAttempt = () => ({
  type: RESEND_EMAIL_ATTEMPT
})
// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {}

export default createReducer(initialState, {

})
