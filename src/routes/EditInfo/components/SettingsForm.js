import React from 'react'
import cn from 'classnames'
import { pure } from 'recompose'
import { reduxForm, Field } from 'redux-form'
import Form from 'react-bootstrap/lib/Form'
import Button from 'react-bootstrap/lib/Button'
import validator from 'validator'
import { Translate, I18n } from 'react-redux-i18n'
import {
  DateFieldGroup,
  InputFieldGroup,
  PhoneFieldGroup,
  TextFieldGroup,
  TitleFieldGroup,
} from 'components/FieldGroup'
import s from './EditInfo.scss'
import titles from '../../../utils/titles'
import moment from 'moment'

const today = moment()
const past = moment(new Date(1900, 1, 1))


const requiredFields = [
  'first_name',
  'last_name',
  'birth_date',
  'email',
  'title'
]

const validate = values => {
  let errors = {}

  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = <Translate value='form.required' />
    }
  })

  if (values.first_name && values.first_name.length > 30) {
    errors.first_name = I18n.t('form.notLongerThen')
  }

  if (values.last_name && values.last_name.length > 30) {
    errors.last_name = I18n.t('form.notLongerThen')
  }

  if (!values.phone || values.phone.length < 3) {
    errors.phone = <Translate value='form.requiredPhoneNumber' />
  } else if (values.phone.length < 9) {
    errors.phone = <Translate value='form.invalidPhoneNumber' />
  }

  if (!values.email) {
    errors.email = <Translate value='form.required' />
  } else if (!validator.isEmail(values.email)) {
    errors.email = <Translate value='form.invalidEmailError' />
  }

  if (values.password) {
    if (/^[0-9]+$/.test(values.password)) {
      errors.password = <Translate value='form.invalidPasswordRegex' />
    } else if (values.password.length < 8) {
      errors.password = <Translate value='form.tooShortPassword' />
    } else if (values.confirmPassword && values.password !== values.confirmPassword) {
      errors.password = <Translate value='form.indenticalPasswordError' />
    }

    if (!values.confirmPassword) {
      errors.confirmPassword = <Translate value='form.requiredConfirmPassword' />
    }
  }

  const { birth_date } = values
  if (birth_date) {
    if (/^((?!_).)*$/.test(birth_date)) {
      if (moment(birth_date, 'DD-MM-YYYY').isAfter(today)) {
        errors['birth_date'] = I18n.t('form.yearMustBeRange')
      } else if (moment(birth_date, 'DD-MM-YYYY').isBefore(past)) {
        errors['birth_date'] = I18n.t('form.yearMustBeRange')
      }
    } else {
      errors['birth_date'] = I18n.t('form.wrongDateFormat')
    }
  }

  return errors
}

const FormGroup = pure(props =>
  <div className={s.col}>
    <Field {...props} />
  </div>
)

const SettingsForm = pure(({ handleSubmit, valid, anyTouched }) =>
  <Form
    onSubmit={handleSubmit}
  >

    <h1 className={s.formTitle}>{I18n.t('form.editInformation')}</h1>

    <div className={s.row}>
      <FormGroup
        component={TextFieldGroup}
        selectInputComponent={
          <Field
            name='title'
            component={TitleFieldGroup}
            valueKey='value'
            labelKey='label'
            options={titles}
            placeholder={I18n.t('form.title')}
            customStyleSm
          />
        }
        name='first_name'
        placeholder={I18n.t('form.firstName')}
        type='text'
        normalize={value => value && value.replace(/[^a-z^A-Z]/g, '')}
        customStyleSm
      />
      <FormGroup
        component={InputFieldGroup}
        name='last_name'
        placeholder={I18n.t('form.lastName')}
        type='text'
        normalize={value => value && value.replace(/[^a-z^A-Z]/g, '')}
        customStyleSm
      />
    </div>
    <div className={s.row}>
      <FormGroup
        component={DateFieldGroup}
        type='text'
        name='birth_date'
        placeholder={I18n.t('form.dateOfBirth')}
        customStyleSm
      />
    </div>
    <div className={s.row}>
      <FormGroup
        component={InputFieldGroup}
        name='email'
        placeholder='Email'
        type='email'
        customStyleSm
      />
      <FormGroup
        component={PhoneFieldGroup}
        name='phone'
        placeholder='form.mobilePhoneNumber'
        customStyleSm
      />
    </div>

    <h4 className={s.formTitle}>{I18n.t('form.changeYourPassword')}</h4>

    <div className={s.row}>
      <FormGroup
        component={InputFieldGroup}
        name='password'
        placeholder={I18n.t('form.newPassword')}
        type='password'
        customStyleSm
        passwordField
      />
      <FormGroup
        component={InputFieldGroup}
        name='confirmPassword'
        placeholder={I18n.t('form.confirmPassowrd')}
        type='password'
        customStyleSm
        passwordField
      />
    </div>

    <div className={s.submitButtonRow}>
      <Button
        type='submit'
        bsSize='lg'
        bsStyle={null}
        disabled={!valid || !anyTouched}
        className={cn(s.confirmBtn)}
      >
        {I18n.t('form.save')}
      </Button>
    </div>

  </Form>
)

export default reduxForm({
  form: 'SettingsForm',
  enableReinitialize: true,
  validate
})(SettingsForm)
