import React, {Component} from 'react'
import PreloaderOverlay from 'components/PreloaderOverlay'
import SettingsForm from './SettingsForm'
import s from './EditInfo.scss'

export default class EditInfo extends Component {
  render() {
    const {userGetLoading, userUpdateAttempt, user, noExpiration, resendEmailAttempt} = this.props

    return <div>
			{userGetLoading && <PreloaderOverlay/>}
      <div className={s.pageWrap}>
        <section className={s.container}>
          <SettingsForm
            initialValues={{...user}}
            onSubmit={userUpdateAttempt}
            noExpiration={noExpiration}
            resendEmailAttempt={resendEmailAttempt}
            isActivated={user && user.can_buy_tickets}
          />
        </section>
      </div>
    </div>
  }
}
