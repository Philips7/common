import {connect} from 'react-redux'
import { pure, compose } from 'recompose'
import EditInfo from '../components/EditInfo'
import {userUpdateAttempt} from '../../../redux/modules/User'
import {resendEmailAttempt} from '../modules/EditInfo'

const mapActionCreators = {
  userUpdateAttempt,
  resendEmailAttempt
}

const mapStateToProps = ({
  User: {
    userGetLoading,
    user
  },
  form: {
    SettingsForm
  }
}) => ({
  userGetLoading,
  user,
  noExpiration: SettingsForm && SettingsForm.values && SettingsForm.values.noExpiration
})

const enhance = compose(
	connect(mapStateToProps, mapActionCreators),
	pure
)

export default enhance(EditInfo)
