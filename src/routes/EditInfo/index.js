import {injectSagas} from 'store/sagas'

export default (store) => ({
  path: 'settings',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
      const EditInfo = require('./containers/EditInfoContainer').default

      const resendEmail = require('./modules/resendEmail').default
      injectSagas(store, {key: 'EditInfo', sagas: [resendEmail]})

      cb(null, EditInfo)
  }
})
