export default (store) => ({
  path: 'unavailable',
  getComponent (nextState, cb) {
      const Unavailable = require('./components/Unavailable').default

      cb(null, Unavailable)
  }
})
