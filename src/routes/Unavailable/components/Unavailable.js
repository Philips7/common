import React from 'react'
import s from './Unavailable.scss'
import InlineSVG from 'svg-inline-react'
import icon from './assets/work_in_progress.svg'
import Newsletter from 'routes/Landing/components/Newsletter/Newsletter'

export const Unavailable = () => (
	<div className={s.UnavailablePage}>
		<div className={s.messageWrap}>
			<h2 className={s.title}>Sorry, you can’t <br/>	buy tickets yet :(</h2>

			<div className={s.imageWrap}>
				<InlineSVG src={icon} />
			</div>

			<p className={s.text}>
				Subscribe to be the 1st one to know when it’s live
			</p>

			<div className={s.newsletter}>
				<Newsletter />
			</div>
		</div>
	</div>
)

export default Unavailable
