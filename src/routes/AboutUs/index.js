export default (store) => ({
  path: 'about-us',
  getComponent (nextState, cb) {
      const AboutUs = require('./components/AboutUs').default

      cb(null, AboutUs)
  }
})
