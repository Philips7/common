import React, {Component} from 'react'
import s from './AboutUs.scss'
import cn from 'classnames'
import Grid from 'react-bootstrap/lib/Grid'
import Row from 'react-bootstrap/lib/Row'
import Col from 'react-bootstrap/lib/Col'
import {Translate} from 'react-redux-i18n'

// import foundersPhoto from '../assets/founders-photo.jpg'
// import teamPhoto from '../assets/team-photo.jpg'
//
// import TeamLogo from 'static/images/ninjas-logo-text.svg'
import plainline from '../assets/plane-line.png'
import {Link} from 'react-router'

import Icon1 from 'static/images/plain-big-image.svg'
import Icon2 from 'static/images/Thank_you_icon.svg'
import Icon3 from 'static/images/price-has-changed-img.svg'

import PlainIcon1 from '../assets/plane-up.svg'
import PlainIcon2 from '../assets/plane-down.svg'

import ArrowRight from 'static/images/arrow-pointing-to-right.svg'
import InlineSVG from 'svg-inline-react'
import config from '../../../config'

class AboutUs extends Component {
  constructor() {
    super()
    this.state = {
      activeTab: 1
    }
  }

  render() {
    const {activeTab} = this.state
    return (
      <div className={s.AboutUs}>

				<Grid>
					<Row>
						<Col sm={10} smOffset={1} xs={12}>
							<h1 className={s.title}>
								<Translate value='AboutUs.title'/>
							</h1>
						</Col>
					</Row>
				</Grid>

        {/*<Grid className={s.aboutUsWrap} fluid={true}>*/}
          {/*<Row>*/}
            {/*<Col lg={9} lgOffset={2} xs={12} className={s.descriptionLeftCol}>*/}

              {/*<h1 className={s.title}>*/}
                {/*<Translate value='AboutUs.title'/>*/}
              {/*</h1>*/}

            {/*</Col>*/}
          {/*</Row>*/}

          {/*<Row className={s.contentRow}>*/}

            {/*<Col md={6} xs={12} mdPush={6} className={s.rightPhoto}>*/}
              {/*<div className={s.photoWrap}>*/}
                {/*<img src={foundersPhoto} alt='founders photo'/>*/}
              {/*</div>*/}
            {/*</Col>*/}

            {/*<Col lg={4} md={5} lgOffset={2} xs={12} mdPull={6} className={s.descriptionLeftCol}>*/}
              {/*<h2 className={s.subtitleRed}>*/}
                {/*<Translate value='AboutUs.foundersTitle'/>*/}
              {/*</h2>*/}

              {/*<p className={s.describe}>*/}
                {/*<Translate value='AboutUs.foundersDescribe' dangerousHTML/>*/}
              {/*</p>*/}
            {/*</Col>*/}
          {/*</Row>*/}

          {/*<Row>*/}
            {/*<Col md={6} xs={12} className={s.leftPhoto}>*/}
              {/*<div className={s.photoWrap}>*/}
                {/*<img src={teamPhoto} alt='team photo'/>*/}
              {/*</div>*/}
            {/*</Col>*/}
            {/*<Col md={6} xs={12} className={s.descriptionRightCol}>*/}
              {/*<div className={s.subtitleRed}>*/}
                {/*<Translate value='AboutUs.teamTitle'/>*/}
              {/*</div>*/}
              {/*<p className={s.describe}>*/}
                {/*<Translate value='AboutUs.teamDescribe'/>*/}
              {/*</p>*/}
              {/*<div className={s.developersBlock}>*/}
                {/*<a href='http://7ninjas.com' target='_blank' className={s.developersLogoWrap}>*/}
									{/*<InlineSVG src={TeamLogo}/>*/}
                {/*</a>*/}

                {/*<div className={cn(s.grayText)}>*/}
                  {/*<Translate value='AboutUs.teamCheckout'/> <a href='http://7ninjas.com' target='_blank'*/}
                                                               {/*className={s.link}>7ninjas.com</a>*/}
                {/*</div>*/}
              {/*</div>*/}
            {/*</Col>*/}
          {/*</Row>*/}
        {/*</Grid>*/}

        <div className={s.plane}>
          <img src={plainline} alt='plane'/>
        </div>

        <Grid className={s.aboutUsWrap}>
          <h3 className={s.subtitleBlack}>
            <Translate value='AboutUs.anotherTitle' dangerousHTML/>
          </h3>
          <Row>
            <Col sm={10} smOffset={1} xs={12} className={s.describe}>
              <p>
                <Translate value='AboutUs.anotherDescribe'/>
              </p>
            </Col>
          </Row>

          <Row className={s.iconsRow}>
            <Col sm={10} smOffset={1} xs={12}>
              <Row className={s.icons}>
                <Col sm={4} xs={12}>
									<InlineSVG src={Icon1}/>
                </Col>
                <Col sm={4} xs={12}>
									<InlineSVG src={Icon2}/>
                </Col>
                <Col sm={4} xs={12}>
									<InlineSVG src={Icon3}/>
                </Col>
              </Row>
            </Col>
          </Row>

          <Row className={s.contentRow}>
            <Col xs={12}>
              <h3 className={s.subtitleBlack}>
                <Translate value='AboutUs.missionTitle' dangerousHTML/>
              </h3>
            </Col>
            <Col sm={10} smOffset={1} xs={12} className={s.describe}>
              <p>
                <Translate value='AboutUs.missionDescribe'/>
              </p>
            </Col>
          </Row>

          <Row className={s.contentRow}>
            <Col xs={12}>
              <h3 className={s.subtitleBlack}>
                <Translate value='AboutUs.productTitle' dangerousHTML/>
              </h3>
            </Col>

            <Col sm={10} smOffset={1} xs={12} className={s.tabs}>
              <div>
                <button onClick={() => this.setState({activeTab: 1})}
                        className={cn(s.inactiveTab, activeTab === 1 && s.activeTab)}>
                  <span className={s.desctopText}><Translate value='AboutUs.productTab1'/></span>
                  <span className={s.mobileText}><Translate value='AboutUs.productTab1Mobile'/></span>
                </button>
                <button onClick={() => this.setState({activeTab: 2})}
                        className={cn(s.inactiveTab, activeTab === 2 && s.activeTab)}>
                  <span className={s.desctopText}><Translate value='AboutUs.productTab2'/></span>
                  <span className={s.mobileText}><Translate value='AboutUs.productTab2Mobile'/></span>
                </button>
              </div>
            </Col>

            <Col sm={10} smOffset={1} xs={12} className={s.describe}>
              {activeTab === 1 &&
              <Translate value='AboutUs.productContent1' dangerousHTML/>
              }
              {activeTab === 2 &&
              <Translate value='AboutUs.productContent2' dangerousHTML/>
              }
            </Col>

            <Col xs={12}>
              <div className={s.statusBlock}>
                <h3 className={s.subtitleBlack}>
                  <Translate value='AboutUs.statusTitle' dangerousHTML/>
                </h3>
                <div className={s.raised}>
                  <span className={s.iconWrap}>
                    <InlineSVG src={PlainIcon1}/>
                  </span>
                  <span className={s.textWrap}>Raised: 170k {config.currency}</span>
                  <span className={s.iconWrap}>
                    <InlineSVG src={PlainIcon2}/>
                  </span>
                </div>
              </div>
            </Col>

            <Col sm={10} smOffset={1} xs={12} className={s.describe}>
              <Translate value='AboutUs.statusDescribe' dangerousHTML/>
            </Col>
          </Row>
          <div className={s.contactUs}>
            <Link to='/contact-us'>
              <Translate value='AboutUs.contactUs'/> <InlineSVG src={ArrowRight}/>
            </Link>
          </div>
        </Grid>
      </div>
    )
  }
}

export default AboutUs
