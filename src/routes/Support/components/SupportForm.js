import React from 'react'
import cn from 'classnames'
import {pure} from 'recompose'
import {reduxForm, Field} from 'redux-form'
import Form from 'react-bootstrap/lib/Form'
import Button from 'react-bootstrap/lib/Button'
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger'
import Tooltip from 'react-bootstrap/lib/Tooltip'
import {Translate, I18n} from 'react-redux-i18n'
import config from 'config.json'
import {
  SelectFieldGroup,
  InputFieldGroup,
  FileInput,
} from 'components/FieldGroup'

import s from './Support.scss'
import ReCAPTCHA from 'react-google-recaptcha'

const CaptchaField = pure(({input: {onChange, onFocus}}) =>
  <div className={s.captchaWrap}>
    <ReCAPTCHA
      sitekey={config.captcha_site_key}
      onChange={e => {
        onFocus(e)
        onChange(e)
      }}
    />
  </div>
)

const EditRow = pure(props =>
  <div className={s.row}>
    <Field {...props} />
  </div>
)

const SupportForm = pure(({handleSubmit, types, valid, loggedIn}) =>
  <Form onSubmit={handleSubmit}>
    <EditRow
      component={SelectFieldGroup}
      name='type'
      valueKey='id'
      labelKey='label'
      options={types}
      inputStyle={s.select}
      placeholder={I18n.t('Support.problemType')}
      clearable={false}
			customStyleLg
			absoluteValidation
		/>

    <OverlayTrigger placement='top' overlay={
      <Tooltip className={cn(s.hiddenTooltip, loggedIn && s.showTooltip)} id='tooltip'>
        <Translate value='Support.canEditEmail'/>
      </Tooltip>
    }>
      <div>
        <EditRow
          component={InputFieldGroup}
          name='name'
          disabled={loggedIn}
          placeholder={I18n.t('Support.name')}
					absoluteValidation
					customStyleLg
				/>
        <EditRow
          component={InputFieldGroup}
          name='email'
          disabled={loggedIn}
          placeholder={I18n.t('Support.email')}
          type='email'
					absoluteValidation
					customStyleLg/>
      </div>
    </OverlayTrigger>

    <EditRow
      component={InputFieldGroup}
      rows={4}
      name='message'
      placeholder={I18n.t('Support.message')}
			absoluteValidation
      customStyleLg/>

    <EditRow
      name='file'
      classNameLabel='file-input-label'
      className='file-input'
      dropzone_options={{multiple: false}}
      component={FileInput}>
      <div className={s.dropZone}>
        <Translate value='Support.clickOrDrop' dangerousHTML/>
      </div>
    </EditRow>

    {!loggedIn &&
    <EditRow
      component={CaptchaField}
      name='captcha'/>
    }
    <div className='text-center'>
      <Button
        type='submit'
        bsSize='lg'
        bsStyle={null}
        disabled={__CLIENT__ ? !valid : true}
        className={cn(s.confirmBtn)}>
        <Translate value='Support.send'/>
      </Button>
    </div>
  </Form>
)

const validate = (values, {loggedIn}) => {
  const requiredFields = [
    'name',
    'type',
    'email',
    'message',
    !loggedIn && 'captcha'
  ]
  let errors = {}

  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = I18n.t('form.required')
    }
  })
  return errors
}

export default reduxForm({
  form: 'SupportForm',
  enableReinitialize: true,
  validate
})(SupportForm)
