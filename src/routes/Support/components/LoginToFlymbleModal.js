import React from 'react'
import Modal from 'react-bootstrap/lib/Modal'
import {connectModal} from 'redux-modal'
import {Translate} from 'react-redux-i18n'
import {pure} from 'recompose'
import s from './Support.scss'
import CrossIcon from 'static/images/cross-icon.svg'
import IllustrationIcon from 'static/images/Support_icon.svg'
import InlineSVG from 'svg-inline-react'

const LoginToFlymbleModal = pure(({
    show,
    openLoginModal,
    hideLoginModal
  }) =>
    <Modal
      show={show}
      onHide={hideLoginModal}
      className={s.modal}
    >
      <Modal.Body className={s.modalBody}>
        <span
          onClick={hideLoginModal}
          className={s.closeModalBtn}
        >
          <InlineSVG src={CrossIcon}/>
        </span>
        <h2 className={s.title}><Translate value='Support.loginToFlymble'/></h2>

        <div className={s.imageWrap}>
					<InlineSVG src={IllustrationIcon}/>
        </div>

        <p className={s.infoText}><Translate value='Support.typeAccountInfo'/></p>

        <div className={s.buttonsWrap}>
          <div className={s.buttonWrap}>
            <span
              className={s.loginBtn}
              onClick={openLoginModal}>
              <Translate value='Support.login'/>
            </span>
          </div>
          <div className={s.buttonWrap}>
            <span
              className={s.noThanksBtn}
              onClick={hideLoginModal}>
               <Translate value='Support.noThanks'/>
            </span>
          </div>
        </div>


      </Modal.Body>
    </Modal>
)

export default connectModal({name: 'loginToFlymbleModal'})(LoginToFlymbleModal)
