import React, {Component} from 'react'
import s from './Support.scss'
import SupportForm from './SupportForm'
import ContactUs from './ContactUs'
// import LoginToFlymbleModal from './LoginToFlymbleModal'
// import PreloaderOverlay from 'components/PreloaderOverlay'
import {Translate} from 'react-redux-i18n'
import {pure} from 'recompose'
import Grid from 'react-bootstrap/lib/Grid'
import Row from 'react-bootstrap/lib/Row'
import Col from 'react-bootstrap/lib/Col'
import Flag from 'static/images/uk.svg'
import InlineSVG from 'svg-inline-react'

@pure
class Support extends Component {
	componentWillMount() {
		this.props.getProblemTypes()
	}

	componentDidMount() {
		const {userGetLoading, loggedIn, openPreLoginModal} = this.props
		!userGetLoading && !loggedIn && openPreLoginModal()
	}

	render() {
		const {
			types, isSupportMessageLoading, sendSupportMessage, showMessage,
			email, first_name, last_name, loggedIn
		} = this.props

		return (
			<div className={s.supportPageWrap}>
				<Grid>
					<Row>
						<Col md={6} xs={12}>
							<ContactUs/>

							<div className={s.supportBlockWrap}>
								<h2 className={s.title}><Translate value='Support.tickets'/></h2>

								<div className={s.phoneWrap}>
									<h5 className={s.title}>24/7 customer service by Kiwi</h5>

									<a href='tel:+442038085910'><InlineSVG src={Flag}/> <span>+44 2038085910</span></a>
								</div>

							</div>
						</Col>
						<Col lg={5} lgPush={1} md={6} xs={12}>
							<div className={s.formWrap}>
								<h2 className={s.title}>Feedback</h2>

								{types && !showMessage
									? <SupportForm
										onSubmit={sendSupportMessage}
										types={types}
										loggedIn={loggedIn}
										initialValues={{email, name: loggedIn ? `${first_name} ${last_name}` : ''}}
									/>
									: <div>
										<h1>Thank you for the feedback!</h1>
										<h2>Our admins will solve your problem as soon as possible.</h2>
									</div>
								}

								{/*{isSupportMessageLoading && <PreloaderOverlay/>}*/}
								{/*<LoginToFlymbleModal*/}
									{/*openLoginModal={openLoginModal}*/}
									{/*hideLoginModal={hideLoginModal}*/}
								{/*/>*/}
							</div>
						</Col>
					</Row>
				</Grid>
			</div>
		)
	}
}

export default Support
