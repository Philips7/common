import React from 'react'
import s from './ContactUs.scss'
import PhoneIcon from 'static/images/phone-icon.svg'
import EnvelopeIcon from 'static/images/envelope-icon.svg'
import InlineSVG from 'svg-inline-react'
import {I18n} from 'react-redux-i18n'

export const ContactUs = () => (
  <div className={s.ContactUs}>

    <div className={s.contactsWrap}>
      <h3 className={s.title}>{I18n.t('Footer.contactUs')}</h3>

      <div className={s.contactCol}>
        <div className={s.infoWrap}>
          8 Brewhouse Yard <br/>Clerkenwell, London<br/>EC1V
				</div>
				<ul className={s.list}>
					<li><a href='tel:+4407925717068'><InlineSVG src={PhoneIcon}/><span>+44 07925717068</span></a></li>
					<li><a href='mailto:founders@flymble.com'><InlineSVG src={EnvelopeIcon}/><span>founders@flymble.com</span></a></li>
				</ul>
			</div>
    </div>
  </div>
)

export default ContactUs
