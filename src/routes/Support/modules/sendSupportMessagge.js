import {take, call, put} from 'redux-saga/effects'
import request from 'sagas/api'
import {SubmissionError} from 'redux-form'
import {SEND_SUPPORT_MESSAGE_ATTEMPT} from './Support'

export default () => {
  function * worker({responseSuccess, responseFailure, values, reject, resolve}) {
    const body = {
      method: 'POST',
      body: values
    }

    try {
      const {res, err} = yield call(request, 'support/tickets/', body, false)

      if (res) {
        yield put(responseSuccess(res))
        resolve && resolve()
      } else if (err) {
        yield put(responseFailure(err))
        reject && reject(new SubmissionError(err))
      }
    } catch (error) {
      yield put(responseFailure())
    }
  }

  function * watcher() {
    while (true) {
      const data = yield take(SEND_SUPPORT_MESSAGE_ATTEMPT)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
