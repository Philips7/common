import createReducer from '../../../utils/createReducer'
import {errorNotification} from '../../../utils/notifications'
import {show, hide} from 'redux-modal'

const serializeSupportForm = values => ({
  ...values,
  file: values.file ? values.file[0] : '',
  captcha: '',
  type: String(values.type)
})
// ------------------------------------
// Constants
// ------------------------------------
export const GET_PROBLEM_TYPES_ATTEMPT = 'Support.GET_PROBLEM_TYPES_ATTEMPT'
export const GET_PROBLEM_TYPES_SUCCESS = 'Support.GET_PROBLEM_TYPES_SUCCESS'
export const GET_PROBLEM_TYPES_FAILURE = 'Support.GET_PROBLEM_TYPES_FAILURE'

export const SEND_SUPPORT_MESSAGE_ATTEMPT = 'Support.SEND_SUPPORT_MESSAGE_ATTEMPT'
export const SEND_SUPPORT_MESSAGE_SUCCESS = 'Support.SEND_SUPPORT_MESSAGE_SUCCESS'
export const SEND_SUPPORT_MESSAGE_FAILURE = 'Support.SEND_SUPPORT_MESSAGE_FAILURE'

// ------------------------------------
// Actions
// ------------------------------------
export const getProblemTypes = () => ({
  type: GET_PROBLEM_TYPES_ATTEMPT,
  responseSuccess: getProblemTypesSuccess,
  responseFailure: getProblemTypesFailure
})

export const getProblemTypesSuccess = types => ({type: GET_PROBLEM_TYPES_SUCCESS, types})

export const getProblemTypesFailure = () => dispatch => {
  dispatch(errorNotification())
  dispatch({type: GET_PROBLEM_TYPES_FAILURE})
}

export const sendSupportMessage = values => dispatch => new Promise((resolve, reject) => {
  dispatch({
    type: SEND_SUPPORT_MESSAGE_ATTEMPT,
    values: serializeSupportForm(values),
    responseSuccess: sendSupportMessageSuccess,
    responseFailure: sendSupportMessageFailure,
    resolve,
    reject
  })
})

export const sendSupportMessageSuccess = () => ({type: SEND_SUPPORT_MESSAGE_SUCCESS})

export const sendSupportMessageFailure = () => dispatch => {
  dispatch(errorNotification())
  dispatch({type: SEND_SUPPORT_MESSAGE_FAILURE})
}

// export const openLoginModal = () => dispatch => {
//   dispatch(hide('loginToFlymbleModal'))
//   dispatch(show('loginModal'))
// }

export const openPreLoginModal = () => show('loginToFlymbleModal')

// export const hideLoginModal = () => dispatch => {
//   window && window.scrollTo(0, 0)
//   dispatch(hide('loginToFlymbleModal'))
// }

// ------------------------------------
// Reducer
// ------------------------------------
export const initialState = {
  types: [],
  isProblemTypesLoading: false,
  isSupportMessageLoading: false,
  showMessage: false
}

export default createReducer(initialState, {
  [GET_PROBLEM_TYPES_ATTEMPT]: () => ({
    isProblemTypesLoading: true,
    showMessage: false
  }),
  [GET_PROBLEM_TYPES_SUCCESS]: (state, {types}) => ({
    isProblemTypesLoading: false,
    types
  }),
  [GET_PROBLEM_TYPES_FAILURE]: () => ({isProblemTypesLoading: false}),
  [SEND_SUPPORT_MESSAGE_ATTEMPT]: () => ({isSupportMessageLoading: true}),
  [SEND_SUPPORT_MESSAGE_SUCCESS]: () => ({
    isSupportMessageLoading: false,
    showMessage: true
  }),
  [SEND_SUPPORT_MESSAGE_FAILURE]: () => ({isSupportMessageLoading: false})
})
