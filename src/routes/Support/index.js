import {injectSagas} from '../../store/sagas'

export default (store) => ({
  path: 'contact-us',
  getComponent (nextState, cb) {
      const Support = require('./containers/SupportContainer').default
      const getProblemTypes = require('./modules/getProblemTypes').default
      const sendSupportMessagge = require('./modules/sendSupportMessagge').default

      injectSagas(store, {key: 'Support', sagas: [getProblemTypes, sendSupportMessagge]})

      cb(null, Support)
  }
})
