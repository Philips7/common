import {connect} from 'react-redux'
import Support from '../components/Support'
import { pure, compose } from 'recompose'
import {
  getProblemTypes,
  sendSupportMessage,
  openPreLoginModal,
  hideLoginModal
} from '../modules/Support'

const mapActionCreators = {
  getProblemTypes,
  sendSupportMessage,
  // openLoginModal,
  openPreLoginModal,
  // hideLoginModal
}

const mapStateToProps = ({
  Support: {
    types,
    isSupportMessageLoading,
    showMessage
  },
  User: {
    userGetLoading,
    user,
    loggedIn
  }
}) => ({
  first_name: user && user.first_name ? user.first_name : '',
  last_name: user && user.last_name ? user.last_name : '',
  email: user && user.email ? user.email : '',
  userGetLoading,
  loggedIn,
  types,
  isSupportMessageLoading,
  showMessage
})

const enhance = compose(
	connect(mapStateToProps, mapActionCreators),
  pure
)

export default enhance(Support)
