import {injectSagas} from 'store/sagas'

export default (store) => ({
  path: 'confirm-reset-password',
  getComponent (nextState, cb) {
      const ConfirmResetPassword = require('./containers/ConfirmResetPasswordContainer').default
      const ConfirmResetPasswordSaga = require('./modules/ConfirmResetPasswordSaga').default

      injectSagas(store, {key: 'ConfirmResetPassword', sagas: [ConfirmResetPasswordSaga]})

      cb(null, ConfirmResetPassword)
  }
})
