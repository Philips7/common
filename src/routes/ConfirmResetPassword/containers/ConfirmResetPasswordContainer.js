import {connect} from 'react-redux'
import { pure, compose } from 'recompose'
import {resetPasswordAttempt} from '../modules/ConfirmResetPassword'

import ConfirmResetPassword from '../components/ConfirmResetPassword'

const mapActionCreators = {
  onSubmit: resetPasswordAttempt
}

const mapStateToProps = (state) => ({

})

const enhance = compose(
	connect(mapStateToProps, mapActionCreators),
	pure
)

export default enhance(ConfirmResetPassword)
