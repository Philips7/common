import {take, call, put} from 'redux-saga/effects'
import request from 'sagas/api'
import {I18n} from 'react-redux-i18n'
import {SubmissionError} from 'redux-form'
import {CONFIRM_RESET_PASSWORD_ATTEMPT} from './ConfirmResetPassword'

export default () => {
  function * worker({
    values: {
      uid,
      new_password,
      new_password_confirm
    },
    responseSuccess,
    responseFailure,
    resolve,
    reject
  }) {

    const body = {
      method: 'POST',
      body: {
        code: uid,
        new_password,
        new_password_confirm
      }
    }

    try {
      const {res, err} = yield call(request, `confirm/reset/${uid}/confirm_reset_password`, body)

      if (res) {
        yield put(responseSuccess(res))
        resolve()
      } else {
        yield put(responseFailure(err))
        reject(new SubmissionError(err))
      }
    } catch (error) {
      const errorMessage = {error_description: I18n.t('Notify.tryAgain')}
      yield put(responseFailure(errorMessage))
      reject(new SubmissionError(errorMessage))
    }
  }

  function * watcher() {
    while (true) {
      const data = yield take(CONFIRM_RESET_PASSWORD_ATTEMPT)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
