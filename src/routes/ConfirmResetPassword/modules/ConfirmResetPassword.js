import createReducer from '../../../utils/createReducer'
import {push} from 'react-router-redux/lib/actions'
import {I18n} from 'react-redux-i18n'
import {errorNotification, successNotification} from '../../../utils/notifications'

// ------------------------------------
// Constants
// ------------------------------------
export const CONFIRM_RESET_PASSWORD_ATTEMPT = 'ConfirmResetPassword.CONFIRM_RESET_PASSWORD_ATTEMPT'
export const CONFIRM_RESET_PASSWORD_SUCCESS = 'ConfirmResetPassword.CONFIRM_RESET_PASSWORD_SUCCESS'
export const CONFIRM_RESET_PASSWORD_FAILURE = 'ConfirmResetPassword.CONFIRM_RESET_PASSWORD_FAILURE'

// ------------------------------------
// Actions
// ------------------------------------
export const resetPasswordAttempt = values => (dispatch, getState) => new Promise((resolve, reject) => {
  const {uid} = getState().routing.locationBeforeTransitions.query

  dispatch({
    type: CONFIRM_RESET_PASSWORD_ATTEMPT,
    values: {
      ...values,
      uid
    },
    responseSuccess: resetPasswordSuccess,
    responseFailure: resetPasswordFailure,
    resolve,
    reject
  })
})

export const resetPasswordSuccess = data => dispatch => {
  dispatch(successNotification(I18n.t('Notify.successfulyChangePassword')))
  dispatch({type: CONFIRM_RESET_PASSWORD_SUCCESS})
  dispatch(push('/'))
}


export const resetPasswordFailure = data => dispatch => {
  if(data.error_description) {
    dispatch(errorNotification( I18n.t('Notify.tryAgain')))
  }
  dispatch({type: CONFIRM_RESET_PASSWORD_FAILURE})
}

// ------------------------------------
// Reducer
// ------------------------------------
export const initialState = {
  resetPasswordLoading: true
}

export default createReducer(initialState, {
  [CONFIRM_RESET_PASSWORD_ATTEMPT]: (state, action) => ({
    resetPasswordLoading: true
  }),
  [CONFIRM_RESET_PASSWORD_SUCCESS]: (state, action) => ({
    resetPasswordLoading: false
  }),
  [CONFIRM_RESET_PASSWORD_FAILURE]: (state, action) => ({
    resetPasswordLoading: false
  })
})
