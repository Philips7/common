import React from 'react'
import s from './ConfirmResetPassword.scss'
import Grid from 'react-bootstrap/lib/Grid'
import Form from 'react-bootstrap/lib/Form'
import Button from 'react-bootstrap/lib/Button'
import cn from 'classnames'
import {I18n} from 'react-redux-i18n'
import {reduxForm, Field} from 'redux-form'
import {InputFieldGroup} from 'components/FieldGroup'
import PreloaderOverlay from 'components/PreloaderOverlay'
import {pure} from 'recompose'

const validate = values => {
  let errors = {}

  if (values.new_password) {
    if (/^[0-9]+$/.test(values.new_password)) {
      errors.new_password = I18n.t('form.invalidPasswordRegex')
    } else if (values.new_password.length < 8) {
      errors.new_password = I18n.t('form.tooShortPassword')
    } else if (values.new_password_confirm && values.new_password !== values.new_password_confirm) {
      errors.new_password = I18n.t('form.indenticalPasswordError')
    }
  }

  if (!values.new_password_confirm) {
    errors.new_password_confirm = I18n.t('form.required')
  }

  return errors
}

const FormGroup = pure(props =>
  <div className={s.col}>
    <Field {...props}/>
  </div>
)

export const ConfirmResetPassword = ({handleSubmit, submitSucceeded, submitting}) =>
  <Grid className={cn(s.pageWrap)}>

    {submitting && <PreloaderOverlay/>}

    <h1 className={s.formTitle}>{I18n.t('Header.resetPassword')}</h1>


    <Form
      noValidate
      onSubmit={handleSubmit}
      className={s.form}
    >

      <div className={s.row}>
        <FormGroup
          component={InputFieldGroup}
          name='new_password'
          placeholder={I18n.t('form.newPassword')}
          type='password'
          customStyleSm
        />
      </div>

      <div className={s.row}>
        <FormGroup
          name='new_password_confirm'
          component={InputFieldGroup}
          type='password'
          placeholder={I18n.t('form.retypeNewPassword')}
          customStyleSm
        />
      </div>

      <div className={s.submitButtonRow}>
        <Button
          block
          bsSize='large'
          bsStyle='default'
          className={s.confirmBtn}
          type='submit'
        >{I18n.t('Header.submit')}
        </Button>
      </div>

    </Form>

  </Grid>

export default reduxForm({
  form: 'ConfirmResetPassword',
  validate
})(ConfirmResetPassword)
