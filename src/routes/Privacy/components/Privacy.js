import React from 'react'
import Grid from 'react-bootstrap/lib/Grid'
import Row from 'react-bootstrap/lib/Row'
import Col from 'react-bootstrap/lib/Col'
import Nav from 'react-bootstrap/lib/Nav'
import NavItem from 'react-bootstrap/lib/NavItem'
import Tab from 'react-bootstrap/lib/Tab'
import s from './Privacy.scss'

export const Privacy = () => (
	<div className={s.TermsConditions}>
		<Tab.Container id='left-tabs-example' defaultActiveKey='terms'>
			<Grid>
				<Row>
					<Col xs={12}>
						<h1 className={s.pageTitle}>Cookies, Security & Privacy Policy</h1>
					</Col>
				</Row>
				<Row>
					<Col xs={12} sm={4} md={3}>
						<Nav bsStyle='pills' stacked className={s.navbar}>
							<NavItem eventKey='terms' className={s.navItem}>
								Cookies, Security & Privacy Policy
							</NavItem>
						</Nav>
					</Col>
					<Col xs={12} sm={8} md={9}>
						<Tab.Content animation>
							<Tab.Pane eventKey='terms'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										{/*<h2 className={s.articleTitle}>Article title</h2>*/}
										<h3 className={s.articleSubtitle}>
											FLYMBLE TRAVEL Ltd.
										</h3>

										<p className={s.paragraphStandard}>
											Incorporated in England and Wales with company number 10749735 whose registered office is at <br/>
											1 Cartwright Gardens, 9006, Kings Cross, London WC1H 9EN, UK <br/> <br/>

											The site you are visiting is owned and administered by FLYMBLE TRAVEL Ltd. Please note there may
											be links on this site to third parties at which point you must make your self aware of their own
											privacy and security policies.

										</p>

										<p className={s.paragraphStandard}>
											<h4 className={s.paragraphTitle}>Cookie Usage</h4>

											This website uses cookies (as detailed below). If you do not want to us to store cookies, you can
											modify your browser's settings so that it notifies you when we attempt to store a cookie. You can
											refuse to store cookies altogether. You can also delete cookies that have already been set. If you
											wish to change your browser's setting the Help function within your browser should tell you how
											to.
										</p>

										<p className={s.paragraphStandard}>
											<h4 className={s.paragraphTitle}>Required Cookies</h4>

											This site uses cookies to maintain session information about your current visit. These cookies are
											necessary for us to process your enquiry/booking. This information only lasts for the duration of
											your current web session. Without these cookies searching and bookings functions will not work.

										</p>

										<p className={s.paragraphStandard}>
											<h4 className={s.paragraphTitle}>Analysis Cookies
											</h4>

											This site uses cookies that can last longer (up to 2 years). We hold cookies to allow us to
											analyse our visitors (although we do not hold information that will allow us to personally
											identify your visit). These cookies can be safely deleted as they do not prevent the website from
											working.

										</p>

										<p className={s.paragraphStandard}>
											<h4 className={s.paragraphTitle}>Third Party Sources Cookies</h4>

											For certain sources (like money back websites) we store the source of a booking for up to 30 days
											from initial visit. These cookies can be safely deleted, but we will not know where the booking
											has come from if you do, and potentially any money back transaction information will be lost.

										</p>

										<p className={s.paragraphStandard}>
											<h4 className={s.paragraphTitle}>Email Alerts
											</h4>

											We have an opt-in email alert service where we will hold your email address. This email address is
											only used for marketing what we believe will be useful information about FLYMBLE TRAVEL Ltd
											services - we will not rent or sell your email address to any one else. If you are unhappy about
											the emails we are sending you please email to founders@flymble.com from the email address that is
											receiving the mails.

										</p>

										<p className={s.paragraphStandard}>
											<h4 className={s.paragraphTitle}>Online Booking
											</h4>

											If you are making a booking with FLYMBLE TRAVEL Ltd or one of it's affliated sites we will capture personal information about the lead passenger (name, address, telephone number and email address). In addition we will collect the name and date of birth of all of the travelling passengers. This information will be stored on our own servers to help us handle your booking, in addition your name and travelling details will be passed onto the airline or supplier we are buying the service from. It should be noted that this information may then be passed onto certain government organisations as required by law.

										</p>

										<p className={s.paragraphStandard}>
											<h4 className={s.paragraphTitle}>Payment Details
											</h4>

											We can collect payment details including credit card details from you if this payment method is selected, as part of the booking transaction. These payments are handled via STRIPE Ltd (stripe.com) who is PCI Compliant. For debit card transactions, payments are handled via FLYNOWPAYLATER Ltd (flynowpaylater.com). This information is used to gain payment for services and is then removed from our servers. In some situations we will contact you to confirm these details by telephone. We will never ask for your payment details over email as this not a secure method of transport and we would ask you never to send such details in this way.

										</p>

										<p className={s.paragraphStandard}>
											<h4 className={s.paragraphTitle}>Collection of personal information
											</h4>

											When you send personal information to us over the Internet, your data is protected by Secure Socket Layer (SSL) technology to ensure safe transmission. This information will be stored on secure fileservers which are protected from the Internet by industry standard firewalls. Your information will be backed up periodically on to remote backup devices. At any time, you can contact us to find out the details we hold on you.

										</p>

										<p className={s.paragraphStandard}>
											<h4 className={s.paragraphTitle}>Logging
											</h4>
											FLYMBLE TRAVEL Ltd logs all activity on it's web sites for System administration reasons, ie tracking faults with the sites and for planning purposes - to see where people are visiting and the effectiveness of campaigns. This tracking will store the IP address of your computer and the pages you visit on our site. We will not associate this with any personal information and this information is only used for planning and fault finding. We use this information for our own purposes and do not pass this information on to third parties (with the exception of general trend information)

										</p>

									</article>
								</div>
							</Tab.Pane>
						</Tab.Content>
					</Col>
				</Row>
			</Grid>
		</Tab.Container>
	</div>
)

export default Privacy
