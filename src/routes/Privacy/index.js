export default (store) => ({
  path: 'privacy',
  getComponent (nextState, cb) {
      const Privacy = require('./components/Privacy').default

      cb(null, Privacy)
  }
})
