// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/CoreLayout/CoreLayout'
//import Dashboard from './Dashboard'
import EditInfo from './EditInfo'
import Checkout from './Checkout'
import ThankYou from './ThankYou'
import EmailConfirm from './EmailConfirm'
import Support from './Support'
import Landing from './Landing'
import ActivateUser from './ActivateUser'
import TermsConditions from './TermsConditions'
import Privacy from './Privacy'
import ConfirmResetPassword from './ConfirmResetPassword'
import AboutUs from './AboutUs'
import Story from './Story'
import FAQ from './FAQ'
import Unavailable from './Unavailable'
import { checkIsVerifyUser } from '../redux/modules/User'

/*  Note: Instead of using JSX, we recommend using react-router
 PlainRoute objects to build route definitions.   */

export const createRoutes = store => ({
  path: '/',
  component: CoreLayout,
  indexRoute: Landing(store),
  childRoutes: [
    ActivateUser(store),
    //Dashboard(store),
    EditInfo(store),
    //EmailConfirm(store),
    Support(store),
    Checkout(store),
    ThankYou(store),
    Landing(store),
    TermsConditions(store),
    Privacy(store),
    //ConfirmResetPassword(store),
    AboutUs(store),
    Story(store),
    FAQ(store),
		Unavailable(store),
    {
      path: '*',
      onEnter: (location, replace) => {
        replace('/')
      }
    }
  ],
  onChange: (prevState, { location: { pathname, action } }) => {
    if (action !== 'POP' && window) {
      if (pathname === '/') {
        setTimeout(() => window.scrollTo(0, 0), 100)
      } else {
        window.scrollTo(0, 0)
      }
    }
  }
})

export default createRoutes
