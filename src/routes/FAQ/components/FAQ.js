import React, {Component} from 'react'
import s from './FAQ.scss'
import Collapse from './Collapse'
import { Link } from 'react-router'

class FAQ extends Component {
  render() {
    return (
      <div className={s.storyPage}>
        <div className={s.container}>
          <div className={s.faqWrap}>
            <h2 className={s.title}>Frequently asked questions</h2>
            <ul className={s.questionsList}>
              <Collapse title='ABOUT FLYMBLE' isOpen={true}>
                <div>
                  <div className={s.block}>
                    <h3>What is Flymble?</h3>
                    <div className={s.paragraph}>
                      Flymble is a flight booking platform that helps you travel anywhere affordably. In addition to
                      financing, our powerful search engine compares prices across dozens of different airlines to find
                      you the cheapest rates. Simply search for a flight on our website, fill in your details and pay a
                      small fraction upfront to receive your tickets. Easy as that.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>What is required to book with Flymble?</h3>
                    <div className={s.paragraph}>
                      To book with Flymble:
                      <ol>
                        <li>You must be over 18 in order to pass the credit check.</li>
                        <li>You must be a resident of the UK in order to apply.</li>
                        <li>You must have a UK mobile phone in order to apply.</li>
                        <li>You must have a UK Debit or Credit Card.</li>
                      </ol>
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>Do you charge a fee?</h3>
                    <div className={s.paragraph}>
                    Yes, we do. Flymble charges a one-off service fee ranging between 10.5% - 17%,
                     depending on the payback period. The corresponding finance charge
                     is the only fee associated with a Flymble loan.
                     The one-off fee will be payable at the point of
                     booking and we may request a small deposit on selected bookings.
                     We don’t charge late fees, service fees, prepayment fees,
                     or any other hidden fees. We strive always to be more transparent
                     and fair than any other form of financing.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>When will I receive my booking confirmation?</h3>
                    <div className={s.paragraph}>
                      Our travel partner Kiwi.com will send you the booking
                       confirmation as soon as the booking process
                      is fully completed.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>Does Flymble perform a credit check?</h3>
                    <div className={s.paragraph}>
                      Yes, when you book your first flight with Flymble, we perform a ‘soft’ credit check to help verify
                      your identity and determine your eligibility for financing. This ‘soft’ credit check will not
                      affect your credit score.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>How long will the credit check take?</h3>
                    <div className={s.paragraph}>
                      The check simply takes a matter of seconds, and we will provide you with an instant credit
                      decision.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>I can only book flights between £250 and £2000. Why?</h3>
                    <div className={s.paragraph}>
                      Our credit provider currently only allows payment plans for order values ranging from £250 to
                      £2000. The order value equals the total sum of all airfares and fees.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>Do I need to pay off all my instalments before I can fly?</h3>
                    <div className={s.paragraph}>
                      No! Simply pay a small fraction upfront and you receive your ticket. Pay the rest later over 3 -
                      10 monthly instalments.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>What are my payment options?</h3>
                    <div className={s.paragraph}>
                      When you make a purchase of £250 or more with Flymble, you can pay over 3, 4, 5, 6, 7, 8, 9, or 10
                      months. Maximum order values are £2000.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>Is my personal information secure with Flymble?</h3>
                    <div className={s.paragraph}>
                      Yes, protecting your personal information is very important to us. We encrypt sensitive data
                      including your passport data. We also maintain physical and procedural safeguards to protect your
                      information. We do not sell or rent your information to anyone. You can read more about our
                      Privacy Policy here: <Link to='privacy'>https://flymble.com/privacy</Link>
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>Can I change or cancel my booking?</h3>
                    <div className={s.paragraph}>
                      Cancellations and booking amendments are possible however please be advised that additional
                      charges will apply as governed by the terms and conditions of your booking.
                      <br/>
                      In the event of booking cancellation, you will still be required to settle any outstanding balance
                      on your Fly Now Pay Later account. To request a cancellation or booking amendment, please contact&nbsp;
                      <a href="mailto:founders@flymble.com">founders@flymble.com</a>
                    </div>
                  </div>
                </div>
              </Collapse>
              <Collapse title='ABOUT CREDIT'>
                <div>
                  <div className={s.block}>
                    <i>All loans will be issued and handled by our credit partner Fly Now Pay Later.</i>
                  </div>
                  <div className={s.block}>
                    <h3>What is Fly Now Pay Later and how is it related to Flymble?</h3>
                    <div className={s.paragraph}>
                      Fly Now Pay Later Limited is the credit provider for all Flymble payment plans, and is authorized
                      and regulated by the Financial Conduct Authority under registration number 778540.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>Do I have a credit limit with Fly Now Pay Later?</h3>
                    <div className={s.paragraph}>
                      Unlike a credit card, Fly Now Pay Later is not a revolving line of credit. While customers can
                      take out multiple Fly Now Pay Later payment plans at once, each Fly Now Pay Later loan application
                      is evaluated separately as a closed-end transaction. An application from a returning customer may
                      be denied, however, if that customer has failed to repay other Fly Now Pay Later payment plans on
                      time or if the customer shows excessive borrowing behavior.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>When will my first bill be due?</h3>
                    <div className={s.paragraph}>
                      Your first monthly payment will be due (Due Date) one calendar month from the date your account
                      was created. Subsequent bills will be due on the same day each month.
                      <br/>
                      We will create your bill 7 days prior to your due date (Billing Date) and send you an email
                      notification confirming the amount and due date each month.
                      <br/>
                      Should you wish to amend your due date, please contact&nbsp;
                      <a href='mailto:support@flynowpaylater.com'>support@flynowpaylater.com</a>.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>Can I reuse my Fly Now Pay Later account?</h3>
                    <div className={s.paragraph}>
                      Once your outstanding balance has been settled, you can reuse your Fly Now Pay Later account time
                      and time again.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>How can I update my payment details?</h3>
                    <div className={s.paragraph}>
                      Simply visit&nbsp;
                      <a href='https://my.flynowpaylater.com' target='_blank' rel='noopener noreferrer'>my.flynowpaylater.com </a>
                      and select the settings tab.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>How can I contact Fly Now Pay Later?</h3>
                    <div className={s.paragraph}>
                      Email: <a href='mailto:support@flynowpaylater.com'>support@flynowpaylater.com</a>.
                      <br/>
                      Telephone: Existing account holders can contact us via phone as detailed within your credit
                      agreement.
                      <br/>
                      Online: Existing account holders can use the instant chat function found in the&nbsp;
                      <a href='https://my.flynowpaylater.com' target='_blank' rel='noopener noreferrer'>
                        my.flynowpaylater.com </a> portal after logging in.
                      <br/>
                      Read more here: <a href='https://www.flynowpaylater.com/faqs' target='_blank'
                                         rel='noopener noreferrer'>https://www.flynowpaylater.com/faqs </a>
                    </div>
                  </div>
                </div>
              </Collapse>
              <Collapse title='ABOUT FLIGHTS'>
                <div>
                  <div className={s.block}>
                    <i>All our flights are powered by our travel partner Kiwi.com</i>
                  </div>
                  <div className={s.block}>
                    <h3>What is Kiwi.com and how is it related to Flymble?</h3>
                    <div className={s.paragraph}>
                      Kiwi.com is our travel partner and provides all Flymble flights. Kiwi.com ensures the largest
                      range of flights and lowest fares. They have processed over 2.000.000 bookings so far and are a
                      trusted brand with an IATA license. Read more here:&nbsp;
                      <a href='https://www.kiwi.com/us/pages/content/about' target='_blank' rel='noopener noreferrer'>
                        https://www.kiwi.com/us/pages/content/about
                      </a>
                      <br/>
                      In addition, they offer:
                      <ul>
                        <li>Kiwi.com Guarantee for protection from cancellations, delays and rescheduling.</li>
                        <li>24/7 customer support exceptionally delivered in 13 languages.</li>
                        <li>Free additional services to make you feel as if you’ve ordered first class.
                          <ul>
                            <li>Online check-in</li>
                            <li>E-ticketing</li>
                            <li>Assistance</li>
                          </ul>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>How will I receive my tickets?</h3>
                    <div className={s.paragraph}>
                      You will receive a booking confirmation from our travel partner Kiwi.com. Your e-tickets will be
                      sent by the respective airline.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>What if there is a delay in receiving my confirmation email?</h3>
                    <div className={s.paragraph}>
                      We have already started processing your booking. There may be a delay due to minor technical
                      issues and we will send you confirmation as soon as the booking has been completed <br/>
                      If you have not received any email from us at all, please contact the Kiwi.com Customer Support
                      Team to ensure that we have the correct email address for you.
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>Which name should I use for my booking?</h3>
                    <div className={s.paragraph}>
                      You should use the names exactly as they are written in your passport or travel ID. <br/>
                      Your passport will indicate which names are first names and which names are surnames.
                      <br/><br/>
                      Examples:
                      <ol>
                        <li>
                          If you have recently been married but your passport still has your maiden name, you should use
                          your maiden name in the booking.
                        </li>
                        <li>
                          If your passport says "John James Smith" but you don't commonly use your middle name, you
                          still need to add it to your booking.
                        </li>
                        <li>
                          Your middle name should be added together with your first name in the same input box. In this
                          example, the first names would be John James.
                        </li>
                        <li>
                          Similarly, all surnames should be added together in the correct order in the surname box.
                          <ul>
                            <li>
                              Some airlines will not allow you to board the aircraft if the name on your booking does
                              not match your travel ID or passport.
                            </li>
                            <li>
                              Airlines can charge a fee to correct your name after the booking is made.
                            </li>
                          </ul>
                        </li>
                      </ol>
                    </div>
                  </div>
                  <div className={s.block}>
                    <h3>What if I did not receive an email confirmation for my flights?</h3>
                    <div className={s.paragraph}>
                      Please check your spam folder and then contact us if you do not find anything. <br/>
                      You should receive an email confirmation for your flights within two hours of making your booking,
                      otherwise:
                      <ul>
                        <li>
                          It could be in your spam folder
                        </li>
                        <li>
                          There could have been a typo in your email address when you created the booking, so please
                          contact us on chat or by phone to correct this for you
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </Collapse>
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

export default FAQ
