import React, {Component} from 'react'
import Collapse from 'react-bootstrap/lib/Collapse'
import {pure} from 'recompose'
import s from './Collapse.scss'
import cn from 'classnames'
import ArrowDown from 'static/images/down-arrow.svg'
import InlineSVG from 'svg-inline-react'

@pure
class CollapseComponent extends Component {
  constructor(props) {
    super(props)

    this.state = { open: this.props.isOpen || false }
  }

  handleClick = () => {
    this.setState({open: !this.state.open})
  }

  render() {
    const {children, title, isOpen} = this.props
    const {open} = this.state
    return (
      <li className={cn(s.collapseListItem, open && s.opened)}>
        <a onClick={this.handleClick} className={s.collapseTitle}><span>{title}</span> <InlineSVG src={ArrowDown}/></a>
        <Collapse in={open}>
          {children}
        </Collapse>
      </li>
    )
  }
}

export default CollapseComponent
