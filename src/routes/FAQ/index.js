export default (store) => ({
  path: 'faq',
  getComponent (nextState, cb) {
      const FAQ = require('./components/FAQ').default
      cb(null, FAQ)
  }
})
