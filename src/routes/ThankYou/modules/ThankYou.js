import createReducer from '../../../utils/createReducer'


// ------------------------------------
// Constants
// ------------------------------------

// ------------------------------------
// Actions
// ------------------------------------

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {}

export default createReducer(initialState, {})
