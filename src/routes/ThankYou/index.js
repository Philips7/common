export default (store) => ({
  path: 'thank-you',
  getComponent (nextState, cb) {
      const ThankYou = require('./containers/ThankYouContainer').default

      cb(null, ThankYou)
  }
})
