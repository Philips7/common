import React from 'react'
import {Translate} from 'react-redux-i18n'
import s from './ThankYou.scss'
import Postmail from './assets/ticket-icon.svg'
import Arrow from 'static/images/arrow-pointing-to-right.svg'
import InlineSVG from 'svg-inline-react'

const ThankYou = ({email, goHome}) =>
  <div className={s.pageWrap}>
    <div className={s.container}>
      <div className={s.col}>

        <h2 className={s.title}>
          <Translate value='ThankYou.bookingSuccess'/>
        </h2>

        <div className={s.ticketMessage}>
          <Translate value='ThankYou.willSent'/>
        </div>

        <div className={s.iconWrap}>
          <InlineSVG src={Postmail}/>
        </div>

        <div className={s.buttonWrap}>
          <button onClick={goHome} className={s.linkBtn}>
            <Translate value='ThankYou.goToDashboard'/>
            <InlineSVG src={Arrow} className={s.icon}/>
          </button>
        </div>

      </div>
    </div>
  </div>

export default ThankYou
