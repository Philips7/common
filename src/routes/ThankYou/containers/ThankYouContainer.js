import { connect } from 'react-redux'
import { pure, compose } from 'recompose'
import { replace } from 'react-router-redux/lib/actions'

import ThankYou from '../components/ThankYou'

const mapActionCreators = {
  goHome: () => replace('/')
}

const mapStateToProps = state => ({
  email: state.Checkout.email
})

const enhance = compose(
  connect(mapStateToProps, mapActionCreators),
  pure
)

export default enhance(ThankYou)
