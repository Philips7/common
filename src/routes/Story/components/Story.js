import React, {Component} from 'react'
import s from './Story.scss'
import cn from 'classnames'
import FlymbleTeamImage from '../assets/FlymbleTeam.jpg'

class Story extends Component {
  render() {
    return (
      <div className={s.storyPage}>
        <div className={s.container}>
          <h1 className={s.title}>Our story</h1>
          <p className={s.paragraph}>
            Flymble was founded in June 2016 in the Netherlands by Vincent Hus and Henry Wynaendts, while planning a
            trip to Bangkok.
          </p>
          <p className={s.paragraph}>
            Online they found a flight ticket for £500, but they didn’t have the money at the time. It took 2 months to
            save up for the holiday, by working in a bar. When they looked for the flight again, they had to pay over
            £200 extra because the flight ticket increased in price. Since there was no solution to make such a big
            upfront payment without having saved up all of it or using a credit card, they dropped out of college and
            moved to London to start a company to solve that problem.
          </p>
          <p className={s.paragraph}>
            That company is Flymble. It is a booking platform that lets you book a flight for 1/10th upfront and spread
            the rest over time. Here is how it works: search for a flight ticket and choose to spread the cost over
            3-to-10 equal monthly payments. Pay 1/10th upfront and receive your tickets.
          </p>
          <p className={s.paragraph}>
            A year later – Flymble has closed partnerships with the largest credit providers in the EU, are generating
            sales, has a team of 6 software engineers, and is ranked one of&nbsp;
            <a href='https://www.producthunt.com/topics/travel?order=most-upvoted'
               target='_blank' rel='noopener noreferrer'
               className={s.link}> the top 10 </a>
            most popular travel products in the world on ProductHunt.com (a couple places below ‘Google Trips’).
          </p>
          <p className={s.paragraph}>
            Flymble HQ is based in Clerkenwell, London. So if you’re around, don’t hesitate to stop by for a cup of
            coffee or a game of pool!
          </p>
          <img src={FlymbleTeamImage} alt='Flymble team' className={s.image} />
        </div>
      </div>
    )
  }
}

export default Story
