export default (store) => ({
  path: 'story',
  getComponent (nextState, cb) {
      const AboutUs = require('./components/Story').default

      cb(null, AboutUs)
  }
})
