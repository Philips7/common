import { connect } from 'react-redux'
import { pure, compose } from 'recompose'
import path from 'ramda/src/path'

import {
  changeOnConnectBehaviour,
  searchTripsDisconnect,
  searchTripsConnect,
  searchTripsSend,
  cleanResults,
} from '../../../redux/modules/Trips'
import Landing from '../components/Landing'
import { changePassengersNumber } from '../../../redux/modules/Ticket'

import {
  changeOrigin,
  togglePortal,
  searchAirports,
  changeDateRange,
  changeWayNumber,
  slideInputRange,
  showPaybackModal,
  hidePaybackModal,
  resetDatePickers,
  changeDateSingle,
  getFlightsFromPath,
  unmountSearchField,
  searchAirportsConnect,
  changeFocusOnDateRange,
  changeFocusOnDateSingle,
  changeFocusOfSearchField,
  searchAirportsDisconnect,
  untouchFieldInSearchField,
  searchFeatureFlightsAttempt
} from '../modules/Landing'

import { signUpNewsletterAttempt } from '../../../redux/modules/User'

const mapActionCreators = {
  searchFeatureFlightsAttempt,
  changeOnConnectBehaviour,
  changeFocusOfSearchField,
  searchAirportsDisconnect,
  untouchFieldInSearchField,
  signUpNewsletterAttempt,
  changeFocusOnDateSingle,
  changePassengersNumber,
  changeFocusOnDateRange,
  searchTripsDisconnect,
  searchAirportsConnect,
  searchTripsConnect,
  getFlightsFromPath,
  unmountSearchField,
  showPaybackModal,
  hidePaybackModal,
  resetDatePickers,
  changeDateSingle,
  changeDateRange,
  changeWayNumber,
  slideInputRange,
  searchTripsSend,
  searchAirports,
  cleanResults,
  changeOrigin,
  togglePortal,
}

const mapStateToProps = ({
  Landing,
  form: { SearchField, NewsletterForm },
  i18n: { locale },
  Trips,
  routing: { locationBeforeTransitions: { pathname } }
}) => ({
  Landing,
  searchFieldValue: SearchField ? SearchField.values : null,
  departureName: path(['values', 'outbound_place', 'name'], SearchField),
  Trips,
  newsletterForm: NewsletterForm,
  pathname,

    // Put there to trig re-rendering SearchField
  locale
})

const enhance = compose(
  connect(mapStateToProps, mapActionCreators),
  pure
)

export default enhance(Landing)
