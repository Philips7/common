import React from 'react'
import s from './WhyFlymble.scss'
import cn from 'classnames'
import { Translate } from 'react-redux-i18n'
import InlineSVG from 'svg-inline-react'

import support from './assets/flymble-icon.svg'
import kiwi from './assets/kiwi-icon.svg'
import cards from './assets/card-icon.svg'
import locked from './assets/locked-icon.svg'

const WhyFlymble = () => (
  <section className={s.WhyFlymbleSection}>
    <div className={s.container}>
      <div className={s.col}>
        <h1 className={s.title}><Translate value='WhyFlymble.why'/></h1>
        <div className={s.featureBlocksWrap}>

          <div className={s.featureBlock}>
            <div className={s.iconWrap}>
              <InlineSVG src={support}/>
            </div>
            <h4 className={s.blockTitle}>24/7 support</h4>
					<span className={s.text}>
						24/7 support on chat, phone and email in 13 languages with a rating
            of “Excellent“ on Trustpilot!
					</span>
             <a href='#' onClick={() => typeof window !== 'undefined' && Intercom('show')} className={s.link}>Learn more</a>
          </div>

          <div className={s.featureBlock}>
            <div className={s.iconWrap}>
              <InlineSVG src={kiwi}/>
            </div>
            <h4 className={s.blockTitle}>Kiwi.com flight guarantee</h4>
          <span className={s.text}>
           Protects you from flight cancellations, delays and rescheduling.
            We’ll offer you an alternative flight to your destination or
            refund the affected part of your journey
          </span>
            <a href='https://www.kiwi.com/us/pages/guarantee' target='_blank'
               rel="noopener noreferrer"
               className={s.link}>Learn more</a>
          </div>

          <div className={s.featureBlock}>
            <div className={s.iconWrap}>
              <InlineSVG src={cards}/>
            </div>
            <h4 className={s.blockTitle}>Pay with debit card</h4>
          <span className={s.text}>
            Book a flight in under 90 seconds without having to leave our
            website. The process is fully integrated and the credit check
            just takes seconds.
          </span>
            <a href='https://flynowpaylater.com' target='_blank' rel="noopener noreferrer" className={s.link}>Learn more</a>
          </div>

          <div className={s.featureBlock}>
            <div className={s.iconWrap}>
              <InlineSVG src={locked} />
            </div>
            <h4 className={s.blockTitle}>Transparent and secure</h4>
            <span className={s.text}>
          Pay a one-off fee upfront ranging from 10.5% - 17%, depending on the departure date.
           We will never charge a dime more than the original price disclosed,
            no additional interest or fees.
          </span>
          </div>
        </div>
      </div>
    </div>
  </section>
)

export default WhyFlymble
