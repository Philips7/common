import React from 'react'
import s from './UserStories.scss'
import cn from 'classnames'
import { Translate } from 'react-redux-i18n'
import { Link } from 'react-router'
import InlineSVG from 'svg-inline-react'

import LinkIcon from './assets/LinkSymbol.svg'
import ProductHunt from './assets/ProductHunt.svg'
import firmImage1 from './assets/firm1.png'
import firmImage2 from './assets/firm2.png'
import firmImage3 from './assets/firm3.png'
import firmImage4 from './assets/firm4.png'

const UserStories = () => (
  <section className={s.UserStoriesSection}>
    <div className={s.container}>
      <div className={s.col}>
        <h1 className={s.title}>What people say about us</h1>

        <div className={s.testimonial}>
          <div className={s.firmLogo}>
            <InlineSVG src={ProductHunt} />
          </div>
          <h2 className={s.content}>
            ‘’Top 5 most popular travel startup all-time‘’
          </h2>
          <a href='https://www.producthunt.com/posts/flymble' target='_blank' className={s.moreLink}>
            <span className={s.icon}>
              <InlineSVG src={LinkIcon} />
            </span>
            <span>read more</span>
          </a>
        </div>
        <div className={s.featuredTrip}>
          <div className={s.container}>
            {/* TrustBox widget - List */}
            <div
              className='trustpilot-widget'
              data-locale='en-US'
              data-template-id='539ad60defb9600b94d7df2c'
              data-businessunit-id='58b9e5060000ff00059dc07f'
              data-style-height='500px'
              data-style-width='100%'
              data-group='a'
              data-stars='4,5'
            >
              <a href='https://www.trustpilot.com/review/flymble.com' target='_blank' rel='noopener noreferrer'>
          Trustpilot
        </a>
            </div>
            {/* End TrustBox widget */}
          </div>
        </div>
      </div>
    </div>
  </section>
)

export default UserStories
