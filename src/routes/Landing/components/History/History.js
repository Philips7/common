import React from 'react'
import s from './History.scss'
import cn from 'classnames'
import {I18n} from 'react-redux-i18n'
import Newsletter from '../Newsletter/Newsletter'

export const Slider = ({signUpNewsletterAttempt}) => (
  <section className={s.historySection}>

    <div className={s.headingPart}>
      <div className={s.headedingContainer}>
        <h2 className={s.titleXl}>{I18n.t('Landing.flymbleStory')}</h2>

        <p className={s.featuredParagraph}>
          {I18n.t('Landing.featuredParagraph1')}<span className={s.textFeatured}>{I18n.t('Landing.textFeatured1')}</span>
          {I18n.t('Landing.featuredParagraph2')}<br/> <br/>
          {I18n.t('Landing.featuredParagraph3')}
          <span
          className={s.textFeatured}>{I18n.t('Landing.textFeatured2')}</span>
        </p>
      </div>
    </div>

    <div className={s.historyTimeLine}>
      <div className={s.container}>

        <div className={s.step1}>
          <div className={cn(s.col, s.decorationCol)}>
          </div>
          <div className={cn(s.col, s.contentCol)}>
            <div className={s.content}>
              <h4 className={s.title}>
                {I18n.t('Landing.historyTimeLineTitle1')}
              </h4>
              <p>
                {I18n.t('Landing.historyTimeLineP1')}
              </p>
            </div>
          </div>
        </div>

        <div className={s.step2}>
          <div className={cn(s.col, s.contentCol)}>
            <div className={s.content}>
              <h4 className={s.title}>
                {I18n.t('Landing.historyTimeLineTitle2')}
              </h4>
              <p>
                {I18n.t('Landing.historyTimeLineP2')}
              </p>
            </div>
          </div>
          <div className={cn(s.col, s.decorationCol)}>
          </div>
        </div>

        <div className={s.step3}>
          <div className={s.step31}>
            <div className={cn(s.col, s.decorationCol)}></div>
            <div className={cn(s.col, s.contentCol)}>
              <div className={s.content}>
                <h4 className={s.title}>
                  {I18n.t('Landing.historyTimeLineTitle3')}
                </h4>
                <p>
                  {I18n.t('Landing.historyTimeLineP3')}
                </p>
              </div>
            </div>
          </div>
          <div className={s.step32}>
            <div className={cn(s.col, s.contentCol)}>
              <div className={s.content}>
                <h4 className={s.title}>
                  {I18n.t('Landing.historyTimeLineTitle4')}
                </h4>
                <p>
                  {I18n.t('Landing.historyTimeLineP4')}
                </p>
              </div>
            </div>
            <div className={cn(s.col, s.decorationCol)}></div>
          </div>
        </div>
      </div>
      <div className={s.endHistory}>
        <h2 className={s.titleXl}>{I18n.t('Landing.whatNextTitleXl')}</h2>
        <h4 className={s.smalltitle}>{I18n.t('Landing.signUpForNewsletter')}</h4>
        <Newsletter onSubmit={signUpNewsletterAttempt}/>
      </div>
    </div>

  </section>
)

export default Slider
