import React, { Component } from 'react'
import { DayPickerRangeController } from 'react-dates'
import { START_DATE, END_DATE, VERTICAL_SCROLLABLE } from 'react-dates/constants'

class DayPickerRangeControllerWrapper extends Component {
  constructor(props) {
    super(props)
    this.state = {
      focusedInput: props.autoFocusEndDate ? END_DATE : START_DATE,
      startDate: props.startDate,
      endDate: props.endDate,
    }
  }

  componentDidMount() {
    const element = document.getElementsByClassName('transition-container transition-container--vertical')
    const startDate = this.state.startDate
    if (startDate && !!element.length) {
      element[0].scrollTop = startDate.diff(this.props.disableDate, 'months', true) * 220
    }
  }

  componentWillReceiveProps({ startDate, endDate, portalOpened }) {
    const endDateState = this.state.endDate
    const startDateState = this.state.startDate
    if (!startDate && !endDate && startDateState && endDateState) {
      this.setState({
        startDate,
        endDate,
      })
    }
    if (!portalOpened && this.props.portalOpened) {
      this.props.onDatesChange({ startDate: startDateState, endDate: endDateState })
    }
  }

  shouldComponentUpdate = ({ startDate, endDate }, nextState) => (!startDate && !endDate) || startDate !== nextState.startDate || endDate !== nextState.endDate

  onDatesChange = ({ startDate, endDate }) => this.setState({ startDate, endDate })

  onFocusChange = focusedInput => this.setState({
    // Force the focusedInput to always be truthy so that dates are always selectable
    focusedInput: !focusedInput ? START_DATE : focusedInput,
  })

  render() {
    const {
      portalOpened,
      startDatePlaceholderText,
      endDatePlaceholderText,
      displayFormat,
      showClearDates,
      disableDate,
      ...props } = this.props
    const { focusedInput, startDate, endDate } = this.state
    return <DayPickerRangeController
      {...props}
      initialVisibleMonth={() => disableDate}
      onDatesChange={this.onDatesChange}
      onFocusChange={this.onFocusChange}
      focusedInput={focusedInput}
      startDate={startDate}
      endDate={endDate}
      orientation={VERTICAL_SCROLLABLE}
      numberOfMonths={7}
    />
  }
}

export default DayPickerRangeControllerWrapper
