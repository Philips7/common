import React, { Component } from 'react'
import Autosuggest from 'react-autosuggest'
import { pure } from 'recompose'
import { I18n } from 'react-redux-i18n'

import CityIcon from 'static/images/buildings.svg'
import CompassIcon from 'static/images/compass.svg'
import StarIcon from 'static/images/star.svg'
import InlineSVG from 'svg-inline-react'
import { anywhereCheck } from '../../../../../utils/helpers'
import { topDepartures, getDestSuggestions } from '../../../../../utils/topAirports'
import styles from './Autosuggest.scss'

const shouldRenderSuggestions = () => true

const renderSuggestion = suggestion => {
  if (suggestion.type === 'city' && suggestion.custom) {
    return <span><strong>{`${suggestion.name},`}</strong> <span>{suggestion.country}{` - ${I18n.t('SearchField.allAirports')}`}</span></span>
  } else if (suggestion.type === 'city') {
    return <span><span
      className={styles.iconWrap}><InlineSVG
      src={CityIcon}/></span><strong>{`${suggestion.name},`}</strong> <span>{suggestion.country}{suggestion.nested ? ` - ${I18n.t('SearchField.allAirports')}` : ''}</span></span>
  } else if (suggestion.type === 'airport' && suggestion.hasOwnProperty('nested')) {
    return <span><span className={styles.nested}></span><span
      className={styles.code}>{`${suggestion.code}`}</span><span>{`${suggestion.name}`}</span></span>
  } else if (suggestion.type === 'airport') {
    return <span><span
      className={styles.code}>{`${suggestion.code}`}</span><strong>{`${suggestion.city},`}</strong> <span>{`${suggestion.country} - ${suggestion.name}`}</span></span>
  } else if (anywhereCheck(suggestion.name)) {
    return <span><span className={styles.iconWrap}><InlineSVG
      src={CompassIcon}/></span><span>{I18n.t('SearchField.anywhere')}</span></span>
  } else if (suggestion.type === 'country') {
    return <span><span className={styles.iconWrap}><img className={styles.flag}
                                                        src={suggestion.flag}/></span><span>{`${suggestion.name}`}</span></span>
  }
}

const getSectionSuggestions = suggestion => {
  if (suggestion && suggestion.type === 'city' && ! suggestion.custom) {
    const { airports, name, country } = suggestion
    if (suggestion.airports.length > 1) {
      const tempAirports = airports.map(obj => ({
        ...obj,
        city: name,
        country,
        nested: true
      }))
      return [{ ...suggestion, nested: true }, ...tempAirports]
    }
    return [{
      ...airports[0],
      city: name,
      country
    }]
  }
  return [suggestion]
}

export const TopDepartures = ({ fromDepartures }) =>
    <div className={styles.topDepartures}>
      <h2 className={styles.departuresTitle}>
        <span>TOP {fromDepartures ? 'DEPARTURES' : 'DESTINATIONS'}</span>
        <span className={styles.icon}>
        <InlineSVG src={StarIcon}/>
      </span>
      </h2>
    </div>

@pure
export default class AutosuggestField extends Component {

  storeInputReference = autosuggest => autosuggest &&
    this.props.getRef && this.props.getRef(autosuggest.input)

  onSuggestionFetchRequest = ({ value }) => {
    this.props.searchAirports({ search_string: value })
    this.state.showTriangle = true
    console.log('show', this.state.showTriangle)
  }

  render() {
    const { placeholder, value, name, onChange, getSuggestionValue, onBlur, onFocus, onKeyDown, suggestions, clearSuggestions, departureName } = this.props
    const inputProps = {
      placeholder,
      value,
      onChange,
      onBlur,
      onFocus,
      onKeyDown
    }
    return (
      <Autosuggest
        id={placeholder}
        multiSection={true}
        focusFirstSuggestion={true}
        ref={this.storeInputReference}
        suggestions={
          value === ''
            ? name === 'outbound_place'
            ? topDepartures
            : getDestSuggestions(departureName)
            : name === 'outbound_place' && ! ! suggestions.length && anywhereCheck(suggestions[suggestions.length - 1].name)
            ? suggestions.slice(0, suggestions.length - 1)
            : suggestions
        }
        shouldRenderSuggestions={shouldRenderSuggestions}
        focusInputOnSuggestionClick={false}
        onSuggestionsFetchRequested={this.onSuggestionFetchRequest}
        onSuggestionsClearRequested={clearSuggestions}
        onSuggestionSelected={(event, { suggestion }) => onChange(suggestion)}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        renderSectionTitle={suggestion => suggestion.first &&
          <TopDepartures
            fromDepartures={name === 'outbound_place'}
          />
        }
        getSectionSuggestions={getSectionSuggestions}
        inputProps={inputProps}
        theme={styles}
      />
    )
  }
}
