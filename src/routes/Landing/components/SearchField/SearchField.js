import React from 'react'
import cn from 'classnames'
import FormGroup from 'react-bootstrap/lib/FormGroup'
import Form from 'react-bootstrap/lib/Form'
import Tooltip from 'react-bootstrap/lib/Tooltip'
import Select from 'react-select'
import ReactTooltip from 'react-tooltip'
import {
  SingleDatePicker,
  isInclusivelyAfterDay,
  DateRangePicker,
} from 'react-dates'
import Portal from 'react-portal'
import {
  HORIZONTAL_ORIENTATION,
  VERTICAL_ORIENTATION,
  VERTICAL_SCROLLABLE,
} from 'react-dates/constants'
import Field from 'redux-form/lib/Field'
import reduxForm from 'redux-form/lib/reduxForm'
import AutosuggestField from 'containers/AutosuggestContainer'
import { pure, compose, withState } from 'recompose'
import { scrollTo, iOSHandlers, globalBodyStyles } from 'utils/helpers'
import { isSmallScreen, isiOS } from 'utils/responsive'
import passengerConfig from 'utils/passenger.json'
import { I18n, Translate } from 'react-redux-i18n'
import moment from 'moment'
import CrossIcon from 'static/images/cross-icon.svg'
import CalendarIcon from 'static/images/landing/searchField/calendar-icon.svg'
import MapPointIcon from 'static/images/landing/searchField/map-point-icon.svg'
import PassengerIcon from 'static/images/landing/searchField/passenger-icon.svg'
import SearchIcon from 'static/images/landing/search.svg'
import InlineSVG from 'svg-inline-react'
import DayPickerMobile from './DayPickerMobile'
import s from './SearchField.scss'

const passangerOptions = passengerConfig.passenger.map(({ value, label }) => ({
  value,
  label: <span className={s.label}>
    <Translate value={`SearchField.passenger${value === 1 ? '' : 's'}Option`} passenger={label} />
  </span>
}))

const validate = (values) => {
  const errors = {}

  const requiredFields = [
    'outbound_place',
    'destination_place',
  ]

  requiredFields.forEach((field) => {
    if (!values[field]) {
      errors[field] = ' '
    }
  })

  const { outbound_place, destination_place } = values

  if ((outbound_place && destination_place) &&
    (typeof outbound_place === 'object' && typeof destination_place === 'object') &&
    (((outbound_place.city && destination_place.city) && outbound_place.city === destination_place.city && outbound_place.country === destination_place.country) ||
      (outbound_place.city === destination_place.name && outbound_place.country === destination_place.country) ||
      (destination_place.city === outbound_place.name && outbound_place.country === destination_place.country) ||
      (destination_place.name === outbound_place.name && outbound_place.country === destination_place.country))
  ) {
    errors.destination_place = I18n.t('Notify.different')
  }

  if (typeof outbound_place === 'string') {
    errors.outbound_place = ' '
  }

  if (typeof destination_place === 'string') {
    errors.destination_place = ' '
  }

  if (!values.range_date || (values.range_date && !values.range_date.endDate)) {
    errors.range_date = ' '
  }

  return errors
}

const onSubmitFail = (errors, dispatch, submitError, props) => {
  const {
    changeFocusOutboundField,
    changeFocusToDestinationField,
    changeFocusOfSearchField,
   } = props

  const {
    outbound_place,
    destination_place,
    range_date,
  } = errors
  const portalNode = document.querySelector('[id="portalHook"]')
  if (outbound_place) {
    changeFocusOutboundField()
  } else if (destination_place) {
    changeFocusToDestinationField()
  } else if (range_date) {
    isSmallScreen && portalNode && portalNode.click()
    changeFocusOfSearchField('range_date')
  }
}

const today = moment()
const disableDate = moment().add(13, 'days')
const afterDate = moment().add(7, 'months')

const SingleDatePickerField = pure(({
  input,
  changeFocusOnDateSingle,
  changeDateSingle,
  rangePicker,
  closePortal,
  portal }) =>
  <FormGroup className={cn(s.formGroup, portal && s.portalDatesWrap)}>
    <SingleDatePicker
      // {...input}
      focused={portal ? true : rangePicker.focused}
      date={rangePicker.startDate}
      onDateChange={(value) => {
        input.onChange(value)
        changeDateSingle(value)
        portal && closePortal()
      }}
      isOutsideRange={day =>
        !isInclusivelyAfterDay(day, moment())
        || isInclusivelyAfterDay(day, moment().add(1, 'year'))
      }
      isDayBlocked={day => day.isBefore(disableDate)}
      placeholder={I18n.t('SearchField.depart')}
      numberOfMonths={portal ? 1 : 2}
      orientation={portal ? VERTICAL_ORIENTATION : HORIZONTAL_ORIENTATION}
      displayFormat='DD/MM/YYYY'
      onFocusChange={changeFocusOnDateSingle}
      showClearDate={!portal}
    />
  </FormGroup>
)

const composedHOC = compose(
  withState('shouldRender', 'changeShouldRender', true),
  pure
)

const DatePickerWrapper = ({ portalOpened, ...props }) => isSmallScreen
  ? <DayPickerMobile {...props} portalOpened={portalOpened} disableDate={disableDate} />
  : <DateRangePicker {...props} />

const DateRangePickerField = composedHOC(({
  input,
  meta: { error, touched, submitFailed },
  changeFocusOnDateRange,
  changeDateRange,
  rangePicker,
  closePortal,
  portal,
  portalOpened,
  shouldRender,
  }) => <FormGroup className={cn(s.formGroup, portal && s.portalDatesWrap, s.datePickerWrap)}>
    {touched && error && submitFailed && rangePicker.focusedInput === 'startDate' &&
      <div className={cn(s.tooltipWraper, s.tooltipMargin)}><Tooltip className='in' placement='top' id='tooltip-top'>
        Oops, check if you have completed all fields
      </Tooltip></div>}
    {shouldRender && <DatePickerWrapper
      portalOpened={portalOpened}
      focusedInput={portal ? rangePicker.focusedInput || 'startDate' : rangePicker.focusedInput}
      startDate={rangePicker.startDate}
      endDate={rangePicker.endDate}
      onDatesChange={(value) => {
        input.onChange(value)
        changeDateRange(value)
        portal && rangePicker.startDate && value && (rangePicker.focusedInput === 'endDate') && closePortal()
      }}
      isOutsideRange={day =>
        !isInclusivelyAfterDay(day, moment())
        || isInclusivelyAfterDay(day, moment().add(1, 'year'))
      }
      isDayBlocked={day => day.isSameOrBefore(disableDate, 'day') || day.isSameOrAfter(afterDate, 'day')}
      renderDay={day =>
        (day.isSameOrBefore(disableDate, 'day')
          && day.isSameOrAfter(today, 'day'))
          || day.isAfter(afterDate)
          ? (
            <span
              data-tip={day.isAfter(afterDate)
                ? 'It is not possible to book a flight 7 months ahead'
                : 'Please book at least two weeks in advance'}
              className={s.tooltipPointer}
            >
              {day.format('D')}
              <ReactTooltip className={s.tooltip} type='dark' effect='solid' place='right' />
            </span>)
          : day.format('D')
      }
      minimumNights={0}
      numberOfMonths={portal ? 12 : 2}
      startDatePlaceholderText={I18n.t('SearchField.depart')}
      endDatePlaceholderText={I18n.t('SearchField.return')}
      orientation={portal ? VERTICAL_SCROLLABLE : HORIZONTAL_ORIENTATION}
      displayFormat='DD/MM/YYYY'
      onFocusChange={changeFocusOnDateRange}
      showClearDates={!portal}
      initialVisibleMonth={() => (rangePicker.focusedInput === 'startDate' && input.value.startDate && moment(input.value.startDate, 'MM YYYY'))
        || (rangePicker.focusedInput === 'endDate' && input.value.endDate && moment(input.value.endDate, 'MM YYYY'))
        || moment(new Date())}
    />}
  </FormGroup>
)

const PseudoModal = pure(({
  input,
  meta,
  changeFocusOnDateRange,
  changeDateRange,
  changeFocusOnDateSingle,
  changeDateSingle,
  rangePicker,
  closePortal,
  oneWay,
  portalOpened,
  resetDatePickers
 }) => {
  portalOpened ? globalBodyStyles('disableBodyScroll', true) : globalBodyStyles('disableBodyScroll', false)
  return (
    <div className={cn(s.pseudoModal, portalOpened ? s.pseudoModalFadeIn : s.pseudoModalFadeOut)}>
      <div className={s.miniMenuContainer}>
        <div className={s.tableLeft}>
          <span className={s.iconSpan} onClick={closePortal}>
            <InlineSVG src={CrossIcon} />
          </span>
          <span  className={s.reset} onClick={resetDatePickers}>{I18n.t('SearchField.reset')}</span>
        </div>

        <span className={s.tableCenter}>{I18n.t('SearchField.dateMobileTitle')}</span>
        <div className={s.tableRight} >

          <div onClick={closePortal} className={s.accept}>
            <span>{I18n.t('SearchField.done')}</span>
          </div>

        </div>
      </div>
      {oneWay ?
        <SingleDatePickerField
          closePortal={closePortal}
          rangePicker={rangePicker}
          changeDateSingle={changeDateSingle}
          changeFocusOnDateSingle={changeFocusOnDateSingle}
          input={input}
          portal
        /> :
        <DateRangePickerField
          portalOpened={portalOpened}
          closePortal={closePortal}
          rangePicker={rangePicker}
          changeDateRange={changeDateRange}
          changeFocusOnDateRange={changeFocusOnDateRange}
          input={input}
          meta={meta}
          portal
        />
      }
    </div>
  )
}
)

const dateDisplayer = val => moment(val).format('Do MMM')

const DatepickerWithPortal = pure(({
  input,
  meta,
  changeFocusOnDateRange,
  changeFocusOnDateSingle,
  changeDateRange,
  changeDateSingle,
  rangePicker,
  togglePortal,
  rangePicker: { startDate, endDate },
  closePortal,
  portalOpened,
  oneWay,
  resetDatePickers }) => {
  const hook = <FormGroup id='portalHook' className={cn(s.formGroup, s.dateSpanField)}>

    {oneWay && startDate && (
      <span className={s.date}>{dateDisplayer(startDate)}</span>
    )}

    {oneWay && !startDate && (
      <span className={s.anytime}>Anytime</span>
    )}

    {!oneWay && startDate && endDate && (
      <span className={s.date}>
        {dateDisplayer(startDate)} - {dateDisplayer(endDate)}
      </span>
    )}

    {!oneWay && startDate && !endDate && (
      <span className={s.date}>{dateDisplayer(startDate)} - Anytime</span>
    )}

    {!oneWay && !startDate && endDate && (
      <span className={s.date}>Anytime - {dateDisplayer(endDate)}</span>
    )}

    {!oneWay && !startDate && !endDate && (
      <span className={s.anytime}>Anytime</span>
    )}

  </FormGroup>
  return (
    <Portal
      closeOnEsc
      openByClickOn={hook}
      beforeClose={({ }, removeFromDOM) => {
        togglePortal(false)
        setTimeout(() => {
          togglePortal(true)
          removeFromDOM()
          globalBodyStyles('disableBodyScroll', false)
        }, 500)
      }}
    >
      <PseudoModal
        closePortal={closePortal}
        rangePicker={rangePicker}
        oneWay={oneWay}
        portalOpened={portalOpened}
        resetDatePickers={resetDatePickers}
        changeDateSingle={changeDateSingle}
        changeDateRange={changeDateRange}
        changeFocusOnDateSingle={changeFocusOnDateSingle}
        changeFocusOnDateRange={changeFocusOnDateRange}
        input={input}
        meta={meta}
      />
    </Portal>
  )
})

const InputField = pure(({
  placeholder,
  input,
  meta: { error, active, touched, submitFailed },
  changeFocus,
  getRef,
  label,
  departureName,
  searchAirports }) => {
  const { value, onChange, onFocus, onBlur } = input
  const displayedValue = typeof value === 'string' ? value : `${value.city || value.name}`
  let onTabKeyDown = null

  const code = typeof value.code === 'string' ? value.code : ''

  const _onChange = data => {
    if (data.type === 'change') onChange(data)
    if (input.name === 'outbound_place' && (data.hasOwnProperty('code') || data.type === 'click')) changeFocus()
  }

  const _onBlur = (e, { focusedSuggestion }) => {
    if (onTabKeyDown === 'Tab' && focusedSuggestion) {
      onChange(focusedSuggestion)
    }
    onBlur()
  }

  const _onFocus = e => {
    e.target.select()
    searchAirports({ search_string: e.target.value })
    onFocus(e)
    if (isSmallScreen) {
      isiOS && iOSHandlers()
      const element = document.getElementById(input.name)
      const searchField = document.getElementById('searchField')
      scrollTo(((searchField.offsetTop + element.offsetTop + 200)), { duration: 500 })
    }
  }

  const _onKeyDown = (e) => {
    onTabKeyDown = e.key
  }

  return (
    <FormGroup className={cn(s.formGroup, s.cityField)}>
      {touched && error && (error !== ' ' || (submitFailed && active)) &&
        <div className={s.tooltipWraper}><Tooltip className='in' placement='top' id='tooltip-top'>
          {submitFailed && active && error === ' ' ? 'Oops, check if you have completed all fields' : error}
        </Tooltip></div>}
      {active && !value &&
        <label htmlFor='autosgst' className={s.placeholder}>{label}</label>}
      <AutosuggestField
        {...input}
        id='autosgst'
        value={displayedValue}
        getRef={getRef}
        onChange={_onChange}
        onBlur={_onBlur}
        onKeyDown={_onKeyDown}
        active={active}
        placeholder={placeholder}
        onFocus={_onFocus}
        departureName={departureName}
        getSuggestionValue={onChange}
      />
      <span className={s.cityCode}>{code}</span>
    </FormGroup>
  )
})

const SelectField = pure(({
  input,
  options,
  getSelectInput,
  placeholder,
  handleSubmit,
  pathname,
  changePassengersNumber
}) =>
  <FormGroup className={cn(s.formGroup)}>
    <Select
      {...input}
      ref={ref => ref && getSelectInput(ref)}
      className={cn('SearchField', s.select)}
      placeholder={placeholder}
      options={options}
      clearable={false}
      deleteRemoves={false}
      backspaceRemoves={false}
      searchable={false}
      openOnFocus
      isOpen={true}
      onChange={e => {
        input.onChange(e)
        changePassengersNumber(e.value)
        pathname === '/search' && e.value != input.value.value && handleSubmit()
      }}
    />
  </FormGroup>
)

const SearchField = pure(({
  rangePicker,
  oneWay,
  onChangeOrigin,
  getOutboundInput,
  changeFocusOnDateRange,
  changePassengersNumber,
  changeDateRange,
  changeDateSingle,
  resetDatePickers,
  changeFocusOnDateSingle,
  getDestinationInput,
  getSelectInput,
  handleSubmit,
  looseFocusPlaceField,
  togglePortal,
  portalOpened,
  searchAirports,
  departureName,
  locale,
  pathname,
}) =>
  <Form
    className={s.form}
    noValidate
    onSubmit={handleSubmit}
  >
    <div className={s.searchFieldWrap}>
      <div className={s.searchField} id='searchField'>
        <div className={cn(s.col, s.cityCol)} id='outbound_place'>
          <div>
            <span className={s.icon}>
              <InlineSVG src={MapPointIcon} />
            </span>
            <Field
              name='outbound_place'
              component={InputField}
              getRef={getOutboundInput}
              placeholder={I18n.t('SearchField.yourOrigin')}
              label={I18n.t('SearchField.typeInDeparture')}
              changeFocus={looseFocusPlaceField}
              onChangeOrigin={onChangeOrigin}
              showIcon={false}
              departureName={departureName}
              searchAirports={searchAirports}
            />
          </div>
        </div>
        <div className={cn(s.col, s.cityCol)} id='destination_place'>
          <div>
            <span className={s.icon}>
              <InlineSVG src={MapPointIcon} />
            </span>
            <Field
              name='destination_place'
              getRef={getDestinationInput}
              component={InputField}
              departureName={departureName}
              changeFocus={looseFocusPlaceField}
              placeholder={I18n.t('SearchField.yourDestination')}
              label={I18n.t('SearchField.typeInDestination')}
              showIcon={false}
              searchAirports={searchAirports}
            />
          </div>
        </div>
        {isSmallScreen ?
          <div className={cn(s.col, s.dateCol)} id='range_date'>
            <div>
              <span className={s.icon}>
                <InlineSVG src={CalendarIcon} />
              </span>
              <Field
                name='range_date'
                rangePicker={rangePicker}
                resetDatePickers={resetDatePickers}
                component={DatepickerWithPortal}
                oneWay={oneWay}
                togglePortal={togglePortal}
                portalOpened={portalOpened}
                changeFocusOnDateSingle={changeFocusOnDateSingle}
                changeFocusOnDateRange={changeFocusOnDateRange}
                changeDateSingle={changeDateSingle}
                changeDateRange={changeDateRange}
              />
            </div>
          </div>
          :
          <div className={cn(s.col, s.dateCol, s.dateColDesktop)} id='range_date'>
            <div>
              <Field
                name='range_date'
                rangePicker={rangePicker}
                component={oneWay ? SingleDatePickerField : DateRangePickerField}
                changeFocusOnDateSingle={changeFocusOnDateSingle}
                changeFocusOnDateRange={changeFocusOnDateRange}
                changeDateSingle={changeDateSingle}
                changeDateRange={changeDateRange}
              />
            </div>
          </div>
        }
        <div className={cn(s.col, s.simpleCol, s.passengerCount)}>
          <div>
            <span className={s.icon}>
              <InlineSVG src={PassengerIcon} />
            </span>
            <Field
              name='number_of_passengers'
              component={SelectField}
              locale={locale}
              placeholder={I18n.t('SearchField.passengerOption').replace('%{passenger}', 1)}
              changePassengersNumber={changePassengersNumber}
              options={passangerOptions}
              getSelectInput={getSelectInput}
              handleSubmit={handleSubmit}
              pathname={pathname}
            />
          </div>
        </div>

        <div className={cn(s.col, s.btnCol)}>
          <div>
            <button
              className={cn(s.btnSubmit)}
              type='button'
              onClick={handleSubmit}
            >
              <InlineSVG src={SearchIcon} />
              <span>{I18n.t('SearchField.search')}</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  </Form>
)

export default reduxForm({
  form: 'SearchField',
  enableReinitialize: true,
  validate,
  onSubmitFail,
})(SearchField)
