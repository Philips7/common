import React from 'react'
import s from './Slider.scss'
import cn from 'classnames'
import SlickSlider from 'react-slick'
import InlineSVG from 'svg-inline-react'
import ArrowRight from './assets/right-arrow.svg'
import ArrowLeft from './assets/left-arrow.svg'

const standardSettings = {
	// className: 'center',
	infinite: true,
	slidesToShow: 1,
	speed: 500,
	draggable: false,
	fade: true,
	dots: false,
	arrows: true,
	nextArrow: <InlineSVG src={ArrowRight}/>,
	prevArrow: <InlineSVG src={ArrowLeft}/>,
	responsive: [
		{breakpoint: 992, settings: {fade: false, draggable: true, arrows: false}}
	]
};

const withControlsSettings = {
	className: 'center',
	infinite: true,
	slidesToShow: 1,
	speed: 500,
	draggable: false,
	fade: true,
	dots: true,
	arrows: true,
	nextArrow: <InlineSVG src={ArrowRight}/>,
	prevArrow: <InlineSVG src={ArrowLeft}/>,
	responsive: [
		{breakpoint: 992, settings: {fade: false, draggable: true, arrows: false}}
	]
};

const twoSlidesSettings = {
	className: 'center',
	infinite: true,
	slidesToShow: 2,
	speed: 500,
	autoplay: true,
	draggable: true,
	dots: true,
	arrows: true,
	nextArrow: <InlineSVG src={ArrowRight}/>,
	prevArrow: <InlineSVG src={ArrowLeft}/>,
	responsive: [
		{breakpoint: 992, settings: {slidesToShow: 1, arrows: false}}
	]
};



const airlines = {
	infinite: true,
	slidesToShow: 5,
	speed: 500,
	autoplay: true,
	draggable: false,
	arrows: false,
	loop: true,
	responsive: [
		{breakpoint: 768, settings: {slidesToShow: 3}},
		{breakpoint: 992, settings: {slidesToShow: 4}}
	]
};

function renderChildren (type, children, settings) {
	switch (type) {
		case 'controls':
			return <SlickSlider {...withControlsSettings}>
				{children}
			</SlickSlider>
			break;
		case 'twoSlides':
			return <SlickSlider {...twoSlidesSettings}>
				{children}
			</SlickSlider>
			break;
		case 'airlines':
			return <SlickSlider {...airlines}>
				{children}
			</SlickSlider>
			break;
		case 'custom':
			return <SlickSlider {...settings}>
				{children}
			</SlickSlider>
			break;
		default:
			return <SlickSlider {...standardSettings}>
			{children}
		</SlickSlider>
	}
}

export const Slider = ({children, type, light, paginationOffset, settings}) => (
	<div className={cn(s.slider, light ? s.light : s.standard, paginationOffset && s.paginationOffset)}>
		{renderChildren(type, children, settings)}
	</div>
)

export default Slider
