import React from 'react'
import s from './Articles.scss'

import TheTimes from './assets/TheTimes.png'
import BusinessInsider from './assets/BusinessInsider.png'
import EveningStandard from './assets/EveningStandard.png'
import YahooNews from './assets/YahooNews.png'

const Articles = () => (
  <section className={s.ArticlesSection}>
    <div className={s.container}>
      <div className={s.col}>

        <div className={s.linksBlock}>
          <div className={s.linkWrap}>
            <a
              className={s.link}
              href='https://www.thetimes.co.uk/edition/travel/aman-hotel-opens-in-shanghai-t5vrgp7s6'
              target='_blank' rel='nofollow noreferrer noopener'
            >
              <img src={TheTimes} className={s.image} alt='TheTimes logo' />
            </a>
          </div>
          <div className={s.linkWrap}>
            <a
              className={s.link}
              href='https://www.standard.co.uk/lifestyle/travel/flymble-you-can-now-book-a-flight-and-pay-for-it-later-a3711821.html'
              target='_blank' rel='nofollow noreferrer noopener'
            >
              <img src={EveningStandard} className={s.image} alt='EveningStandard logo' />
            </a>
          </div>
          <div className={s.linkWrap}>
            <a
              className={s.link}
              href='http://www.businessinsider.com/you-can-now-book-a-flight-and-pay-for-it-later-2017-12?r=US&IR=T'
              target='_blank' rel='nofollow noreferrer noopener'
            >
              <img src={BusinessInsider} className={s.image} alt='BusinessInsider logo' />
            </a>
          </div>
          <div className={s.linkWrap}>
            <a
              className={s.link}
              href='https://uk.news.yahoo.com/platform-allows-book-flight-pay-111400420.html'
              target='_blank' rel='nofollow noreferrer noopener'
            >
              <img src={YahooNews} className={s.image} alt='YahooNews logo' />
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
)

export default Articles
