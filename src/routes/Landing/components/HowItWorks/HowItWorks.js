import React from 'react'
import s from './HowItWorks.scss'
import cn from 'classnames'
import {pure} from 'recompose'
import {Translate} from 'react-redux-i18n'
import InlineSVG from 'svg-inline-react'
import Slider from '../Slider/Slider'

import stepImage1 from './assets/step-1.svg'
import stepImage2 from './assets/step-2.svg'
import stepImage3 from './assets/step-3.svg'
import stepImage4 from './assets/step-4.svg'

const stepsArray = [
  {
    image: stepImage1,
    title: '1. Search a flight',
    description: 'Browse flights to anywhere at the lowest rates.'
  },
  {
    image: stepImage4,
    title: '2. Fill in information',
    description: 'Fill in your details and a credit check is performed. This' +
    ' takes just seconds.'
  },
  {
    image: stepImage2,
    title: '3. Pay a fraction upfront',
    description: 'Use a debit or credit card to pay the small amount upfront. Our upfront service fee ranges from 10.5% - 17%.'
  },
  {
    image: stepImage3,
    title: '4. Receive ticket',
    description: 'Pay the rest in a flexible, monthly payment plan. Choose between 3 - 10 months to pay back.'
  },
]

const sliderSettings = {
  speed: 500,
  accessibility: false,
  arrows: false,
  centerMode: false,
  infinite: false,
  autoplay: false,
  dots: false,
  slidesToShow: 4,
  draggable: false,
  swipe: false,
  touchMove: false,
  swipeToSlide: false,
  centerPadding: '15px',

  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: true,
        draggable: true,
        swipe: true,
        touchMove: true,
        swipeToSlide: true
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        dots: true,
        draggable: true,
        swipe: true,
        touchMove: true,
        swipeToSlide: true
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        draggable: true,
        swipe: true,
        touchMove: true,
        swipeToSlide: true
      }
    }
  ]
};

const HowItWorks = pure(({ slideInputRange, slideVal }) =>
  <section className={s.HowItWorksSection} id='how-it-works'>
    <div className={s.contentWrap}>
      <div className={s.container}>
        <div className={s.col}>
          <h1 className={s.sectionTitle}>How it works</h1>

          <div className={s.sliderWrap}>
            <Slider type='custom' settings={sliderSettings}>
              {
                stepsArray.map((el, i) =>
                  <div className={s.slide} key={i}>
                    <div className={s.slideContent}>
                      <div className={s.image}>
                        <InlineSVG src={el.image}/>
                      </div>
                      <h3 className={s.title}>{el.title}</h3>
                      <p className={s.description}>{el.description}</p>
                    </div>
                  </div>
                )
              }
            </Slider>
          </div>
        </div>
      </div>
    </div>
  </section>
)
export default HowItWorks
