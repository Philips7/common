import React from 'react'
import cn from 'classnames'
import { Translate } from 'react-redux-i18n'
import PerMonthIcon from 'static/images/landing/euro.svg'
import LocationIcon from 'static/images/landing/plane-icon.svg'
import Arrow from 'static/images/arrow-right.svg'
import InlineSVG from 'svg-inline-react'
import s from './FeaturedTrip.scss'

export const FeaturedTrip = ({
  trip,
  onBook,
  className,
  loadingFeatureFlights,
  changeSlideIndex }) => (
    <div className={cn(className, s.featuredTrip)}>

      <h4 className={s.title}><Translate value="FeaturedTrip.featuredTrip" /><i /></h4>
      <div className={s.container}>
        <div className={cn(s.col, s.smallCol)}>
          <div className={s.iconWrap}>
            <InlineSVG src={PerMonthIcon} />
          </div>
          <div className={s.info}>
            <span className={s.text}><Translate value="FeaturedTrip.price" />:</span>
            {loadingFeatureFlights ?
              <span className={s.skeletonPlaceholder}>
                <div className={s.load} />
              </span>
              :
              <span className={s.price}>{trip.price} EUR</span>
            }
          </div>
        </div>
        <div className={cn(s.col, s.cityCol)}>
          <div className={s.iconWrap}>
            <InlineSVG src={LocationIcon} />
          </div>
          <div className={s.info}>
            <span className={s.text}><Translate value="FeaturedTrip.location" />:</span>
            {loadingFeatureFlights ?
              <span className={s.skeletonPlaceholder}>
                <div className={s.load} />
              </span>
              :
              trip.destination_place
              && <span className={s.place}>{trip.destination_place.city}{trip.destination_place.city && trip.destination_place.country && ','} {trip.destination_place.country}</span>
            }
          </div>
        </div>
        <div className={s.controlsCol}>
          <InlineSVG src={Arrow} className={s.left} onClick={() => changeSlideIndex(-1)} />
          <InlineSVG src={Arrow} onClick={() => changeSlideIndex(1)} />
        </div>
        <div className={s.submitCol}>
          <button onClick={() => { onBook(trip) }} disabled={loadingFeatureFlights}><Translate value="FeaturedTrip.seeDetails" /></button>
        </div>
      </div>
    </div>
  )

export default FeaturedTrip
