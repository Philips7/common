import React from 'react'
import s from './CountryPresentation.scss'
import cn from 'classnames'
import PerMonthIcon from  'static/images/landing/euro.svg'
import PaybackIcon from  'static/images/landing/icon-payback.svg'
import InlineSVG from 'svg-inline-react'

import {I18n} from 'react-redux-i18n'

export const CountryPresentation = ({country, city, repaymentTime, monthPayment, persons, className}) => (
  <div className={cn(s.countyPresentation, className)}>
    <div className={s.infoCol}>
      <div>
        <div className={s.title}>
          {city}, <b>{country}</b>
        </div>
        <div className={s.info}>
          <span>{repaymentTime} {I18n.t('Landing.months')}</span> <InlineSVG src={PaybackIcon}/>
        </div>
      </div>
    </div>
    <div className={s.priceCol}>
      <div>
        <div className={s.info}><InlineSVG src={PerMonthIcon}/><span>{monthPayment} {I18n.t('Landing.perMonth')}</span></div>

        <ul className={s.personsList}>
          {persons.map((obj, i) =>
            <li key={i}>
                <img src={obj} alt='person photo'/>
            </li>
          )}
        </ul>

      </div>
    </div>
  </div>
)

export default CountryPresentation
