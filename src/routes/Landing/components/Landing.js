import React, { Component } from 'react'
import cn from 'classnames'
import { I18n, Translate } from 'react-redux-i18n'
import { Element } from 'react-scroll'
import { isSmallScreen } from 'utils/responsive'
import homepageBg0 from 'static/images/landing/flymble_bg.jpg'
import { SearchField } from './index'
import PreloaderOverlay from '../../../components/PreloaderOverlay'
import s from './Landing.scss'


const time1 = 3000
const time2 = 4000
const time3 = 500

const labels = [
  { text: `${I18n.t('Preloader.comparing')}`, time: time1 },
  { text: `${I18n.t('Preloader.preparing')}.`, time: time2 },
  { text: `${I18n.t('Preloader.finalizing')}..`, time: time3 },
  { text: `${I18n.t('Preloader.finalizing')}...`, time: time3 },
  { text: `${I18n.t('Preloader.finalizing')}....`, time: time3 },
  { text: `${I18n.t('Preloader.finalizing')}.....`, time: time3 },
  { text: `${I18n.t('Preloader.finalizing')}......`, time: time3 },
  { text: `${I18n.t('Preloader.finalizing')}.`, time: time3 },
  { text: `${I18n.t('Preloader.finalizing')}..`, time: time3 },
  { text: `${I18n.t('Preloader.finalizing')}...`, time: time3 },
  { text: `${I18n.t('Preloader.finalizing')}....`, time: time3 },
  { text: `${I18n.t('Preloader.finalizing')}.....`, time: time3 },
  { text: `${I18n.t('Preloader.finalizing')}......`, time: time3 },
]
const debug = require('debug')('app:Landing')

export default class Landing extends Component {

  constructor() {
    super()
    // this.state = {
    // 	slideIndex: -1
    // }
    this.homePageBackground = {}
    this.persons1 = []
    this.persons2 = []
  }

  componentWillMount() {
    this.props.unmountSearchField()
    if (__CLIENT__) {
      const {
        searchFeatureFlightsAttempt,
        searchAirportsConnect,
        pathname
      } = this.props
      searchAirportsConnect()
      searchFeatureFlightsAttempt(pathname)
    }
  }

  componentDidMount() {
    const { getFlightsFromPath, location: { query } } = this.props
    getFlightsFromPath(query)
    this.homePageBackground = { backgroundImage: `url('${homepageBg0}')` }
  }

  componentWillReceiveProps(nextProps) {
    const { searchFeatureFlightsAttempt, getFlightsFromPath, children, location: { query } } = this.props
    // init feature flights after changing route from /search to /
    !nextProps.Trips.searchResults.length && JSON.stringify(nextProps.location.query) !== '{}' && JSON.stringify(query) === '{}' && getFlightsFromPath(nextProps.location.query, true)
    !nextProps.children && children && searchFeatureFlightsAttempt('/')
  }

  componentWillUnmount() {
    this.props.searchAirportsDisconnect()
  }

  getSelectInput = input => this.selectInput = input
  getFromInput = input => this.fromInput = input
  getDestinationInput = input => this.destinationInput = input
  getOutboundInput = input => this.outboundInput = input

  changeFocusToDestinationField = () => this.destinationInput && this.destinationInput.focus()
  changeFocusOutboundField = () => this.outboundInput && this.outboundInput.focus()

  looseFocusPlaceField = destinationInput => {
    if (destinationInput) {
      this.destinationInput && this.destinationInput.blur(null)
      this.props.changeFocusOfSearchField('range_date')
    } else {
      this.outboundInput && this.outboundInput.blur(null)
      this.changeFocusToDestinationField()
    }
  }

  openSelectFiled = () => this.selectInput && this.selectInput.focus()
  changeDateRange = val => this.props.changeDateRange(val)

  renderSecondSection = () => {
    const LandingSecondSection = require('./LandingSecondSection').default
    const {
      signUpNewsletterAttempt, newsletterForm, showPaybackModal, hidePaybackModal, slideInputRange,
      Landing: { slideVal, interestRateLanding }, Trips: { featureFlights, loadingFeatureFlights }, cleanResults,
    } = this.props
    return (<LandingSecondSection
      signUpNewsletterAttempt={signUpNewsletterAttempt}
      newsletterForm={newsletterForm}
      slideInputRange={slideInputRange}
      slideVal={slideVal}
      interestRateLanding={interestRateLanding}
      showPaybackModal={showPaybackModal}
      hidePaybackModal={hidePaybackModal}
      cleanResults={cleanResults}
      featureFlights={featureFlights}
      loadingFeatureFlights={loadingFeatureFlights}
    />)
  }

  render() {
    const {
      changeOrigin,
      location: {
        pathname
      },
      Landing: {
        searchFieldValues,
        rangePicker,
        oneWay,
        portalOpened
      },
      Trips: {
        searchTripsLoading,
      },
      departureName,
      children,
      togglePortal,
      searchAirports,
      searchTripsSend,
      changeWayNumber,
      resetDatePickers,
      changeDateSingle,
      changeFocusOnDateRange,
      changeFocusOnDateSingle,
      changePassengersNumber,
      changeFocusOfSearchField,
      locale,
      searchFieldValue
    } = this.props

    return (
      <div className={s.Landing}>
        <div
          className={cn(s.firstSection, !children && s.full)}
          style={children ? null : this.homePageBackground}
        >
          {
            !children &&
            <h1 className={s.title} id='firstTitle'>
              {
                isSmallScreen
                  ? <Translate value='Landing.firstSectionTitleXS' dangerousHTML />
                  : <Translate value='Landing.firstSectionTitle' dangerousHTML />
              }
            </h1>
          }

          {
            !children &&
            <h2 className={s.subtitle}>
              {I18n.t('Landing.firstSectionSubTitle')}
            </h2>
          }

          {
            !children && <div className={s.landingOverlay} />
          }

          <SearchField
            ref='searchField'
            onChangeOrigin={changeOrigin}
            locale={locale}
            changeFocusOnDateRange={changeFocusOnDateRange}
            changeDateRange={this.changeDateRange}
            changeDateSingle={changeDateSingle}
            changeFocusOnDateSingle={changeFocusOnDateSingle}
            rangePicker={rangePicker}
            oneWay={oneWay}
            togglePortal={togglePortal}
            portalOpened={portalOpened}
            resetDatePickers={resetDatePickers}
            getDestinationInput={this.getDestinationInput}
            getOutboundInput={this.getOutboundInput}
            changeFocusToDestinationField={this.changeFocusToDestinationField}
            changeFocusOutboundField={this.changeFocusOutboundField}
            looseFocusPlaceField={this.looseFocusPlaceField}
            changeFocusOfSearchField={changeFocusOfSearchField}
            changePassengersNumber={changePassengersNumber}
            getSelectInput={this.getSelectInput}
            changeWayNumber={changeWayNumber}
            openSelectFiled={this.openSelectFiled}
            searchAirports={searchAirports}
            initialValues={{ ...searchFieldValue }}
            onSubmit={searchTripsSend}
            pathname={pathname}
            departureName={departureName}
          />

          <div id='landingScrollPoint' />

          {/*{!children &&<div className={s.scrollDownBlock}>*/}
          {/*<div className={s.scrollText}>Find out how it works!</div>*/}
          {/*<div className={s.scrollIconWrap}><InlineSVG src={IconArrow}/></div>*/}
          {/*</div>}*/}

          {/*{ !children &&*/}
          {/*<FeaturedTrip className={s.featuredTrip}*/}
          {/*trip={featureFlights[slideIndex]}*/}
          {/*loadingFeatureFlights={loadingFeatureFlights || !featureFlights.length || !featureFlights[slideIndex]}*/}
          {/*changeSlideIndex={this.changeSlideIndex}*/}
          {/*onBook={bookNowAttempt}*/}
          {/*/>*/}
          {/*}*/}
        </div>

        <Element name='howItWorksElement' />
        {children || this.renderSecondSection()}

        {!children && searchTripsLoading && <PreloaderOverlay labels={labels} />}
      </div>
    )
  }
}
