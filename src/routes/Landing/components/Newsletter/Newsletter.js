import React from 'react'
import {pure} from 'recompose'
import {reduxForm, Field} from 'redux-form'
import Form from 'react-bootstrap/lib/Form'
import validator from 'validator'
import {Translate, I18n} from 'react-redux-i18n'
import s from './Newsletter.scss'
import {
  InputFieldGroup,
} from 'components/FieldGroup'

import EnvelopeIcon from 'static/images/envelope-icon.svg'
import InlineSVG from 'svg-inline-react'

const validate = values => {
  let errors = {}
  if (!values.email) {
    errors.email = <Translate value='form.required'/>
  } else if (!validator.isEmail(values.email)) {
    errors.email = <Translate value='form.invalidEmailError'/>
  }
  return errors
}

const NewsletterForm = pure(({handleSubmit, valid}) =>
  <Form
    onSubmit={handleSubmit}
    className={s.form}
  >
    <div className={s.newsletterForm}>
      <div className={s.fieldWrap}>
				<InlineSVG src={EnvelopeIcon}/>
        <Field
          component={InputFieldGroup}
          name='email'
          placeholder={I18n.t('Landing.email')}
          type='email'
          absoluteValidation
        />
      </div>
      <div>
        <button
          type='submit'
          disabled={!valid}
          className={s.submitBtn}
        >
          {I18n.t('Landing.signUp')}
        </button>
      </div>
    </div>
  </Form>
)

export default reduxForm({
  form: 'NewsletterForm',
  validate
})(NewsletterForm)
