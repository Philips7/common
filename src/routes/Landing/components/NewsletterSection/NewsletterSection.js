import React from 'react'
import s from './NewsletterSection.scss'
import {I18n} from 'react-redux-i18n'
import {Newsletter} from '../index'

const NewsletterSection = ({signUpNewsletterAttempt}) => (
	<section className={s.newsletterSection}>
		<h2 className={s.titleXl}>{I18n.t('Landing.newsletter')}</h2>
		<h4 className={s.smalltitle}>{I18n.t('Landing.signUpForNewsletter')}</h4><Newsletter onSubmit={signUpNewsletterAttempt}/>
	</section>
)

export default NewsletterSection
