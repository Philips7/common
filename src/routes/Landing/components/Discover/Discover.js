import React from 'react'
import s from './Discover.scss'
import { Translate } from 'react-redux-i18n'
import pure from 'recompose/pure'
import { PaybackUnavailableModal } from '../index'
import InlineSVG from 'svg-inline-react'
import { Link } from 'react-router'
import { showFeaturedPrice } from '../../../../utils/helpers'

import pinIcon from './assets/pin.svg'
import FijiCover from './assets/FijiCover.jpg'
import HawaiiCover from './assets/Hawaii.jpg'
import LosAngelesCover from './assets/LosAngeles.jpg'
import NewYorkCover from './assets/NewYork.jpg'

const discoveryBlocks = [
  {
    title: 'Discover the Undiscovered',
    text: null,
    link: '/search?destination_place=BOG&departureTakeOff=true&departureTime=0&departureTime=1439&number_of_months=10&departureTimeLanding=0&departureTimeLanding=1439&number_of_passengers=1&oneWay=false&outbound_place=London&returnTakeOff=true&returnTime=0&returnTime=1439&returnTimeLanding=0&returnTimeLanding=1439&sortBy=0&stopover=0&stopover=25&stops&totalDuration=60',
    city: 'Bogota',
    country: 'Colombia',
    code: 'BOG',
    background: HawaiiCover
  },
  {
    title: 'Golden City by The Bay',
    text: null,
    link: '/search?destination_place=San+Francisco&departureTakeOff=true&departureTime=0&departureTime=1439&number_of_months=10&departureTimeLanding=0&departureTimeLanding=1439&number_of_passengers=1&oneWay=false&outbound_place=London&returnTakeOff=true&returnTime=0&returnTime=1439&returnTimeLanding=0&returnTimeLanding=1439&sortBy=0&stopover=0&stopover=25&stops&totalDuration=60',
    city: 'San Francisco, California',
    country: 'USA',
    code: 'SJC',
    code2: 'SFO',
    background: FijiCover
  },
  {
    title: 'The Magical New York',
    text: null,
    link: '/search?destination_place=New+York&departureTakeOff=true&departureTime=0&departureTime=1439&number_of_months=10&departureTimeLanding=0&departureTimeLanding=1439&number_of_passengers=1&oneWay=false&outbound_place=London&returnTakeOff=true&returnTime=0&returnTime=1439&returnTimeLanding=0&returnTimeLanding=1439&sortBy=0&stopover=0&stopover=25&stops&totalDuration=60',
    city: 'New York City, New York',
    country: 'USA',
    code: 'JFK',
    code2: 'LGA',
    background: NewYorkCover
  },
  {
    title: 'Fabulous <br/> Los Angeles',
    text: null,
    link: '/search?destination_place=LAX&departureTakeOff=true&departureTime=0&departureTime=1439&number_of_months=10&departureTimeLanding=0&departureTimeLanding=1439&number_of_passengers=1&oneWay=false&outbound_place=London&returnTakeOff=true&returnTime=0&returnTime=1439&returnTimeLanding=0&returnTimeLanding=1439&sortBy=0&stopover=0&stopover=25&stops&totalDuration=60',
    city: 'Los Angeles, California',
    country: 'USA',
    code: 'LAX',
    background: LosAngelesCover
  }
]

const Discover = pure(({ signUpNewsletterAttempt, hidePaybackModal, cleanResults, featureFlights }) =>
  <section className={s.DiscoverSection}>
    <div className={s.container}>
      <h1 className={s.sectionTitle}><Translate value='Discover.possibilities' />
      </h1>
      <div className={s.cardsWrap}>
        {
          discoveryBlocks.map(({ title, text, link, city, country, background, code, code2 }, i) =>
            <Link to={link} onClick={cleanResults} key={i} className={s.card}>
              <div className={s.coverImageContainer}>
                <div
                  className={s.coverImage}
                  style={{ backgroundImage: `url(${background})` }}
                />
              </div>
              <div className={s.priceWrap}>
                <div className={s.price}>{!!featureFlights.length && '£'}{showFeaturedPrice(featureFlights, code, code2)}</div>
                <div className={s.priceText}>Upfront</div>
              </div>
              <div className={s.info}>
                {
                  title &&
                  <h2 className={s.title} dangerouslySetInnerHTML={{__html: title}}/>
                }
                {
                  city && country &&
                  <div className={s.location}>
                    <span className={s.icon}><InlineSVG src={pinIcon}/></span>
                    <span>{city}, {country}</span>
                  </div>
                }
                {
                  text &&
                  <p className={s.description}>{text}</p>
                }
              </div>
            </Link>
          )
        }
      </div>
    </div>
    <PaybackUnavailableModal fromLanding
      hidePaybackModal={hidePaybackModal}
      onSubmit={signUpNewsletterAttempt}
    />
  </section>
)

export default Discover
