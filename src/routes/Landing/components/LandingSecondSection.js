import React from 'react'
import { WhyFlymble, HowItWorks, Discover, UserStories, NewsletterSection, Articles } from './index'

const SecondSection = ({
  signUpNewsletterAttempt,
  interestRateLanding,
  showPaybackModal,
  hidePaybackModal,
  slideInputRange,
  newsletterForm,
  featureFlights,
  loadingFeatureFlights,
  slideVal,
  cleanResults,
  }) =>
    <div>
      <HowItWorks
        slideInputRange={slideInputRange}
        slideVal={slideVal}
        interestRateLanding={interestRateLanding}
      />
      <Articles />
      <Discover
        signUpNewsletterAttempt={signUpNewsletterAttempt}
        newsletterForm={newsletterForm}
        showPaybackModal={showPaybackModal}
        hidePaybackModal={hidePaybackModal}
        cleanResults={cleanResults}
        featureFlights={featureFlights}
        loadingFeatureFlights={loadingFeatureFlights}
      />
      <UserStories />
      <WhyFlymble />
      {
      // <NewsletterSection signUpNewsletterAttempt={signUpNewsletterAttempt}/>
    }
    </div>

export default SecondSection
