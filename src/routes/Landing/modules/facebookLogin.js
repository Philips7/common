import {push} from 'react-router-redux/lib/actions'
import config from 'config.json'
import {signUpFacebookAttempt} from '../../../redux/modules/User'
const findParam = name => new RegExp(`(${name}=[^&]*&?)`, 'gm')

export const facebookLogin = () => (dispatch, getState) => {
  // Async init
  window.fbAsyncInit = () => {
    window.FB.init({
      version: `v${2.3}`,
      appId: config.facebook.app_key,
      xfbml: false,
      cookie: false,
    })

    window.FB.login(({authResponse}) => {
      const {state} = getState().routing.locationBeforeTransitions.query

      if (window) {
        const url = window.location.href
          .replace(findParam('code'), '')
          .replace(findParam('state'), '')
          .replace('/', '')

        dispatch(push(url))
      }

      if (state === 'facebookdirect') {
        dispatch(signUpFacebookAttempt(authResponse))
      }
    }, {scope: 'public_profile,email'})
  }

  // Load SDK
  ((d, s, id) => {
    const element = d.getElementsByTagName(s)[0]
    const fjs = element
    let js = element
    if (d.getElementById(id)) {
      return
    }
    js = d.createElement(s)
    js.id = id
    js.src = '//connect.facebook.net/en_US/all.js'
    fjs.parentNode.insertBefore(js, fjs)
  })(document, 'script', 'facebook-jssdk')
}
