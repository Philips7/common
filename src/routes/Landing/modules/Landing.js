import { change, touch, untouch, reset } from 'redux-form'
import moment from 'moment'
import { show, hide } from 'redux-modal'
import { I18n } from 'react-redux-i18n'
import { push } from 'react-router-redux/lib/actions'
import { isEmpty, anywhereCheck, anywhereObj, anywhere } from '../../../utils/helpers'
import createReducer from '../../../utils/createReducer'
import { errorNotification } from '../../../utils/notifications'
import { checkTrip } from '../../../redux/modules/Ticket'
import { selectPayback } from '../../../redux/modules/Trips'
import { facebookLogin } from './facebookLogin'
import passengerConfig from '../../../utils/passenger.json'
import {
  updateFromUrl,
  searchFeatureFlightsSend,
  searchFromUrl,
  FROM_URL,
  resetTempFilters,
} from '../../../redux/modules/Trips'

const enabledPassengers = passengerConfig.passenger.map(({ value }) => value.toString())

// ------------------------------------
// Constants
// ------------------------------------
export const SELECT_TRIP = 'Landing.SELECT_TRIP'
export const CLEAR_TRIP = 'Landing.CLEAR_TRIP'
export const SLIDE_INPUT_RANGE = 'Landing.SLIDE_INPUT_RANGE'

export const CLOSE_DROPDOWN = 'Landing.CLOSE_DROPDOWN'
export const CHANGE_TAB_NUMBER = 'Landing.CHANGE_TAB_NUMBER'

export const TOGGLE_RANGE_PICKER = 'Landing.TOGGLE_RANGE_PICKER'
export const TOGGLE_SINGLE_PICKER = 'Landing.TOGGLE_SINGLE_PICKER'

export const CHANGE_RANGE_PICKER = 'Landing.CHANGE_RANGE_PICKER'
export const CHANGE_SINGLE_PICKER = 'Landing.CHANGE_SINGLE_PICKER'
export const RESET_DATE_PICKERS = 'LANDING.RESET_DATE_PICKERS'

export const SEARCH_AIRPORTS_CONNECT = 'Landing.SEARCH_AIRPORTS_CONNECT'
export const SEARCH_AIRPORTS_DISCONNECT = 'Landing.SEARCH_AIRPORTS_DISCONNECT'
export const SEARCH_AIRPORTS_SEND = 'Landing.SEARCH_AIRPORTS_SEND'
export const SEARCH_AIRPORTS_EMIT = 'Landing.SEARCH_AIRPORTS_EMIT'

export const CLEAR_SUGGESTIONS = 'Landing.CLEAR_SUGGESTIONS'
export const CHANGE_ORIGIN = 'Landing.CHANGE_ORIGIN'
export const CHANGE_WAY_NUMBER = 'Landing.CHANGE_WAY_NUMBER'

export const SCROLL_UP = 'Landing.SCROLL_UP'

export const INIT_SEARCH_FIELD = 'Landing.INIT_SEARCH_FIELD'
export const FORGOT_ABOUT_TICKET = 'Landing.FORGOT_ABOUT_TICKET'

export const UNMOUNT_SEARCH_FIELD = 'Landing.UNMOUNT_SEARCH_FIELD'
export const TOGGLE_PORTAL = 'Landing.TOGGLE_PORTAL'
// ------------------------------------
// Actions
// ------------------------------------
export const searchAirportsConnect = () => dispatch => dispatch({
  type: SEARCH_AIRPORTS_CONNECT,
  dispatch
})

export const searchAirportsDisconnect = () => ({
  type: SEARCH_AIRPORTS_DISCONNECT
})

export const searchAirports = data => ({
  type: SEARCH_AIRPORTS_SEND,
  values: {
    data,
  },
})

export const refreshAirportsConnection = () => (dispatch, getState) => {
  const { airportWebsocketConnected } = getState().Landing
  dispatch(searchAirportsDisconnect())
  airportWebsocketConnected && dispatch(searchAirportsConnect())
}

export const onSearchAirportsConnect = () => (dispatch, getState) => {
  const { form: { SearchField }, Trips: { onConnectBehaviour } } = getState()
  if (onConnectBehaviour === FROM_URL && SearchField && SearchField.values) {
    const { outbound_place, destination_place } = SearchField.values
    dispatch(searchAirports({ search_string: outbound_place }))
    !anywhereCheck(destination_place) && dispatch(searchAirports({ search_string: destination_place }))
  }
}

export const clearSuggestions = () => ({
  type: CLEAR_SUGGESTIONS
})

const place_regexp = place => new RegExp(place, 'i')

export const initPlaceInSearchField = data => (dispatch, getState) => {
  const {
    form: {
      SearchField: {
        values: {
          outbound_place,
    destination_place,
        }
      }
    },
    Trips: { onConnectBehaviour }
  } = getState()
  if (onConnectBehaviour === FROM_URL) {
    if (data.length > 0) {
      if (outbound_place && typeof outbound_place !== 'object') {
        let outboundPlaceIndex = data.findIndex(({ code }) => code === outbound_place)
        if (outboundPlaceIndex === -1) {
          outboundPlaceIndex = data.findIndex(({ name }) => place_regexp(outbound_place).test(name))
        }
        dispatch(initFieldSearchField('outbound_place', data[outboundPlaceIndex]))
      }

      if (destination_place && typeof destination_place !== 'object') {
        let destinationPlaceIndex = data.findIndex(({ code }) => code === destination_place)
        if (destinationPlaceIndex === -1) {
          destinationPlaceIndex = data.findIndex(({ name }) => place_regexp(destination_place).test(name))
        }
        dispatch(initFieldSearchField('destination_place', data[destinationPlaceIndex]))
      }

      // Trick for init from hash. outbound_place has already taken and destination_place will have init in this action
      // (outbound_place) && !destination_place is needed for anywhere search from url
      if ((outbound_place && anywhereCheck(destination_place.name)) ||
        ((outbound_place && typeof outbound_place === 'object') && (destination_place && typeof destination_place === 'string')))
        dispatch(searchFromUrl())

    } else {
      dispatch(reset('SearchField'))
      dispatch(push('/'))
      dispatch(errorNotification(I18n.t('Notify.invalidUrl')))
    }
  }
}

export const searchAirportsEmit = response => (dispatch, getState) => {
  const { Trips: { onConnectBehaviour }, form: { SearchField: { values: { destination_place } } } } = getState()
  const message = JSON.parse(response.data)
  if (message.status === 'success' && message.data && Array.isArray(message.data)) {
    if (!(onConnectBehaviour === FROM_URL) && (anywhere.toLowerCase().indexOf(destination_place) >= 0) ||
      (destination_place && anywhereCheck(destination_place.name))) {
      message.data[message.data.length] = anywhereObj
    }
    onConnectBehaviour === FROM_URL && dispatch(initPlaceInSearchField(message.data))
    dispatch({ type: SEARCH_AIRPORTS_EMIT, suggestions: message.data })
  } else if (message.status === 'error') {
    dispatch(clearSuggestions())
  }
}

export const forgetAboutTicket = () => ({ type: FORGOT_ABOUT_TICKET })

export const selectTrip = trip => (dispatch, getState) => {
  const { form: { SearchField: { values } }, Trips: { paybackSelected } } = getState()
  const { booking_token } = trip
  trip.document_need = true
  trip.baggage_info = {}
  trip.paybackSelected = paybackSelected
  dispatch({
    type: SELECT_TRIP,
    searchFieldValues: values,
    booking_token
  })
  dispatch(checkTrip(trip))
}

export const changeOrigin = () => (dispatch, getState) => {
  const { form: { SearchField: { values } } } = getState()
  if (values) {
    const { destination_place, outbound_place } = values
    dispatch(change('SearchField', 'outbound_place', destination_place || ''))
    dispatch(change('SearchField', 'destination_place', outbound_place || ''))
  }
}

export const initFieldSearchField = (name, value) => change('SearchField', name, value)

export const initSearchField = ({ outbound_place, destination_place, range_date, number_of_passengers, oneWay }) => dispatch => {
  dispatch(initFieldSearchField('outbound_place', outbound_place))
  dispatch(destination_place ? initFieldSearchField('destination_place', destination_place) :
    initFieldSearchField('destination_place', anywhereObj))
  dispatch(initFieldSearchField('oneWay', oneWay))
  dispatch(changeWayNumber(oneWay))
  dispatch(initFieldSearchField('range_date', range_date))
  dispatch(moment.isMoment(range_date) ? changeDateSingle(range_date) :
    changeDateRange(range_date))
  dispatch(initFieldSearchField('number_of_passengers', {
    value: number_of_passengers,
    label: I18n.t('SearchField.passengerOption', { passenger: number_of_passengers })
  }))
  dispatch({ type: INIT_SEARCH_FIELD })
}

export const getFlightsFromPath = (tripHash, shouldSearch = false) => dispatch => {
  if (!isEmpty(tripHash)) {
    const { hjVerifyInstall, code, month, outbound_place, destination_place, outbound_date, return_date, number_of_passengers, number_of_months, oneWay, ...filters } = tripHash
    const convertedOneWay = oneWay == 'false'
    if (enabledPassengers.indexOf(number_of_passengers) >= 0 && outbound_place && convertedOneWay) {
      dispatch(initSearchField({
        outbound_place: outbound_place && outbound_place.toUpperCase(),
        destination_place: destination_place && destination_place.toUpperCase(),
        range_date: ({
          startDate: outbound_date && moment(outbound_date, 'YYYY-MM-DD'),
          endDate: convertedOneWay ? return_date && moment(return_date, 'YYYY-MM-DD') : null
        }),
        oneWay: !convertedOneWay,
        number_of_passengers: number_of_passengers || 1,
      }))
      dispatch(updateFromUrl(tripHash, filters))
      dispatch(selectPayback(+number_of_months))
      shouldSearch && dispatch(onSearchAirportsConnect())
    } else if (code) {
      dispatch(facebookLogin())
    } else if (hjVerifyInstall) {
      __DEV__ && console.info('Hotjar Installed')
    } else {
      dispatch(push('/'))
      dispatch(errorNotification(I18n.t('Notify.invalidUrl')))
    }
  }
}

export const searchFeatureFlightsAttempt = pathname => dispatch => {
  pathname !== '/search' && dispatch(searchFeatureFlightsSend())
}

export const changeFocusOfSearchField = nameOfField => (dispatch, getState) => {
  const { Landing: { oneWay } } = getState()
  if (/^((?!date).)*$/.test(nameOfField)) {
    dispatch(touch('SearchField', nameOfField))
  } else {
    dispatch(oneWay ? changeFocusOnDateSingle({ focused: true })
      : changeFocusOnDateRange('startDate'))
  }
}

export const untouchFieldInSearchField = nameOfField => dispatch => dispatch(untouch('SearchField', nameOfField))

export const scrollUp = backPosition => ({
  type: SCROLL_UP,
  backPosition
})

export const changeFocusOnDateRange = focusedInput => (dispatch) => {
  dispatch({ type: TOGGLE_RANGE_PICKER, focusedInput })
}

export const changeDateRange = ({ startDate, endDate }) => ({
  type: CHANGE_RANGE_PICKER, startDate, endDate
})

export const changeFocusOnDateSingle = ({ focused }) => (dispatch) => {
  dispatch({ type: TOGGLE_SINGLE_PICKER, focused })
}

export const changeDateSingle = startDate => ({ type: CHANGE_SINGLE_PICKER, startDate })

export const unmountSearchField = () => ({ type: UNMOUNT_SEARCH_FIELD })

export const changeWayNumber = oneWay => ({
  type: CHANGE_WAY_NUMBER,
  oneWay
})

export const closeDropdown = dropClosed => (dispatch) => {
  dispatch({
    type: CLOSE_DROPDOWN,
    dropClosed
  })
}

export const changeTab = tabNumber => ({
  type: CHANGE_TAB_NUMBER,
  tabNumber
})

const interestRateOption = (slideVal) => {
  const val = {
    3: 4,
    6: 6,
    9: 10,
    12: 14
  }
  return { interestRateLanding: val[slideVal] }
}

export const slideInputRange = slideVal => ({
  type: SLIDE_INPUT_RANGE,
  slideVal,
  ...interestRateOption(slideVal)
})

export const resetDatePickers = () => dispatch => {
  dispatch(change('SearchField', 'range_date', {}))
  dispatch({
    type: RESET_DATE_PICKERS,
  })
}

export const showPaybackModal = () => show('paybackUnavailableModal')
export const hidePaybackModal = () => hide('paybackUnavailableModal')

export const togglePortal = portalOpened => ({
  type: TOGGLE_PORTAL,
  portalOpened
})
// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  oneWay: false,
  searchFieldValues: {
    oneWay: false,
    number_of_passengers: {
      value: 1,
    },
  },
  suggestions: [],
  airportWebsocketConnected: false,
  backPosition: 0,
  dropClosed: true,
  tabNumber: 4,
  slideVal: 9,
  portalOpened: true,
  interestRateLanding: 10,
  rangePicker: {
    focusedInput: null,
    startDate: null,
    endDate: null,
    focused: null
  }
}

export default createReducer(initialState, {
  [SELECT_TRIP]: (state, { booking_token, searchFieldValues }) => ({
    booking_token,
    searchFieldValues
  }),
  [SCROLL_UP]: (state, { backPosition }) => ({ backPosition }),
  [CLOSE_DROPDOWN]: (state, { dropClosed }) => ({ dropClosed }),
  [CHANGE_TAB_NUMBER]: (state, { tabNumber }) => ({ tabNumber }),
  [SLIDE_INPUT_RANGE]: (state, { slideVal, interestRateLanding }) => ({ slideVal, interestRateLanding }),
  [CLEAR_SUGGESTIONS]: (state, action) => ({
    suggestions: [anywhereObj]
  }),
  [SEARCH_AIRPORTS_EMIT]: (state, { suggestions }) => ({ suggestions }),
  [TOGGLE_RANGE_PICKER]: (state, { focusedInput }) => ({
    rangePicker: {
      ...state.rangePicker,
      focusedInput
    }
  }),
  [TOGGLE_SINGLE_PICKER]: (state, { focused }) => ({
    rangePicker: {
      ...state.rangePicker,
      focused
    }
  }),
  [CHANGE_RANGE_PICKER]: (state, { startDate, endDate }) => ({
    rangePicker: {
      ...state.rangePicker,
      startDate,
      endDate
    }
  }),
  [CHANGE_SINGLE_PICKER]: (state, { startDate }) => ({
    rangePicker: {
      ...state.rangePicker,
      startDate
    }
  }),
  [FORGOT_ABOUT_TICKET]: () => ({ booking_token: null }),
  [CHANGE_WAY_NUMBER]: (state, { oneWay }) => ({ oneWay }),
  [TOGGLE_PORTAL]: (state, { portalOpened }) => ({ portalOpened }),
  [SEARCH_AIRPORTS_CONNECT]: () => ({ airportWebsocketConnected: true }),
  [SEARCH_AIRPORTS_DISCONNECT]: () => ({ airportWebsocketConnected: false }),
  [RESET_DATE_PICKERS]: () => ({ rangePicker: initialState.rangePicker }),
  [UNMOUNT_SEARCH_FIELD]: () => initialState
})
