import { take, call, put, fork, cancel } from 'redux-saga/effects'
import { eventChannel, END } from 'redux-saga'
import { ws } from 'services/api'

import {
  SEARCH_AIRPORTS_CONNECT,
  SEARCH_AIRPORTS_DISCONNECT,
  SEARCH_AIRPORTS_SEND,
  onSearchAirportsConnect,
  searchAirportsEmit,
  refreshAirportsConnection
} from './Landing'


export default () => {
  function* connect(dispatch) {
    const socket = ws('flights/airports/search')

    socket.onopen = () => {
      dispatch(onSearchAirportsConnect())
    }

    socket.onclose = () => {
      dispatch(refreshAirportsConnection())
    }

    return socket
  }

  const subscribe = socket => {
    return eventChannel(emit => {
      socket.onmessage = response => {
        emit(searchAirportsEmit(response))
      }
      return () => {
        emit(END)
      }
    })
  }

  function* emit(socket) {
    const channel = yield call(subscribe, socket)
    while (true) {
      const action = yield take(channel)
      yield put(action)
    }
  }

  function* send(socket) {
    while (true) {
      const { values } = yield take(SEARCH_AIRPORTS_SEND)
      if (socket.readyState === socket.OPEN) {
        const lowerValues = JSON.stringify(values).toLowerCase()
        socket.send(lowerValues)
      }
    }
  }

  function* handleIO(socket) {
    yield fork(emit, socket)
    yield fork(send, socket)
  }

  function* watcher() {
    while (true) {
      const { dispatch } = yield take(SEARCH_AIRPORTS_CONNECT)

      const socket = yield call(connect, dispatch)
      const io = yield fork(handleIO, socket)

      yield take(SEARCH_AIRPORTS_DISCONNECT)
      socket.close()
      yield cancel(io)
    }
  }

  return {
    watcher
  }
}
