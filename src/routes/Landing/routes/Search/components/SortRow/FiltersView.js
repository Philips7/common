import React from 'react'
import Grid from 'react-bootstrap/lib/Grid'
import cn from 'classnames'
import { pure, withHandlers, compose } from 'recompose'
import s from './SortRow.scss'
import Dropdown from 'components/Dropdown'
import { DropdownTrigger, DropdownContent } from 'react-simple-dropdown'
import CaretDown from 'static/images/carret-dropdown.svg'
import InlineSVG from 'svg-inline-react'
import { I18n } from 'react-redux-i18n'
import { sortByStops, sortByTypes } from '../../../../../../utils/filters'
import { PaybackFilter } from './PaybackFilter/PaybackFilter'
import { DurationFilter } from './DurationFilter/DurationFilter'
import { StopsFilter } from './StopsFilter/StopsFilter'
import { TypesFilter } from './TypesFilter/TypesFilter'
import { isDefaultFilter } from '../../../../../../utils/helpers'


const composedHOC = compose(
  withHandlers({
    onHideHandler: ({ closeDropdown }) => () => {
      closeDropdown(true)
    },
    onShowHandler: ({ closeDropdown }) => () => {
      closeDropdown()
    },
  }),
  pure
)

export default composedHOC(({
  filters: {
    stops,
  sortBy,
  totalDuration,
  stopover,
  departureTime,
  returnTime,
  departureTakeOff,
  returnTakeOff
    },
  filters,
  selectPayback,
  paybackSelected,
  searchTripsWithFiltersSend,
  saveFilters,
  submitFilters,
  closeDropdown,
  updateFilters,
  dropClosed,
  disabled,
  onHideHandler,
  onShowHandler
  }) => {
  const isDefaultDuration = isDefaultFilter(totalDuration, 'totalDuration')
  const isDefaultStopover = isDefaultFilter(stopover, 'stopover')
  const isDefaultDepartureTime = isDefaultFilter(departureTime, 'departureTime')
  const isDefaultReturnTime = isDefaultFilter(returnTime, 'returnTime')
  const isDefaultDepartureTakeOff = isDefaultFilter(departureTakeOff, 'departureTakeOff')
  const isDefaultReturnTakeOff = isDefaultFilter(returnTakeOff, 'returnTakeOff')

  const isDefaultTimeFilter = isDefaultDepartureTime && isDefaultReturnTime && isDefaultDepartureTakeOff && isDefaultReturnTakeOff
  const isDefaultDurationFilter = isDefaultDuration && isDefaultStopover
  const isDefaultStopsFilter = typeof stops === 'number'
  return (
    <div className={cn(s.sortRow)}>
      <Grid>
        <div className={s.sortElementsWrap}>
          <div className={s.filtersWrap}>

            <div className={s.paybackFilterWrap}>
              <Dropdown
                className={cn(s.filterDropdown, s.paybackBackground)}
                onHide={onHideHandler}
                onShow={onShowHandler}
                dropClosed={dropClosed}
                disabled={disabled}
              >
                <DropdownTrigger className={s.filterTrigger}>
                  <span className={s.paybackBackground}>{`${paybackSelected} Payback months`}</span>
                  <InlineSVG src={CaretDown} />
                </DropdownTrigger>
                <DropdownContent className={s.dropdownContentLeft}>
                  <PaybackFilter
                    sortBy={sortBy}
                    closeDropdown={closeDropdown}
                    disabled={disabled}
                    selectPayback={selectPayback}
                    paybackSelected={paybackSelected}
                  />
                </DropdownContent>
              </Dropdown>
            </div>

            <ul className={s.filtersList}>

              <li className={cn(s.listItem, s.changed)}>
                <Dropdown
                  className={s.filterDropdown}
                  onHide={onHideHandler}
                  onShow={onShowHandler}
                  dropClosed={dropClosed}
                  disabled={disabled}
                >
                  <DropdownTrigger className={s.filterTrigger}>
                    <span>
                      {I18n.t(`Landing.${sortByTypes[sortBy].name}`)}
                    </span>
                    <InlineSVG src={CaretDown} />
                  </DropdownTrigger>
                  <DropdownContent className={s.dropdownContentLeft}>
                    <TypesFilter
                      sortBy={sortBy}
                      closeDropdown={closeDropdown}
                      disabled={disabled}
                      searchTripsWithFiltersSend={searchTripsWithFiltersSend}
                    />
                  </DropdownContent>
                </Dropdown>
              </li>

              <li className={cn(s.listItem, isDefaultStopsFilter && s.changed)}>
                <Dropdown
                  className={s.filterDropdown}
                  onHide={onHideHandler}
                  onShow={onShowHandler}
                  dropClosed={dropClosed}
                  disabled={disabled}
                >
                  <DropdownTrigger className={s.filterTrigger}>
                    <span>{I18n.t(`Landing.${typeof stops === 'object' ? 'Stops' : sortByStops[1 + +stops].name}`)}</span>
                    &nbsp;
                    <InlineSVG src={CaretDown} />
                  </DropdownTrigger>
                  <DropdownContent className={s.dropdownContentLeft}>
                    <StopsFilter
                      searchTripsWithFiltersSend={searchTripsWithFiltersSend} //ToDo: refactor
                      closeDropdown={closeDropdown}
                      filters={filters}
                    />
                  </DropdownContent>
                </Dropdown>
              </li>

              <li className={cn(s.listItem, !isDefaultDurationFilter && s.changed)}>
                <Dropdown
                  className={s.filterDropdown}
                  onHide={onHideHandler}
                  onShow={onShowHandler}
                  dropClosed={dropClosed}
                  disabled={disabled}
                >
                  <DropdownTrigger className={s.filterTrigger}>
                    <span>{I18n.t('Landing.Duration')}</span>&nbsp;<InlineSVG
                      src={CaretDown}
                    />
                  </DropdownTrigger>
                  <DropdownContent className={s.dropdownContentRight}>
                    <DurationFilter
                      filters={filters}
                      saveFilters={saveFilters}
                      closeDropdown={closeDropdown}
                      updateFilters={updateFilters}
                      submitFilters={submitFilters}
                      searchTripsWithFiltersSend={searchTripsWithFiltersSend} //ToDo: refactor
                    />
                  </DropdownContent>
                </Dropdown>
              </li>

              {/*<li className={cn(s.listItem, !isDefaultTimeFilter && s.changed)}>
                <Dropdown
                  className={s.filterDropdown}
                  onHide={onHideHandler}
                  onShow={onShowHandler}
                  dropClosed={dropClosed}
                  disabled={disabled}
                >
                  <DropdownTrigger className={s.filterTrigger}>
                    <span>{I18n.t('Landing.arrivalAndDeparture')}</span>&nbsp;
                    <InlineSVG src={CaretDown}/>
                  </DropdownTrigger>
                  <DropdownContent className={s.dropdownContentRight}>
                    <TimeFilter
                      registeredFilters={registeredFilters}
                      filters={filters}
                      oneWay={oneWay}
                      closeDropdown={closeDropdown}
                      updateFilters={updateFilters}
                      submitFilters={submitFilters}
                      saveFilters={saveFilters}
                      searchTripsWithFiltersSend={searchTripsWithFiltersSend} //ToDo: refactor
                    />
                  </DropdownContent>
                </Dropdown>
              </li>*/}
            </ul>
          </div>
        </div>
      </Grid>
    </div>
  )
})
