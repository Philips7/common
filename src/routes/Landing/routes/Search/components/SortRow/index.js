import React from 'react'
import { pure } from 'recompose'
import { isTabletScreen } from '../../../../../../utils/responsive'
import FiltersView from './FiltersView'
import SmFiltersView from './SmFiltersView'

const Filters = isTabletScreen ? SmFiltersView : FiltersView

export default pure(props => <Filters id='SortRowPage' key='SortRowPage' {...props} />)
