import React from 'react'
import Grid from 'react-bootstrap/lib/Grid'
import cn from 'classnames'
import Dropdown from 'components/Dropdown'
import InlineSVG from 'svg-inline-react'
import { pure, withHandlers, withState, compose } from 'recompose'
import { DropdownTrigger, DropdownContent } from 'react-simple-dropdown'
import CaretDown from 'static/images/carret-dropdown.svg'
import s from './SortRow.scss'
import { PaybackFilter } from './PaybackFilter/PaybackFilter'
import FiltersModal from './FilterModal/FiltersModal'
import FilterIcon from './assets/filterIcon.svg'

const composedHOC = compose(
  withState('showModal', 'setToggleModal', false),
  withHandlers({
    onCloseHandle: ({ closeDropdown }) => () => {
      closeDropdown(true)
    },
    onShow: ({ closeDropdown, disabled }) => () => {
      closeDropdown(disabled)
    },
    closeDropdownHandler: ({ closeDropdown }) => () => {
      closeDropdown(true)
    },
    closeModal: ({ setToggleModal }) => () => setToggleModal(false),
    openModal: ({ setToggleModal }) => () => setToggleModal(true),
  }),
  pure
)

export default composedHOC(({
  filters: { sortBy },
  filters,
  searchTripsWithFiltersSend,
  saveFilters,
  updateFilters,
  submitFilters,
  closeDropdown,
  registeredFilters,
  oneWay,
  disabled,
  dropClosed,
  onCloseHandle,
  searchTripsSend,
  onShow,
  showModal,
  closeModal,
  openModal,
  selectPayback,
  paybackSelected,
  updateFiltersOnMobile,
  searchFiltersWithModal,
  }) => <div className={cn(s.sortRow, s.mobile)}>
    <Grid>
      <div className={s.sortElementsWrap}>
        <div className={s.filtersContainer}>

          <Dropdown
            className={cn(s.filterDropdown, s.paybackBackground)}
            onHide={onCloseHandle}
            onShow={onShow}
            dropClosed={dropClosed}
            disabled={disabled}
          >
            <DropdownTrigger className={s.filterTrigger}>
              <span>{`${paybackSelected} Payback months`}</span>
              <InlineSVG src={CaretDown} />
            </DropdownTrigger>
            <DropdownContent>
              <PaybackFilter
                sortBy={sortBy}
                closeDropdown={closeDropdown}
                disabled={disabled}
                selectPayback={selectPayback}
                paybackSelected={paybackSelected}
              />
            </DropdownContent>
          </Dropdown>

          <span onClick={openModal} className={s.openModal}>
            <span>Filters</span>
            <span className={s.iconWrap}>
              <InlineSVG src={FilterIcon} />
            </span>
          </span>

          <FiltersModal
            show={showModal}
            onHide={closeModal}
            sortBy={sortBy}
            filters={filters}
            searchTripsWithFiltersSend={searchTripsWithFiltersSend}
            saveFilters={saveFilters}
            updateFilters={updateFilters}
            submitFilters={submitFilters}
            closeDropdown={closeDropdown}
            registeredFilters={registeredFilters}
            oneWay={oneWay}
            closeModal={closeModal}
            updateFiltersOnMobile={updateFiltersOnMobile}
            searchFiltersWithModal={searchFiltersWithModal}
          />
        </div>
      </div>
    </Grid>
  </div>
)
