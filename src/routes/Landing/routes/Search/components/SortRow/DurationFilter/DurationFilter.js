import React from 'react'
import s from './DurationFilter.scss'
import RangeInput from 'components/RangeInput'
import { I18n, Translate } from 'react-redux-i18n'
import { pure, withHandlers, compose } from 'recompose'

const composedHOC = compose(
  withHandlers({
    onChangeCompleteHandler: ({ updateFilters, submitFilters, fromMobile, searchTripsWithFiltersSend }) => obj => val => {
      if (fromMobile) {
        updateFilters({ [obj]: val })
        searchTripsWithFiltersSend({ [obj]: val })
      } else {
        updateFilters({ [obj]: val })
      }
      submitFilters()
    },
    onChangeHandler: ({ updateFilters, saveFilters, fromMobile, searchTripsWithFiltersSend }) => (obj, name) => val => {
      if (fromMobile) {
        searchTripsWithFiltersSend({ [obj]: val })
      } else {
        updateFilters({ [obj]: val })
        saveFilters({ [obj]: val }, name)
      }
    },
  }),
  pure
)

export const DurationFilter = composedHOC(({
  saveFilters,
  filters: {
      totalDuration,
    stopover
  },
  updateFilters,
  runFiltersHandler,
  onChangeCompleteHandler,
  onChangeHandler
  }) => <div className={s.filterBlock}>
    <div className={s.rangeBlock}>
      <h5 className={s.title}>
        <Translate
          value='Landing.maxTotalDuration'
          totalDuration={totalDuration}
          dangerousHTML
        />
      </h5>
      <RangeInput
        maxValue={60}
        minValue={0}
        value={totalDuration}
        onChange={onChangeHandler('totalDuration', 'duration')}
        onChangeComplete={onChangeCompleteHandler('totalDuration')}
      />
    </div>

    <div className={s.rangeBlock}>
      <h5 className={s.title}>
        <Translate value='Landing.stopoverHours' {...stopover} dangerousHTML />
      </h5>
      <RangeInput
        maxValue={25}
        minValue={2}
        value={stopover}
        onChange={onChangeHandler('stopover', 'stopover')}
        onChangeComplete={onChangeCompleteHandler('stopover')}
      />
    </div>
  </div>
)
