import React from 'react'
import Modal from 'react-bootstrap/lib/Modal'
import { TimeFilter } from './../TimeFilter/TimeFilter'
import { DurationFilter } from './../DurationFilter/DurationFilter'
import { StopsFilter } from './../StopsFilter/StopsFilter'
import { TypesFilter } from './../TypesFilter/TypesFilter'
import InlineSVG from 'svg-inline-react'
import s from './FiltersModal.scss'
import closeIcon from '../assets/crossIcon.svg'
import { pure, withHandlers, compose } from 'recompose'

const composedHOC = compose(
  withHandlers({
    applyFilters: ({ searchFiltersWithModal, closeModal }) => () => {
      searchFiltersWithModal()
      closeModal()
    }
  }),
  pure
)


const FiltersModal = composedHOC(({
	sortBy,
  filters,
  searchTripsWithFiltersSend,
  saveFilters,
  updateFilters,
  submitFilters,
  closeDropdown,
  registeredFilters,
  oneWay,
  onHide,
  show,
  applyFilters,
  updateFiltersOnMobile,
  searchFiltersWithModal,
}) =>
  <Modal show={show} onHide={onHide} className={s.modal}>
    <div className={s.header}>
      <span className={s.label}>Filters</span>
      <span className={s.closeBtn} onClick={onHide}>
        <span>close</span>
        <span className={s.iconWrap}>
          <InlineSVG src={closeIcon} />
        </span>
      </span>
    </div>

    <div className={s.filterWrap}>
      <button className={s.applyBtn} onClick={applyFilters}>
        Apply filters
          </button>
    </div>

    <div className={s.filterWrap}>
      <h4 className={s.filterTitle}>
        <span>Sort by</span>
      </h4>
      <TypesFilter
        sortBy={sortBy}
        closeDropdown={closeDropdown}
        searchTripsWithFiltersSend={updateFiltersOnMobile}
        fromMobile={true}
      />
    </div>

    <div className={s.filterWrap}>
      <h4 className={s.filterTitle}>
        <span>Stops</span>
      </h4>
      <StopsFilter
        searchTripsWithFiltersSend={updateFiltersOnMobile}
        closeDropdown={closeDropdown}
        filters={filters}
        fromMobile={true}
      />
    </div>

    <div className={s.filterWrap}>
      <h4 className={s.filterTitle}>
        <span>Duration</span>
      </h4>
      <DurationFilter
        filters={filters}
        saveFilters={saveFilters}
        closeDropdown={closeDropdown}
        updateFilters={updateFilters}
        submitFilters={submitFilters}
        searchTripsWithFiltersSend={updateFiltersOnMobile}
        fromMobile={true}
      />
    </div>

    {/*<div className={s.filterWrap}>
          <h4 className={s.filterTitle}>
            <span>Takeoff/Landing time</span>
          </h4>
          <TimeFilter
            registeredFilters={registeredFilters}
            filters={filters}
            oneWay={oneWay}
            updateFilters={updateFilters}
            saveFilters={saveFilters}
            closeDropdown={closeDropdown}
            submitFilters={submitFilters}
            searchTripsWithFiltersSend={updateFiltersOnMobile}
						fromMobile={true}
          />
        </div>*/}
  </Modal>
)

export default FiltersModal
