import React from 'react'
import cn from 'classnames'
import s from './TimeFilter.scss'
import RangeInput from 'components/RangeInput'
import {I18n} from 'react-redux-i18n'
import {parseTimeToHHMM} from '../../../../../../../utils/helpers'
import {pure, withHandlers, compose} from 'recompose'

//ToDo: Rework
const checkMaxValue = ({min, max}) => ({min, max: max === 1440 ? 1439 : max})
const composedHOC = compose(
  withHandlers({
    resetHandler: ({updateFilters, closeDropdown, fromMobile, searchTripsWithFiltersSend}) => () => {
    	if (fromMobile) {
				searchTripsWithFiltersSend(null, 'time')
			} else {
				closeDropdown(true)
				updateFilters(null, 'time')
			}
    },
    changeTypeHandler: ({updateFilters, fromMobile, searchTripsWithFiltersSend}) => val => () => {
    	if (fromMobile) {
				searchTripsWithFiltersSend(val, 'time')
			} else {
				updateFilters(val, 'time')
			}
    },
    onChangeCompleteHandler: ({updateFilters,  submitFilters, fromMobile, searchTripsWithFiltersSend}) => obj => val => {
    	if (fromMobile) {
				(val.max === 1440) ?
					searchTripsWithFiltersSend({[obj]: {min: val.min, max: val.max-1}}) :
					searchTripsWithFiltersSend({[obj]: val})
			} else {
				(val.max === 1440) ?
					updateFilters({[obj]: {min: val.min, max: val.max-1}}) :
					updateFilters({[obj]: val})
				submitFilters()
			}
    },
		onChangeHandler: ({saveFilters, fromMobile, searchTripsWithFiltersSend}) => obj => val => {
    	if (fromMobile) {
				(val.max === 1440) ?
					searchTripsWithFiltersSend({[obj]: {min: val.min, max: val.max-1}}) :
					searchTripsWithFiltersSend({[obj]: val})
			} else {
				(val.max === 1440) ?
					saveFilters({[obj]: {min: val.min, max: val.max-1}}) :
					saveFilters({[obj]: val})
			}
		},
  }),
  pure
)

export const TimeFilter = composedHOC(({
    saveFilters,
    filters: {
      departureTime,
      returnTime,
      departureTakeOff,
      returnTakeOff,
      departureTimeLanding,
      returnTimeLanding
    },
    oneWay,
    resetHandler,
    changeTypeHandler,
    submitHandler,
    onChangeHandler,
    onChangeCompleteHandler
  }) =>
    <div className={s.filterBlock}>

      <div className={s.rangeBlock}>
        <div className={s.resetBtnWrap}>
          <h5 className={s.title}>
            {I18n.t('Landing.DepartureTime')}
          </h5>

          <div className={s.resetBtn} onClick={resetHandler}>
            {I18n.t('Landing.reset')}
          </div>
        </div>


        <div className={s.timeFilters}>

          <div className={s.filterToggle}>
            <div onClick={changeTypeHandler({departureTakeOff: true})}
                 className={cn(s.toggleElement, departureTakeOff && s.selected)}>
              {I18n.t('Landing.takeOff')}
            </div>
            <div onClick={changeTypeHandler({departureTakeOff: false})}
                 className={cn(s.toggleElement, !departureTakeOff && s.selected)}>
              {I18n.t('Landing.landing')}
            </div>
          </div>

          <div className={s.previewResults}>
            {I18n.t('Landing.fromTo', {
              from: parseTimeToHHMM(departureTakeOff ? departureTime.min : departureTimeLanding.min),
              to: parseTimeToHHMM(departureTakeOff ? departureTime.max : departureTimeLanding.max)
            })}
          </div>
        </div>

        {departureTakeOff ?
          <RangeInput
            maxValue={1440} //24*60
            minValue={0}
            step={10}
            value={departureTime}
            onChange={onChangeHandler('departureTime')}
            onChangeComplete={onChangeCompleteHandler('departureTime')}
          /> :
          <RangeInput
            maxValue={1440} //24*60
            minValue={0}
            step={10}
            value={departureTimeLanding}
            onChange={onChangeHandler('departureTimeLanding')}
            onChangeComplete={onChangeCompleteHandler('departureTimeLanding')}
          />
        }
      </div>

      {!oneWay &&
        <div className={s.rangeBlock}>
          <h5 className={s.title}>
            {I18n.t('Landing.ReturnTime')}
          </h5>

          <div className={s.timeFilters}>
            <div className={s.filterToggle}>
              <div onClick={changeTypeHandler({returnTakeOff: true})}
                   className={cn(s.toggleElement, returnTakeOff && s.selected)}>{I18n.t('Landing.takeOff')}</div>
              <div onClick={changeTypeHandler({returnTakeOff: false})}
                   className={cn(s.toggleElement, !returnTakeOff && s.selected)}>{I18n.t('Landing.landing')}</div>
            </div>
            <div className={s.previewResults}>
              {I18n.t('Landing.fromTo', {
                from: parseTimeToHHMM(returnTakeOff ? returnTime.min : returnTimeLanding.min),
                to: parseTimeToHHMM(returnTakeOff ? returnTime.max : returnTimeLanding.max)
              })}
            </div>
          </div>

          { returnTakeOff
            ? <RangeInput
                maxValue={1440} //24*60
                minValue={0}
                step={10}
                value={returnTime}
                onChange={onChangeHandler('returnTime')}
                onChangeComplete={onChangeCompleteHandler('returnTime')}
              />
            : <RangeInput
                maxValue={1440} //24*60
                minValue={0}
                step={10}
                value={returnTimeLanding}
                onChange={onChangeHandler('returnTimeLanding')}
                onChangeComplete={onChangeCompleteHandler('returnTimeLanding')}
              />
          }
        </div>
      }
    </div>
)
