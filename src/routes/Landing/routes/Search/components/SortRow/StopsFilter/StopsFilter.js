import React from 'react'
import cn from 'classnames'
import {pure, withHandlers, compose} from 'recompose'
import {I18n} from 'react-redux-i18n'
import {sortByStops} from '../../../../../../../utils/filters'
import s from './StopsFilter.scss'
import InlineSVG from 'svg-inline-react'

import CheckIcon from './../assets/checkmark.svg'
import DirectFlyIcon from './../assets/direct-flights-icon.svg'
import OneStopIcon from './../assets/one-stop-icon.svg'
import TwoStopsIcon from './../assets/two-stops-icon.svg'


const composedHOC = compose(
  withHandlers({
    onClickHandler: ({searchTripsWithFiltersSend, closeDropdown}) => stops => () => {
      searchTripsWithFiltersSend({stops})
      closeDropdown(true)
    }
  }),
  pure
)

export const StopsFilter = composedHOC(({
    searchTripsWithFiltersSend,
    closeDropdown,
    filters,
    onClickHandler
  }) =>
    <div className={s.filterBlock}>
      <ul className={s.list}>
        {
          sortByStops.map(({name, val}) =>
            <li key={name}
                className={cn(s.listItem, val === filters.stops && s.selected)}
                onClick={onClickHandler(val)}>
              <span className={s.iconWrap}>
                {val === null && <InlineSVG src={CheckIcon}/>}
                {val === 0 && <InlineSVG src={DirectFlyIcon}/>}
                {val === 1 && <InlineSVG src={OneStopIcon}/>}
                {val === 2 && <InlineSVG src={TwoStopsIcon}/>}
              </span>
              <span>{I18n.t(`Landing.${name}`)}</span>
            </li>
          )
        }
      </ul>
    </div>
)




