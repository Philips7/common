import React from 'react'
import cn from 'classnames'
import s from './PaybackFilter.scss'
import {sortByMonth} from '../../../../../../../utils/filters'
import {pure, withHandlers, compose} from 'recompose'

import FNLPImage from 'static/images/FNLPlogo.png'

const composedHOC = compose(
  withHandlers({
    selectHandler: ({selectPayback, closeDropdown}) => (value) => () => {
			selectPayback(value)
      closeDropdown(true)
    }
  }),
  pure
)
const mostPopular = 5

export const PaybackFilter = composedHOC(({
  selectHandler,
  sortBy,
  disabled,
	paybackSelected
  }) =>
  <div id={'test_id'} className={s.paybackFilter}>
    <div className={s.content}>
      <div className={s.label}>Payback months:</div>
      <ul className={s.monthList}>
        { sortByMonth.map(({value, rate}) =>
          <li key={value}
              className={cn(s.monthItem, value === paybackSelected && s.selected, value === mostPopular && s.popular)}
              onClick={selectHandler(value)}>
            <span className={s.value}>{value}</span>

            { value === mostPopular &&
            <span className={s.popularLabel}>Most popular</span>
            }
          </li>
        )}
      </ul>
    </div>
    <div className={s.footer}>
      <span className={s.label}>Powered by:</span>
      <img className={s.footerImg} src={FNLPImage} alt="powered by FNLP" />
    </div>
  </div>
)
