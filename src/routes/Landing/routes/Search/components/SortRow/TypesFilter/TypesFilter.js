import React from 'react'
import cn from 'classnames'
import s from './TypesFilter.scss'
import {Translate} from 'react-redux-i18n'
import {sortByTypes} from '../../../../../../../utils/filters'
import {pure, withHandlers, compose} from 'recompose'

const composedHOC = compose(
  withHandlers({
    selectHandler: ({searchTripsWithFiltersSend, closeDropdown}) => (sortBy) => () => {
      closeDropdown(true)
      searchTripsWithFiltersSend({sortBy})
    }
  }),
  pure
)

export const TypesFilter = composedHOC(({
  selectHandler,
  sortBy,
  disabled,
	fromMobile
  }) =>
  <div className={s.filterBlock}>
    <ul className={s.list}>
      { sortByTypes.map(({name, val}) =>
        <li key={name}
            className={cn(s.listItem, val === sortBy && s.selected)}
            onClick={selectHandler(val)}>
          <Translate value={`Landing.${name}`}/>
        </li>
      )}
    </ul>
  </div>
)
