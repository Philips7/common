import React from 'react'
import cn from 'classnames'
import Grid from 'react-bootstrap/lib/Grid'
import Row from 'react-bootstrap/lib/Row'
import Col from 'react-bootstrap/lib/Col'
import { pure } from 'recompose'
import { Translate } from 'react-redux-i18n'
import InlineSVG from 'svg-inline-react'
import ScrollArrorwUp from 'static/images/up-arrow.svg'
import TripDescription from 'components/TripDescription'
import WarningIcon from 'static/images/warning-icon.svg'
import { isSmallScreen } from 'utils/responsive'
import s from './SearchResults.scss'

const SearchResultsPage = pure(({
  searchResults,
  creditPoint,
  orderByDuration,
  searchTripsLoading,
  showError,
  passengersNumber,
  selectTrip,
  searchTripsPagination,
  toggleScroll,
  scrollProps,
  num_results,
  interestRate,
  showMobileGuide,
  paybackSelected,
  show,
  apr,
}) =>
  <div className={s.searchResultsPage}>
    <div
      className={cn(s.scrollBtn, scrollProps.showScrollArrow && s.visible, !scrollProps.upScrollArrow && s.down)}
      onClick={toggleScroll}
    >
      <InlineSVG src={ScrollArrorwUp}/>
    </div>
    {!showError &&
    <div className={s.showMoreBlock}>
      <div className={cn(s.showMoreMessage, s.smallMargin)}>
        <span><Translate value='Landing.priceInformation'/><InlineSVG src={WarningIcon} className={s.iconWrap}/></span>
      </div>
    </div>
    }
    <Grid>
      {/*<div className={s.flightsCount}>We found <span className={s.textFeatured}>{num_results} flights</span></div>*/}
      {
        searchResults.map((trip, i) =>
          <Row key={trip.booking_token}>
            <Col xs={12}>
              <div className={s.tripDescriptionWrap}>
                <TripDescription
                  trip={trip}
                  apr={apr}
                  expandOnStart={!isSmallScreen && showMobileGuide && i === 0}
                  showMonth={false}
                  creditPoint={creditPoint}
                  selectTrip={selectTrip}
                  passengersNumber={passengersNumber}
                  interestRate={interestRate}
                  paybackSelected={paybackSelected}
                />
              </div>
            </Col>
          </Row>
        )
      }
    </Grid>
    {!showError &&
    <div>
      <div className={s.showMoreBlock}>
        <div className={s.showMoreMessage}>
          <span>
            <Translate value='Landing.priceRestriction'/>
            <InlineSVG src={WarningIcon} className={s.iconWrap}/>
          </span>
          <a onClick={() => show('LearnMoreModal')} className={s.link}>Learn More</a>
        </div>
      </div>
      <div className={s.showMoreBlock}>
        <button
          className={s.showMoreBtn}
          onClick={searchTripsPagination}
          disabled={searchTripsLoading}
        >
          Show more flights
        </button>
      </div>
    </div>
    }
  </div>
)

export default SearchResultsPage
