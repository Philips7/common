import React, { Component } from 'react'
import cn from 'classnames'
import Grid from 'react-bootstrap/lib/Grid'
import Row from 'react-bootstrap/lib/Row'
import Col from 'react-bootstrap/lib/Col'
import { scrollToTop, scrollTo, isEmpty, isClient } from 'utils/helpers'
import ScrollListener from 'react-scroll-listener'
import { I18n } from 'react-redux-i18n'
import { sortByMonth } from 'utils/filters'
import { isTabletScreen, isSmallScreen } from 'utils/responsive'
import Guide from 'components/Guide'
import { SearchResults } from './'
import s from './Search.scss'
import SortRow from './SortRow'

const Loader = isClient ? require('halogen/PulseLoader') : 'div'

export default class Search extends Component {
  constructor() {
    super()
    this.state = { upScrollArrow: false, showScrollArrow: false }
    this.scrollListener = new ScrollListener()
  }

  componentWillMount() {
    this.props.getGuideStatus()
  }

  componentDidMount() {
    this.scrollListener.addScrollHandler('SearchResults', this.scrollingHandle, null)
  }

  componentWillUnmount() {
    this.scrollListener.removeScrollHandlers()
  }

  componentWillReceiveProps(nextProps) {
    const { searchResults, setGuideStatus } = this.props
    if (nextProps.guideStatus === 0 && nextProps.searchResults.length && !this.once) {
      this.once = true
      setGuideStatus(1)
      !isSmallScreen && this.checkGuide()
    }
  }

  resetOnce = () => {
    this.once = false
  }

  scrollingHandle = () => {
    if (window) {
      const landingScrollPoint = document.getElementById('landingScrollPoint').getBoundingClientRect().top - document.body.getBoundingClientRect().top
      const currentPosition = window.scrollY
      this.setState({
        upScrollArrow: landingScrollPoint < currentPosition,
        showScrollArrow: landingScrollPoint < (currentPosition + 10)
      })
    }
  }

  toggleScroll = () => {
    if (window) {
      const { backPosition, scrollUp } = this.props
      const currentPosition = window.scrollY
      const landingScrollPoint = document.getElementById('landingScrollPoint').getBoundingClientRect().top - document.body.getBoundingClientRect().top
      if (this.state.upScrollArrow) {
        !this.scrollListener.isScrolling && scrollUp(currentPosition)
        scrollTo(landingScrollPoint)
      } else {
        scrollTo(backPosition)
      }
    } else {
      scrollToTop()
    }
  }

  checkGuide = () => {
    if (isTabletScreen && !this.scrolled) {
      const element = document.getElementsByClassName('Landing__firstSection___198Ci')[0]
      const element2 = document.getElementsByClassName('Header__Header___1_1R8')[0]
      scrollTo(element.clientHeight + element2.clientHeight - 80, { duration: 1 })
      setTimeout(() => { this.scrolled = true }, 50)
    }
  }

  render() {
    const {
       backPosition,
      searchResults,
      filters,
      show,
      selectTrip,
      SearchField,
      searchTripsLoading,
      searchTripsPagination,
      num_results,
      searchTripsWithFiltersSend,
      closeDropdown,
      changeTab,
      tabNumber,
      dropClosed,
      saveFilters,
      submitFilters,
      oneWay,
      guideStatus,
      updateFilters,
      registeredFilters,
      showError,
      setGuideStatus,
      paginationLoading,
      selectPayback,
      paybackSelected,
      passengersNumber,
      updateFiltersOnMobile,
      searchTripsSend,
      searchFiltersWithModal } = this.props
    const { upScrollArrow, showScrollArrow } = this.state
    const { apr, rate } = sortByMonth.find(el => el.value === paybackSelected)
    return (
      <div className={s.SearchWrap}>
        <Guide
          run={this.once}
          searchResult={(this.scrolled || !isTabletScreen) && !!searchResults.length && searchResults[0]}
          paybackSelected={paybackSelected}
          passengersNumber={passengersNumber}
          resetOnce={this.resetOnce}
        />
        <SortRow
          oneWay={oneWay}
          filters={filters}
          saveFilters={saveFilters}
          closeDropdown={closeDropdown}
          changeTab={changeTab}
          updateFilters={updateFilters}
          tabNumber={tabNumber}
          submitFilters={submitFilters}
          dropClosed={dropClosed}
          updateFiltersOnMobile={updateFiltersOnMobile}
          searchFiltersWithModal={searchFiltersWithModal}
          selectPayback={selectPayback}
          paybackSelected={paybackSelected}
          registeredFilters={registeredFilters}
          searchTripsWithFiltersSend={searchTripsWithFiltersSend}
          disabled={showError && !searchResults.length && isEmpty(registeredFilters)}
        />

        <div className={s.ticketsLoadWrap}>
          {(num_results > 0 && SearchField && SearchField.values) &&
            <SearchResults
              searchResults={searchResults}
              creditPoint={paybackSelected}
              interestRate={rate}
              searchTripsLoading={searchTripsLoading}
              selectTrip={selectTrip}
              num_results={num_results}
              passengersNumber={passengersNumber}
              toggleScroll={this.toggleScroll}
              searchTripsPagination={searchTripsPagination}
              scrollProps={{ backPosition, upScrollArrow, showScrollArrow }}
              showError={showError}
              show={show}
              apr={apr}
              paybackSelected={paybackSelected}
              showMobileGuide={isTabletScreen}
            />}

          {showError &&
            <div className={s.searchErrorPanelWrap}>
              <Grid>
                <Row>
                  <Col xs={12}>
                    <div className={s.searchErrorPanel}>
                      {I18n.t((searchResults.length > 0) ? 'Landing.searchErrorPanelNoMore' : 'Landing.searchErrorPanelNoFlights')}
                    </div>
                  </Col>
                </Row>
              </Grid>
            </div>
          }
        </div>
        {searchTripsLoading &&
          <div className={cn(paginationLoading ? s.loadingPaginationOverlay : s.loadingOverlay)}>
            <span>
              <Loader className={s.loader} color='#fff' size='20px' />
            </span>
          </div>}
      </div>
    )
  }
}
