export default (store) => ({
  path: '/search',
  getComponent (nextState, cb) {
      const Search = require('./containers/SearchContainer').default

      cb(null, Search)
  }
})
