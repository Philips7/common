import { connect } from 'react-redux'
import {
  searchTripsPagination,
  searchTripsWithFiltersSend,
  updateFilters,
  submitFilters,
  saveTempFilters,
  selectPayback,
  changeOnConnectBehaviour,
  updateFiltersOnMobile,
  searchFiltersWithModal,
} from '../../../../../redux/modules/Trips'
import {
  scrollUp,
  selectTrip,
  closeDropdown,
  changeTab
} from '../../../modules/Landing'

import {
  getGuideStatus,
  setGuideStatus
} from '../../../../../redux/modules/Common'

import Search from '../components/Search'

import { show } from 'redux-modal'

const mapActionCreators = {
  changeOnConnectBehaviour,
  searchTripsWithFiltersSend,
  searchTripsPagination,
  updateFiltersOnMobile,
  searchFiltersWithModal,
  changeTab,
  getGuideStatus,
  setGuideStatus,
  closeDropdown,
  saveFilters: saveTempFilters,
  selectPayback,
  updateFilters,
  submitFilters,
  selectTrip,
  scrollUp,
  show,
}

const mapStateToProps = ({
  Landing: { backPosition, oneWay, dropClosed, tabNumber },
  Trips: {
    searchResults,
    searchTripsLoading,
    num_results,
    filters,
    registeredFilters,
    showError,
    paginationLoading,
    tempFilters,
    paybackSelected
  },
  Ticket: {
    passengersNumber,
  },
  Common: {
    guideStatus,
  },
  form: { SearchField }
}) => ({
    backPosition,
    dropClosed,
    tabNumber,
    searchResults,
    filters: tempFilters,
    SearchField,
    searchTripsLoading,
    num_results,
    registeredFilters,
    oneWay,
    guideStatus,
    showError,
    paginationLoading,
    paybackSelected,
    passengersNumber,
  })

export default connect(mapStateToProps, mapActionCreators)(Search)
