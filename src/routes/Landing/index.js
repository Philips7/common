import {injectSagas} from '../../store/sagas'

const debug = require('debug')('app:LandingIndex')

export default (store) => ({
  getComponent: (location, cb) => {
		const Landing = require('./containers/LandingContainer').default
		const searchAirports = require('./modules/searchAirports').default
		injectSagas(store, {key: 'Landing', sagas: [searchAirports]})
		cb(null, Landing)
  },
  getChildRoutes: (location, cb) => {
      cb(null, [
        // Remove imports!
				require('./routes/Search').default(store)
      ])
  }
})
