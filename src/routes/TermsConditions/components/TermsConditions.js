import React from 'react'
import Grid from 'react-bootstrap/lib/Grid'
import Row from 'react-bootstrap/lib/Row'
import Col from 'react-bootstrap/lib/Col'
import Nav from 'react-bootstrap/lib/Nav'
import NavItem from 'react-bootstrap/lib/NavItem'
import Tab from 'react-bootstrap/lib/Tab'
import s from './TermsConditions.scss'

export const TermsConditions = () => (
	<div className={s.TermsConditions}>
		<Tab.Container id='left-tabs-example' defaultActiveKey='terms'>
			<Grid>
				<Row>
					<Col xs={12}>
						<h1 className={s.pageTitle}>Terms and Conditions</h1>
					</Col>
				</Row>
				<Row>
					<Col xs={12} sm={4} md={3}>
						<Nav bsStyle='pills' stacked className={s.navbar}>
							<NavItem eventKey='terms' className={s.navItem}>
								Terms and Conditions
							</NavItem>
							<NavItem eventKey='part1' className={s.navItem}>
								1.1 General Provisions
							</NavItem>
							<NavItem eventKey='part2' className={s.navItem}>
								1.2 Complete Information
							</NavItem>
							<NavItem eventKey='part3' className={s.navItem}>
								1.3 Intellectual Property
							</NavItem>
							<NavItem eventKey='part4' className={s.navItem}>
								1.4 Code of Conduct
							</NavItem>
							<NavItem eventKey='part5' className={s.navItem}>
								1.5 Flymble reserves the right to change or cancel any Flights that is offered to you
							</NavItem>
							<NavItem eventKey='part6' className={s.navItem}>
								Article 2. Service Agreement
							</NavItem>
							<NavItem eventKey='part7' className={s.navItem}>
								Article 3. Spread Payment Plan
							</NavItem>
							<NavItem eventKey='part8' className={s.navItem}>
								Article 4. Fees, Costs and Full Price of Your Booking
							</NavItem>
							<NavItem eventKey='part9' className={s.navItem}>
								Article 5. KIWI.com Guarantee and Additional Services – Delays and
								Cancellations
							</NavItem>
							<NavItem eventKey='part10' className={s.navItem}>
								Article 6. Luggage Policy
							</NavItem>
							<NavItem eventKey='part11' className={s.navItem}>
								Article 7. Governing Law
							</NavItem>
							<NavItem eventKey='part12' className={s.navItem}>
								Article 8. Complaints Policy
							</NavItem>
							<NavItem eventKey='part13' className={s.navItem}>
								Article 9. Delays and Cancellations
							</NavItem>
							<NavItem eventKey='part14' className={s.navItem}>
								Article 10. Conditions on Changes by You
							</NavItem>
							<NavItem eventKey='part15' className={s.navItem}>
								Article 11. Liability for Damage
							</NavItem>

						</Nav>
					</Col>
					<Col xs={12} sm={8} md={9}>
						<Tab.Content>
							<Tab.Pane eventKey='terms'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.articleSubtitle}>
											Terms and Conditions FLYMBLE.COM
										</h3>

										<div className={s.paragraphStandard}>
											These terms & conditions regulate the legally binding relationship between us, the private limited
											company FLYMBLE TRAVEL LTD, trading as Flymble.com, company ID No.: 10749735, with registered
											office at 9.06, 1 Cartwright Gardens, Kings Cross, postcode WC1H 9EN London, the United Kingdom,
											(“Flymble.com”, “we”, “our”, “us”) in the capacity of agent for the relevant service provider, and
											you as our customer (“you”, “your”, “yourself”).
										</div>

										<div className={s.paragraphStandard}>
											<b>In London on May 26, 2017.</b> <br/> <br/>

											Links: <br/>
											Terms and conditions in accordance to <a
											href='https://www.kiwi.com/us/pages/content/legal'
											className={s.link} target='_blank' rel="noopener noreferrer">KIWI.com</a> <br/>
											Terms and conditions in accordance to <a
											href='https://s3-eu-west-1.amazonaws.com/skypicker-pdf/terms/terms-before-9th-june-en.pdf'
											className={s.link} target='_blank' rel="noopener noreferrer">Flynowpaylater.com</a>
										</div>
									</article>

									<div className={s.articleInfo}>
										Last update: 12 June 2017
									</div>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part1'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>1.1 General Provisions</h3>
										<div className={s.paragraphStandard}>
											The booking conditions described apply to the contract you enter into for any travel services
											arranged by or through Flymble. Other terms and conditions may also apply as referred to below.
											Where Flymble acts as agent in connection with your booking, these booking conditions also set out
											the terms applicable to the agency relationship.

										</div>

									</article>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part2'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>1.2 Complete Information</h3>
										<div className={s.paragraphStandard}>

											You must provide Us with complete, accurate and correct information and all data necessary for Our
											provision of the Services to You (especially the information and data (including personal data)
											necessary for the conclusion of the Contract of Carriage with a Selected Carrier(s), for billing
											and <br/>
											Flight tickets (itinerary, delivery etc.), for which you are requested during the booking or any
											time before or after. It is your responsibility to ensure that all information is correct and
											updated at the time of the booking and/or travel. It should also be noted that you must provide
											all information using Latin script. Flymble is not responsible for any damages, additional cost or
											any other issues or complications, which may arise as a result of your failure to provide Flymble
											with complete and accurate information.
										</div>

									</article>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part3'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>1.3 Intellectual Property</h3>
										<div className={s.paragraphStandard}>

											Flymble retains any, and all, rights to our website and its content; including software, hardware,
											products, processes, algorithms, user interfaces, know-how, technologies, designs and other
											tangible or intangible technical materials or information made available to you by us throughout
											the provision of the services or by using our Website. Unless expressly set forth herein, no
											expressed or implied license, trade mark, design right, content right or right of any kind is
											granted to you regarding our Website, or any part thereof.

										</div>

									</article>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part4'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>1.4 Code of Conduct</h3>
										<div className={s.paragraphStandard}>

											Throughout the provision of our services, We always comply with all applicable laws and
											regulations of the United Kingdom and European Union and our internal principles of customer
											satisfaction and rules for personal data protection. We choose not to make these internal rules
											and principles public.

										</div>

									</article>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part5'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>1.5 Flymble reserves the right to change or cancel any Flights
											that is offered to you</h3>
										<div className={s.paragraphStandard}>

											If a selected carrier’s offer changes (especially if the change concerns the features of the
											Flight tickets You have selected). If, during the provision of our services, the conditions of
											carriage are changed by a selected carrier, Flymble will notify you accordingly in a reasonable
											manner and make every effort to offer You reasonable alternative carriage options

										</div>

									</article>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part6'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>Article 2. Service Agreement</h3>
										<div className={s.paragraphStandard}>

											Our service shall consist of the following: (1) Displaying the offered Flights and their
											combination on our website; (2) Brokerage between you and the selected carrier in the capacity of
											the relevant service provider; (3) Facility for the application of a point of sale loan and to
											service the introduction of Fly Now Pay Later LTD; and; (4) Delivery of the Flight tickets
											(itinerary) for the purchased Flight(s). <br/> <br/>

											Flymble is an intermediary and does not itself operate or provide any travel services. For all
											flight bookings your contract will be with KIWI.com whose flights are made available through
											Flymble. The agency´s terms and conditions will apply to your contract in addition to the relevant
											sections of these booking conditions. For more information regarding terms and conditions
											regarding your flight journey – see the travel conditions of the relevant air carrier.

										</div>

									</article>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part7'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>Article 3. Spread Payment Plan</h3>
										<div className={s.paragraphStandard}>

											Full payment must be made immediately before we are able to confirm your booking. Flymble
											facilitates the introduction of Fly Now Pay Later Ltd. being a service allowing you to apply for a
											point of sale loan (spread payment plan) to finance your flight ticket. For more information, see
											www.flynowpaylater.com. At the moment your spread payment plan is approved, your contract for the
											payment plan is with Fly Now Pay Later and not with Flymble. Any questions related to the spread
											payment plan must be directed to Fly Now Pay Later.
										</div>

									</article>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part8'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>Article 4. Fees, Costs and Full Price of Your Booking</h3>
										<div className={s.paragraphStandard}>
											The price that is displayed on our website is the price for the Flight ticket(s) and it includes
											the base fare to the Destination, the airport charges, fuel charges, VAT and price of Our
											Services. However, it does not include fees related to your contractual agreement with Fly Now Pay
											Later LTD. You must pay the full price with a payment card or by another online payment method
											offered on the Flymble.com website by entering the required data in the relevant online form on
											our Website. We are not obliged to commence the provision of any service(s), until we receive the
											payment of the full price from you and confirm you its acceptance.
										</div>

									</article>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part9'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>Article 5. KIWI.com Guarantee and Additional Services – Delays
											and Cancellations</h3>

										<div className={s.paragraphStandard}>
											As an agent of KIWI.com – we offer you the best service possible we offer you an exclusive and
											unique flight change, delay and cancellation policy, called the Kiwi.com Guarantee (or Assistant
											Services) as described hereunder (hereinafter referred as “Guarantee”).
										</div>

									</article>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part10'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>Article 6. Luggage Policy</h3>

										<div className={s.paragraphStandard}>
											Luggage policies differ from air carrier to air carrier and depending on the ticket conditions. In
											most cases air carrier allow you to bring 1 hold item and 2 hand luggage items. Flymble emphasises
											that for your ticket’s specific luggage allowances it is recommended to contact the relevant air
											carrier directly. Luggage could come at a separate charge and is not displayed or included in the
											ticket price on our website.

										</div>

									</article>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part11'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>Article 7. Governing Law</h3>

										<div className={s.paragraphStandard}>
											The law of England and Wales (and no other) will apply to your contract with Flymble. Your
											contract with other service providers and KIWI specifically, will be handled in the jurisdiction
											mentioned by the service providers policy. For specific details, see
											kiwi.com/us/pages/content/legal.
										</div>

									</article>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part12'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>Article 8. Complaints Policy</h3>
										<div className={s.paragraphStandard}>
											In the unlikely event of complaints, Flymble´s policy is set for according to the following terms:
											(I) In cases of disputes the courts of England and Wales shall have complete jurisdiction over all
											disputes arising between you and us. (ii) According to EU legislation all consumers residing in EU
											countries are, prior to filing any legal action to the court, pursuant to the Act No. 634/1992
											Coll., on Consumer Protection, as amended, entitled to commence the out-of-court settlement of
											their dispute with Flymble, provided that any such dispute between an EU consumer and Flymble has
											not been successfully settled directly. (iii) Pursuant to the EU Regulation No. 524/2013, EU
											consumers are also entitled to commence the out-of-court settlement of consumer disputes on-line
											through the ODR platform for on-line resolution of consumer disputes accessible at
											ec.europa.eu/consumers/odr. (iv) Prior to commencement of any of the above methods of dispute
											resolution, You are advised to directly contact Flymble through our contact form available on our
											homepage or to resolve any of your complaints or suggestions.
										</div>

									</article>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part13'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>Article 9. Delays and Cancellations</h3>
										<div className={s.paragraphStandard}>
											Scenario A (More than 72 hours to departure) – Flymble.com guarantees to a change or cancellation
											of flight(s) announced more than 72 hours prior to departure of the first concerned flight, which
											may negatively impact your ability to reach your destination or your arrival to a destination
											shall be postponed for more than 24 hours after the original scheduled arrival, i.e. due to flight
											change(s) or cancellation(s) you would either miss your flight connection(s) or your flight(s)
											would be cancelled (hereinafter referred as “Scenario A”). <br/><br/> In this case, you learn
											about a change
											or cancellation of four flight(s) under Scenario A You must inform Flymble of such a change or
											cancellation without undue delay either by telephone or e-mail. Should you fail to inform us about
											such flight change(s) or cancellation(s) without undue delay, you will not be entitled to the
											guarantee. In case, We are informed first about the change or cancellation of your flight(s)
											according to Scenario A, We will contact You within a reasonable time after we learn about such
											flight change(s) or cancellation(s). <br/><br/> Scenario B (Less than 78 hours to departure) -
											Flymble.com
											guarantees to a flight delay(s) or cancellation(s) of Flight(s) announced less than 72 hours prior
											to departure of the first concerned flight, which may negatively impact your ability to reach your
											destination or your arrival to the destination shall be postponed for more than 24 hours after the
											original scheduled arrival, i.e. due to the flight delay(s) or cancellation(s) you would either
											miss your flight(s) connection(s) or your flight(s) would be cancelled (hereinafter referred as
											“Scenario B”). <br/><br/> In this case, if You learn about a flight delay(s) or cancellation(s) of
											your
											flight under Scenario B, you are obliged to inform us of such flight delay(s) or cancellation(s)
											without undue delay either by telephone or via e-mail as displayed on our website. Should you fail
											to inform us about such flight delay(s) or cancellation(s) without undue delay, You will not be
											entitled to the guarantee. In case, We are informed first about the flight delay(s) or
											cancellation(s) of your flight under Scenario B, Flymble will contact you within a reasonable time
											after we learn about it. <br/><br/> After We are informed about the flight delay(s) or
											cancellation(s) under
											Scenario B, you may choose one of the following solutions at your discretion: (i) Flymble will
											search for an alternative transportation to your destination and should we find a reasonable
											alternative We will offer you an alternative flight(s) or other means of transportation to your
											destination entirely at Our expense. (ii) Flymble will refund you the price you paid for all the
											unused Flights en route to your destination. (iii) Should you be offered any of the aforementioned
											alternatives by the airline and/or airport, Flymble is absolved of all responsibility to further
											compensate and/or reimburse you.
										</div>

									</article>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part14'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>Article 10. Conditions on Changes by You</h3>

										<div className={s.paragraphStandard}>
											If you wish to make a change to your booking, you must contact us by email or via telephone
											immediately. We will assist in the endeavour but cannot guarantee a refund in this scenario.
											Depending whether the booking process is fully executed airlines typically handle a booking
											amendment as a booking cancellation. The longer in advance the cancellation is requested before
											departure date the more likely the ticket can be resold by the service provider, increasing your
											chance for a refund. Flymble does not charge any fees in the event of a cancellation request or
											booking amendment.
										</div>

									</article>
								</div>
							</Tab.Pane>

							<Tab.Pane eventKey='part15'>
								<div className={s.articleWrap}>
									<article className={s.article}>
										<h3 className={s.paragraphTitle}>Article 11. Liability for Damage</h3>
										<div className={s.paragraphStandard}>
											Flymble is not liable for any damage, harm or loss arising out of any actions or omissions of a
											Selected Carrier or other third parties in connection with carriage to the Destination. Neither is
											Flymble liable for any damage, harm or loss arising out of your actions or omissions that are
											contrary to these Terms & Conditions.

										</div>

									</article>
								</div>
							</Tab.Pane>

						</Tab.Content>
					</Col>
				</Row>
			</Grid>
		</Tab.Container>
	</div>
)

export default TermsConditions
