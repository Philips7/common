export default (store) => ({
  path: 'terms-conditions',
  getComponent (nextState, cb) {
      const TermsConditions = require('./components/TermsConditions').default

      cb(null, TermsConditions)
  }
})
