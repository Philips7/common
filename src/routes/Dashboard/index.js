import {injectSagas} from 'store/sagas'

export default (store) => ({
  path: 'dashboard',
  getComponent (nextState, cb) {
      const Dashboard = require('./containers/DashboardContainer').default

      const getTickets = require('./modules/getTickets').default
      const getDashboardData = require('./modules/getDashboardData').default

      injectSagas(store, {key: 'Dashboard', sagas: [getTickets, getDashboardData]})

      cb(null, Dashboard)
  }
})
