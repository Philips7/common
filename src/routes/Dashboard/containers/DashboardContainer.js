import {connect} from 'react-redux'
import { pure, compose } from 'recompose'

import {
  getDashboardDataAttempt,
  getTicketsAttempt,
  showFailedModal,
  clearTickets
} from '../modules/Dashboard'

import Dashboard from '../components/Dashboard'

const mapActionCreators = {
  getDashboardDataAttempt,
  getTicketsAttempt,
  showFailedModal,
  clearTickets
}

const mapStateToProps = ({
  Dashboard: {
    getDashboardDataLoading,
    getTicketsLoading,
    tickets,
    statistics
  },
  Trips: { paybackSelected },
  modal
}) => ({
  getDashboardDataLoading,
  getTicketsLoading,
  tickets,
  statistics,
  paybackSelected,
  modal
})

const enhance = compose(
	connect(mapStateToProps, mapActionCreators),
	pure
)

export default enhance(Dashboard)
