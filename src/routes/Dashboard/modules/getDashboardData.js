import {take, call, put} from 'redux-saga/effects'
import request from 'sagas/api'
import {GET_DASHBOARD_DATA_ATTEMPT} from './Dashboard'

const debug = require('debug')('app:dashboardData')

export default () => {
  function * worker({responseSuccess, responseFailure}) {
    const body = {
      method: 'GET'
    }

    try {
      const {res, err} = yield call(request, 'dashboard/dashboard-api/statistics', body)

      if (res) {
        yield put(responseSuccess(res))
      } else {
        yield put(responseFailure(err))
      }
    } catch (error) {

    }
  }

  function * watcher() {
    while (true) {
      const data = yield take(GET_DASHBOARD_DATA_ATTEMPT)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
