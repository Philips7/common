import {take, call, put} from 'redux-saga/effects'
import request from 'sagas/api'
import {GET_TICKETS_ATTEMPT} from './Dashboard'

export default () => {
  function * worker({values: {page_size, page}, responseSuccess, responseFailure}) {
    const body = {
      method: 'GET'
    }

    try {
      const {res, err} = yield call(request, `trips/trip?page=${page}&page_size=${page_size}`, body)

      if (res) {
        yield put(responseSuccess(res))
      } else {
        yield put(responseFailure(err))
      }
    } catch (error) {
      console.log(error)
    }
  }

  function * watcher() {
    while (true) {
      const data = yield take(GET_TICKETS_ATTEMPT)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
