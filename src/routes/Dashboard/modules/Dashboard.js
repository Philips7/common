import createReducer from '../../../utils/createReducer'
import { show } from 'redux-modal'
import { LOGOUT } from '../../../redux/modules/User'
import { addCompaniesMap } from '../../../utils/helpers'
import moment from 'moment'

// ------------------------------------
// Constants
// ------------------------------------
export const GET_DASHBOARD_DATA_ATTEMPT = 'Dashboard.GET_DASHBOARD_DATA_ATTEMPT'
export const GET_DASHBOARD_DATA_SUCCESS = 'Dashboard.GET_DASHBOARD_DATA_SUCCESS'
export const GET_DASHBOARD_DATA_FAILURE = 'Dashboard.GET_DASHBOARD_DATA_FAILURE'

export const GET_TICKETS_ATTEMPT = 'Dashboard.GET_TICKETS_ATTEMPT'
export const GET_TICKETS_SUCCESS = 'Dashboard.GET_TICKETS_SUCCESS'
export const GET_TICKETS_FAILURE = 'Dashboard.GET_TICKETS_FAILURE'

export const REMOVE_TICKETS = 'Dashboard.REMOVE_TICKETS'

// ------------------------------------
// Actions
// ------------------------------------
export const getDashboardDataAttempt = () => ({
  type: GET_DASHBOARD_DATA_ATTEMPT,
  responseSuccess: getDashboardDataSuccess,
  responseFailure: getDashboardDataFailure
})

export const getDashboardDataSuccess = data => ({
  type: GET_DASHBOARD_DATA_SUCCESS,
  data
})

export const getDashboardDataFailure = () => ({
  type: GET_DASHBOARD_DATA_FAILURE
})

export const getTicketsAttempt = () => (dispatch, getState) => {
  const {ticketsPage, ticketsPageSize, tickets: {next}} = getState().Dashboard

  if (next) {
    dispatch({
      type: GET_TICKETS_ATTEMPT,
      values: {
        page: ticketsPage,
        page_size: ticketsPageSize
      },
      responseSuccess: getTicketsSuccess,
      responseFailure: getTicketsFailure
    })
  }
}

export const getTicketsSuccess = ({results, ...data}) => ({
  type: GET_TICKETS_SUCCESS,
  data: {
    ...data,
    results: results.map(({status, ...item}) => ({
      status,
      trip: addCompaniesMap(item)
    }))
  }
})

export const getTicketsFailure = () => ({
  type: GET_TICKETS_FAILURE
})

export const showFailedModal = updatedAt => show('contactUsModal', {updatedAt: moment(updatedAt).add(1, 'w').format('Do MMMM')})

export const clearTickets = () => ({type: REMOVE_TICKETS})

// ------------------------------------
// Reducer
// ------------------------------------
export const initialState = {
  getDashboardDataLoading: false,
  getTicketsLoading: false,
  ticketDeleting: false,
  tickets: {
    count: 0,
    next: true,
    previous: null,
    results: []
  },
  ticketsPage: 1,
  ticketsPageSize: 20,
  statistics: {
    next_payment: '',
    payments_on_time: '',
    finished_cycles: 0,
    countries_visited: 0,
    distance_traveled: 0,
    flights_booked: 0,
    time_in_air: 0
  }
}

export default createReducer(initialState, {
  [GET_DASHBOARD_DATA_ATTEMPT]: (state, action) => ({
    getDashboardDataLoading: true
  }),
  [GET_DASHBOARD_DATA_SUCCESS]: (state, action) => ({
    getDashboardDataLoading: false,
    statistics: action.data
  }),
  [GET_DASHBOARD_DATA_FAILURE]: (state, action) => ({
    getDashboardDataLoading: false
  }),

  [GET_TICKETS_ATTEMPT]: (state, action) => ({
    getTicketsLoading: true
  }),
  [GET_TICKETS_SUCCESS]: ({tickets: {count, results}, ticketsPageSize, ticketsPage}, {data}) => ({
    getTicketsLoading: false,
    ticketsPage: count > ticketsPageSize ? ticketsPage + 1 : ticketsPage,
    tickets: {
      ...data,
      ...results.concat(data.results)
    }
  }),
  [GET_TICKETS_FAILURE]: (state, action) => ({
    getTicketsLoading: false
  }),
  [REMOVE_TICKETS]: (state, action) => ({
    tickets: initialState.tickets
  }),
  [LOGOUT]: (state, action) => initialState
})
