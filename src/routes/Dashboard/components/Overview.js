import React from 'react'
import Grid from 'react-bootstrap/lib/Grid'
import {pure} from 'recompose'
import {Translate} from 'react-redux-i18n'
import s from './Overview.scss'
import Map from './../assets/Dashboard_icon_1.svg'
import Plane from './../assets/Dashboard_icon_2.svg'
import Ticket from './../assets/Dashboard_icon_3.svg'
import Points from './../assets/Dashboard_icon_4.svg'
import InlineSVG from 'svg-inline-react'

const Overview = pure(({
    statistics: {
      next_payment,
      finished_cycles,
      payments_on_time,
      countries_visited,
      distance_traveled,
      flights_booked,
      time_in_air
    }
  }) =>
    <Grid className={s.dashboard}>
      <div className={s.statsBlockWrap}>
        <div className={s.statsBlock}>
          <div className={s.iconWrap}>
						<InlineSVG src={Map}/>
          </div>
          <div className={s.statValue}>
            {countries_visited}
          </div>
          <div className={s.lable}>
            <Translate value='Dashboard.countriesVisited'/>
          </div>
        </div>
        <div className={s.statsBlock}>
          <div className={s.iconWrap}>
						<InlineSVG src={Plane}/>
          </div>
          <div className={s.statValue}>
            {distance_traveled}
          </div>
          <div className={s.lable}>
            <Translate value='Dashboard.distanceTraveled'/>
          </div>
        </div>
        <div className={s.statsBlock}>
          <div className={s.iconWrap}>
						<InlineSVG src={Ticket}/>
          </div>
          <div className={s.statValue}>
            {flights_booked}
          </div>
          <div className={s.lable}>
            <Translate value='Dashboard.flightsBooked'/>
          </div>
        </div>
        <div className={s.statsBlock}>
          <div className={s.iconWrap}>
						<InlineSVG src={Points}/>
          </div>
          <div className={s.statValue}>
            {time_in_air}
          </div>
          <div className={s.lable}>
            <Translate value='Dashboard.timeInAir'/>
          </div>
        </div>
      </div>
    </Grid>
)

export default Overview
