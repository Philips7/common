import React from 'react'
import Grid from 'react-bootstrap/lib/Grid'
import Row from 'react-bootstrap/lib/Row'
import Col from 'react-bootstrap/lib/Col'
import cn from 'classnames'
import {Circle} from 'rc-progress'
import Waypoint from 'react-waypoint'
import ContactUsModal from './ContactUsModal'
import s from './Dashboard.scss'
import Overview from './Overview'
import TripDescription from 'components/TripDescription'
import PreloaderOverlay from 'components/PreloaderOverlay'
import NoTicketsIcon from './../assets/Dashboard_empty_icon.svg'
import InlineSVG from 'svg-inline-react'
import {I18n} from 'react-redux-i18n'

const STATUS_BOOKING_FAILED = 2
const STATUS_ABORTED = 2

class Dashboard extends React.Component {
  componentWillMount() {
    const {getDashboardDataAttempt, getTicketsAttempt} = this.props
    getDashboardDataAttempt()
    getTicketsAttempt()
  }

  componentWillUnmount() {
    this.props.clearTickets()
  }

  handleShowFailedModal = updatedAt => () => {
    this.props.showFailedModal(updatedAt)
  }

  render() {
    const {
      tickets,
      getTicketsAttempt,
      statistics,
      getDashboardDataLoading,
      getTicketsLoading,
      paybackSelected,
    } = this.props

    return (
      <div className={cn(s.dashboard)}>
        <Grid>
          <Row>
            <Col xs={12}>
              <h1 className={s.title}>{I18n.t('Header.dashboard')}</h1>
            </Col>
          </Row>
        </Grid>
        <Overview statistics={statistics} />
        <section className={s.round}>
          <Grid>
            <div className={s.subtitle}>{I18n.t('Dashboard.yourTickets')}</div>
            <Row>
              {tickets.count === 0
                ? <Col xs={12}>
                <div className={s.noTicketsBlock}>
									<InlineSVG src={NoTicketsIcon}/>
                  <p>{I18n.t('Dashboard.anyTickets')}</p>
                </div>
              </Col>
                : tickets.results.map(({trip, months, status, updated_at, month_amount}, index) =>
                  <Col xs={12} key={index}>
                    <div className={s.ticketWrap}>
                      <TripDescription
                        trip={trip}
                        creditPoint={months || 1}
                        showMonth={false}
                        noSelectButton={true}
                        paybackSelected={paybackSelected}
                        // showFailedModal={this.handleShowFailedModal(updated_at)}
                        bookingFailed={status === STATUS_BOOKING_FAILED}
                        // monthAmount={month_amount}
                        dashboard
                        panelView
                      />
                    </div>
                  </Col>
              )
              }
            </Row>
            <Row>
              <Waypoint onEnter={getTicketsAttempt}/>
            </Row>
          </Grid>
        </section>
        {
          (getDashboardDataLoading || getTicketsLoading) && <PreloaderOverlay/>
        }
        <ContactUsModal/>
      </div>
    )
  }
}

export default Dashboard
