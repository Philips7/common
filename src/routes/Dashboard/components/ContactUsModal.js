import React from 'react'
import {connectModal} from 'redux-modal'
import Modal from 'react-bootstrap/lib/Modal'
import Button from 'react-bootstrap/lib/Button'
import {pure} from 'recompose'
import {I18n} from 'react-redux-i18n'
import s from './ContactUsModal.scss'
import NoTicketIcon from '../assets/no_ticket_icon.svg'
import CrossIcon from 'static/images/cross-icon.svg'
import InlineSVG from 'svg-inline-react'

const ContactUsModal = pure(({show, handleHide, updatedAt}) =>
  <Modal
    show={show}
    onHide={handleHide}
  >
    <Modal.Body className={s.wrap}>
      <span
        onClick={handleHide}
        className={s.closeModalBtn}
      >
            <InlineSVG src={CrossIcon}/>
      </span>
      <h3 className={s.title}>{I18n.t('Dashboard.weHaveProblem')}</h3>
      <p className={s.describe}>{I18n.t('Dashboard.unableTicket')}</p>
			<InlineSVG src={NoTicketIcon} className={s.iconWrap}/>
      <div className={s.describe}>
        <div>{I18n.t('Dashboard.yourMoneyWilLBeReturned')}</div><b>{I18n.t('Dashboard.before')} {updatedAt}</b>
      </div>
      <Button className={s.button} onClick={handleHide}>{I18n.t('Dashboard.iUnderstand')}</Button>
    </Modal.Body>
  </Modal>
)

export default connectModal({name: 'contactUsModal'})(ContactUsModal)
