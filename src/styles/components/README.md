Consistent font sizes
Set a rem font size in your document, to use for all your text elements. This is a better way to scale up and down fonts instead of em. Using em will compound based on the other CSS attributes, but rem will only scale up or down based on the size you define in your HTML document. Separating a .scss file for all your typography and using the below mixin to define your default font sizes will drastically automate your typography coding workflow.

// Define default font size
@function calculateRem($size) {
  $remSize: $size / 16px;
  @return $remSize * 1rem;
}

@mixin font-size($size) {
  font-size: $size;
  font-size: calculateRem($size);
}
//Usage
p {
  @include font-size(14px)
}
//Output
p {
  font-size: 14px; //For unsupported browsers
  font-size: 0.8rem;
}


// Sizing shortcuts

@mixin size($width, $height: $width) {
  width: $width;
  height: $height;
}

@mixin flex($direction: false, $cross: false, $main: false, $wrap: false) {
  @include flex-variant(flex, $direction: $direction, $cross: $cross, $main: $main, $wrap: $wrap);
}

@mixin inline-flex($direction: false, $cross: false, $main: false, $wrap: false) {
  @include flex-variant(inline-flex, $direction: $direction, $cross: $cross, $main: $main, $wrap: $wrap);
}




Retina images
Add high resolution images to your site, with a fallback for devices that aren’t displaying high resolution images. When using retina images I would recommend compressing as much as possible without destroying the image. A good tool for this is TinyPNG (supports PNG and JPG).
This mixin will allow you to add a rule in the same spot as the CSS property defining the original image.

// Image Mixins
// - Responsive image
// - Retina image


// Responsive image
//
// Keep images from scaling beyond the width of their parents.

@mixin img-fluid($display: block) {
  display: $display;
  max-width: 100%; // Part 1: Set a maximum relative to the parent
  height: auto; // Part 2: Scale the height according to the width, otherwise you get stretching
}


// Retina image
//
// Short retina mixin for setting background-image and -size.

@mixin img-retina($file-1x, $file-2x, $width-1x, $height-1x) {
  background-image: url($file-1x);

  // Autoprefixer takes care of adding -webkit-min-device-pixel-ratio and -o-min-device-pixel-ratio,
  // but doesn't convert dppx=>dpi.
  // There's no such thing as unprefixed min-device-pixel-ratio since it's nonstandard.
  // Compatibility info: http://caniuse.com/#feat=css-media-resolution
  @media
  only screen and (min-resolution: 192dpi), // IE9-11 don't support dppx
  only screen and (min-resolution: 2dppx) { // Standardized
    background-image: url($file-2x);
    background-size: $width-1x $height-1x;
  }
}


@mixin image-2x($image, $width, $height) {
  @media (min--moz-device-pixel-ratio: 1.3),
         (-o-min-device-pixel-ratio: 2.6/2),
         (-webkit-min-device-pixel-ratio: 1.3),
         (min-device-pixel-ratio: 1.3),
         (min-resolution: 1.3dppx) {
    background-image: url($image);
    background-size: $width $height;
  }
}
//Usage
div.logo {
  background: url("logo.png") no-repeat;
  @include image-2x("logo2x.png", 100px, 25px);
}

@mixin image-2x($image, $width, $height) {
  @media (min--moz-device-pixel-ratio: 1.3),
         (-o-min-device-pixel-ratio: 2.6/2),
         (-webkit-min-device-pixel-ratio: 1.3),
         (min-device-pixel-ratio: 1.3),
         (min-resolution: 1.3dppx) {
    background-image: url($image);
    background-size: $width + px $height + px;
    background-size: $width / 10 + rem $height / 10 + rem;
  }
}



Positioning
Easy and quick CSS positioning, thanks to Hugo Giraudel.

@mixin position($position, $args) {
  @each $o in top right bottom left {
    $i: index($args, $o);
    @if $i and $i + 1 <= length($args) and type-of(nth($args, $i + 1)) == number {
      #{$o}: nth($args, $i + 1);
    }
  }
  position: $position;
}

// Positioning helpers
@mixin absolute($args: '') {
  @include position(absolute, $args);
}

@mixin fixed($args: '') {
  @include position(fixed, $args);
}

@mixin relative($args: '') {
  @include position(relative, $args);
}Usage
You should only use absolute(), relative(), and fixed() functions to add the respective positioning to the elements. Parameters are optional and can be given as demonstrated below:

.menu li {
  @include relative;
}

.sub-menu {
  @include absolute(top 100% left 0);
}

.sticky-bar {
  @include fixed(top 0 left 0);
}





// CSS image replacement
@mixin text-hide() {
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}


// Text truncate
// Requires inline-block or block for proper styling

@mixin text-truncate() {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}



// Lists

// Unstyled keeps list items block level, just removes default browser padding and list-style
@mixin list-unstyled {
  padding-left: 0;
  list-style: none;
}




// Breakpoint viewport sizes and media queries.
//
// Breakpoints are defined as a map of (name: minimum width), order from small to large:
//
//    (xs: 0, sm: 544px, md: 768px)
//
// The map defined in the `$grid-breakpoints` global variable is used as the `$breakpoints` argument by default.

// Name of the next breakpoint, or null for the last breakpoint.
//
//    >> breakpoint-next(sm)
//    md
//    >> breakpoint-next(sm, (xs: 0, sm: 544px, md: 768px))
//    md
//    >> breakpoint-next(sm, $breakpoint-names: (xs sm md))
//    md
@function breakpoint-next($name, $breakpoints: $grid-breakpoints, $breakpoint-names: map-keys($breakpoints)) {
  $n: index($breakpoint-names, $name);
  @return if($n < length($breakpoint-names), nth($breakpoint-names, $n + 1), null);
}

// Minimum breakpoint width. Null for the smallest (first) breakpoint.
//
//    >> breakpoint-min(sm, (xs: 0, sm: 544px, md: 768px))
//    544px
@function breakpoint-min($name, $breakpoints: $grid-breakpoints) {
  $min: map-get($breakpoints, $name);
  @return if($min != 0, $min, null);
}

// Maximum breakpoint width. Null for the largest (last) breakpoint.
// The maximum value is calculated as the minimum of the next one less 0.1.
//
//    >> breakpoint-max(sm, (xs: 0, sm: 544px, md: 768px))
//    767px
@function breakpoint-max($name, $breakpoints: $grid-breakpoints) {
  $next: breakpoint-next($name, $breakpoints);
  @return if($next, breakpoint-min($next, $breakpoints) - 1px, null);
}

// Media of at least the minimum breakpoint width. No query for the smallest breakpoint.
// Makes the @content apply to the given breakpoint and wider.
@mixin media-breakpoint-up($name, $breakpoints: $grid-breakpoints) {
  $min: breakpoint-min($name, $breakpoints);
  @if $min {
    @media (min-width: $min) {
      @content;
    }
  } @else {
    @content;
  }
}

// Media of at most the maximum breakpoint width. No query for the largest breakpoint.
// Makes the @content apply to the given breakpoint and narrower.
@mixin media-breakpoint-down($name, $breakpoints: $grid-breakpoints) {
  $max: breakpoint-max($name, $breakpoints);
  @if $max {
    @media (max-width: $max) {
      @content;
    }
  } @else {
    @content;
  }
}

// Media between the breakpoint's minimum and maximum widths.
// No minimum for the smallest breakpoint, and no maximum for the largest one.
// Makes the @content apply only to the given breakpoint, not viewports any wider or narrower.
@mixin media-breakpoint-only($name, $breakpoints: $grid-breakpoints) {
  @include media-breakpoint-up($name, $breakpoints) {
    @include media-breakpoint-down($name, $breakpoints) {
      @content;
    }
  }
}

// Media that spans multiple breakpoint widths.
// Makes the @content apply between the min and max breakpoints
@mixin media-breakpoint-between($lower, $upper, $breakpoints: $grid-breakpoints) {
  @include media-breakpoint-up($lower, $breakpoints) {
    @include media-breakpoint-down($upper, $breakpoints) {
      @content;
    }
  }
}



// Gradients

// Horizontal gradient, from left to right
//
// Creates two color stops, start and end, by specifying a color and position for each color stop.
// Color stops are not available in IE9.
@mixin gradient-x($start-color: #555, $end-color: #333, $start-percent: 0%, $end-percent: 100%) {
  background-image: linear-gradient(to right, $start-color $start-percent, $end-color $end-percent);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#{ie-hex-str($start-color)}', endColorstr='#{ie-hex-str($end-color)}', GradientType=1); // IE9
}

// Vertical gradient, from top to bottom
//
// Creates two color stops, start and end, by specifying a color and position for each color stop.
// Color stops are not available in IE9.
@mixin gradient-y($start-color: #555, $end-color: #333, $start-percent: 0%, $end-percent: 100%) {
  background-image: linear-gradient(to bottom, $start-color $start-percent, $end-color $end-percent);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#{ie-hex-str($start-color)}', endColorstr='#{ie-hex-str($end-color)}', GradientType=0); // IE9
}

@mixin gradient-directional($start-color: #555, $end-color: #333, $deg: 45deg) {
  background-repeat: repeat-x;
  background-image: linear-gradient($deg, $start-color, $end-color);
}
@mixin gradient-x-three-colors($start-color: #00b3ee, $mid-color: #7a43b6, $color-stop: 50%, $end-color: #c3325f) {
  background-image: linear-gradient(to right, $start-color, $mid-color $color-stop, $end-color);
  background-repeat: no-repeat;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#{ie-hex-str($start-color)}', endColorstr='#{ie-hex-str($end-color)}', GradientType=1); // IE9 gets no color-stop at all for proper fallback
}
@mixin gradient-y-three-colors($start-color: #00b3ee, $mid-color: #7a43b6, $color-stop: 50%, $end-color: #c3325f) {
  background-image: linear-gradient($start-color, $mid-color $color-stop, $end-color);
  background-repeat: no-repeat;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#{ie-hex-str($start-color)}', endColorstr='#{ie-hex-str($end-color)}', GradientType=0); // IE9 gets no color-stop at all for proper fallback
}
@mixin gradient-radial($inner-color: #555, $outer-color: #333) {
  background-image: radial-gradient(circle, $inner-color, $outer-color);
  background-repeat: no-repeat;
}
@mixin gradient-striped($color: rgba(255,255,255,.15), $angle: 45deg) {
  background-image: linear-gradient($angle, $color 25%, transparent 25%, transparent 50%, $color 50%, $color 75%, transparent 75%, transparent);
}





// Resize anything

@mixin resizable($direction) {
  resize: $direction; // Options: horizontal, vertical, both
  overflow: auto; // Per CSS3 UI, `resize` only applies when `overflow` isn't `visible`
}




@mixin clearfix() {
  &::after {
    content: "";
    display: table;
    clear: both;
  }
}



External fonts
Using the CSS font-face property, we can easily add fonts in a directory and include all font types. When using font files, different browsers have different compatibility with file types. This mixin helps solve that by using any relevant font file in a directory.


@mixin font-face($font-name, $file-name, $weight: normal, $style: normal) { 
  @font-face {
    font-family: quote($font-name);
    src: url($file-name + '.eot');
    src: url($file-name + '.eot?#iefix')  format('embedded-opentype'),
         url($file-name + '.woff') format('woff'),
         url($file-name + '.ttf')  format('truetype'),
         url($file-name + '.svg##{$font-name}')  format('svg');
    font-weight: $weight;
    font-style: $style;
  }
 }
 //Usage
 @include font-face('gotham', '/fonts/gotham');
 
 
 @mixin font-face($font-name, $file-name, $weight: normal, $style: normal) {
   @font-face {
     font-family: quote($font-name);
     src: url($file-name + '.eot');
     src: url($file-name + '.eot?#iefix')  format('embedded-opentype'),
     url($file-name + '.woff') format('woff'),
     url($file-name + '.ttf')  format('truetype'),
     url($file-name + '.svg##{$font-name}')  format('svg');
     font-weight: $weight;
     font-style: $style;
   }
 }Usage
 @include font-face("MyFont", "path/to/MyFont", $style: normal, $weight: normal);Output
 @font-face {
   font-family: "MyFont";
   src: url("path/to/MyFont.eot");
   src: url("path/to/MyFont.eot?#iefix") format("embedded-opentype"), url("path/to/MyFont.woff") format("woff"), url("path/to/MyFont.ttf") format("truetype"), url("path/to/MyFont.svg#MyFont") format("svg");
   font-weight: normal;
   font-style: normal;
 }
 
 
 
 
 Adding dimensions
 A simple mixin to give dimensions to a box.
 Usage
 You must provide at least one parameter to make it work:
 
 .square {
   @include size(50px);
 }
 
 .rectangle {
   @include box(100px, 50px);
 }Output
 .square {
   width: 50px;
   height: 50px;
 }
 
 .rectangle {
   width: 100px;
   height: 50px;
 }
 
 
 
 


2. Pseudo
 When using ::before and ::after you'll always need these three, so we're saving two lines of code every time you use this.

@mixin pseudo($display: block, $pos: absolute, $content: ''){
  content: $content;
  display: $display;
  position: $pos;
}
See below an example of using it without any arguments (You will also see this mixin used in some of our other mixins below too).

                                                                                                                                div::after {
                                                                                                                                  @include pseudo;
                                                                                                                                  top: -1rem; left: -1rem;
                                                                                                                                  width: 1rem; height: 1rem;
                                                                                                                                }



4. CSS triangles
 This mixin takes all the hassle out of creating that triangle you'll see coming out of most traditional tooltips, all without images, you just specify it's colour, how big you want it, the direction it's going to come out of your element and you're done!

@mixin css-triangle($color, $direction, $size: 6px, $position: absolute, $round: false){
  @include pseudo($pos: $position);
  width: 0;
  height: 0;
  @if $round {
    border-radius: 3px;
  }
  @if $direction == down {
    border-left: $size solid transparent;
    border-right: $size solid transparent;
    border-top: $size solid $color;
    margin-top: 0 - round( $size / 2.5 );
  } @else if $direction == up {
    border-left: $size solid transparent;
    border-right: $size solid transparent;
    border-bottom: $size solid $color;
    margin-bottom: 0 - round( $size / 2.5 );
  } @else if $direction == right {
    border-top: $size solid transparent;
    border-bottom: $size solid transparent;
    border-left: $size solid $color;
    margin-right: -$size;
  } @else if  $direction == left {
    border-top: $size solid transparent;
    border-bottom: $size solid transparent;
    border-right: $size solid $color;
    margin-left: -$size;
  }
}




8. Z-index
While technically this is just a pure function, but I felt it's worth being on the list as it's so easy to loose track of your z-index values when working in several different files, so we created this so that we could store them in one place for easy editing/recording. Just place this snippet in your main variables/settings.scss file (needs to be imported before any of your other files that reference a x-index of course).

@function z($name) {
    @if index($z-indexes, $name) {
        @return (length($z-indexes) - index($z-indexes, $name)) + 1;
    } @else {
        @warn 'There is no item "#{$name}" in this list; choose one of: #{$z-indexes}';
        @return null;
    }
}
$z-indexes: (
    "outdated-browser",
    "modal",
    "site-header",
    "page-wrapper",
    "site-footer"
);
Then where ever you're wanting to use a z-index value, name it the same as your class and add in it into your variables/setting file, like below. You'll never have a "z-index: 99999999;" headache again.

.site-header {
    z-index: z('site-header');
}



9. Hardware
Simple and effective for when you need to trigger hardware acceleration for some animation, keeping everything fast, slick and flicker-free.

@mixin hardware($backface: true, $perspective: 1000) {
    @if $backface {
        backface-visibility: hidden;
    }
    perspective: $perspective;
}



10. Truncate
Adding Truncation to an element isn't as simple as it should be, although it is with this handy time saver, meaning when you include the mixin, you only have to specify your boundry with a max width value, happy days!

@mixin text-truncate




////
# sass-mixins

## List of mixins included:


### font-face
- include local fonts easy
- usage `@include font-face('hk_grotesklight', '../fonts/hk-grotesk/hkgrotesk-light-webfont')`

### breakpoint
- for mediquerys with singe argument
- usage: `@include breakpoint(xl){}` or `@include breakpoint(600px, min-width){}` or `@include breakpoint(number or predefined point, max-width(px)/min-width(px)/or any other feature){}`
- define variables for breakpoints before usage
- demo: http://codepen.io/riogrande/pen/jqPJPO
