import React from 'react'
import {render, unmountComponentAtNode} from 'react-dom'
import {Provider} from 'react-redux'
import {match, Router, browserHistory} from 'react-router'
import {syncHistoryWithStore} from 'react-router-redux'
import Raven from 'raven-js'
import {AppContainer} from 'react-hot-loader'
import configureStore from '../store/createStore'
import routes from '../routes'
import config from 'config.json'

Raven
	.config(__PROD__ && config.sentry)
	.install()

const debug = require('debug')('app:clientIndex')


try {
	const preloadedState = window.__PRELOADED_STATE__
	const store = configureStore(preloadedState, browserHistory)
	const history = syncHistoryWithStore(browserHistory, store)
	const rootEl = document.getElementById('root')

	const renderApp = () => {
		// Sync routes both on client and server
		match({history, routes: routes(store)}, (error, redirectLocation, renderProps = {}) => {
			renderProps.history = history
			render(
				<AppContainer>
					<Provider store={store}>
						<Router {...renderProps} />
					</Provider>
				</AppContainer>,
				rootEl,
			)
		})
	}

// Enable hot reload by react-hot-loader
	if (module.hot) {
		module.hot.accept('../routes', () => {
			setImmediate(() => {
				// Preventing the hot reloading error from react-router
				unmountComponentAtNode(rootEl)
				renderApp()
			})
		})
	}

	renderApp()
} catch (err) {
	if (__PROD__) {
		Raven.captureException(err)
	}
	debug(err)
}
