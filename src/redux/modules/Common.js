import createReducer from '../../utils/createReducer'
import { loadTranslations, setLocale } from 'react-redux-i18n'
import { setCookie, getCookie } from 'redux-cookie'
import { defaultFilters } from './Trips'
import common_en from 'static/i18n/en.json'
import moment from 'moment'

const debug = require('debug')('app:redux:Modules:Common')

const LANG = 'lang'
const IP = 'ip'
export const IP_CHECK = 'IP_CHECK'
export const IP_CHECK_SUCCESS = 'IP_CHECK_SUCCESS'

const GUIDE_STATUS = 'guide'
export const GET_GUIDE_STATUS = 'Common.GET_GUIDE_STATUS'
export const SET_GUIDE_STATUS = 'Common.SET_GUIDE_STATUS'

// ------------------------------------
// Constants
// ------------------------------------
export const STARTUP = 'Common.STARTUP'

// ------------------------------------
// Actions
// ------------------------------------
export const startup = () => dispatch => {
  let lang = ''
  if (typeof window !== 'undefined') {
    lang = dispatch(getCookie(LANG))
  }
  dispatch(loadTranslations({
    en: common_en,
    // du: common_du
  }))
  dispatch(setLocale(lang || 'en'))
  moment.updateLocale(lang || 'en', {
    week: {
      dow: 1
    }
  })
  dispatch({ type: STARTUP })
}

export const changeLanguage = lang => dispatch => {
  dispatch(setCookie(LANG, lang))
  dispatch(setLocale(lang))
}

export const checkIpCookie = () => dispatch => {
  const ipShown = dispatch(getCookie(IP))
  if (ipShown !== 'true') {
    dispatch(checkIfIpUk())
  }
}

export const filtersPermanentlyInCookies = () => dispatch => {
  dispatch(setCookie('filters', JSON.stringify(defaultFilters), {}))
}

export const checkIfIpUk = () => dispatch => dispatch({
  type: IP_CHECK,
  responseSuccess: ipChecked,
  dispatch
})

export const ipChecked = ipUK =>  dispatch => {
  dispatch(setCookie(IP, 'true', { expires: 14 }))
  dispatch({
    type: IP_CHECK_SUCCESS,
    ipUK,
  })
}

export const getGuideStatus = () => (dispatch) => {
  const guideStatus = dispatch(getCookie(GUIDE_STATUS))
  dispatch({
    type: GET_GUIDE_STATUS,
    guideStatus: +guideStatus >= 0 && +guideStatus <= 3 ? +guideStatus : 0,
  })
}

export const setGuideStatus = status => (dispatch) => {
  const oldGuideStatus = dispatch(getCookie(GUIDE_STATUS))
  const guideStatus = oldGuideStatus >= 0 && oldGuideStatus <= 3 ? oldGuideStatus : 0
  const newGuideStatus = +guideStatus + status
  dispatch(setCookie(GUIDE_STATUS, newGuideStatus))
  dispatch({
    type: SET_GUIDE_STATUS,
    guideStatus: +newGuideStatus,
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  ipUK: true,
  guideStatus: -1,
}

export default createReducer(initialState, {
  [IP_CHECK_SUCCESS]: (state, { ipUK }) => ({ ipUK }),
  [GET_GUIDE_STATUS]: (state, { guideStatus }) => ({ guideStatus }),
  [SET_GUIDE_STATUS]: (state, { guideStatus }) => ({ guideStatus })
})
