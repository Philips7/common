import { expireCookie, getCookie, setCookie } from 'redux-cookie'
import { push } from 'react-router-redux/lib/actions'
import { arrayPush, arrayRemove } from 'redux-form/lib/actions'
import { show } from 'redux-modal/lib/actions'
import isEmpty from 'ramda/src/isEmpty'

import { breakDownPrices } from '../../utils/price'
import createReducer from '../../utils/createReducer'
import { addCompaniesMap } from '../../utils/helpers'
import { saveSearchCriteria, searchFromTicketSend, selectPayback } from './Trips'
import { changeFocusOnDateRange } from 'routes/Landing/modules/Landing'

const TICKET_ID = 'ticket_id'
const PAYBACK = 'payback'
const SEARCH_HASH = 'search_hash'
const FILTERS = 'filters'
// 25 hours = 90000000
// const INTERVAL_TIME = 90000000

// ------------------------------------
// Constants
// ------------------------------------
export const CHECK_FLIGHT_CONNECT = 'Ticket.CHECK_FLIGHT_CONNECT'
export const CHECK_FLIGHT_DISCONNECT = 'Ticket.CHECK_FLIGHT_DISCONNECT'
export const CHECK_FLIGHT_SEND = 'Ticket.CHECK_FLIGHT_SEND'
export const CHECK_FLIGHT_EMIT = 'Ticket.CHECK_FLIGHT_EMIT'
export const CHECK_FLIGHT_EMIT_ERROR = 'Ticket.CHECK_FLIGHT_EMIT_ERROR'

export const CHECK_FLIGHT_LAST_SEND = 'Ticket.CHECK_FLIGHT_LAST_SEND'
export const CHECK_TRIP = 'Ticket.CHECK_TRIP'

export const START_TIMER = 'Ticket.START_TIMER'
export const STOP_TIMER = 'Ticket.STOP_TIMER'

export const REMOVE_TICKET_FROM_CACHE = 'Ticket.REMOVE_TICKET_FROM_CACHE'
export const CREATE_INVOICE_CONNECT = 'Checkout.CREATE_INVOICE_CONNECT'
export const SHOW_PAYMENT_BUTTON = 'Checkout.SHOW_PAYMENT_BUTTON'
export const CHANGE_PASSENGER_COUNT = 'Ticket.CHANGE_PASSENGER_COUNT'
export const CHANGE_BAGGAGE_COUNT = 'Ticket.CHANGE_BAGGAGE_COUNT'

export const CLEAN_TICKET = 'Ticket.CLEAN_TICKET'

// ------------------------------------
// Actions
// ------------------------------------
//
// export const stopTimer = () => (dispatch, getState) => dispatch({
//   type: STOP_TIMER,
//   interval: clearInterval(getState().Ticket.interval)
// })

export const checkFlight = data => ({
  type: CHECK_FLIGHT_SEND,
  values: { data }
})

export const checkFlightSend = (
  booking_token,
  passengersNumber,
  baggageNumber,
  paybackSelected
) => (dispatch) => {
  if (booking_token) {
    dispatch(checkFlight({ booking_token, passengersNumber, baggageNumber, paybackSelected }))
  } else {
    dispatch(push('/'))
  }
}

export const onCheckFlightOpen = () => (dispatch, getState) => {
  const booking_token = dispatch(getCookie(TICKET_ID))
  const cookiePayback = dispatch(getCookie(PAYBACK))
  const paybackSelected = (cookiePayback && JSON.parse(cookiePayback).value)
  const { Ticket: { passengersNumber, baggageNumber } } = getState()
  dispatch(selectPayback(paybackSelected, true))
  dispatch(checkFlightSend(booking_token, passengersNumber, baggageNumber, paybackSelected))
}

export const removeTicketFromCache = () => (dispatch) => {
  dispatch(expireCookie(TICKET_ID))
  dispatch({ type: REMOVE_TICKET_FROM_CACHE })
}

export const checkFlightConnect = submit => dispatch =>
  dispatch({
    type: CHECK_FLIGHT_CONNECT,
    dispatch,
    submit
  })

export const checkFlightDisconnect = () => (dispatch) => {
  dispatch({
    type: CHECK_FLIGHT_DISCONNECT
  })
}

export const refreshCheckFlightConnection = () => (dispatch, getState) => {
  const { websocketConnected } = getState().Ticket
  dispatch(checkFlightDisconnect())
  websocketConnected && dispatch(checkFlightConnect())
}

export const checkFlightEmit = (response, submit) => (dispatch, getState) => {
  const message = JSON.parse(response.data)
  const filters = JSON.parse(dispatch(getCookie(FILTERS)))
  const { Ticket: { ticket } } = getState()
  const creditPoint = ticket ? ticket.creditPoint : 10

  // Uncomment below for testing unavailableTicketModal
  // dispatch(searchFromTicketSend(ticket, filters))
  // dispatch(show('changePriceModal', { newPrice: 100.23234234 }))
  // dispatch(show('changePriceModal', { destinationCity: 'Bogota' }))

  if (message.status === 'error') {
    if (ticket) {
      if (message.message === 'This ticket has unsupported price') {
        dispatch(show('extraBaggageModal', { noRemove: true, fromError: true }))
      } else {
        const segments = ticket.outbound.segments
        ticket.needPassportData = true
        dispatch(searchFromTicketSend(ticket, filters))
        dispatch(show('unavailableTicketModal', { destinationCity: segments[segments.length - 1].destination.city }))
        dispatch(checkFlightDisconnect())
      }
    } else {
      dispatch(push('/'))
    }
    dispatch({
      type: CHECK_FLIGHT_EMIT_ERROR,
      message
    })
  } else if (message.status === 'success' && (message.data && message.data.booking_token)) {
    const { lastCheck } = getState().Ticket

    if (message.data.price_changed && message.data.total_price > ticket.total_price) {
      dispatch(searchFromTicketSend(message.data, filters))
      dispatch(show('changePriceModal', { newPrice: message.data.total_price - ticket.total_price }))
      dispatch({
        type: CHECK_FLIGHT_EMIT,
        ticket: {
          creditPoint,
          ...addCompaniesMap(message.data),
          needPassportData: true,
        }
      })
    } else if (lastCheck) {
      dispatch({ type: SHOW_PAYMENT_BUTTON })
    }
    dispatch({
      type: CHECK_FLIGHT_EMIT,
      ticket: {
        creditPoint,
        ...addCompaniesMap(message.data),
        needPassportData: true,
      }
    })
    if (typeof submit === 'function') {
      submit()
    }
    dispatch(checkFlightDisconnect())
  }
}

export const checkTrip = (trip, fromModal) => (dispatch, getState) => {
  const { Trips: { filters, paybackSelected }, Ticket: { websocketConnected, passengersNumber, baggageNumber }, Landing } = getState()
  const { total_price } = trip.all_months[paybackSelected]
  if (total_price > 250 && total_price < 2000) {
    const cookiePayback = dispatch(getCookie(PAYBACK))
    const payback = { value: paybackSelected } || (cookiePayback && JSON.parse(cookiePayback)) || {}
    dispatch({
      type: CHECK_TRIP,
      ticket: {
        ...trip,
        ...breakDownPrices(trip, payback)
      }
    })
    if (Landing.searchFieldValues && Landing.searchFieldValues.outbound_place) dispatch(setCookie(SEARCH_HASH, JSON.stringify(Landing.searchFieldValues), {}))
    if (filters) dispatch(setCookie(FILTERS, JSON.stringify(filters), {}))
    if (payback) dispatch(setCookie(PAYBACK, JSON.stringify(payback), {}))
    dispatch(setCookie(TICKET_ID, trip.booking_token, {}))
    if (websocketConnected) dispatch(checkFlightSend(trip.booking_token, passengersNumber, fromModal ? 0 : baggageNumber, paybackSelected))
    dispatch(push('/checkout'))
  } else {
    const isAbove = total_price > 2000
    const passengers = +Math.round(((isAbove ? 2000 : 250) / (total_price / passengersNumber)) + (isAbove ? -0.5 : 0.5)).toFixed(0)
    dispatch(show('priceLimitModal', { isAbove, total_price, passengers: Math.abs(passengersNumber - passengers) }))
  }
}

export const sendLastCheck = () => (dispatch) => {
  dispatch(checkFlightConnect())
  dispatch({ type: CHECK_FLIGHT_LAST_SEND })
}

export const cleanTicket = () => ({type: CLEAN_TICKET})

export const changePassengersNumber = (newPassengersNumber = 1, addPassenger, removePassenger, notConnect) => (dispatch, getState) => {
  const {
    form: { SearchField },
    Ticket: { passengersNumber, baggageNumber, ticket },
    Trips: { searchCriteria, paybackSelected },
    routing: { locationBeforeTransitions: { pathname } }
  } = getState()

  const range_date = (SearchField && SearchField.values && SearchField.values.range_date) || (!SearchField && { })

  if (ticket) {
    const { total_price } = (addPassenger || removePassenger) ? ticket : ticket.all_months[paybackSelected]
    const acceptablePrice = total_price > 250 && total_price < 2000

    if (!acceptablePrice && addPassenger) {
      const isAbove = total_price > 2000
      const passengers = +Math.round(((isAbove ? 2000 : 250) / (total_price / passengersNumber)) + (isAbove ? -0.5 : 0.5)).toFixed(0)
      dispatch(show('priceLimitModal', { isAbove, total_price, passengers: Math.abs(passengersNumber - passengers) }))
      return
    }
  }
  const bookingToken = dispatch(getCookie(TICKET_ID))
  if (newPassengersNumber > 0 && !removePassenger) {
    dispatch(arrayPush('PassengerInfoForm', 'passengers', { title: '17' }))
  }
  const passengerCount = addPassenger ? passengersNumber + 1 : newPassengersNumber

  dispatch({
    type: CHANGE_PASSENGER_COUNT,
    passengersNumber: passengerCount,
  })

  dispatch(saveSearchCriteria({
    ...searchCriteria,
    number_of_passengers: passengerCount
  }))

  !range_date && changeFocusOnDateRange('startDate')

  !notConnect && pathname !== '/' && range_date && isEmpty(range_date) && dispatch(checkFlightConnect())
}

export const removePassenger = fieldIndex => (dispatch, getState) => {
  const { passengersNumber, ticket } = getState().Ticket
  dispatch(changePassengersNumber(passengersNumber - 1, null, true))
  setTimeout(() => dispatch(arrayRemove('PassengerInfoForm', 'passengers', fieldIndex)), 0)
  const baggagesToDelete = ticket.baggage_fees[fieldIndex]
  baggagesToDelete && baggagesToDelete.forEach(() => dispatch(changeBaggageNumber(-1, false)))
}

export const changeBaggageNumber = (newBaggageNumber = 1, reset) => (dispatch, getState) => {
  const { Ticket: { passengersNumber, baggageNumber }, Trips: { paybackSelected } } = getState()
  const booking_token = dispatch(getCookie(TICKET_ID))
  const updatedBagagedNumber = baggageNumber + newBaggageNumber
  dispatch({
    type: CHANGE_BAGGAGE_COUNT,
    baggageNumber: reset ? 0 : updatedBagagedNumber,
  })
  dispatch(checkFlightConnect())
}

// ------------------------------------
// Reducer
// ------------------------------------
export const initialState = {
  checkFlightLoading: false,
  needShowLoader: false,
  ticket: null,
  websocketConnected: false,
  lastCheck: false,
  passengersNumber: 1,
  baggageNumber: 0,
}

export default createReducer(initialState, {
  [REMOVE_TICKET_FROM_CACHE]: () => ({ ticket: null }),
  [CHECK_FLIGHT_SEND]: ({ ticket, lastCheck }, { values: { data: { booking_token } } }) => ({
    checkFlightLoading: true,
    needShowLoader: !ticket || (ticket.booking_token !== booking_token) || lastCheck
  }),
  [CHECK_FLIGHT_EMIT]: (state, { ticket }) => ({
    checkFlightLoading: false,
    ticket,
    needShowLoader: false
  }),
  [CHECK_FLIGHT_EMIT_ERROR]: () => ({
    checkFlightLoading: false,
    needShowLoader: false
  }),
  [CHECK_FLIGHT_CONNECT]: () => ({ websocketConnected: true }),
  [CHECK_FLIGHT_DISCONNECT]: () => ({
    websocketConnected: false,
    lastCheck: false
  }),
  [START_TIMER]: (state, { interval }) => ({ interval }),
  [STOP_TIMER]: () => ({ interval: null }),
  [CHECK_FLIGHT_LAST_SEND]: () => ({
    lastCheck: true,
    needShowLoader: true
  }),
  [CHECK_TRIP]: (state, { ticket }) => ({
    lastCheck: false,
    ticket,
    baggageNumber: 0
  }),
  [CHANGE_PASSENGER_COUNT]: (state, { passengersNumber }) => ({ passengersNumber }),
  [CHANGE_BAGGAGE_COUNT]: (state, { baggageNumber }) => ({ baggageNumber }),
  [CLEAN_TICKET]: () => ({
    ticket: null,
  }),
})
