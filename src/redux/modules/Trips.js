import createReducer from '../../utils/createReducer'
import moment from 'moment'
import { push, replace } from 'react-router-redux/lib/actions'
import { addCompaniesMap, parseTimeToHHMM, anywhereCheck } from '../../utils/helpers'
import { defaultFeatureMonth, defaultFeatureRate } from '../../utils/payback.json'
import { countFeatureTripPrice } from '../../utils/price'
import { setCookie } from 'redux-cookie'
import equals from 'ramda/src/equals'
import { changePassengersNumber } from './Ticket'

// ------------------------------------
// Constants
// ------------------------------------

const PAYBACK = 'payback'
export const SEARCH_TRIPS_CONNECT = 'Trips.SEARCH_TRIPS_CONNECT'
export const SEARCH_TRIPS_DISCONNECT = 'Trips.SEARCH_TRIPS_DISCONNECT'
export const SEARCH_TRIPS_SEND = 'Trips.SEARCH_TRIPS_SEND'
export const SEARCH_TRIPS_EMIT = 'Trips.SEARCH_TRIPS_EMIT'
export const SEARCH_TRIPS_EMIT_ERROR = 'Trips.SEARCH_TRIPS_EMIT_ERROR'
export const SEARCH_TRIPS_RECONNECT = 'Trips.SEARCH_TRIPS_RECONNECT'
export const SEARCH_TRIPS_STOP_TIMEOUT = 'Trips.SEARCH_TRIPS_STOP_TIMEOUT'
export const SEARCH_TRIPS_PAGINATION = 'Trips.SEARCH_TRIPS_PAGINATION'

export const SEARCH_FEATURE_FLIGHTS_EMIT = 'Trips.SEARCH_FEATURE_FLIGHTS_EMIT'
export const CHANGE_ON_CONNECT_BEHAVIOUR = 'Trips.CHANGE_ON_CONNECT_BEHAVIOUR'

export const SAVE_SEARCH_CRITERIA = 'Trips.SAVE_SEARCH_CRITERIA'
export const RESET_SEARCH_CRITERIA = 'Trips.RESET_SEARCH_CRITERIA'
export const UPDATE_FROM_URL = 'Trips.UPDATE_FROM_URL'
export const CLEAN_RESULTS = 'Trips.CLEAN_RESULTS'
export const SAVE_FILTERS = 'Trips.SAVE_FILTERS'
export const UNMOUNT_SEARCH_FIELD = 'Landing.UNMOUNT_SEARCH_FIELD'

export const SAVE_TEMP_FILTERS = 'Trips.SAVE_TEMP_FILTERS'
export const RESET_TEMP_FILTERS = 'Trips.RESET_TEMP_FILTERS'
export const SUBMIT_TEMP_FILTERS = 'Trips.SUBMIT_TEMP_FILTERS'
export const SELECT_PAYBACK = 'Trips.SELECT_PAYBACK'

export const NONE = '0'
export const FEATURE = '1'
export const NO_SCROLL = '2'
export const FROM_URL = '3'
const PAGE_SIZE = 20

export const durationFilters = ['totalDuration', 'stopover']
export const timeFilters = ['departureTime', 'returnTime', 'departureTakeOff', 'returnTakeOff', 'departureTimeLanding', 'returnTimeLanding']
export const defaultFilters = {
  totalDuration: 60,
  stopover: { min: 2, max: 25 },
  departureTime: { min: 0, max: 1439 },
  returnTime: { min: 0, max: 1439 },
  departureTimeLanding: { min: 0, max: 1439 },
  returnTimeLanding: { min: 0, max: 1439 },
  departureTakeOff: true,
  returnTakeOff: true,
  sortBy: 2,
  number_of_passengers: 1,
  stops: null
}

// ------------------------------------
// Actions
// ------------------------------------

export const searchTripsConnect = () => dispatch => dispatch({
  type: SEARCH_TRIPS_CONNECT,
  dispatch
})

export const searchTripsDisconnect = () => ({
  type: SEARCH_TRIPS_DISCONNECT
})

export const searchTrips = () => (dispatch) => {
  dispatch(searchTripsDisconnect())
  dispatch(searchTripsConnect())
}

export const searchTripStopTimeout = () => ({ type: SEARCH_TRIPS_STOP_TIMEOUT })

export const searchTripReconnect = () => (dispatch) => {
  const timeout = setTimeout(() => {
    dispatch(searchTrips())
  }, 500) // Delay between two reconnection request to avoid stackowerflow

  dispatch(searchTripStopTimeout())
  dispatch({
    type: SEARCH_TRIPS_RECONNECT,
    timeout
  })
}

const parserTimeRange = (range, outbound, returnn) => ({
  [`${outbound ? 'outbound' : 'return'}_${returnn ? 'departure' : 'arrival'}_time_between`]: [
    parseTimeToHHMM(range.min),
    parseTimeToHHMM(range.max)
  ]
})
export const onSearchTripConnect = () => (dispatch, getState) => {
  const {
    Trips: {
      onConnectBehaviour,
    searchCriteria,
    filters,
    paybackSelected
    }
  } = getState()
  switch (onConnectBehaviour) {
    case FEATURE:
      dispatch({
        type: SEARCH_TRIPS_SEND,
        values: { data: { ...searchCriteria } },
        searchTripsLoading: false,
        loadingFeatureFlights: true
      })
      break
    default:
      const {
        totalDuration,
        stopover,
        departureTime,
        returnTime,
        departureTimeLanding,
        returnTimeLanding,
        sortBy,
        stops
      } = filters
      const { outbound_date, return_date } = searchCriteria
      dispatch({
        type: SEARCH_TRIPS_SEND,
        values: {
          data: {
            ...searchCriteria,
            ordering: sortBy,
            max_stopovers: stops,
            max_fly_duration: totalDuration,
            stopover_duration: [stopover.min, stopover.max],
            number_of_months: paybackSelected,
            ...(outbound_date && parserTimeRange(departureTime, true, true)),
            ...(outbound_date && parserTimeRange(departureTimeLanding, true, false)),
            ...(outbound_date && return_date && (!searchCriteria.one_way && parserTimeRange(returnTime, false, true))),
            ...(outbound_date && return_date && (!searchCriteria.one_way && parserTimeRange(returnTimeLanding, false, false)))
          }
        },
        searchTripsLoading: true,
        loadingFeatureFlights: false
      })

      break
  }
}

export const searchFeatureFlightsSend = () => dispatch => {
  dispatch(saveSearchCriteria({
    page_num: 1,
    page_size: 4,
    one_for_city: true,
    number_of_months: 10,
    sortBy: 0,
    outbound_place: ['LTN', 'SEN', 'LCY', 'STN', 'LGW'],
    destination_place: ['BOG', 'SFO', 'JFK', 'LAX',]
  }))
  dispatch(changeOnConnectBehaviour(FEATURE))
}

export const serializeFeaturePlace = ({ airport_name, airport_IATA, city, country }) => ({
  name: airport_name,
  code: airport_IATA,
  city,
  country
})

export const dateYearFirst = date => (date && typeof date === 'object') ? date.format('YYYY-MM-DD') : undefined

export const serializeTicket = (ticket, short = false) => {
  const outbound_place = ticket.outbound.segments[0].origin
  const outbound_date = moment.parseZone(outbound_place.date)
  const destination_place = ticket.returnn && ticket.returnn.segments[0].origin
  const return_date = ticket.returnn && moment.parseZone(destination_place.date)

  return {
    outbound_place: short ? outbound_place.airport_IATA : serializeFeaturePlace(outbound_place),
    outbound_date: short ? dateYearFirst(outbound_date) : outbound_date,
    destination_place: ticket.returnn && (short ? destination_place.airport_IATA : serializeFeaturePlace(destination_place)),
    return_date: ticket.returnn && (short ? dateYearFirst(return_date) : return_date)
  }
}

export const cleanResults = () => ({ type: CLEAN_RESULTS })
export const saveSearchCriteria = searchCriteria => ({
  type: SAVE_SEARCH_CRITERIA,
  searchCriteria
})
export const saveFilters = (filters, name) => ({
  type: SAVE_FILTERS,
  filters,
  name
})
export const getDefaultFilters = keys => {
  let temp = {}
  keys.forEach(key => {
    temp[key] = defaultFilters[key]
  })
  return temp
}
export const resetFilter = name => dispatch => {
  dispatch({
    type: RESET_SEARCH_CRITERIA,
    filters: name === 'duration' ? getDefaultFilters(durationFilters) : name === 'time' ? getDefaultFilters(timeFilters) : defaultFilters,
    name
  })
  dispatch(resetTempFilters())
  dispatch(submitFilters())
}
export const prepareSearch = (searchCriteria, type) => dispatch => {
  dispatch(saveSearchCriteria(searchCriteria))
  dispatch(changeOnConnectBehaviour(type))
}

export const searchFromTicketSend = (ticket, filters, isNeedSerialize = true) => (dispatch, getState) => {
  dispatch(cleanResults())
  dispatch(serializeFilters(filters))
  const { Trips: { searchCriteria: { number_of_passengers } }, Ticket: { passengersNumber } } = getState()
  dispatch(prepareSearch({
    ...(isNeedSerialize ? serializeTicket(ticket, true) : ticket),
    page_num: 1,
    page_size: 2,
    number_of_passengers: number_of_passengers || passengersNumber
  },
    NO_SCROLL
  ))
}

export const serializeDestinationPlace = destination_place => anywhereCheck(destination_place.name) ?
  { one_for_city: true } :
  { destination_place: destination_place.code || destination_place }
const GA_serializeDestinationPLace = destination => destination.one_for_city ? 'Anywhere' : [destination.destination_place].join(',')
export const prepareValues = ({ outbound_place, range_date, destination_place, oneWay, outbound_date, return_date, number_of_passengers }) => {
  const _outbound_place = outbound_place.code || outbound_place,
    _outbound_date = moment.isMoment(outbound_date || range_date) ? outbound_date || dateYearFirst(range_date) : outbound_date || dateYearFirst(range_date && range_date.startDate),
    _return_date = oneWay ? null : return_date || dateYearFirst(range_date && range_date.endDate),
    _destination_place = serializeDestinationPlace(destination_place)

  return {
    label: `domain - ${window.location.hostname}, outbound_place ${_outbound_place}, outbound_date ${_outbound_date}, return_date ${
    _return_date}, destination_place ${GA_serializeDestinationPLace(_destination_place)}`,
    data: {
      outbound_place: _outbound_place,
      outbound_date: _outbound_date,
      return_date: _return_date,
      one_way: oneWay,
      number_of_passengers: number_of_passengers && number_of_passengers.value,
      ..._destination_place
    }
  }
}
export const searchTripsSend = (values) => (dispatch, getState) => {
  const { pathname } = getState().routing.locationBeforeTransitions
  const preparedValues = prepareValues(values)
  const { number_of_passengers } = getState().Trips.searchCriteria
  window.dataLayer && window.dataLayer.push({
    'event': 'search',
    ...preparedValues.data
  })

  dispatch(resetFilter())
  dispatch(prepareSearch({
    page_size: PAGE_SIZE,
    page_num: 1,
    ...preparedValues.data,
    number_of_passengers: number_of_passengers || preparedValues.data.number_of_passengers
  },
    NONE
  ))
}

export const updateFiltersOnMobile = (filters, name) => dispatch => {
  dispatch(updateFilters(filters, name))
  dispatch(submitTempFilters())
}

export const searchFiltersWithModal = () => (dispatch, getState) => {
  const { Trips: { searchCriteria } } = getState()

  dispatch(prepareSearch({
    ...searchCriteria,
    page_size: 1,
    page_num: 1,
  }), NONE)
}

export const searchTripsWithFiltersSend = (filters, name) => (dispatch, getState) => {
  const { Trips: { searchCriteria } } = getState()

  dispatch(updateFilters(filters, name))
  dispatch(submitTempFilters())
  dispatch(prepareSearch({
    ...searchCriteria,

    page_size: PAGE_SIZE,
    page_num: 1
  },
    NONE
  ))
}

export const updateFilters = (filters, name) => dispatch => {
  dispatch(filters ? saveTempFilters(filters, name) : resetFilter(name))
}

export const submitFilters = () => (dispatch, getState) => {
  const { Trips: { searchCriteria } } = getState()
  dispatch(submitTempFilters())
  dispatch(prepareSearch({
    ...searchCriteria,

    page_size: PAGE_SIZE,
    page_num: 1
  }))
}

export const searchTripsModalPagination = () => (dispatch, getState) => {
  const { Trips: { searchCriteria, nextPage, paybackSelected } } = getState()

  if (nextPage) {
    dispatch(saveSearchCriteria({
      ...searchCriteria,
      page_num: searchCriteria.page_num + 1,
      number_of_months: paybackSelected,
    }))
    dispatch(changeOnConnectBehaviour(NO_SCROLL))
  } else {
    dispatch({
      type: SEARCH_TRIPS_EMIT_ERROR,
      message: 'No more flights'
    })
  }
}

export const searchTripsPagination = () => (dispatch, getState) => {
  const { Trips: { searchCriteria, nextPage, paybackSelected } } = getState()

  if (nextPage) {
    dispatch(saveSearchCriteria({
      ...searchCriteria,
      page_size: PAGE_SIZE,
      page_num: searchCriteria.page_num + 1,
      number_of_months: paybackSelected,
    }))
    dispatch(searchTrips())
    dispatch({ type: SEARCH_TRIPS_PAGINATION })
  } else {
    dispatch({
      type: SEARCH_TRIPS_EMIT_ERROR,
      message: 'No more flights'
    })
  }
}

export const displayCorrectPlace = val => (val && val.code) ? (Array.isArray(val.code) ? val.name : val.code) : val

export const createPushQuery = ({ outbound_place, destination_place, number_of_passengers, range_date, outbound_date, return_date, oneWay, paybackSelected }, {
  totalDuration,
  stopover,
  departureTime,
  returnTime,
  departureTakeOff,
  returnTakeOff,
  departureTimeLanding,
  returnTimeLanding,
  sortBy,
  stops
}) => ({
    outbound_place: displayCorrectPlace(outbound_place),
    destination_place: ((destination_place && anywhereCheck(destination_place.name)) ? undefined :
      displayCorrectPlace(destination_place)) || undefined,
    outbound_date: (moment.isMoment(range_date) ? dateYearFirst(range_date) :
      dateYearFirst(range_date && range_date.startDate)) || undefined,
    return_date: (!oneWay ? dateYearFirst(range_date && range_date.endDate) : undefined) || undefined,
    number_of_passengers: number_of_passengers && number_of_passengers.value || 1,
    number_of_months: paybackSelected,
    oneWay: oneWay || false,
    totalDuration,
    stopover: [stopover.min, stopover.max],
    departureTime: [departureTime.min, departureTime.max],
    returnTime: [returnTime.min, returnTime.max],
    departureTimeLanding: [departureTimeLanding.min, departureTimeLanding.max],
    returnTimeLanding: [returnTimeLanding.min, returnTimeLanding.max],
    departureTakeOff,
    returnTakeOff,
    sortBy,
    stops
  })

export const serializeTickets = trips => trips.map(item => addCompaniesMap(item))

export const searchTripsEmit = response => (dispatch, getState) => {
  const message = JSON.parse(response.data)

  if (message.status === 'error') {
    dispatch({ type: SEARCH_TRIPS_EMIT_ERROR })
    dispatch(searchTripReconnect())
  } else if (message.status === 'success' && message.data && message.data.items) {
    const { data: { items, next_page, num_results } } = message
    const {
      form: { SearchField },
      Trips: {
        onConnectBehaviour,
        searchCriteria: { page_num },
        filters,
        paybackSelected,
      }
    } = getState()
    switch (onConnectBehaviour) {
      case FEATURE:
        dispatch({
          type: SEARCH_FEATURE_FLIGHTS_EMIT,
          featureFlights: items.map(item => ({
            ...serializeTicket(item),
            payback: defaultFeatureMonth,
            price: item.per_month
          }))
        })
        break
      case NO_SCROLL:
        dispatch({
          type: SEARCH_TRIPS_EMIT,
          searchResults: serializeTickets(items),
          nextPage: next_page,
          num_results
        })
        break
      default:
        page_num === 1 && dispatch(cleanResults())
        dispatch({
          type: SEARCH_TRIPS_EMIT,
          searchResults: serializeTickets(items),
          nextPage: next_page,
          num_results
        })
        if (page_num === 1 && SearchField && SearchField.values) {
          dispatch(push({
            pathname: '/search',
            query: createPushQuery({ ...SearchField.values, paybackSelected }, filters)
          }))
        }

        if (items && items.length === 0) { dispatch({ type: SEARCH_TRIPS_EMIT_ERROR }) }
    }
    dispatch(searchTripsDisconnect())
  }
}

const serializeRange = arrayRange => {
  if (Array.isArray(arrayRange)) {
    const r0 = Number(arrayRange[0])
    const r1 = Number(arrayRange[1])
    if (r1 > r0) {
      return { min: r0, max: r1 }
    } else {
      return { min: r1, max: r0 }
    }
  } else {
    return arrayRange
  }
}
const checkChanges = (filters, values, type) => filters.some(k => equals(defaultFilters[k], values[k])) ? type : null
const convertToNumber = (val, defaultVal) => parseInt(val) ? +val : defaultVal
const serializeFilters = ({
  totalDuration,
  stopover,
  departureTime,
  returnTime,
  departureTakeOff,
  returnTakeOff,
  departureTimeLanding,
  returnTimeLanding,
  sortBy,
  stops }) => dispatch => {
    dispatch(saveFilters({
      sortBy: convertToNumber(sortBy, defaultFilters.sortBy),
      stops: convertToNumber(stops, defaultFilters.stops)
    }))
    const durationFilt = {
      stopover: serializeRange(stopover || defaultFilters.stopover),
      totalDuration: convertToNumber(totalDuration, defaultFilters.totalDuration)
    }
    const timeFilt = {
      departureTakeOff: departureTakeOff == 'true',
      returnTakeOff: returnTakeOff == 'true',
      departureTime: serializeRange(departureTime || defaultFilters.departureTime),
      returnTime: serializeRange(returnTime || defaultFilters.returnTime),
      departureTimeLanding: serializeRange(departureTimeLanding || defaultFilters.departureTimeLanding),
      returnTimeLanding: serializeRange(returnTimeLanding || defaultFilters.returnTimeLanding),
    }
    dispatch(saveFilters(timeFilt, checkChanges(timeFilters, timeFilt, 'time')))
    dispatch(saveFilters(durationFilt, checkChanges(durationFilters, durationFilt, 'duration')))
  }

export const updateFromUrl = (searchCriteria, filters) => dispatch => {
  dispatch({
    type: UPDATE_FROM_URL,
    onConnectBehaviour: FROM_URL,
    searchCriteria,
  })
  dispatch(serializeFilters(filters))
}

export const searchFromUrl = () => (dispatch, getState) => {
  const {
    form: { SearchField: { values: { outbound_place, destination_place, range_date, oneWay, number_of_passengers } } },
    routing: { locationBeforeTransitions: { pathname } }
  } = getState()
  dispatch(cleanResults())
  dispatch(saveSearchCriteria({
    outbound_place: outbound_place.code,
    ...serializeDestinationPlace(destination_place),
    number_of_passengers: Number(number_of_passengers.value),
    outbound_date: oneWay ? dateYearFirst(range_date.startDate) : dateYearFirst(range_date && range_date.startDate),
    return_date: oneWay ? null : dateYearFirst(range_date && range_date.endDate),
    page_num: 1,
    page_size: PAGE_SIZE,
    one_way: oneWay
  }))
  dispatch(changePassengersNumber(Number(number_of_passengers.value), false, null, true))
  pathname !== '/' && dispatch(searchTrips())
}

export const changeOnConnectBehaviour = onConnectBehaviour => dispatch => {
  dispatch({
    type: CHANGE_ON_CONNECT_BEHAVIOUR,
    onConnectBehaviour
  })
  dispatch(searchTrips())
}

export const saveTempFilters = (filters, name) => ({
  type: SAVE_TEMP_FILTERS,
  filters,
  name
})
export const resetTempFilters = () => ({ type: RESET_TEMP_FILTERS })
export const submitTempFilters = () => ({ type: SUBMIT_TEMP_FILTERS })

export const selectPayback = (paybackSelected, justSelect) => (dispatch, getState) => {
  const payback = { value: paybackSelected || 10 }
  const { routing: { locationBeforeTransitions: { query } } } = getState()

  if (payback && !justSelect) {
    dispatch(setCookie(PAYBACK, JSON.stringify(payback), {}))
    dispatch(replace({
      pathname: '/search',
      query: { ...query, number_of_months: paybackSelected }
    }))
  }
  dispatch({
    type: SELECT_PAYBACK,
    paybackSelected: payback.value,
  })
}

// ------------------------------------
// Reducer
// ------------------------------------

export const initialState = {
  searchCriteria: {
    page_num: 1
  },
  filters: defaultFilters,
  registeredFilters: {},
  tempFilters: defaultFilters,
  tempRegisteredFilters: {},

  searchTripsLoading: false,
  showError: false,
  searchResults: [],
  nextPage: false,
  num_results: 0,
  paybackSelected: 10,

  onConnectBehaviour: NONE,
  loadingFeatureFlights: false,
  featureFlights: []
}

export default createReducer(initialState, {
  [SEARCH_TRIPS_SEND]: (state, { searchTripsLoading, loadingFeatureFlights }) => ({
    searchTripsLoading,
    showError: false,
    reconnect: false,
    loadingFeatureFlights
  }),
  [UPDATE_FROM_URL]: (state, { onConnectBehaviour, searchCriteria }) => ({
    onConnectBehaviour,
    searchCriteria
  }),
  [SEARCH_TRIPS_EMIT_ERROR]: () => ({
    searchTripsLoading: false,
    showError: true
  }),
  [SEARCH_TRIPS_EMIT]: (state, { nextPage, searchResults, num_results }) => ({
    searchTripsLoading: false,
    showError: false,
    nextPage,
    num_results,
    onConnectBehaviour: NONE,
    paginationLoading: false,
    searchResults: state.searchResults.concat(searchResults)
  }),
  [SEARCH_FEATURE_FLIGHTS_EMIT]: (state, { featureFlights }) => ({
    featureFlights,
    loadingFeatureFlights: false,
    onConnectBehaviour: NONE,
    searchTripsLoading: false
  }),
  [CLEAN_RESULTS]: () => ({
    searchResults: [],
    nextPage: false
  }),
  [CHANGE_ON_CONNECT_BEHAVIOUR]: (state, { onConnectBehaviour }) => ({ onConnectBehaviour }),
  [SAVE_SEARCH_CRITERIA]: (state, { searchCriteria }) => ({ searchCriteria }),
  [SAVE_FILTERS]: (state, { filters, name }) => ({
    filters: { ...state.filters, ...filters },
    registeredFilters: { ...state.registeredFilters, [name]: true }
  }),
  [RESET_SEARCH_CRITERIA]: (state, { filters, name }) => ({
    filters: { ...state.filters, ...filters },
    registeredFilters: { ...state.registeredFilters, [name]: false }
  }),
  [SAVE_TEMP_FILTERS]: (state, { filters, name }) => ({
    tempFilters: { ...state.tempFilters, ...filters },
    tempRegisteredFilters: { ...state.tempRegisteredFilters, [name]: true }
  }),
  [RESET_TEMP_FILTERS]: ({ filters, registeredFilters }) => ({
    tempFilters: filters,
    tempRegisteredFilters: registeredFilters,
  }),
  [SEARCH_TRIPS_CONNECT]: ({ onConnectBehaviour }) => ({ searchTripsLoading: onConnectBehaviour === NONE }),
  [SEARCH_TRIPS_RECONNECT]: ({ timeout }) => ({ timeout }),
  [SEARCH_TRIPS_DISCONNECT]: () => ({ searchTripsLoading: false }),
  [SEARCH_TRIPS_STOP_TIMEOUT]: ({ timeout }) => ({ timeout: clearTimeout(timeout) }),
  [UNMOUNT_SEARCH_FIELD]: () => initialState,
  [SEARCH_TRIPS_PAGINATION]: () => ({ paginationLoading: true }),
  [SUBMIT_TEMP_FILTERS]: ({ tempFilters, tempRegisteredFilters }) => ({
    filters: tempFilters,
    registeredFilters: tempRegisteredFilters
  }),
  [SELECT_PAYBACK]: (state, { paybackSelected }) => ({ paybackSelected }),
})
