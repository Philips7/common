import createReducer from '../../utils/createReducer'
import {hide} from 'redux-modal'
import { getCookie, setCookie, removeCookie} from 'redux-cookie'
import {errorNotification, successNotification} from '../../utils/notifications'
import {push} from 'react-router-redux/lib/actions'
import {I18n} from 'react-redux-i18n'
import {serializeUser, convertTitle, isClient, scrollToElement} from '../../utils/helpers'
import moment from 'moment'
import {reset} from 'redux-form'

const debug = require('debug')('app:UserReducer')

const validateUser = user => ({
  ...user,
  birth_date: user.birth_date && moment(user.birth_date).format('DD-MM-YYYY'),
  title: convertTitle(user.title)
})

const constants = {
  week: 604800
}

// ------------------------------------
// Constants
// ------------------------------------
export const SIGN_UP_FACEBOOK_ATTEMPT = 'User.SIGN_UP_FACEBOOK_ATTEMPT'
export const SIGN_UP_FACEBOOK_SUCCESS = 'User.SIGN_UP_FACEBOOK_SUCCESS'
export const SIGN_UP_FACEBOOK_FAILURE = 'User.SIGN_UP_FACEBOOK_FAILURE'

export const SIGN_UP_ATTEMPT = 'User.SIGN_UP_ATTEMPT'
export const SIGN_UP_SUCCESS = 'User.SIGN_UP_SUCCESS'
export const SIGN_UP_FAILURE = 'User.SIGN_UP_FAILURE'

export const SIGN_IN_ATTEMPT = 'User.SIGN_IN_ATTEMPT'
export const SIGN_IN_SUCCESS = 'User.SIGN_IN_SUCCESS'
export const SIGN_IN_FAILURE = 'User.SIGN_IN_FAILURE'

export const USER_GET_ATTEMPT = 'User.USER_GET_ATTEMPT'
export const USER_GET_SUCCESS = 'User.USER_GET_SUCCESS'
export const USER_GET_FAILURE = 'User.USER_GET_FAILURE'

export const USER_UPDATE_ATTEMPT = 'User.USER_UPDATE_ATTEMPT'
export const USER_UPDATE_SUCCESS = 'User.USER_UPDATE_SUCCESS'
export const USER_UPDATE_FAILURE = 'User.USER_UPDATE_FAILURE'

export const REFRESH_TOKEN_ATTEMPT = 'User.REFRESH_TOKEN_ATTEMPT'
export const REFRESH_TOKEN_SUCCESS = 'User.REFRESH_TOKEN_SUCCESS'
export const REFRESH_TOKEN_FAILURE = 'User.REFRESH_TOKEN_FAILURE'

export const SIGN_UP_NEWSLETTER_ATTEMPT = 'Landing.SIGN_UP_NEWSLETTER_ATTEMPT'

export const SET_AUTH_COOKIES = 'User.SET_AUTH_COOKIES'
export const EXPIRE_AUTH_COOKIES = 'User.EXPIRE_AUTH_COOKIES'

export const CHANGE_TAB_NUMBER = 'Checkout.CHANGE_TAB_NUMBER'

export const LOGOUT = 'User.LOGOUT'
// ------------------------------------
// Actions
// ------------------------------------
export const signUpFacebookAttempt = facebookResponse => dispatch => new Promise((resolve, reject) => {
  const {status, accessToken} = facebookResponse

  if (status !== 'not_authorized') {
    dispatch({
      type: SIGN_UP_FACEBOOK_ATTEMPT,
      values: {
        accessToken
      },
      responseSuccess: singUpFacebookSuccess,
      responseFailure: singUpFacebookFailure,
      resolve,
      reject
    })
  }
})

export const singUpFacebookSuccess = authData => dispatch => {
  dispatch(hide('registerModal'))
  dispatch({type: SIGN_UP_FACEBOOK_SUCCESS})
  dispatch(redirectAfterLogin(authData))
}

export const singUpFacebookFailure = error => dispatch => {
  dispatch(errorNotification(I18n.t('Notify.failedSignUp')))
  dispatch({
    type: SIGN_UP_FACEBOOK_FAILURE,
    error
  })
}

export const signUpAttempt = values => dispatch => new Promise((resolve, reject) => {
  dispatch({
    type: SIGN_UP_ATTEMPT,
    values: {
      ...values,
      email: values.email.toLowerCase()
    },
    responseSuccess: signUpSuccess,
    responseFailure: signUpFailure,
    resolve,
    reject
  })
})

export const signUpSuccess = values => dispatch => {
  dispatch(hide('registerModal'))
  dispatch({type: SIGN_UP_SUCCESS})
  dispatch(signInAttempt(values))
}

export const signUpFailure = error => dispatch => {
  dispatch(errorNotification(I18n.t('Notify.failedSignUp')))
  dispatch({
    type: SIGN_UP_FAILURE,
    error
  })
}

export const userUpdateAttempt = values => (dispatch, getState) => new Promise((resolve, reject) => {
  dispatch({
    type: USER_UPDATE_ATTEMPT,
    values: {...serializeUser(values)},
    responseSuccess: userUpdateSuccess,
    responseFailure: userUpdateFailure,
    resolve,
    reject
  })
})

export const userUpdateSuccess = user => (dispatch, getState) => {
  const {PassengerInfoForm, SettingsForm} = getState().form
  const {email} = PassengerInfoForm ? PassengerInfoForm.values : SettingsForm.values
  const notification = (email && email !== user.email) ? I18n.t('Notify.emailChanged') : I18n.t('Notify.saveChanges')
  if (SettingsForm) {
    dispatch(successNotification(notification))
    dispatch(reset('SettingsForm'))
  }

  dispatch(updateUserCallBack(user))
}

export const updateUserCallBack = user => ({
  type: USER_UPDATE_SUCCESS,
  user: validateUser(user)
})

export const userUpdateFailure = () => dispatch => {
  dispatch(errorNotification())
  dispatch({type: USER_UPDATE_FAILURE})
}

export const signInAttempt = values => dispatch => new Promise((resolve, reject) => {
  dispatch({
    type: SIGN_IN_ATTEMPT,
    values: {
      ...values,
      username: values.email.toLowerCase()
    },
    responseSuccess: signInSuccess,
    responseFailure: signInFailure,
    resolve,
    reject
  })
})

export const setAuthCookies = ({access_token, refresh_token, expires_in}, rememberMe) => dispatch => {
  dispatch({type: SET_AUTH_COOKIES})
  dispatch(setCookie('token', access_token, {expires: expires_in}))
  dispatch(setCookie('refresh_token', refresh_token, {expires: rememberMe ? constants.week : expires_in}))
}

export const expireAuthCookies = (leaveAccessToken = false) => dispatch => {
  dispatch({type: EXPIRE_AUTH_COOKIES})
  !leaveAccessToken && dispatch(removeCookie('token'))
  dispatch(removeCookie('refresh_token'))
}

const changeTabNumber = tabNumber => ({
  type: CHANGE_TAB_NUMBER,
  tabNumber
})

export const signInSuccess = authData => (dispatch, getState) => {
  const {User: {rememberMe}, Checkout} = getState()

  dispatch(redirectAfterLogin(authData, rememberMe))
  dispatch({type: SIGN_IN_SUCCESS})

  if (Checkout && Checkout.emailRegistered) {
    dispatch(changeTabNumber(1))
  }
}

export const signInFailure = () => ({type: SIGN_IN_FAILURE})

export const userGetAttempt = () => ({
  type: USER_GET_ATTEMPT,
  responseSuccess: userGetSuccess,
  responseFailure: userGetFailure
})

export const userGetSuccess = (user, loggedIn = true) =>({
  type: USER_GET_SUCCESS,
  user: validateUser(user),
  loggedIn
})

export const userGetFailure = () => dispatch => {
  dispatch(expireAuthCookies())
  dispatch({type: USER_GET_FAILURE})
	if (isClient) {
		dispatch(push('/'))
	}
}

export const refreshToken = () => (dispatch, getState) => {

	let refresh_token, token

	if (!isClient) {
		refresh_token = getState().Common.refresh_token
		token = getState().Common.token
	} else {
		refresh_token = dispatch(getCookie('refresh_token'))
		token = dispatch(getCookie('token'))
	}

  if (refresh_token || token) {
    if (!token) {
      dispatch({
        type: REFRESH_TOKEN_ATTEMPT,
        values: {
          refresh_token
        },
        responseSuccess: refreshTokenSuccess,
        responseFailure: refreshTokenFailure
      })
    } else {
      dispatch(userGetAttempt())
    }
  } else {
    dispatch(refreshTokenFailure(true))
  }
}

export const refreshTokenSuccess = authData => dispatch => {
  dispatch(setAuthCookies(authData, true))
  dispatch({type: REFRESH_TOKEN_SUCCESS})
}

export const refreshTokenFailure = leaveAccessToken => dispatch => {
  dispatch({type: REFRESH_TOKEN_FAILURE})
  dispatch(expireAuthCookies(leaveAccessToken))
}

export const logout = () => (dispatch, getState) => {
  const {routing: {locationBeforeTransitions: {pathname}}} = getState()
  dispatch(expireAuthCookies())
  dispatch({type: LOGOUT})
  dispatch(changeTabNumber(1))
  protectedPath.indexOf(pathname) >= 0 && dispatch(push('/'))
}

const redirectAfterLogin = (authData, rememberMe = true) => dispatch => {
  dispatch(hide('loginModal'))
  dispatch(setAuthCookies(authData, rememberMe))
  dispatch(userGetAttempt())
}

export const signUpNewsletterAttempt = values => (dispatch, getState) => new Promise((resolve, reject) => {
	const {routing: {locationBeforeTransitions: {pathname}}} = getState()
  dispatch({
    type: SIGN_UP_NEWSLETTER_ATTEMPT,
    values: {
      ...values,
      email: values.email.toLowerCase(),
			fromLanding: pathname === '/'
    },
    resolve,
    reject
  })
})

export const scrollToHowItWorks = () => (dispatch, getState) => {
    const {routing: {locationBeforeTransitions: {pathname}}} = getState()
  dispatch(push('/#how-it-works'))
  //Hack to scroll from /search page
  setTimeout(() => scrollToElement('howItWorksElement'), pathname !== '/' ? 500 : 0)
}

// ------------------------------------
// Reducer
// ------------------------------------
export const initialState = {
  signUpLoading: false,
  signInLoading: false,
  userGetLoading: false,
  userUpdateLoading: false,
  loggedIn: false,

  user: {},
  rememberMe: false
}

export default createReducer(initialState, {
  [SIGN_UP_FACEBOOK_ATTEMPT]: () => ({
    signUpLoading: true,
    isVerifiedMessage: ''
  }),
  [SIGN_UP_FACEBOOK_SUCCESS]: () => ({signUpLoading: false}),
  [SIGN_UP_FACEBOOK_FAILURE]: () => ({signUpLoading: false}),

  [SIGN_UP_ATTEMPT]: () => ({signUpLoading: true}),
  [SIGN_UP_SUCCESS]: () => ({signUpLoading: false}),
  [SIGN_UP_FAILURE]: () => ({
    signUpLoading: false,
    signUpSuccess: true
  }),

  [USER_GET_ATTEMPT]: () => ({userGetLoading: true}),
  [USER_GET_SUCCESS]: (state, {user, loggedIn}) => ({
    userGetLoading: false,
    loggedIn,
    user
  }),
  [USER_GET_FAILURE]: () => ({userGetLoading: false}),

  [USER_UPDATE_ATTEMPT]: () => ({userUpdateLoading: true}),
  [USER_UPDATE_SUCCESS]: (state, action) => ({
    userUpdateLoading: false,
    loggedIn: true,
    user: {...state.user, ...action.user}
  }),
  [USER_UPDATE_FAILURE]: () => ({userUpdateLoading: false}),

  [SIGN_IN_ATTEMPT]: (state, {values: {rememberMe}}) => ({signInLoading: true, rememberMe}),
  [SIGN_IN_SUCCESS]: () => ({
    signInLoading: false,
    loggedIn: true
  }),
  [SIGN_IN_FAILURE]: () => ({signInLoading: false}),
  [REFRESH_TOKEN_SUCCESS]: () => ({loggedIn: true}),
  [LOGOUT]: () => ({loggedIn: false, user: null})
})
