import createReducer from '../../utils/createReducer'
import config from 'config.json'

import configI18n from '../../config.js'
// ------------------------------------
// Constants
// ------------------------------------
export const GET_COUNTRIES_ATTEMPT = 'Countries.GET_COUNTRIES_ATTEMPT'
export const GET_COUNTRIES_SUCCESS = 'Countries.GET_COUNTRIES_SUCCESS'
export const GET_COUNTRIES_FAILURE = 'Countries.GET_COUNTRIES_FAILURE'

// ------------------------------------
// Actions
// ------------------------------------
export const getCountries = () => ({
  type: GET_COUNTRIES_ATTEMPT,
  responseSuccess: getCountriesSuccess,
  responseFailure: getCountriesFailure
})

export const getCountriesSuccess = countries => {
  const id = countries.findIndex( country => country.name === configI18n.countryName)
  countries.unshift(countries[id])
  countries.splice(id + 1, 1)
  countries.map((country) => {
    country.image = `${config.staticData.baseUrl}country_flags/${country.pk}.svg`
  })
  return {type: GET_COUNTRIES_SUCCESS, countries}
}

export const getCountriesFailure = () => ({type: GET_COUNTRIES_FAILURE})

// ------------------------------------
// Reducer
// ------------------------------------
export const initialState = {
  countries: []
}

export default createReducer(initialState, {
  [GET_COUNTRIES_SUCCESS]: (state, {countries}) => ({countries})
})
