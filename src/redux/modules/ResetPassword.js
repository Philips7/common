import createReducer from '../../utils/createReducer'
import {successNotification, errorNotification} from '../../utils/notifications'
import {hide} from 'redux-modal'
import {I18n} from 'react-redux-i18n'

// ------------------------------------
// Constants
// ------------------------------------
export const RESET_PASSWORD_ATTEMPT = 'ResetPassword.RESET_PASSWORD_ATTEMPT'
export const RESET_PASSWORD_SUCCESS = 'ResetPassword.RESET_PASSWORD_SUCCESS'
export const RESET_PASSWORD_FAILURE = 'ResetPassword.RESET_PASSWORD_FAILURE'

// ------------------------------------
// Actions
// ------------------------------------
export const resetPasswordAttempt = values => dispatch => new Promise((resolve, reject) => dispatch({
  type: RESET_PASSWORD_ATTEMPT,
  values,
  responseSuccess: resetPasswordSuccess,
  responseFailure: resetPasswordFailure,
  resolve,
  reject
}))

export const resetPasswordSuccess = () => dispatch => {
  dispatch(successNotification(I18n.t('Notify.changePasswordAttempt')))
  dispatch(hide('resetPasswordModal'))
  dispatch({type: RESET_PASSWORD_SUCCESS})
}

export const resetPasswordFailure = () => dispatch => {
  dispatch(errorNotification(I18n.t('Notify.tryAgain')))
  dispatch({type: RESET_PASSWORD_FAILURE})
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  resetPasswordLoading: true
}

export default createReducer(initialState, {
  [RESET_PASSWORD_ATTEMPT]: (state, action) => ({
    resetPasswordLoading: true
  }),
  [RESET_PASSWORD_SUCCESS]: (state, action) => ({
    resetPasswordLoading: false
  }),
  [RESET_PASSWORD_FAILURE]: (state, action) => ({
    resetPasswordLoading: false
  })
})
