import React from 'react'
import pure from 'recompose/pure'
import FormGroup from 'react-bootstrap/lib/FormGroup'
import ControlLabel from 'react-bootstrap/lib/ControlLabel'
import MaskedInput from 'react-text-mask'
import cn from 'classnames'
import s from './FieldGroup.scss'

export const PhonePrefix = pure(({input, disabled, meta: {error, touched}, type, customStyle, button, customStyleSm, inputStyle, labelStyle, label, placeholder, customError, ...props}) =>
  <FormGroup controlId={input.name} className={cn(s.phoneInput, touched && error && s.hasError, touched && !error && s.valid)}>
    {label && <ControlLabel className={labelStyle || s.label}>
      <Translate value={label}/>
    </ControlLabel>}
    <div className={button && s.inputBtn}>
    <MaskedInput
      {...input}
      className={cn(inputStyle || (customStyle && s.customField) || (customStyleSm && s.customFieldSm) || s.input, touched && error && s.hasError, touched && !error && s.valid)}
      type={type}
      name={input.name}
      disabled={disabled}
      placeholder={placeholder}
      guide={false}
      mask={[0, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
    />
      {button}
    </div>
    {touched && (error || customError) && <span className={s.error}>{error || customError}</span>}
  </FormGroup>
)

export default PhonePrefix
