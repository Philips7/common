import React from 'react'
import {pure, compose, withHandlers} from 'recompose'
import cn from 'classnames'
import s from './FieldGroup.scss'

const enchance = compose(
  withHandlers({
    onChange: ({validate, input}) => ({target: {value}}) => validate ? (validate(value) && input.onChange(value)) : input.onChange(value)
  }),
  pure
)

const InputFieldGroup = enchance(({
  onChange, input, button, type, meta: {touched, error, visited}, disabled, labelStyle, id, greyButton,
  placeholder, customError, inputStyle, customStyle, customStyleSm, customStyleLg, showError, validate, absoluteValidation, passwordField, ...props
  }) =>
  <div>
    <div className={cn(button && s.inputBtn, greyButton && s.greyInputButton)}>
      <input
        {...input}
        className={cn(inputStyle || (customStyle && s.customField) || (customStyleSm && s.customFieldSm)
        || (customStyleLg && s.customFieldLg) || s.input, touched && error && s.hasError, touched && (!passwordField || visited) && !error && s.valid)}
        type={type}
        onChange={onChange}
        id={id}
        disabled={disabled}
        placeholder={placeholder}
      />
      {button}
    </div>
    {touched && showError && (error || customError) &&
    <span className={s.error}>{error || customError}</span>}
  </div>
)

InputFieldGroup.defaultProps = {
  showError: true
};

export default InputFieldGroup
