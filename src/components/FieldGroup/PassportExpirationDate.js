import React from 'react'
import {pure} from 'recompose'
import FormGroup from 'react-bootstrap/lib/FormGroup'
import ControlLabel from 'react-bootstrap/lib/ControlLabel'
import Checkbox from 'react-bootstrap/lib/Checkbox'
import s from './FieldGroup.scss'

export const CheckboxComponent = pure(({input, meta, label, disabled, inputStyle, ...props}) =>
  <div className='flex-1'>
    <Checkbox
      {...props}
      {...input}
      disabled={disabled}
      className={inputStyle || s.checkboxLabel}
    >
      {label}
    </Checkbox>
  </div>
)

export const PassportExpirationDate = pure(({labelStyle, label, checkboxComponent, datePickerComponent}) =>
  <FormGroup className={s.formGroup}>
    {label &&
    <ControlLabel className={labelStyle || s.label}>
      {label}
    </ControlLabel>
    }
    <div>
      {datePickerComponent}
    </div>
    <div>
      {checkboxComponent}
    </div>
  </FormGroup>
)

PassportExpirationDate.propTypes = {
  checkboxComponent: React.PropTypes.element.isRequired,
  datePickerComponent: React.PropTypes.element.isRequired,
  label: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.object
  ]),
  labelStyle: React.PropTypes.string,
  inputStyle: React.PropTypes.string
}

export default PassportExpirationDate
