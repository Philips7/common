import React from 'react'
import Select from 'react-select'
import {pure} from 'recompose'
import FormGroup from 'react-bootstrap/lib/FormGroup'
import ControlLabel from 'react-bootstrap/lib/ControlLabel'
import cn from 'classnames'
import s from './FieldGroup.scss'

export const CountryFieldGroup = pure(({input, label, meta: {error, touched}, labelStyle, inputStyle, customStyle, customStyleSm, valueKey, ...props}) =>
  <FormGroup controlId={input.name} className={cn(s.formGroup, s.countrySelect)}>
    {label &&
    <ControlLabel className={labelStyle || s.label}>
      {label}
    </ControlLabel>
    }
    <Select
      {...props}
      {...input}
      className={cn(inputStyle || s.select, customStyle && s.customSelect, customStyleSm && s.customSelectSm, touched && error && 'error-valid', touched && !error && 'select-valid')}
      valueKey={valueKey}
      matchPos='start'
      matchValue={false}
      matchLabel={true}
      onBlur={() => input.onBlur(input.value[valueKey])}
      // if option is not selected it returns empty array and throw error, so we should check it and return null
      onChange={value => input.onChange(!Array.isArray(value) ? value[valueKey] : null)}
      valueRenderer={countryWithFlag}
      optionRenderer={countryWithFlag}
    />
    {touched && error && <span className={s.error}>{error}</span>}
  </FormGroup>
)

const countryWithFlag = ({image, name}) =>
  <div>
    <img src={image} style={{ width: 22, marginRight: 10 }} />
    {name}
  </div>


export default CountryFieldGroup
