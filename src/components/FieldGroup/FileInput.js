import React, { Component } from 'react'
import Dropzone from 'react-dropzone'
import {pure} from 'recompose'
import cn from 'classnames'
import s from './FileInput.scss'

const SelectedFiles = pure(({files, onClick}) =>
  <div>
    {files.map(file =>
      <div key={file.lastModified} className={s.resulstsWrap}>
        {file.type.match('image') && <img style={{width: '100px'}} src={file.preview} className={s.image}/>}
        <div>{file.name}</div>
        <button className={s.removeBtn} onClick={onClick}>X</button>
      </div>)
    }
  </div>
)

@pure
class FileInput extends Component {
  static defaultProps = {
    className: ''
  };

  render() {
    const {className, input: {onChange, value}, dropzone_options, meta: {error, touched}, label, classNameLabel, children, name} = this.props;
    const onDrop = (f = null) => {
      onChange(f)
    }
    return (
      <div className={`${className}` + (error && touched ? ' has-error ' : '')}>
        {label && <p className={classNameLabel || ''}>{label}</p>}
        <Dropzone
          {...dropzone_options}
          onDrop={onDrop}
          className={cn('dropzone-input', s.dropzone)}
          name={name}
        >
          {!value ? children : ''}
        </Dropzone>
        {value && <SelectedFiles files={value} onClick={() => onDrop()}/>}
        {error && touched ? error : ''}
      </div>
    );
  }
}

export default FileInput
