import React from 'react'
import {pure} from 'recompose'
import FormGroup from 'react-bootstrap/lib/FormGroup'
//import ControlLabel from 'react-bootstrap/lib/ControlLabel'
import MaskedInput from 'react-text-mask'
import cn from 'classnames'
import s from './FieldGroup.scss'

import createAutoCorrectedDatePipe from 'text-mask-addons/dist/createAutoCorrectedDatePipe.js'
const autoCorrectedDatePipe = createAutoCorrectedDatePipe('dd/mm/yyyy')

const DateFieldGroup = pure(({input, label, type, meta: {touched, error, active}, disabled, labelStyle, placeholder, inputStyle, customStyle, customStyleSm, showError}) =>
  <FormGroup controlId={input.name} className={cn(s.formGroup, s.DatePickerGroup)}>
    <MaskedInput
      {...input}
      className={cn(inputStyle || (customStyle && s.customField) || (customStyleSm && s.customFieldSm) || s.input, touched && error && s.hasError, touched && !error && s.valid)}
      type={type}
      name={input.name}
      disabled={disabled}
      placeholder={placeholder}
      guide={false}
      mask={[/\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
      pipe={autoCorrectedDatePipe}
    />
    <label htmlFor={input.name} className={s.placeholder}>DD-MM-YYYY</label>
    {(touched || showError) && error && <span className={s.error}>{error}</span>}
  </FormGroup>
)

export default DateFieldGroup
