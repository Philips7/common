import React from 'react'
import {pure} from 'recompose'
import FormGroup from 'react-bootstrap/lib/FormGroup'
import ControlLabel from 'react-bootstrap/lib/ControlLabel'
import cn from 'classnames'
import s from './FieldGroup.scss'
import Select from 'react-select'

export const TitleFieldGroup = pure(({input, label, meta: {error, touched}, labelStyle, inputStyle, customStyle, customStyleSm, valueKey, ...props}) =>
  <div className={s.firstNameGender}>
    {label && input.value.label}
    <Select
      {...props}
      {...input}
      className={cn(inputStyle || s.select, customStyle && s.customSelect,  customStyleSm && s.customSelectSm, touched && !error && 'select-valid',  touched && error && 'error-valid')}
      valueKey={valueKey}
			searchable={false}
      onBlur={() => input.onBlur(input.value[valueKey])}
      // if option is not selected it returns empty array and throw error, so we should check it and return null
      onChange={value => input.onChange(!Array.isArray(value) ? value[valueKey] : null)}
      clearable={false}
    />
    {touched && error && <span className={s.error}>{error}</span>}
  </div>
)

export const TextFieldGroup = pure(({input, label, meta: {error, touched}, disabled, labelStyle, inputStyle, customStyle, customStyleSm, selectInputComponent, placeholder, ...props}) =>
  <FormGroup controlId={input.name} className={s.formGroup}>
    {label && <ControlLabel className={labelStyle || s.label}>
      {label}
    </ControlLabel>}
    <div className={s.firstNameWrap}>
      {selectInputComponent}

      <div className={s.firstNameName}>
        <input
          className={cn(inputStyle || (customStyle && s.customField) || (customStyleSm && s.customFieldSm) || s.input, touched && error && s.hasError, touched && !error && s.valid)}
          type='text'
          {...input}
          {...props}
          placeholder={placeholder}
          disabled={disabled}
        />
        {touched && error && <span className={s.error}>{error}</span>}
      </div>
    </div>
  </FormGroup>
)

TextFieldGroup.propTypes = {
  selectInputComponent: React.PropTypes.element.isRequired,
  label: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.object
  ]),
  labelStyle: React.PropTypes.string,
  inputStyle: React.PropTypes.string
}

export default TextFieldGroup
