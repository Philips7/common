import React from 'react'
import {pure} from 'recompose'
import FormGroup from 'react-bootstrap/lib/FormGroup'
import ControlLabel from 'react-bootstrap/lib/ControlLabel'
import cn from 'classnames'
import s from './FieldGroup.scss'

const TextAreaFieldGroup = pure(({input, label, type, meta: {touched, error}, disabled, labelStyle, placeholder, inputStyle, customStyle, customStyleSm, customStyleLg, showError, rows,...props}) =>
  <FormGroup controlId={input.name} className={s.formGroup} >
    {label &&
    <ControlLabel className={labelStyle || s.label}>
      {label}
    </ControlLabel>
    }
    <textarea
      rows={rows}
      {...input}
      className={cn(inputStyle || (customStyle && s.customField) || (customStyleSm && s.customFieldSm) || (customStyleLg && s.customFieldLg) || s.input, touched && error && s.hasError, touched && !error && s.valid, s.textArea)}
      disabled={disabled}
      placeholder={placeholder}
    />
    {(touched || showError) && error && <span className={s.error}>{error}</span>}
  </FormGroup>
)

export default TextAreaFieldGroup
