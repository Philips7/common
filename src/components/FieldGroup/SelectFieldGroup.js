import React from 'react'
import Select from 'react-select'
import {pure} from 'recompose'
import FormGroup from 'react-bootstrap/lib/FormGroup'
import ControlLabel from 'react-bootstrap/lib/ControlLabel'
import cn from 'classnames'
import s from './FieldGroup.scss'

export const SelectFieldGroup = pure(({input, label, meta: {error, touched}, labelStyle, placeholder, inputStyle, customStyle, customStyleSm, customStyleLg, valueKey, absoluteValidation, ...props}) =>
  <FormGroup controlId={input.name} className={cn(s.formGroup, absoluteValidation && s.absoluteValidation)}>
    {label &&
    <ControlLabel className={labelStyle || s.label}>
      {label}
    </ControlLabel>
    }
    <Select
      {...props}
      {...input}
      className={cn(inputStyle || s.select, customStyle && s.customSelect,  customStyleSm && s.customSelectSm, customStyleLg && s.customSelectLg, touched && error && 'error-valid', touched && !error && 'select-valid')}
      valueKey={valueKey}
      matchPos='start'
      matchValue={false}
      matchLabel={true}
      placeholder={placeholder}
      onBlur={() => input.onBlur(input.value[valueKey])}
      // if option is not selected it returns empty array and throw error, so we should check it and return null
      onChange={value => input.onChange(!Array.isArray(value) ? value[valueKey] : null)}
    />
    {touched && error && <span className={s.error}>{error}</span>}
  </FormGroup>
)

export default SelectFieldGroup
