import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connectModal } from 'redux-modal'
import { connect } from 'react-redux'
import Modal from 'react-bootstrap/lib/Modal'
import { forgetAboutTicket } from 'routes/Landing/modules/Landing'
import s from './ModalBody.scss'
import cn from 'classnames'

const marginBottom10 = {marginBottom: '10px'}
const marginBottom20 = {marginBottom: '20px'}
const marginBottom50 = {marginBottom: '50px'}

export class ResetPasswordModal extends Component {
  static Component = null
  mounted = false

  state = {
    Component: ResetPasswordModal.Component
  }

  static propTypes = {
    show: PropTypes.bool.isRequired,
    handleHide: PropTypes.func.isRequired,
  }

  componentWillMount() {
    if (this.state.Component === null) {
      System.import('../../../containers/ResetPasswordModalContainer').then(m => m.default).then(Component => {
        ResetPasswordModal.Component = Component
        if (this.mounted) {
          this.setState({Component})
        }
      })
    }
  }

  componentDidMount() {
    this.mounted = true
  }

  componentWillUnmount() {
    this.mounted = false
  }

  render() {
    const {show, handleHide} = this.props
    const {Component} = this.state

    return (
      <Modal
        show={show}
        onHide={() => {
          handleHide()
        }}
        className={s.modal}
      >
        <Modal.Body className={s.modalBody}>
          {Component ? <Component handleHide={handleHide}/> :
            <div className={s.skeletonWrap}>
              <div className={cn(s.titleElement, s.skeletonElement)} style={marginBottom20}></div>
              <div className={cn(s.fieldElement, s.skeletonElement)} style={marginBottom50}></div>
              <div className={cn(s.buttonElement, s.skeletonElement, s.btnRed)} style={marginBottom10}></div>
            </div>
          }
        </Modal.Body>
      </Modal>
    )
  }
}

const mapActionCreators = {
  forgetAboutTicket
}

export default connect(() => ({}), mapActionCreators)(
  connectModal({name: 'resetPasswordModal', destroyOnHide: true})(ResetPasswordModal)
)
