import React from 'react'
import PropTypes from 'prop-types'
import Form from 'react-bootstrap/lib/Form'
import {reduxForm, Field} from 'redux-form'
import validator from 'validator'
import InputFieldGroup from 'components/FieldGroup/InputFieldGroup'
import s from './ResetPasswordModal.scss'
import {I18n} from 'react-redux-i18n'
import CrossIcon from 'static/images/cross-icon.svg'
import InlineSVG from 'svg-inline-react'

const validate = values => {
  let errors = {}

  if (!values.email) {
    errors.email = I18n.t('form.required')
  } else if (!validator.isEmail(values.email)) {
    errors.email = I18n.t('form.invalidEmailError')
  }

  return errors
}

const ResetPasswordModal = ({handleHide, handleSubmit, submitSucceeded}) =>
  <div className={s.modal}>
    <span
      onClick={handleHide}
      className={s.closeModalBtn}
    >
      <InlineSVG src={CrossIcon}/>
    </span>
    <h3 className={s.title}>{I18n.t('Header.forgotPassword')}</h3>

    <Form
      className={s.resetPasswordForm}
      noValidate
      onSubmit={handleSubmit}
    >
      <div className={s.formGroup}>
        <Field
          name='email'
          component={InputFieldGroup}
          type='text'
          placeholder='Email'
          customStyle
        />
      </div>

      <button
        className={s.btnSubmit}
        type='submit'
      >
        {I18n.t('form.submit')}
      </button>
    </Form>
  </div>

ResetPasswordModal.propTypes = {
  handleHide: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitSucceeded: PropTypes.bool.isRequired
}

export default
reduxForm({
  form: 'ResetPasswordModal',
  validate
})(ResetPasswordModal)
