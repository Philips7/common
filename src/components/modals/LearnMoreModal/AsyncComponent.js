import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connectModal } from 'redux-modal'
import Modal from 'react-bootstrap/lib/Modal'
import s from './ModalBody.scss'

class LearnMoreModal extends Component {
  static propTypes = {
    show: PropTypes.bool.isRequired,
    handleHide: PropTypes.func.isRequired
  }
  constructor() {
    super()
    this.state = {
      ModalComponent: null
    }
    this.mounted = false
  }

  componentWillMount() {
    if (this.state.ModalComponent === null) {
      System.import('../../../containers/LearnMoreModalContainer').then(m => m.default).then((ModalComponent) => {
        LearnMoreModal.Component = ModalComponent
        if (this.mounted) {
          this.setState({ ModalComponent })
        }
      })
    }
  }

  componentDidMount() {
    this.mounted = true
  }

  componentWillUnmount() {
    this.mounted = false
  }

  render() {
    const { show, handleHide } = this.props
    const { ModalComponent } = this.state
    return (
      <Modal
        show={show}
        className={s.modal}
      >
        <Modal.Body className={s.modalBody}>
          {
            ModalComponent && <ModalComponent handleHide={handleHide} />
          }
        </Modal.Body>
      </Modal>
    )
  }
}

export default connectModal({ name: 'LearnMoreModal', destroyOnHide: true })(LearnMoreModal)

