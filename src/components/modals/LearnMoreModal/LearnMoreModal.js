import React from 'react'
import { pure } from 'recompose'
import InlineSVG from 'svg-inline-react'
import { Translate } from 'react-redux-i18n'
import WarningIcon from 'static/images/warning-icon.svg'
import closeIcon from './assets/close-icon.svg'
import s from './LearnMoreModal.scss'

const LearnMoreModal = pure(({ handleHide }) =>
  <div className={s.modal}>
    <div className={s.modalHeader}>
      <h2 className={s.modalTitle}>
        <InlineSVG src={WarningIcon} />
        <span><Translate value='Checkout.learnMoreButton' /></span>
      </h2>
      <a onClick={handleHide} className={s.closeModalBtn}>
        <InlineSVG src={closeIcon} />
      </a>
    </div>
    <div className={s.modalBody}>
      <Translate value='Checkout.learnMore' />
    </div>
  </div>
)

export default LearnMoreModal
