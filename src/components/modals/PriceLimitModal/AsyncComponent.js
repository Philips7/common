import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connectModal } from 'redux-modal'
import Modal from 'react-bootstrap/lib/Modal'
import s from './ModalBody.scss'

class PriceLimitModal extends Component {
  static propTypes = {
    show: PropTypes.bool.isRequired,
    handleHide: PropTypes.func.isRequired,
    isAbove: PropTypes.bool.isRequired,
    total_price: PropTypes.number.isRequired,
    passengers: PropTypes.number.isRequired,
  }
  constructor() {
    super()
    this.state = {
      ModalComponent: null
    }
    this.mounted = false
  }

  componentWillMount() {
    if (this.state.ModalComponent === null) {
      System.import('../../../containers/PriceLimitModalContainer').then(m => m.default).then((ModalComponent) => {
        PriceLimitModal.Component = ModalComponent
        if (this.mounted) {
          this.setState({ ModalComponent })
        }
      })
    }
  }

  componentDidMount() {
    this.mounted = true
  }

  componentWillUnmount() {
    this.mounted = false
  }

  render() {
    const { show, handleHide, isAbove, total_price, passengers } = this.props
    const { ModalComponent } = this.state
    return (
      <Modal
        show={show}
        className={s.modal}
      >
        <Modal.Body className={s.modalBody}>
          {
            ModalComponent &&
            <ModalComponent
              handleHide={handleHide}
              isAbove={isAbove}
              total_price={total_price}
              passengers={passengers}
            />
          }
        </Modal.Body>
      </Modal>
    )
  }
}

export default connectModal({ name: 'priceLimitModal', destroyOnHide: true })(PriceLimitModal)

