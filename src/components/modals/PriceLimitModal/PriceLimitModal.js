import React from 'react'
import pure from 'recompose/pure'
import { I18n } from 'react-redux-i18n'
import s from './PriceLimitModal.scss'

const PriceLimitModal = ({ handleHide, isAbove, total_price, passengers }) =>
  <div className={s.modal}>
    <div className={s.modalHeader}>
      <span className={s.modalTitle}>
        {I18n.t(`PriceLimitModal.${isAbove ? 'Over-2000-title' : 'Under-250-title'}`).replace('[A]', total_price)}
      </span>
      <span className={s.closeModalBtn} onClick={handleHide}>
        {I18n.t('PriceLimitModal.close').toUpperCase()}
      </span>
    </div>
    <div className={s.modalBody}>
      <span>{I18n.t(`PriceLimitModal.${isAbove ? 'Over-2000' : 'Under-250'}`).replace('#passengers', passengers)}</span>
      <div onClick={handleHide} className={s.okButton}>
        <span>{I18n.t('PriceLimitModal.gotIt')}</span>
      </div>
    </div>
  </div>

export default pure(PriceLimitModal)
