import React from 'react'
import Form from 'react-bootstrap/lib/Form'
import {reduxForm, Field} from 'redux-form'
import FacebookLogin from 'react-facebook-login'
import validator from 'validator'
import {I18n} from 'react-redux-i18n'
import PreloaderOverlay from 'components/PreloaderOverlay/PreloaderOverlay'
import s from './RegisterModal.scss'
import config from 'config.json'
import InputField from 'components/FieldGroup/InputFieldGroup'
import {pure} from 'recompose'
import ArrowRight from 'static/images/arrow-pointing-to-right.svg'
import CrossIcon from 'static/images/cross-icon.svg'
import InlineSVG from 'svg-inline-react'

const validate = values => {
  let errors = {}

  if (!values.email) {
    errors.email = I18n.t('form.required')
  } else if (!validator.isEmail(values.email)) {
    errors.email = I18n.t('form.invalidEmailError')
  }

  if (!values.first_name) {
    errors.first_name = I18n.t('form.required')
  }

  if (!values.last_name) {
    errors.last_name = I18n.t('form.required')
  }

  if (values.first_name && values.first_name.length > 30) {
    errors.first_name = I18n.t('form.notLongerThen')
  }

  if (values.last_name && values.last_name.length > 30) {
    errors.last_name = I18n.t('form.notLongerThen')
  }

  if (!values.password) {
    errors.password = I18n.t('form.required')
  }

  if (values.password) {
    if (/^[0-9]+$/.test(values.password)) {
      errors.password = I18n.t('form.invalidPasswordRegex')
    }
    if (values.password.length < 8) {
      errors.password = I18n.t('form.tooShortPassword')
    }
  }

  return errors
}

const RegisterModal = pure(({
    handleHide,
    signUpFacebookAttempt,
    handleSubmit,
    isVerifiedMessage,
    signUpLoading,
    userGetLoading,
    forgetAboutTicket,
    showLoginModal,
  }) =>
    <div className={s.modal}>
      <span
        onClick={() => {
          handleHide()
          forgetAboutTicket()
        }}
        className={s.closeModalBtn}
      >
       <InlineSVG src={CrossIcon}/>
      </span>
      {(signUpLoading || userGetLoading) && <PreloaderOverlay/>}

      <p>
        {isVerifiedMessage}
      </p>

      <FacebookLogin
        appId={config.facebook.app_key}
        autoLoad={false}
        fields='name,email'
        callback={signUpFacebookAttempt}
        cssClass={s.facebookBtn}
        textButton={I18n.t('Header.signUpWithFacebook')}
      />

      <div className={s.divider}>
        <span>{I18n.t('Header.orSignUpUsingEmail')}</span>
      </div>

      <Form
        className={s.registrationForm}
        noValidate
        onSubmit={handleSubmit}
      >
        <div className={s.formGroup}>
          <Field
            name='first_name'
            component={InputField}
            type='text'
            placeholder={I18n.t('form.firstName')}
						normalize={value => value && value.replace(/[^a-z^A-Z]/g, '')}
            customStyle
          />
        </div>
        <div className={s.formGroup}>
          <Field
            name='last_name'
            component={InputField}
            type='text'
            placeholder={I18n.t('form.lastName')}
						normalize={value => value && value.replace(/[^a-z^A-Z]/g, '')}
            customStyle
          />
        </div>
        <div className={s.formGroup}>
          <Field
            name='email'
            component={InputField}
            type='email'
            placeholder='Email'
            customStyle
          />
        </div>
        <div className={s.formGroup}>
          <Field
            name='password'
            component={InputField}
            type='password'
            placeholder={I18n.t('Header.password')}
            customStyle
          />
        </div>
        <div className={s.registrationTerm}>
          <p>{I18n.t('Header.registrationTerm')}</p>
        </div>
        <button
          type='submit'
          className={s.signUpBtn}
        >
          {I18n.t('Header.register')}
        </button>
      </Form>
      <button
        onClick={showLoginModal}
        type='button'
        className={s.signInBtn}
      >
        <span>{I18n.t('Header.AlreadyRegisteredSignInNow')}</span>
        <InlineSVG src={ArrowRight}/>
      </button>
    </div>
)

export default
reduxForm({
  form: 'RegisterModal',
  validate
})(RegisterModal)
