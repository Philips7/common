import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {connectModal} from 'redux-modal'
import {connect} from 'react-redux'
import Modal from 'react-bootstrap/lib/Modal'
import {forgetAboutTicket} from 'routes/Landing/modules/Landing'
import s from './ModalBody.scss'
import cn from 'classnames'

const marginBottom20 = {marginBottom: '20px'};
const marginBottom30 = {marginBottom: '30px'};
const marginBottom45 = {marginBottom: '45px'};
const marginBottom50 = {marginBottom: '50px'};

export class RegisterModal extends Component {
  static Component = null
  mounted = false

  state = {
    Component: RegisterModal.Component
  }

  static propTypes = {
    show: PropTypes.bool.isRequired,
    handleHide: PropTypes.func.isRequired,
  }

  componentWillMount() {
    if (this.state.Component === null) {
      System.import('../../../containers/RegisterModalContainer').then(m => m.default).then(Component => {
        RegisterModal.Component = Component
        if (this.mounted) {
          this.setState({Component})
        }
      })
    }
  }

  componentDidMount() {
    this.mounted = true
  }

  componentWillUnmount() {
    this.mounted = false
  }

  render() {
    const {show, handleHide, forgetAboutTicket} = this.props
    const {Component} = this.state

    return (
      <Modal
        show={show}
        onHide={() => {
          handleHide()
          forgetAboutTicket()
        }}
        className={s.modal}
      >
        <Modal.Body className={s.modalBody}>
          {Component ? <Component handleHide={handleHide}/> :
            <div className={s.skeletonWrap}>
              <div className={cn(s.buttonElement, s.skeletonElement, s.btnBlue)} style={marginBottom50}></div>
              <div className={cn(s.textElement, s.skeletonElement)} style={marginBottom45}></div>
              <div className={cn(s.fieldElement, s.skeletonElement)}></div>
              <div className={cn(s.fieldElement, s.skeletonElement)}></div>
              <div className={cn(s.fieldElement, s.skeletonElement)}></div>
              <div className={cn(s.fieldElement, s.skeletonElement)} style={marginBottom30}></div>
              <div className={cn(s.textElement, s.skeletonElement)} style={marginBottom20}></div>
              <div className={cn(s.buttonElement, s.skeletonElement, s.btnRed)} style={marginBottom30}></div>
              <div className={cn(s.textElement, s.skeletonElement)}></div>
            </div>
          }
        </Modal.Body>
      </Modal>
    )
  }
}

const mapActionCreators = {
  forgetAboutTicket
}

export default connect(() => ({}), mapActionCreators)(
  connectModal({name: 'registerModal', destroyOnHide: true})(RegisterModal)
)
