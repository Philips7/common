import React from 'react'
import Modal from 'react-bootstrap/lib/Modal'
import {connectModal} from 'redux-modal'
import s from './PaybackUnavailableModal.scss'
import {pure} from 'recompose'
import cn from 'classnames'
import Link from 'react-router/lib/Link'
import {I18n, Translate} from 'react-redux-i18n'
import Work from '../../../static/images/work_in_progress.svg'
import ArrowRight from '../../../static/images/arrow-right-bold.svg'
import validator from 'validator'
import {reduxForm, Field} from 'redux-form'
import {InputFieldGroup} from 'components/FieldGroup'
import EnvelopeIcon from '../../../static/images/envelope-icon.svg'
import CrossIcon from 'static/images/cross-icon.svg'
import InlineSVG from 'svg-inline-react'

const validate = values => {
  let errors = {}
  if (!values.email) {
    errors.email = I18n.t('form.required')
  } else if (!validator.isEmail(values.email)) {
    errors.email = I18n.t('form.invalidEmailError')
  }
  return errors
}

const PaybackUnavailableModal = pure(({
    show, setPaybackUnavailable, handleSubmit, invalid, fromLanding, hidePaybackModal
  }) =>
    <Modal
      show={show}
			onHide={fromLanding && hidePaybackModal}
      className={cn(s.modal, s.paybackModal)}
      bsSize='lg'
    >
			{fromLanding && <span
					onClick={hidePaybackModal}
					className={s.closeModalBtn}
				>
          <InlineSVG src={CrossIcon}/>
      </span>}
      <Modal.Body className={s.modalBody}>
				{fromLanding ?
					<div className={s.contentWrap}>
						<h1 className={s.title}>
							<Translate value='PaybackUnavailableModal.waitAMinute'/>
						</h1>
						<h3 className={s.subTitle}>
							<Translate value='PaybackUnavailableModal.featureUnavailable' dangerousHTML/>
						</h3>
						<div className={s.imgWrap}>
							<InlineSVG src={Work}/>
						</div>
						<h3 className={s.subTitle}>
							<Translate value='PaybackUnavailableModal.checkWebsite' dangerousHTML/>
						</h3>
					</div>
					:
					<div>
					<div className={s.contentWrap}>
						<h1 className={s.title}>
							<Translate value='PaybackUnavailableModal.oops'/>
						</h1>
						<h3 className={s.subTitle}>
							<Translate value='PaybackUnavailableModal.featureUnavailable' dangerousHTML/>
						</h3>
						<div className={s.imgWrap}>
							<InlineSVG src={Work}/>
						</div>
						<h3 className={s.subTitle}>
							<Translate value='PaybackUnavailableModal.bookingFee' dangerousHTML/>
						</h3>
					</div>
					< span className={s.button} onClick={setPaybackUnavailable}>
					<Translate value='PaybackUnavailableModal.bookBtn'/>
					</span>
					</div>
				}

			</Modal.Body>

      <Modal.Footer className={cn(s.modalBody, s.modalFooter)}>
				{fromLanding ?
				<span className={s.footerText}><Translate value='PaybackUnavailableModal.subscribe'/> <br/>
				<Translate value='PaybackUnavailableModal.beFirst'/></span>
					:
					<span className={s.footerText}><Translate value='PaybackUnavailableModal.subscribe'/></span>
				}
        <form onSubmit={handleSubmit}>

          <div className={s.fieldWrap}>
						<InlineSVG src={EnvelopeIcon}/>
            <Field
              component={InputFieldGroup}
              name='email'
              placeholder='E-mail address'
              type='email'
              absoluteValidation
            />
          </div>
          <div>
            <button className={s.submitBtn} type='submit' disabled={invalid}>
              <Translate value='PaybackUnavailableModal.signUp'/>
            </button>
          </div>
        </form>
				{!fromLanding && <span className={s.footerText}>
          <Link to='/' className={s.backBtn}>
            <Translate value='PaybackUnavailableModal.homePage'/> <InlineSVG src={ArrowRight}/>
          </Link>
        </span>}
      </Modal.Footer>
    </Modal>
)

export default
connectModal({name: 'paybackUnavailableModal'})(
  reduxForm({
    form: 'NewsletterForm',
    validate,
    enableReinitialize: true
  })(PaybackUnavailableModal)
)
