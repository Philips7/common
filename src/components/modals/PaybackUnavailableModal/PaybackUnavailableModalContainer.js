import {connect} from 'react-redux'
import PaybackUnavailableModal from './PaybackUnavailableModal'
import {setPaybackUnavailable} from '../../../routes/Checkout/modules/Checkout'

const mapActionCreators = {
  setPaybackUnavailable
}

const mapStateToProps = ({
  form: {PassengerInfoForm}
}) => ({
  initialValues: {email: PassengerInfoForm && PassengerInfoForm.values && PassengerInfoForm.values.email}
})

export default connect(mapStateToProps, mapActionCreators)(PaybackUnavailableModal)
