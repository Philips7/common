import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connectModal } from 'redux-modal'
import { connect } from 'react-redux'
import Modal from 'react-bootstrap/lib/Modal'
import { forgetAboutTicket } from 'routes/Landing/modules/Landing'
import s from './ModalBody.scss'
import cn from 'classnames'

const marginBottom50 = {marginBottom: '50px'}
const marginBottom45 = {marginBottom: '45px'}
const marginBottom60 = {marginBottom: '60px'}
const marginBottom30 = {marginBottom: '30px'}

class LoginModal extends Component {
  static Component = null
  mounted = false

  state = {
    Component: LoginModal.Component
  }

  static propTypes = {
    show: PropTypes.bool.isRequired,
    handleHide: PropTypes.func.isRequired,
    forgetAboutTicket: PropTypes.func.isRequired,
  }

  componentWillMount() {
    if (this.state.Component === null) {
      System.import('../../../containers/LoginModalContainer').then(m => m.default).then(Component => {
        LoginModal.Component = Component
        if (this.mounted) {
          this.setState({Component})
        }
      })
    }
  }

  componentDidMount() {
    this.mounted = true
  }

  componentWillUnmount() {
    this.mounted = false
  }

  render() {
    const {show, handleHide, forgetAboutTicket, emailRegistered, showResetPasswordModal, showRegisterModal} = this.props
    const {Component} = this.state
    return (
      <Modal
        show={show}
        onHide={() => {
          handleHide()
          forgetAboutTicket()
        }}
        className={s.modal}
      >
        <Modal.Body className={s.modalBody}>
          {Component
            ? <Component handleHide={handleHide}
                         emailRegistered={emailRegistered}
                         showResetPasswordModal={showResetPasswordModal}
                         showRegisterModal={showRegisterModal}/>
            : <div className={s.skeletonWrap}>
              <div className={cn(s.buttonElement, s.skeletonElement, s.btnBlue)} style={marginBottom50}></div>
              <div className={cn(s.textElement, s.skeletonElement)} style={marginBottom45}></div>
              <div className={cn(s.fieldElement, s.skeletonElement)}></div>
              <div className={cn(s.fieldElement, s.skeletonElement)}></div>
              <div className={cn(s.dualElement, s.skeletonElement)} style={marginBottom60}>
                <span></span>
                <span></span>
              </div>
              <div className={cn(s.buttonElement, s.skeletonElement, s.btnRed)} style={marginBottom30}></div>
              <div className={cn(s.textElement, s.skeletonElement)}></div>
            </div>
          }
        </Modal.Body>
      </Modal>
    )
  }
}

const mapActionCreators = {
  forgetAboutTicket
}

export default connect(() => ({}), mapActionCreators)(
  connectModal({name: 'loginModal', destroyOnHide: true})(LoginModal)
)
