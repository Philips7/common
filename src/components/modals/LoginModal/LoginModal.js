import React from 'react'
import Form from 'react-bootstrap/lib/Form'
import {reduxForm, Field} from 'redux-form'
import FacebookLogin from 'react-facebook-login'
import validator from 'validator'
import {I18n} from 'react-redux-i18n'
import PreloaderOverlay from 'components/PreloaderOverlay/PreloaderOverlay'
import s from './LoginModal.scss'
import config from 'config.json'
import InputField from 'components/FieldGroup/InputFieldGroup'
import {pure} from 'recompose'
import ArrowRight from 'static/images/arrow-pointing-to-right.svg'
import CustomCheckbox from 'components/CustomCheckbox/CustomCheckbox'
import CrossIcon from 'static/images/cross-icon.svg'
import InlineSVG from 'svg-inline-react'

const validate = values => {
  let errors = {}

  if (!values.email) {
    errors.email = I18n.t('form.required')
  } else if (!validator.isEmail(values.email)) {
    errors.email = I18n.t('form.invalidEmailError')
  }

  if (!values.password) {
    errors.password = I18n.t('form.required')
  }

  return errors
}

const LoginModal = pure(({
    handleHide,
    signUpFacebookAttempt,
    handleSubmit,
    userGetLoading,
    signInLoading,
    forgetAboutTicket,
    showRegisterModal,
    showResetPasswordModal,
    emailRegistered
  }) =>
    <div className={s.modal}>
       <span
         onClick={() => {
           handleHide()
           forgetAboutTicket()
         }}
         className={s.closeModalBtn}
       >
          <InlineSVG src={CrossIcon}/>
      </span>
      {(signInLoading || userGetLoading) &&
      <PreloaderOverlay/>}
      {emailRegistered &&
      <div className={s.welcomeBackWrap}>
        <h1>{I18n.t('Header.WelcomeBack')}</h1>
        <div className={s.welcomeBackContent}>{I18n.t('Header.AlreadyRegistered')}</div>
      </div>
      }
      <FacebookLogin
        appId={config.facebook.app_key}
        autoLoad={false}
        fields='name,email'
        callback={signUpFacebookAttempt}
        cssClass={s.facebookBtn}
        textButton={I18n.t('Header.signInWithFacebook')}
      />

      <div className={s.divider}>
        <span>{I18n.t('Header.orSignInUsingEmail')}</span>
      </div>

      <Form
        className={s.loginForm}
        noValidate
        onSubmit={handleSubmit}
      >
        <div className={s.formGroup}>
          <Field
            name='email'
            component={InputField}
            type='text'
            placeholder='Email'
            customStyle
          />
        </div>
        <div className={s.formGroup}>
          <Field
            name='password'
            component={InputField}
            type='password'
            placeholder={I18n.t('Header.password')}
            customStyle
          />
        </div>
        <div className={s.otherOptionsWrap}>
          <Field
            component={CustomCheckbox}
            label={I18n.t('Header.rememberMe')}
            name='rememberMe'
            id='loginRememberCheckbox'
          />

          <button
            type='button'
            className={s.forgotBtn}
            onClick={showResetPasswordModal}
          >
            {I18n.t('Header.forgotPassword')}
          </button>
        </div>
        <button
          type='submit'
          className={s.signInBtn}
        >
          {I18n.t('Header.signIn')}
        </button>
      </Form>
      <button
        type='button'
        onClick={showRegisterModal}
        className={s.signUpBtn}
      >
        <span>{I18n.t('Header.DidNotRegisterSignUpNow')}</span>
        <InlineSVG src={ArrowRight}/>
      </button>
    </div>
)

export default
reduxForm({
  form: 'LoginModal',
  validate
})(LoginModal)
