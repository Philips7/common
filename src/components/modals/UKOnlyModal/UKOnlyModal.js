import React from 'react'
import PreloaderOverlay from 'components/PreloaderOverlay/PreloaderOverlay'
import s from './UKOnlyModal.scss'
import {pure} from 'recompose'
import InlineSVG from 'svg-inline-react'
import UK_flag from './assets/UK-flag-icon.svg'
import closeIcon from 'static/images/close-icon.svg'

const UKOnlyModal = pure(({ handleHide}) =>
  <div className={s.modal}>
    <div className={s.modalHeader}>
      <h2 className={s.modalTitle}>
        <InlineSVG src={UK_flag}/>
        <span>Flymble is UK only!</span>
      </h2>

      <a onClick={handleHide} className={s.closeModalBtn}>
        <InlineSVG src={closeIcon}/>
      </a>
    </div>
    <div className={s.modalBody}>
      Please keep in mind that currently we are available only to United Kingdom
      residents.
    </div>
  </div>
)

export default UKOnlyModal
