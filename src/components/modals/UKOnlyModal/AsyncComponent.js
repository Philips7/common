import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connectModal } from 'redux-modal'
import { connect } from 'react-redux'
import Modal from 'react-bootstrap/lib/Modal'
import s from './ModalBody.scss'
import cn from 'classnames'

class UKOnlyModal extends Component {
  static Component = null
  mounted = false

  state = {
    Component: UKOnlyModal.Component
  }

  static propTypes = {
    show: PropTypes.bool.isRequired,
    handleHide: PropTypes.func.isRequired
  }

  componentWillMount() {
    if (this.state.Component === null) {
      System.import('../../../containers/UKOnlyModalContainer').then(m => m.default).then(Component => {
        UKOnlyModal.Component = Component
        if (this.mounted) {
          this.setState({Component})
        }
      })
    }
  }

  componentDidMount() {
    this.mounted = true
  }

  componentWillUnmount() {
    this.mounted = false
  }

  render() {
    const {show, handleHide} = this.props
    const {Component} = this.state
    return (
     <Modal
       show={show}
       className={s.modal}
     >
       <Modal.Body className={s.modalBody}>
         {
           Component && <Component handleHide={handleHide}/>
         }
       </Modal.Body>
     </Modal>
    )
  }
}

export default connectModal({name: 'UKOnlyModal', destroyOnHide: true})(UKOnlyModal)

