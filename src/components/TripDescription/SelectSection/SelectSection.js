import React from 'react'
import Button from 'react-bootstrap/lib/Button'
import cn from 'classnames'
import { pure } from 'recompose'
import { Translate } from 'react-redux-i18n'
import { isSmallScreen } from '../../../utils/responsive'
import InlineSVG from 'svg-inline-react'
import WarningIcon from 'static/images/warning-icon.svg'
import s from './SelectSection.scss'
import config from '../../../config'

const SearchResultsPage = pure(({
    showMonth = true, perMonth, creditPoint, fromLanding,
  selectTrip, noSelectButton, bookingFee
  }) => <div className={cn(s.payment)}>
    <div className={s.info}>
      <div>
        <div className={cn(s.number)}>
          <span>{config.currency}{bookingFee} </span>
          <span className={s.period}> upfront</span>
        </div>
        <div className={s.month}>upfront</div>

        {
          creditPoint > 1 &&
          <div className={s.priceWarning}>
            <span className={s.iconWrap}>
              <InlineSVG src={WarningIcon} />
            </span>
            <div>
              Plus {creditPoint}x {config.currency}
              {perMonth} monthly
              </div>
          </div>
        }
      </div>

      {!showMonth && !noSelectButton &&
        <div>
          <Button
            className={cn(s.select)}
            onClick={fromLanding ? () => ({}) : selectTrip}
          >
            <Translate value='TripDescription.select' />
          </Button>
        </div>
      }
    </div>
  </div>
)

export default SearchResultsPage
