import React, { Component } from 'react'
import s from './TripDescription.scss'
import cn from 'classnames'
import SelectSection from './SelectSection/SelectSection'
import TripDisplay from 'components/ShortDisplayFlight/TripDisplay/TripDisplay'
import TripDisplaySm from 'components/ShortDisplayFlight/TripDisplaySm/TripDisplaySm'
import ExpandDescription from 'components/ShortDisplayFlight/ExpandDescription/ExpandDescription'
import WarningSign from 'static/images/warning-sign-on.svg'
import InlineSVG from 'svg-inline-react'
import { pure } from 'recompose'
import { I18n } from 'react-redux-i18n'
import { oneWay, getValues } from '../../utils/helpers'
import { priceParser } from 'utils/price'
import config from 'config'


@pure
export default class TripDescription extends Component {
  constructor(props) {
    super(props)
    this.state = {
      expanded: props.expandOnStart
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { passengersNumber, trip, paybackSelected } = this.props
    return nextState.expanded !== this.state.expanded
      || (nextProps.passengersNumber !== passengersNumber && JSON.stringify(nextProps.trip) !== JSON.stringify(trip))
      || nextProps.paybackSelected !== paybackSelected
  }

  toggleOpen = () => {
    this.setState({ expanded: !this.state.expanded })
  }

  render() {
    const {
      showMonth, trip, creditPoint, selectTrip, paid_months, months, interestRate, dashboard, fromLanding,
      payFunction, monthAmount, noSelectSection, noSelectButton, compact, bookingFailed, panelView,
      showFailedModal, passengersNumber, paybackSelected, checkout_flag,
    } = this.props

    const { outbound, returnn, type, airfare, all_months } = trip
    const { per_month, total_price, price_per_person, service_fee_percentage, paid_today } = getValues(trip, all_months, paybackSelected, checkout_flag)
    const { expanded } = this.state
    const withReturn = !oneWay(type)
    const perPerson = priceParser(price_per_person)
    const serviceFee = priceParser(service_fee_percentage)
    const total = priceParser(total_price)
    const upfrontPerPerson = passengersNumber > 1 ? paid_today / passengersNumber : paid_today

    return (
      <div
        className={cn(s.tripDescription, compact && s.compact, bookingFailed && s.failedTicket, panelView && s.panelView, withReturn && s.withReturn)}
        onClick={this.toggleOpen}
      >
        {
          bookingFailed &&
          <div className={cn(s.bookingFailed, s.bookingFailedTop)} onClick={showFailedModal}>
            <InlineSVG src={WarningSign} /><span>{I18n.t('TripDescription.bookingFailed')}</span>
          </div>
        }

        <TripDisplay outbound trip={outbound} compact={compact} withReturn={withReturn} />

        {withReturn && <div className={s.divider} />}
        {withReturn && <TripDisplay trip={returnn} compact={compact} withReturn={withReturn} />}

        <div className={cn(s.tripDescriptionSm, expanded && s.bordered)} onClick={this.toggleOpen}>
          <TripDisplaySm
            trip={trip} withReturn={withReturn}
            noSelectSection={noSelectSection}
            perMonth={priceParser(per_month / passengersNumber)}
          />
        </div>
        {
          !noSelectSection
          && <div className={s.payment}>
            {
              bookingFailed &&
              <div className={s.bookingFailed} onClick={showFailedModal}>
                <InlineSVG src={WarningSign} /><span>{I18n.t('TripDescription.bookingFailed')}</span>
              </div>
            }

            <div className={s.contentWrap}>
              <SelectSection
                selectTrip={() => selectTrip(trip)}
                creditPoint={creditPoint}
                interestRate={interestRate}
                perMonth={priceParser(per_month / passengersNumber)}
                showMonth={showMonth}
                expanded={expanded}
                fromLanding={fromLanding}
                payFunction={payFunction}
                noSelectButton={noSelectButton}
                dashboard={dashboard}
                bookingFee={priceParser(upfrontPerPerson)}
              />
            </div>
          </div>
        }

        {expanded && !fromLanding &&
          <div className={cn(s.expandDescriptionWrap, withReturn && s.withReturn)}><ExpandDescription trip={outbound.segments} compact={compact} outbound /></div>}
        {withReturn && expanded && <div className={s.divider} />}
        {withReturn && expanded &&
          <div className={cn(s.expandDescriptionWrap, withReturn && s.withReturn)}><ExpandDescription trip={returnn.segments} compact={compact} /></div>}

        {expanded && !noSelectSection && !fromLanding &&
          <div className={cn(s.otherInfoWrap, dashboard && s.dashboard)}>
            {!dashboard &&
              <div>
                <div className={s.title}>{I18n.t('TripDescription.priceBreakdown')}</div>
                <ul>
                  <li>
                    <span className={s.name}>{I18n.t('TripDescription.airfare')}</span>
                    <span className={s.value}>{config.currency}{priceParser(airfare / passengersNumber)}</span>
                  </li>
                  <li>
                    <span className={s.name}>{I18n.t('TripDescription.numberPassengers')}</span>
                    <span className={s.value}>{passengersNumber}</span>
                  </li>
                  <li>
                    <span className={s.name}>{I18n.t('TripDescription.serviceFee')}</span>
                    <span className={s.value}>{serviceFee}%</span>
                  </li>
                  <li>
                    <span className={s.name}>{I18n.t('TripDescription.perPerson')}</span>
                    <span className={cn(s.value, s.totalPrice)}>{config.currency}{perPerson}</span>
                  </li>
                  <li>
                    <span className={s.name}>{I18n.t('TripDescription.totalPrice')}</span>
                    <span className={cn(s.value, s.totalPrice)}>{config.currency}{total}</span>
                  </li>
                </ul>
              </div>
            }
          </div>
        }

        {!fromLanding && <div className={cn(s.expandBtn, compact && s.compact)}>
          <div className={s.linkWrap}>
            <a className={cn(s.detailsLink, expanded && s.expanded)} onClick={this.toggleOpen}>
              {I18n.t(expanded ? 'TripDescription.hideDetails' : 'TripDescription.viewDetails')}
            </a>
          </div>
          {!noSelectSection && <span className={s.spaceholder} />}
        </div>}

        {expanded && !noSelectSection && !fromLanding &&
          <div className={s.mobileSelectBlock}>
            <div className={s.payment}>
              <div className={s.contentWrap}>


                {!noSelectSection && bookingFailed &&
                  <div className={s.bookingFailed} onClick={showFailedModal}>
                    <InlineSVG src={WarningSign} /><span>{I18n.t('TripDescription.bookingFailed')}</span>
                  </div>
                }

                <SelectSection
                  selectTrip={() => selectTrip(trip)}
                  creditPoint={creditPoint}
                  perMonth={priceParser(per_month / passengersNumber)}
                  showMonth={showMonth}
                  monthAmount={monthAmount}
                  expanded={expanded}
                  paid_months={paid_months}
                  months={months}
                  payFunction={payFunction}
                  noSelectButton={noSelectButton}
                  dashboard={dashboard}
                  bookingFee={priceParser(upfrontPerPerson)}
                />
              </div>
            </div>
          </div>
        }
      </div>
    )
  }
}
