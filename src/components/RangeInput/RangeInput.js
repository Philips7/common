import React, {Component} from 'react'
import InputRange from 'react-input-range'
import s from './RangeInput.scss'

const RangeInput = ({maxValue, minValue, value, onChange, onChangeComplete, step, className}) => (
  <div className={s.rangeInputWrap}>
    <InputRange
      maxValue={maxValue}
      minValue={minValue}
      value={value}
      onChange={onChange}
      onChangeComplete={onChangeComplete}
      step={step}
      classNames={className}
    />
  </div>
)
export default RangeInput;
