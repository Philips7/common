import React from 'react'
import styles from './VoucherCode.scss'

export default  ({ confirm }) => (
  <div className={styles.voucherCodeWrapper}>
    <label htmlFor='voucher'>Enter you code</label>
    <input type='text' id='voucher' className='form-control' disabled={confirm}/>
  </div>
)
