import React from 'react'
import s from './Tabs.scss'
import cn from 'classnames'
import CheckIcon from 'static/images/checked-circle.svg'
import InlineSVG from 'svg-inline-react'
import {Translate} from 'react-redux-i18n'
import {pure} from 'recompose'

const tabs = [
  {id: 1, text: <Translate value='Tab.searchResult'/>},
  {id: 2, text: <Translate value='Tab.checkout'/>},
  {id: 3, text: <Translate value='Tab.confirmation'/>},
  {id: 4, text: <Translate value='Tab.payment'/>}
]

export const Tabs = pure(({tabNumber}) =>
  <div className={cn(s.tabsWrap)}>
    <div className={s.container}>
      <div className={s.col}>
        <div className={cn(s.renderedTabsWrap)}>
          {tabs.map((item, i) =>
            <div key={i} className={cn(s.tabWrap, tabNumber === i && s.active, tabNumber > i && s.done )}>
              <div className={s.circle}/>
              <div className={s.text}>{item.text}</div>
            </div>
          )}
        </div>
      </div>
    </div>
  </div>
)

export default Tabs
