import React from 'react'
import {pure} from 'recompose'
import s from './RadioButton.scss'
import cn from 'classnames'

const RadioButton = pure(({ checked, options, input, input: { onChange }, meta: { error, touched }, withError, ...props}) =>
  <div className={s.radioButton}>
    <div className={s.inputsWrap}>
      {
        options.map(({ value, title }) =>
          <label className={s.label} key={value}>
            <input {...props} onChange={onChange} value={value} type='radio'
                   className={s.radioInput}/>
            <span className={cn(s.checkmark, +checked === +value && s.checked)}/>
            <span>{title}</span>
          </label>
        )
      }
    </div>
    {withError && <div className={s.errorMessage}>{error && touched ? error : ''}</div>}
  </div>
)

export default RadioButton
