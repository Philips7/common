import React from 'react'
import {pure} from 'recompose'
import s from './CustomCheckbox.scss';
import cn from 'classnames';
import Checkmark from 'static/images/checkmark.svg';
import InlineSVG from 'svg-inline-react'

const CustomCheckbox = pure(({label, name, id, checkboxType, meta: {touched, error}, value, disabled, input, onChange, small, showError}) =>
  <div className={cn(s.customCheckbox, checkboxType == 'block' && s.block)}>
    <input {...input} type='checkbox' id={id} name={name} disabled={disabled} checked={input.value}
                      onChange={e => {input.onChange(e); onChange && onChange(e)}}/>
    <label htmlFor={id}><InlineSVG src={Checkmark}/> <span>{label}</span></label>
    {showError && touched && error && <div className={s.error}>{error}</div>}
  </div>
)

export default CustomCheckbox
