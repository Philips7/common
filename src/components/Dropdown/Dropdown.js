import React, {Component} from 'react'
import ReactDropdown from 'react-simple-dropdown'
import cn from 'classnames'
import s from './Dropdown.scss'

class Dropdown extends Component {
  componentDidUpdate () {
    this.props.dropClosed && this._dropdown && this._dropdown.hide()
  }

  render () {
    const {className, onHide, onShow, children, disabled} = this.props
    return (
      <div className={cn(s.dropdownWrap, disabled && s.disabled, className)}>
        <ReactDropdown
          ref={dropdown => this._dropdown = dropdown}
          onHide={onHide}
          onShow={onShow}
        >
          {children}
        </ReactDropdown>
      </div>
    )
  }
}

export default Dropdown
