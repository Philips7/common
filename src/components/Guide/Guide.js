import React, { PureComponent } from 'react'
import Joyride from 'react-joyride';
import { isTabletScreen } from '../../utils/responsive'
import { globalBodyStyles } from '../../utils/helpers'
import InlineSVG from 'svg-inline-react'
import infoIcon from '../../static/images/light-bulb.svg'
import { isSmallScreen } from '../../utils/responsive'
import s from './Guide.scss'
import { priceParser } from 'utils/price'


const stepCheckout = [{
  title: 'Payment schedule',
  text:
  <div className={s.messageText}>
    <InlineSVG className={s.iconWrap} src={infoIcon} />
    <span>
      Here is your payment schedule. You will receive your flight ticket
        directly after completing the booking process and will get a monthly
        email to pay each instalment.
        </span>
  </div>,
  selector: '.PassengerInfo__creditPointList___2LeQ7',
  position: (isTabletScreen ? 'bottom' : 'left'),
  isFixed: true,
  // optional styling
  style: s,
  // custom params...
  name: 'third_step',
}]

class Guide extends PureComponent {

  getSteps = (searchResult, paybackSelected, passengersNumber) => [
    {
      title: 'Payback months',
      text:
      <div className={s.messageText}>
        <InlineSVG className={s.iconWrap} src={infoIcon} />
        <span>
          In the payback period dropdown menu, you choose whether you want to
            split the cost over 3,4,5,6,7,8,9 or 10 installments.
          </span>
      </div>,
      selector: isTabletScreen ? '.Dropdown__dropdownWrap___UCX7T .dropdown .dropdown__trigger' : '.SortRow__paybackFilterWrap___1mu7U',
      position: 'bottom',
      isFixed: true,
      // optional styling
      style: s,
      // custom params...
      name: 'first_step',
    },
    {
      title: 'Ticket',
      text:
      <div className={s.messageText}>
        <InlineSVG className={s.iconWrap} src={infoIcon} />
        <span>
          {`The prices display monthly payments. For a ticket to ${searchResult.outbound.segments[searchResult.outbound.segments.length - 1].destination.city} you
            will pay £${priceParser(searchResult.paid_today / passengersNumber)} today and the rest over ${paybackSelected} installments.`}
        </span>
      </div>,
      selector: isTabletScreen ? '.TripDisplaySm__ticketWrap___1UsHs .TripDisplaySm__price___-ypli' : '.TripDescription__tripDescription___3i_k-',
      position: 'top-right',
      isFixed: true,
      // optional styling
      style: s,
      // custom params...
      name: 'second_step',
    },
    {
      title: 'Select a ticket',
      text:
      <div className={s.messageText}>
        <InlineSVG className={s.iconWrap} src={infoIcon} />
        <span>
          if you press "Select" you will proceed to the checkout where you
            can find the details of your flight and payment schedule.
          </span>
      </div>,
      selector: isTabletScreen ? '.TripDescription__mobileSelectBlock___LplwV .TripDescription__payment___3mNo2 .TripDescription__contentWrap___1ju0E .SelectSection__payment___rl0l6 .SelectSection__info___2xxYA div button' : '.SelectSection__payment___rl0l6 .SelectSection__info___2xxYA .SelectSection__select___3t9QB',
      position: isTabletScreen ? 'top' : 'top-right',
      isFixed: true,
      // optional styling
      style: s,
      // custom params...
      name: 'third_step',
    },
  ]

  bodyOverlay = (data) => {
    const { action, type, steps, step } = data
    if (step && step.title === 'Payment schedule') {
      window.scroll(0, 100)
    }
    console.log(data)
    if ((type === 'finished' && steps && steps.length === 3) || action === 'close') {
      this.props.resetOnce()
    }
    if (
      (data.action === 'start'
        || data.action === 'next'
        || data.action === 'back'
        || data.action === 'beacon:click')
      && data.type !== 'finished'
    ) {
      globalBodyStyles('disableBodyScroll', true)
    } else {
      globalBodyStyles('disableBodyScroll', false)
    }
  }

  render() {
    const { checkout, searchResult = null, paybackSelected, run, passengersNumber } = this.props
    return (
      !isSmallScreen && (searchResult || checkout) ? <Joyride
        ref={c => (this.joyride = c)}
        autoStart
        run={run}
        locale={{
          back: (<span>Back</span>),
          close: (<span>Close</span>),
          last: (<span>{checkout ? 'Got it! Thanks!' : 'Got it!'}</span>),
          next: (<span>Next</span>),
          skip: (<span>Skip</span>),
        }}
        showOverlay={true}
        showSkipButton={!checkout}
        showStepsProgress={!checkout}
        stepIndex={0}
        disableOverlay={true}
        steps={checkout ? stepCheckout : this.getSteps(searchResult, paybackSelected, passengersNumber)}
        type={'continuous'}
        callback={this.bodyOverlay}
      /> : null
    )
  }
}

export default Guide
