import React, {Component} from 'react'
import Navbar from 'react-bootstrap/lib/Navbar'
import Nav from 'react-bootstrap/lib/Nav'
import NavItem from 'react-bootstrap/lib/NavItem'
import Popover from 'react-bootstrap/lib/Popover'
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger'
import cn from 'classnames'
import s from './Header.scss'
import {Link} from 'react-router'
import InlineSVG from 'svg-inline-react'
import isSmallScreen from '../../utils/responsive'
import RegisterModal from 'components/modals/RegisterModal/AsyncComponent'
import ResetPasswordModal from 'components/modals/ResetPasswordModal/AsyncComponent'
import UKOnlyModal from 'components/modals/UKOnlyModal/AsyncComponent'
import LearnMoreModal from 'components/modals/LearnMoreModal/AsyncComponent'
import PriceLimitModal from 'components/modals/PriceLimitModal/AsyncComponent'
import LoginModal from 'components/modals/LoginModal/AsyncComponent'
import MainLogo from 'static/images/flymble-logo.svg'
import Telephone from 'static/images/landing/telephone.svg'
import TrustLogo from 'static/images/landing/trustpilot.svg'
import LockIcon from 'static/images/lock-icon.svg'
import IATAlogo from 'static/images/IATA-logo.svg'

const CustomLi = ({active, onSelect, activeKey, activeHref, ...props}) => <li
  onClick={onSelect} {...props} />

export default class Header extends Component {
  state = {
    menuExpanded: false
  }

  onToggle = () => isSmallScreen && this.setState(prevState => ({menuExpanded: !prevState.menuExpanded}))

  closeHeader = () => {
    isSmallScreen && this.setState({menuExpanded: false})
  }

  componentDidMount() {
    this.props.checkIpCookie()
  }

  // showSignInModal = () => {
  //   const {show, hide} = this.props
  //   show('loginModal')
  //   hide('registerModal')
  //   this.onToggle() //TODO
  // }

  componentWillReceiveProps(nextProps) {
    const {fromLanding, ipUK} = this.props
    if (ipUK !== nextProps.ipUK) {
      fromLanding && !nextProps.ipUK && this.showUKOnlyModal()
    }
  }

  showUKOnlyModal = () => {
    const {show, hide} = this.props
    show('UKOnlyModal')
    hide('loginModal', 'registerModal')
  }

  getIframelyHtml = () => {
    // If you use embed code from API
    return {
      __html: `
     <div class='trustpilot-widget' data-locale='en-GB' data-template-id='5419b6ffb0d04a076446a9af' data-businessunit-id='58b9e5060000ff00059dc07f' data-style-height='100%' data-style-width='100%' data-theme='${isSmallScreen ? 'light' : 'dark'}'>
      <a href='https://uk.trustpilot.com/review/flymble.com' target='_blank' rel='noopener noreferrer'>Trustpilot</a>
     </div>`
    };

    // Alternatively, if you use plain embed.js approach without API calls:
    // return {__html: '<a href="' + this.url + '" data-iframely-url></a>'};
    // no title inside <a> eliminates the flick
  }

  renderTrust = () => {
    return <div dangerouslySetInnerHTML={this.getIframelyHtml()}/>
  }

  render() {
    const {
      fillHeader,
      logout, loggedIn,
      scrollToHowItWorks
      // We will use it later
      // , changeLanguage, locale
    } = this.props
    const {menuExpanded} = this.state

    // const changeLanguageHandler = (local) => () => {
    //   changeLanguage(local)
    //   this.refs.dropdown.hide();
    // }

    const IATAPopover = (
      <Popover id='IATA-popover' className={s.popover}>
        Guaranteed by our partner Kiwi.com
      </Popover>
    )

    return (
      <header className={cn(fillHeader && s.fillBg, s.Header)}>
        <Navbar className={s.navbarWrap} expanded={menuExpanded} onToggle={this.onToggle} fluid={true}>
          <Navbar.Header className={s.navbarHeader}>
            <Navbar.Brand className={s.navbarBrand}>
              <Link to="/" onClick={this.closeHeader}>
                <InlineSVG src={MainLogo}/>
              </Link>
            </Navbar.Brand>
            <Navbar.Toggle className={s.toggleBtn} />
          </Navbar.Header>

          <Navbar.Collapse className={s.navbarCollapse}>
            <Nav pullRight className={s.navItemsList}>
              {
                __CLIENT__
                  ? <CustomLi className={cn(s.navbarLink, s.featureTrustLink)}>
                    {this.renderTrust()}
                  </CustomLi>
                  : <CustomLi className={cn(s.navbarLink, s.trustLink)}>
                      <a href='https://uk.trustpilot.com/review/flymble.com' target='_blank' rel='noopener noreferrer'>
                        <InlineSVG src={TrustLogo} />
                      </a>
                  </CustomLi>
              }

              <CustomLi className={s.navbarLink}>
                <OverlayTrigger trigger={["hover", "focus"]} placement={isSmallScreen ? "right" : "bottom"}
                                overlay={IATAPopover}>
                  <InlineSVG src={IATAlogo} className={s.IATALink}/>
                </OverlayTrigger>
              </CustomLi>

              <CustomLi className={cn(s.navbarLink, s.secureLabel)}>
                <InlineSVG src={LockIcon}/>
                <span>secure connection</span>
              </CustomLi>

              <NavItem href="tel:+4407925717068" className={cn(s.navbarLink, s.callLink)}>
                <InlineSVG src={Telephone}/> <span>+44 07925717068</span>
              </NavItem>

              <NavItem onClick={() => typeof window !== "undefined" && Intercom("show")}
                       className={cn(s.navbarLink, s.helpLink)}>
                <span>Need help?</span>
              </NavItem>

              {
                // We will use it in the future
                // <NavItem className={s.headerDropdown}>
                //   <Dropdown ref='dropdown'>
                //     <DropdownTrigger>
                //       <span>{locale}</span> <CaretDown/>
                //     </DropdownTrigger>
                //     <DropdownContent>
                //       <ul>
                //         <li onClick={changeLanguageHandler('en')}>en</li>
                //         <li onClick={changeLanguageHandler('du')}>du</li>
                //       </ul>
                //     </DropdownContent>
                //   </Dropdown>
                // </NavItem>
              }
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        <UKOnlyModal/>
        <LoginModal/>
        <RegisterModal/>
        <ResetPasswordModal/>
        <LearnMoreModal/>
        <PriceLimitModal/>
      </header>
    )
  }
}
