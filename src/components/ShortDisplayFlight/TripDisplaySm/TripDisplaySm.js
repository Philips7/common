import React from 'react'
import moment from 'moment'
import 'moment-duration-format'
import { pure } from 'recompose'
import s from './TripDisplaySm.scss'
import config from '../../../config'

const TripDisplaySm = pure(({trip, withReturn, noSelectSection, perMonth}) => {
  const outboundOrigin = trip.outbound.segments[0].origin
  const outboundDestination = trip.outbound.segments[trip.outbound.segments.length - 1].destination
  const layoverOutboundCount = trip.outbound.segments.length - 1
  const totalOutboundTime = moment.duration(trip.outbound.duration, 'minutes').format('HH[h] mm[m]', {trim: false})
  let returnOrigin=null
  let returnDestination=null
  let layoverReturnCount=null
  let totalReturnTime=null
  const companyImages = []

  trip.outbound.companiesMap.map(value => {
    companyImages.push(value[1].carrier_icon)
  })

  if(withReturn) {
    returnOrigin = trip.returnn.segments[0].origin
    returnDestination = trip.returnn.segments[trip.returnn.segments.length - 1].destination
    layoverReturnCount = trip.returnn.segments.length - 1
    totalReturnTime = moment.duration(trip.returnn.duration, 'minutes').format('HH[h] mm[m]', {trim: false})

    trip.returnn.companiesMap.map(value => {
      if (!(companyImages.indexOf(value[1].carrier_icon) >= 0)) {
        companyImages.push(value[1].carrier_icon)
      }
    })
  }

return (
    <div className={s.ticketWrap}>
      <div className={s.ticketHeading}>
        <div className={s.airlines}>
          <div className={s.logosWrap}>
            {companyImages.map((value, key) =>
              <img
                key={key}
                src={value}
                alt={value}
                height={36}
              />
            )}
          </div>
          <div>
            {companyImages.length === 1
              ? <span className={s.airlineName}>{trip.outbound.companiesMap[0][1].carrier}</span>
              : <span className={s.airlineName}>{companyImages.length} airlines</span>
            }
          </div>
        </div>
        {
          !noSelectSection &&
          <div className={s.price}>
            <span className={s.cost}>{config.currency} {perMonth}</span> / month
          </div>
        }
      </div>
      <table className={s.table}>
        <tbody>
          <tr className={s.tripWrap}>
            <td className={s.code}>
              {outboundOrigin.airport_IATA}
            </td>
            <td className={s.time}>
              {moment(outboundOrigin.date).format('HH:mm')}
            </td>
            <td className={s.layover}>
              <div className={s.layoverBlock}>{layoverOutboundCount > 0 &&
              <span className={s.layoverCount}>{layoverOutboundCount}</span>} <span className={s.line}></span></div>
            </td>
            <td className={s.code}>
              {outboundDestination.airport_IATA}
            </td>
            <td className={s.time}>
              {moment(outboundDestination.date).format('HH:mm')}
            </td>
            <td className={s.total}>
              {totalOutboundTime}
            </td>
          </tr>

          {withReturn &&
          <tr className={s.tripWrap}>
            <td className={s.code}>
              {returnOrigin.airport_IATA}
            </td>
            <td className={s.time}>
              {moment(returnOrigin.date).format('HH:mm')}
            </td>
            <td className={s.layover}>
              <div className={s.layoverBlock}>{layoverReturnCount > 0 &&
              <span className={s.layoverCount}>{layoverReturnCount}</span>} <span className={s.line}></span></div>
            </td>
            <td className={s.code}>
              {returnDestination.airport_IATA}
            </td>
            <td className={s.time}>
              {moment(returnDestination.date).format('HH:mm')}
            </td>
            <td className={s.total}>
              {totalReturnTime}
            </td>
          </tr>
          }
        </tbody>

			</table>

		</div>
	)
})

export default TripDisplaySm
