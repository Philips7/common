import React, {Component} from 'react'
import s from './Flight.scss'
import cn from 'classnames'
import {pure} from 'recompose'
import moment from 'moment'

const Flight = pure(({data, compact}) => {
	//TODO fixme
	const {date, airport_name, airport_IATA} = data
	const parsedDate = moment(date).utcOffset(date)
	const time = parsedDate.format('HH:mm')
	const flyDate = parsedDate.format('DD MMM')
	return (
		<div className={cn(s.flightWrap, compact && s.compact)}>
			<div className={s.infoWrap}>
				<div className={s.date}>{flyDate}</div>
				<div className={s.time}>{time}</div>
			</div>
			<div className={s.dotWrap}><div className={s.dot}/></div>
			<div className={cn(s.airport)}>{airport_name} <span className={s.code}>({airport_IATA})</span></div>
			<div className={cn(s.date, s.dateDesktop)}>{flyDate}</div>
		</div>
	)
})

export default Flight
