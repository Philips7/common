import React from 'react'
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger'
import Tooltip from 'react-bootstrap/lib/Tooltip'
import {pure} from 'recompose'
import moment from 'moment'
import 'moment-duration-format'
import {I18n, Translate} from 'react-redux-i18n'
import s from './Way.scss'

const Way = pure(({segments}) => {
  function waitInfo(segment, index) {
    const {date, city, airport_name} = segment.destination
    const nextFlyTime = segments[index + 1].origin.date
    const waitTime = moment.duration(moment(nextFlyTime).diff(moment(date))).format('HH[h]:mm[m]')
    return `${I18n.t('TripDescription.waitInfoArrival', {
      airport_name,
      city,
      time: parseTime(date)
    })} ${I18n.t('TripDescription.waitTime', {waitTime})}`
  }

  function parseTime(time) {
    return moment.parseZone(time).format('HH:mm')
  }

  return (
    <div className={s.wayWrap}>
      <OverlayTrigger
        animation={false}
        placement='top'
        overlay={
          <Tooltip id={'Departure'}>
           <Translate
              value='TripDescription.waitInfoDeparture'
              airport_name={segments[0].origin.airport_name}
              city={segments[0].origin.city}
              time={parseTime(segments[0].origin.date)}
            />
          </Tooltip>
        }
      >
        <div className={s.circle}></div>
      </OverlayTrigger>

      {segments.map((item, index) =>
        index < segments.length - 1 && (
          <OverlayTrigger
            animation={false}
            key={index}
            placement='top'
            overlay={
              <Tooltip id={index}>
                {waitInfo(item, index)}
              </Tooltip>
            }
          >
            <div className={s.circle}></div>
          </OverlayTrigger>
        )
      )}

      <OverlayTrigger
        animation={false}
        placement='top'
        overlay={
          <Tooltip id={'Arrival'}>
          <Translate
            value='TripDescription.waitInfoArrival'
            airport_name={ segments[segments.length - 1].destination.airport_name}
            city={segments[segments.length - 1].destination.city}
            time={parseTime(segments[segments.length - 1].destination.date)}
           />
          </Tooltip>
        }
      >
        <div className={s.circle}></div>
      </OverlayTrigger>
    </div>
  )
})

export default Way
