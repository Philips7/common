import React, {Component} from 'react'
import cn from 'classnames'
import {pure} from 'recompose'
import moment from 'moment'
import 'moment-duration-format'
import Time from 'static/images/ticket-icons/icon-layover.svg'
import InlineSVG from 'svg-inline-react'

import s from './WaitPanel.scss'

const WaitPanel = pure(({arrivalTime, departureTime}) => {
  const waitTime = moment.duration(moment.parseZone(departureTime).diff(moment.parseZone(arrivalTime)))

  return (
    <div
      className={cn(s.waitPanelWrap)}
    >
      <div className={s.iconWrap}>
				<InlineSVG src={Time}/>
      </div>

      <div className={s.time}>Layover {waitTime.format('HH[h] mm[m]', {trim: false})}</div>
    </div>)
})

export default WaitPanel
