import React, {Component} from 'react'
import {pure} from 'recompose'
import {Translate} from 'react-redux-i18n'
import s from './ExpandDescription.scss'
import WaitPanel from '../WaitPanel/WaitPanel'
import PartWayDescription from '../PartWayDescription/PartWayDescription'

const ExpandDescription = pure(({trip, outbound, compact}) =>
  <div className={compact && s.compact}>
    <div className={s.wayDirection}>
      <Translate value={outbound ? 'TripDescription.outbound' : 'TripDescription.inbound'}/>
    </div>
    {trip.map((segment, index) =>
      <div
        key={index}
        className={index && s.nextFlight}
      >
        <PartWayDescription trip={segment} compact={compact}/>
        {index < trip.length - 1 && (
          <WaitPanel
            arrivalTime={segment.destination.date}
            departureTime={trip[index + 1].origin.date}
          />
        )}
      </div>
    )}
  </div>
)

export default ExpandDescription
