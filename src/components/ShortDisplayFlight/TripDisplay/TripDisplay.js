import React from 'react'
import cn from 'classnames'
import moment from 'moment'
import 'moment-duration-format'
import {pure} from 'recompose'
import {Translate} from 'react-redux-i18n'
import TimeIcon from 'static/images/ticket-icons/icon-layover.svg'
import InlineSVG from 'svg-inline-react'
import Trip from '../Trip/Trip'
import s from './TripDispaly.scss'

const TripDisplay = pure(({outbound, trip:{companiesMap, segments, duration, total_layover_time}, compact, mobile, withReturn}) => {
  // TODO fixme killme
  const origin = segments[0]
  const destination = segments[segments.length - 1]
  const departTime = moment(origin.origin.date).utcOffset(origin.origin.date)
  const arriveTime = moment(destination.destination.date).utcOffset(destination.destination.date)
  const diff = arriveTime.dayOfYear() - departTime.dayOfYear()
  const diffDays = arriveTime.dayOfYear() >= departTime.dayOfYear() ? diff : (366 + diff)
  const renderAdditionalDays = diffDays ? diffDays > 0 ? `+${diffDays}` : diffDays : ''
  const fullTime = moment.duration(duration, 'minutes').format('HH[h] mm[m]', {trim: false})
  const layoverTime = moment.duration(total_layover_time, 'minutes').format('HH[h] mm[m]', {trim: false})

  return (
    <div
      className={cn(s.tripDisplayWrap, s.mainInfo, compact && s.compact, outbound && s.outbound, withReturn && s.withReturn)}>
      <div className={s.tripDisplay}>

        <div className={s.head}>

          <div className={s.way}>
            <Translate
              value={outbound ? 'TripDescription.outbound' : 'TripDescription.inbound'}/>
          </div>

          <div className={s.carrier}>
            {
              companiesMap.map((value, key) =>
                <img
                  key={key} //Converting Map to Array
                  src={value[1].carrier_icon}
                  alt={value[0][0]}
                  height={36}
                />
              )
            }
            {
              <span>{
                companiesMap.length === 1 ?
                  <span>{origin.carrier}</span>
                  :
                  <span>{companiesMap.length} airlines</span>
              }</span>
            }
          </div>
        </div>

        <Trip
          segments={segments}
          duration={duration}
          origin={origin}
          destination={destination}
          compact={compact}
        />

        <div className={s.tripDirection}>


        </div>

        <div className={s.flight}>

          <div className={s.departTime}>
            <span className={s.time}>{departTime.format('HH:mm')}</span>
            <span className={s.date}>{departTime.format('DD MMM')}</span>
            <span className={cn(s.city, compact && s.compact)}>
              <span className={s.airportCity}>
                {origin.origin.city}
              </span>
            </span>
          </div>

          <div className={s.arriveTime}>
            <span className={s.time}>{arriveTime.format('HH:mm')}</span>
            <span className={s.date}>{arriveTime.format('DD MMM')}
              <span className={s.point}>{renderAdditionalDays}</span>
            </span>
            <span className={cn(s.city, compact && s.compact)}>
              <span className={s.airportCity}>
                {destination.destination.city}
              </span>
            </span>
          </div>

        </div>
      </div>
    </div>
  )
})

export default TripDisplay
