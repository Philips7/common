import React, {Component} from 'react'
import s from './PartWayDescription.scss'
import Flight from '../Flight/Flight'
import {pure} from 'recompose'

const ExpandDescription = pure(({trip: {carrier_icon, carrier_iata, carrier, origin, destination}, compact}) =>
	<div className={s.partWayDescriptionWrap}>
		<Flight data={origin} compact={compact}/>
		<p className={s.airline}>
      <span className={s.imageWrap}>
        <img
					src={carrier_icon}
					alt={carrier_iata}
					height={36}
				/>
      </span>
			<span>{carrier}</span>
		</p>
		<Flight data={destination} compact={compact}/>
	</div>
)

export default ExpandDescription
