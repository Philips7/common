import React from 'react'
import 'moment-duration-format'
import {pure} from 'recompose'
import moment from 'moment'
import Way from '../Way/Way'
import s from './Trip.scss'
import cn from 'classnames'

const Trip = pure(({origin, destination, duration, segments, compact}) => {

  const totalTime = moment.duration(duration, 'minutes').format('HH[h] mm[m]', {trim: false})

  return (
    <div className={cn(s.tripWrap, compact && s.compact)}>
      <span className={s.airportCode}>{origin.origin.airport_IATA}</span>
      <div className={s.wayDirection}>
        <span className={s.changing}>
          {
            segments.length == 1
              ? <span className={s.direct}>Direct</span>
              : <span className={s.changingCount}>{segments.length - 1} stops</span>
          }
        </span>
        <Way segments={segments}/>
        <span className={s.totalTime}>{totalTime}</span>
      </div>
      <span className={s.airportCode}>{destination.destination.airport_IATA}</span>
    </div>
  )
})

export default Trip
