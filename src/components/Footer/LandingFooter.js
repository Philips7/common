import React from 'react'
import s from './LandingFooter.scss'
import { Link } from 'react-router'
import { pure } from 'recompose'
import { I18n } from 'react-redux-i18n'
import InlineSVG from 'svg-inline-react'
import Newsletter from '../Newsletter/Newsletter'

import FacebookIcon from './assets/facebook-logo.svg';
import InstagramIcon from './assets/instagram-logo.svg';
import TwitterIcon from './assets/twitter-logo-silhouette.svg';
import FlymbleLogo from  'static/images/flymble-logo-white.svg'
import partnersImage from './assets/partnersList.png'
import paymentsImage from 'static/images/paymentsList.png'
import KiwiLogo from  'static/images/kiwi-logo.svg'
import IdealLogo from  'static/images/ideal-logo.svg'
import NinjasLogo from 'static/images/ninjas-logo.svg'
import SmallLogo from  'static/images/landing/vliegwell_trans.svg'
import LinkedInLogo from './assets/linkedIn-logo.svg'

export const Footer = pure(({signUpNewsletterAttempt}) => (
  <div className={s.footer}>
    <div className={s.contentWrap}>
      <div className={s.container}>
        <div className={s.mainPart}>
          <div className={s.contact}>
            <div className={s.logoWrap}>
              <InlineSVG src={FlymbleLogo}/>
            </div>

            <Newsletter onSubmit={signUpNewsletterAttempt} />

            <ul className={s.socialList}>
              <li>
                <a href='https://facebook.com/Flymble/'
                   target='_blank' rel='nofollow'>
                  <InlineSVG src={FacebookIcon}/>
                </a>
              </li>
              <li>
                <a href='https://twitter.com/flymbletravel'
                   target='_blank' rel='nofollow'>
                  <InlineSVG src={TwitterIcon}/>
                </a>
              </li>
              <li>
                <a href='https://www.linkedin.com/company/flymble'
                   target='_blank' rel='nofollow'>
                  <InlineSVG src={LinkedInLogo}/>
                </a>
              </li>
              <li>
                <a href='https://www.instagram.com/flymbletravel/'
                   target='_blank' rel='nofollow'>
                  <InlineSVG src={InstagramIcon}/>
                </a>
              </li>
            </ul>
          </div>

          <div className={s.nav}>
            <div className={s.col}>
              <h4 className={s.title}>
                About
              </h4>

              <ul className={s.linksList}>

                <li>
                  <Link to='terms-conditions'>
                    {I18n.t('Footer.termsConditions')}
                  </Link>
                </li>
                <li>
                  <Link to='privacy'>Cookies & Privacy Policy</Link>
                </li>

                <li>
                  <Link to='faq'>FAQ</Link>
                </li>

                {/*<li>*/}
                  {/*<Link to="#">How it Works</Link></a>*/}
                {/*</li>*/}
              {/*<li>*/}
                {/*<Link to="#">Team</Link>*/}
                {/*</li>*/}
              {/*<li>*/}
                {/*<Link to="#">Investor Relations</Link>*/}
                {/*</li>*/}
            </ul>
          </div>
          <div className={s.col}>
            <h4 className={s.title}>
              Press
            </h4>

            <ul className={s.linksList}>
              <li>
                <a href='https://blog.flymble.com/' target='_blank'>{I18n.t('Footer.inTheNews')}</a>
              </li>
              <li>
                <Link to='story'>
                  {I18n.t('Footer.story')}
                </Link>
              </li>
              {/*<li>*/}
                {/*<a href='https://www.facebook.com/Flymble/' target='_blank'>Facebook</a>*/}
                {/*</li>*/}
              {/*<li>*/}
                {/*<Link to="#">Press Releases</Link>*/}
                {/*</li>*/}
              {/*<li>*/}
                {/*<Link to="#">Testimonials</Link>*/}
                {/*</li>*/}
            </ul>
          </div>
          <div className={s.col}>
            <h4 className={s.title}>
              Get in Touch
            </h4>

            <ul className={s.linksList}>
              {/*<li>*/}
                {/*<Link to="#">Careers</Link>*/}
                {/*</li>*/}
              {/*<li>*/}
                {/*<Link to="#">Business Solutions</Link>*/}
                {/*</li>*/}
              <li>
                <Link to='contact-us'>{I18n.t('Footer.contactUs')}</Link>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div className={s.secondaryPart}>
        <div className={s.col}>
          <div className={s.partners}>
            <h4 className={s.colTitle}>
              Partnered with:
            </h4>

            <div className={s.imageWrap}>
              <img src={partnersImage} alt='partners image'/>
            </div>
          </div>
        </div>
        <div className={s.col}>
          <div className={s.payments}>
            <h4 className={s.colTitle}>
              Payments accepted:
            </h4>

            <div className={s.imageWrap}>
              <img src={paymentsImage} alt='payments image'/>
            </div>
          </div>
        </div>
        <div className={s.col}>
          <div className={s.assistance}>
            <h4 className={s.colTitle}>
              Need assistance? Call:
            </h4>

            <div className={s.linkWrap}>
              <a href='tel:+4407925717068'>
                +44 07925717068
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div className={s.infoBlock}>
    <div className={s.container}>
      <p>
        Flymble Travel Limited is an appointed representative of Fly Now Pay Later Limited. Fly Now Pay Later acts as a
        credit intermediary and not a lender, and is authorised and regulated by the Financial Conduct Authority under
        registration number 778540.
        <br/> <br/>
        Flymble.com is a registered trademark under Flymble Travel LTD
        <br/> <br/>
        Flymble Travel LTD is a subsidiary of Vliegwel BV
      </p>
    </div>
  </div>
  <div className={s.developersLine}>
    <span>{I18n.t('Footer.CraftedWith')} <span>♥</span> {I18n.t('Footer.by')}</span>
    <a href="http://7ninjas.com/" target="_blank">
      <InlineSVG src={NinjasLogo}/>
    </a>
  </div>
</div>

))

export default Footer
