import React, {Component} from 'react'
import {pure} from 'recompose'
import InlineSVG from 'svg-inline-react'
import {isClient} from 'utils/helpers'
import PreloaderOverlayText from './PreloaderOverlayText'
import SpinnerSvg from 'static/images/spinner.svg'

import s from './PreloaderOverlay.scss'

@pure
class SpinnerLoader extends Component {

  constructor() {
    super()
    this.state = {
			showModal: false
    }
  }

  componentWillReceiveProps (nextProps) {
    const {show, endAction} = this.props
    if ( show && !nextProps.show) {
      setTimeout(() => {
				this.setState({
					showModal: nextProps.show
				}, () => {
					endAction()
				})
      }, 1000)
    } else {
			this.setState({
				showModal: nextProps.show
			})
    }
  }

  render() {
    const { showModal } = this.state
    const {labels, children, show, beforeCloseIcon} = this.props
		return showModal && <div className={s.loadingOverlay}>
				<InlineSVG src={show ? SpinnerSvg : beforeCloseIcon} className={s.spinner}/>
				<PreloaderOverlayText labels={labels} child={children} className={s.checkoutLabel}/>
			</div> || null
  }

}

export default SpinnerLoader
