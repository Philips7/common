import React, {Component} from 'react'
import {pure} from 'recompose'
import PreloaderOverlayText from './PreloaderOverlayText'
// import InlineSVG from 'svg-inline-react'
// import PlaneLoader from 'static/images/logo_plane_animated.svg'
import s from './PreloaderOverlay.scss'
import {isClient} from '../../utils/helpers'

const Loader = isClient ? require('halogen/PulseLoader') : 'div'

const PreloaderOverlay = pure(({labels}) =>
  <div className={s.loadingOverlay}>
		<div className={s.contentWrap}>
			<div className={s.iconWrap}>
				{/*<InlineSVG src={PlaneLoader}/>*/}
				<Loader className={s.loader}
								color='#fff'
								size='20px' />
			</div>
			<PreloaderOverlayText labels={labels} />
		</div>
  </div>
)

export default PreloaderOverlay
