import React, {Component} from 'react'
import {pure} from 'recompose'
import {I18n} from 'react-redux-i18n'
import s from './PreloaderOverlay.scss'

const labels = [
  {text: `${I18n.t('Preloader.loading')}`, time: 500},
  {text: `${I18n.t('Preloader.loading')}.`, time: 500},
  {text: `${I18n.t('Preloader.loading')}..`, time: 500},
  {text: `${I18n.t('Preloader.loading')}...`, time: 500},
  {text: `${I18n.t('Preloader.loading')}....`, time: 500},
  {text: `${I18n.t('Preloader.loading')}.....`, time: 500},
  {text: `${I18n.t('Preloader.loading')}......`, time: 500},
  {text: `${I18n.t('Preloader.loading')}.......`, time: 500}
]

@pure
class PreloaderOverlayText extends Component {
  constructor() {
    super()
    this.state = {index: 0}
  }

  intervalFunction = () => {
    const {labels} = this.props
    const {index} = this.state
    this.setState({index: (index + 1) % labels.length})
    this.timeOut = setTimeout(this.intervalFunction, labels[index].time)
  }

  componentDidMount() {
    this.timeOut = setTimeout(this.intervalFunction, 3000)
  }

  componentWillUnmount() {
    clearTimeout(this.timeOut)
  }

  render() {
		const {labels, className, child} = this.props
		return <div className={className || s.overlayLabel}>
			{ labels[this.state.index].text }
			{
				child && <div className={s.logo}>
					{child}
				</div>
			}
		</div>
	}
}

PreloaderOverlayText.defaultProps = {labels};

export default PreloaderOverlayText
