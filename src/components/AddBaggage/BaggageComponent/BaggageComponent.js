import React from 'react'
import styles from './BaggageComponent.scss'
import classnames from 'classnames'
import Row from 'react-bootstrap/lib/Row'
import Col from 'react-bootstrap/lib/Col'

export default  () => (
  <div className={styles.baggageComponentWrapper}>
    <Row>
      <Col xs={12}>
        <div>London<i className='fa fa-long-arrow-right'/>New York</div>
      </Col>
    </Row>
    <Row>
      <Col xs={12}>
        <div className={styles.baggageForFlight}>
          <Row>
            <Col xs={6}>
              <i className='fa fa-suitcase'/>
              <span>Cabin baggage</span>
            </Col>
            <Col xs={4}>
              <div className={styles.smallerText}>42x32x20 cm | 7 kg</div>
            </Col>
            <Col xs={2}>
              <div className={classnames(styles.smallerText, 'text-center')}>Free</div>
            </Col>
          </Row>
        </div>
      </Col>
    </Row>
    <Row>
      <Col xs={12}>
        <div className={styles.baggageOption}>
          <Row>
            <Col xs={6}>
              <i className='fa fa-plus-circle'/>
              <span>Add baggage</span>
            </Col>
            <Col xs={4}>
              <span className={styles.smallerText}>15 kg</span>
            </Col>
            <Col xs={2}>
              <div className={classnames(styles.smallerText, 'text-center')}>+ 110{config.currency}</div>
            </Col>
          </Row>
        </div>
      </Col>
    </Row>
  </div>
)
