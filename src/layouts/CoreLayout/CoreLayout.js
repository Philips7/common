import React, { Component } from 'react'
import Header from '../../containers/HeaderContainer'
import Footer from '../../containers/FooterContainer'
import {connect} from 'react-redux'
import {startup, filtersPermanentlyInCookies} from '../../redux/modules/Common'
import {Notifs} from 'redux-notifications'
import {isClient} from '../../utils/helpers'
import Helmet from 'react-helmet'
import favicon from 'static/favicon.png'
import c from './CoreLayout.scss'
import {isiOS} from 'utils/responsive'
import {intercomId} from 'config.json'
//import '../../styles/app.global.scss'

const transparentHeader = ['/search', '/']
const debug = require('debug')('app:CoreLayout')

@connect(null, {startup, filtersPermanentlyInCookies})
export class CoreLayout extends Component {
	static propTypes = {
		children: React.PropTypes.element.isRequired
	}

	componentWillMount() {
    this.props.startup()
    this.props.filtersPermanentlyInCookies()
    if (isClient) {
      window.Intercom('boot', {
        app_id: intercomId,
        hide_default_launcher: true,
      })
    }
  }

	render () {
		const {children, location: {pathname}} = this.props
		return <div
			style={isiOS ? {cursor: 'pointer'} : {}}
			className={'fullPageWrap'}> {/*TODO move class to this component*/}
			<Helmet
				link={[
					{rel: 'icon', href: favicon }
				]}
			/>
			<Header fillHeader={!(transparentHeader.indexOf(pathname) >= 0)} />
			<div className='mainWrap'>
				{children}
			</div>
			<Footer/>
			{__PROD__ ? isClient && <Notifs /> : <Notifs />}
		</div>
	}
}

export default CoreLayout
