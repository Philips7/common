import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { renderToString } from 'react-dom/server'
import Helmet from 'react-helmet'
import serialize from 'serialize-javascript'
import { site, fnpl_integration_key, hotjarSiteId, intercomId } from 'config.json'

const debug = require('debug')('app:Html')

export default class Html extends Component {
  static propTypes = {
    assets: PropTypes.object.isRequired,
    component: PropTypes.node.isRequired,
    store: PropTypes.object.isRequired
  }

  get scripts() {
    const { javascript } = this.props.assets

    return javascript ? Object.keys(javascript).reverse().map((script, i) => {
      const key = `script_${i}`
      return <script src={javascript[script]} key={key} type="application/javascript" />
    }) : []
  }

  get styles() {
    const { assets } = this.props
    const { styles, assets: _assets } = assets
    const stylesArray = styles ? Object.keys(styles) : []

    // styles (will be present only in production with webpack extract text plugin)
    if (stylesArray.length !== 0) {
      return stylesArray.map((style, i) => {
        const key = `style_${i}`
        return <link href={assets.styles[style]} key={key} rel="stylesheet" type="text/css" />
      })
    }

    // (will be present only in development mode)
    // It's not mandatory but recommended to speed up loading of styles
    // (resolves the initial style flash (flicker) on page load in development mode)
    const stylesPaths = _assets ? Object.keys(_assets).filter(asset => asset.indexOf('.css') >= 0 || asset.indexOf('.scss') >= 0) : []
    return stylesPaths.map((style, i) => {
      const key = `style_${i}`
      return <style dangerouslySetInnerHTML={{ __html: _assets[style]._style }} key={key} />
    })
  }

  render() {
    const { component, store } = this.props
    const head = Helmet.rewind()
    const htmlAttributes = head.htmlAttributes.toComponent()

    return <html lang={htmlAttributes.lang}>
        <head>
          <title>Flymble</title>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta name="google-site-verification" content="oahMRqAvMMNi2F8AWMMxkJMugWd26lcNjOs1fDV-va0" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          {/*<link rel="shortcut icon" href='/favicon.ico' />*/}

          {head.title.toComponent()}
          {head.meta.toComponent()}
          {head.link.toComponent()}
          {head.script.toComponent()}

          <link href="https://fonts.googleapis.com/css?family=Heebo:100,300,400,500,700|Playfair+Display" rel="stylesheet" />

          {this.styles}
          <meta name="og:title" content="Book flights for only a fraction upfront and spread the rest over time" />
          <meta name="title" content="Book flights for only a fraction upfront and spread the rest over time" />
          <meta name="description" content="Book flights for only a fraction upfront and spread the rest over time" />
          <meta name="og:description"
                content="Book flights for only a fraction upfront and spread the rest over time" />

          {site.domain === "flymble.com" && <script dangerouslySetInnerHTML={{ __html: `
              // Yandex.Metrika counter
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function() {
                        try {
                            w.yaCounter43811634 = new Ya.Metrika({
                                id:43811634,
                                clickmap:true,
                                trackLinks:true,
                                accurateTrackBounce:true,
                                webvisor:true,
                                trackHash:true
                            });
                        } catch(e) { }
                    });

                    var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () { n.parentNode.insertBefore(s, n); };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = "https://mc.yandex.ru/metrika/watch.js";
                    console.info('Metrika tracking started')

                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else { f(); }
                })(document, window, "yandex_metrika_callbacks");
              ` }} />}

          {/*<!-- Begin Inspectlet Embed Code -->*/}
          {/* <script id='inspectletjs' type='text/javascript' dangerouslySetInnerHTML={{
          __html: `
            window.insp = window.insp || [];
            insp.push(['wid', 1676094345]);
            (function() {
            function ldinsp(){if(typeof window.inspld != 'undefined') return; window.__inspld = 1; var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = 'inspsync'; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); };
            setTimeout(ldinsp, 500); document.readyState != 'complete' ? (window.attachEvent ? window.attachEvent('onload', ldinsp) : window.addEventListener('load', ldinsp, false)) : ldinsp();
            })();
            `
        }}
        /> */}
          {/*<!-- End Inspectlet Embed Code -->*/}

          <script dangerouslySetInnerHTML={{ __html: `
              // Facebook Pixel Code

              !function(f,b,e,v,n,t,s)
              {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window,document,'script',
                'https://connect.facebook.net/en_US/fbevents.js')
              fbq('init', '1881570462086860')
              fbq('track', 'PageView')
            ` }} />

          {site.domain === "flymble.com" && <script dangerouslySetInnerHTML={{ __html: `
            {/* Hotjar Tracking Code */}
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:${hotjarSiteId},hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
              ` }} />}

          {<script dangerouslySetInnerHTML={{ __html: `
            (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;
s.src='https://widget.intercom.io/widget/${intercomId}';
var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()
` }} />}

          {//<!-- Google Tag Manager -->
            <script dangerouslySetInnerHTML={{ __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
              })(window,document,'script','dataLayer','GTM-MGMGC6V');
              ` }} />}
          <noscript>
            <img height="1" width="1" src="https://www.facebook.com/tr?id=1881570462086860&ev=PageView&noscript=1" />
            <img src="https://mc.yandex.ru/watch/43811634" style={{ position: "absolute", left: -9999 }} alt="" />
          </noscript>
        </head>

        <body>
          <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MGMGC6V" height="0" width="0" style={{ display: "none", visibility: "hidden" }} />
          </noscript>
          <div id="root" style={{ height: "100%" }} dangerouslySetInnerHTML={{ __html: renderToString(component) }} />
          <script dangerouslySetInnerHTML={{ __html: `window.__PRELOADED_STATE__=${serialize(store.getState())};` }} />
          {this.scripts}
          {/* TrustBox script */}
          <script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async />
          {/* End Trustbox script */}
        </body>
      </html>;
  }
}
