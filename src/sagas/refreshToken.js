import {take, call, put} from 'redux-saga/effects'
import request from 'sagas/api'
import config from 'config.json'
import {REFRESH_TOKEN_ATTEMPT} from '../redux/modules/User'
const debug = require('debug')('app:refreshTokenSaga')

export default () => {
  function * worker({values: {refresh_token}, responseSuccess, responseFailure}) {
  	debug('workerStart')
    const body = {
      method: 'POST',
      body: {
        client_id: config.client_id,
        client_secret: config.client_secret,
        grant_type: 'refresh_token',
        refresh_token
      }
    }

    try {
      const {res, err} = yield call(request, 'auth/token', body, false)

      if (res) {
        yield put(responseSuccess(res))
      } else {
        yield put(responseFailure(err))
      }
    } catch (error) {
    }
  }

  function * watcher() {
    while (true) {
      const data = yield take(REFRESH_TOKEN_ATTEMPT)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
