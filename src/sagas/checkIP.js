import { call, take } from 'redux-saga/effects'
import fetch from 'isomorphic-fetch'
import { IP_CHECK } from '../redux/modules/Common'

export default () => {
  function* worker({ responseSuccess, dispatch }) {
    const body = {
      method: 'GET',
    }

    try {
      const res = yield call(fetch, 'https://ws-staging.flymble.com/v1/flights/check-country', body)
      res.json().then(r =>
        dispatch(responseSuccess(r.country_code === 'GB'))
      )
    } catch (error) {
      console.log(error)
    }
  }

  function* watcher() {
    while (true) {
      const data = yield take(IP_CHECK)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
