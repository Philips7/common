import { take, call, put } from 'redux-saga/effects'
import request from '../sagas/api'
import { GET_COUNTRIES_ATTEMPT } from '../redux/modules/Countries'

export default () => {
  function* worker({ responseSuccess, responseFailure }) {
    const body = {
      method: 'GET'
    }

    try {
      const { res, err } = yield call(request, 'users/countries', body)

      if (res) {
        yield put(responseSuccess(res))
      } else {
        yield put(responseFailure(err))
      }
    } catch (error) {
      yield put(responseFailure(error))
    }
  }

  function* watcher() {
    while (true) {
      const data = yield take(GET_COUNTRIES_ATTEMPT)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
