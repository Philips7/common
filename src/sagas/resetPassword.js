import {take, call, put} from 'redux-saga/effects'
import request from 'sagas/api'
import config from 'config.json'
import {RESET_PASSWORD_ATTEMPT} from '../redux/modules/ResetPassword'
import {SubmissionError} from 'redux-form'

export default () => {
  function * worker({values: {email}, responseSuccess, responseFailure, resolve, reject}) {
    const body = {
      method: 'POST',
      body: {
        email,
        frontend_domain: config.site.domain,
        reset_url: 'confirm-reset-password'
      }
    }

    try {
      const {res, err} = yield call(request, 'confirm/reset/reset_password', body, false)

      if (res) {
        yield put(responseSuccess(res))
        resolve()
      } else {
        yield put(responseFailure(err))
        reject(new SubmissionError(err))
      }
    } catch (error) {
      yield put(responseFailure())
      reject(new SubmissionError())
    }
  }

  function * watcher() {
    while (true) {
      const data = yield take(RESET_PASSWORD_ATTEMPT)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
