import {take, put} from 'redux-saga/effects'
import {STARTUP} from '../redux/modules/Common'
import {refreshToken} from '../redux/modules/User'

// process STARTUP actions
export  default function *() {
  yield take(STARTUP)
  yield put(refreshToken())
}
