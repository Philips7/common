import {take, call, put} from 'redux-saga/effects'
import {SubmissionError} from 'redux-form'
import {I18n} from 'react-redux-i18n'
import request from 'sagas/api'
import config from 'config.json'
import {SIGN_IN_ATTEMPT} from '../redux/modules/User'

export default () => {
  function * worker({values: {email, password}, responseSuccess, responseFailure, resolve, reject}) {
    const body = {
      method: 'POST',
      body: {
        client_id: config.client_id,
        client_secret: config.client_secret,
        grant_type: 'password',
        username: email,
        password
      }
    }
    try {
      const {res, err} = yield call(request, 'auth/token', body, false)
      if (res) {
        yield put(responseSuccess(res))
        resolve()
      } else {
        yield put(responseFailure(err))
        reject(new SubmissionError({password: I18n.t('form.wrongCredentials')}))
      }
    } catch (error) {
      yield put(responseFailure(error))
      reject(new SubmissionError())
    }
  }

  function * watcher() {
    while (true) {
      const data = yield take(SIGN_IN_ATTEMPT)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
