import {take, call, put} from 'redux-saga/effects'
import request from 'sagas/api'
import {USER_GET_ATTEMPT} from '../redux/modules/User'

const debug = require('debug')('app:userGetSaga')

export default () => {
  function * worker({responseSuccess, responseFailure}) {
    const body = {
      method: 'GET'
    }

    try {
      const {res, err} = yield call(request, 'users/user', body)
      if (res) {
        yield put(responseSuccess(res))
      } else {
        yield put(responseFailure(err))
      }
    } catch (error) {
      // TODO should we show an error here?
    }
  }

  function * watcher() {
    while (true) {
      const data = yield take(USER_GET_ATTEMPT)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
