import {put, select} from 'redux-saga/effects'
import {getCookie} from 'redux-cookie'
import {isClient} from '../utils/helpers'
import fetch from 'isomorphic-fetch'
import config from 'config.json'
import {refreshToken} from '../redux/modules/User'
const debug = require('debug')('app:apiSaga')
// do NOT send Authorization header to urls
const allowedRequests = [
  {
    url: 'auth/token'
  },
  {
    url: 'users/user',
    method: 'post'
  }
]

const isAuthRequest = (url, method) =>
  allowedRequests.find(
    rq =>
      rq.method && rq.method !== method ?
        false :
      rq.url === url
  )

const prepareRequestBody = (body, jsonRequest) => {
  if (jsonRequest) {
    return JSON.stringify(body)
  } else {
    let formDataBody = new FormData()
    if (body) {
      Object.keys(body).forEach((key) => {
        formDataBody.append(key, body[key])
      })
    }

    return formDataBody
  }
}

const prepareRequestHeaders = ({headers = {}, method}, jsonRequest, url, token) => {
  if (token && !isAuthRequest(url, method)) {
    headers['Authorization'] = `Bearer ${token}`
  }

  if (jsonRequest) {
    headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      ...headers
    }
  }

  return headers
}

export default function *(url, data, jsonRequest = true) {
	let token
	if (isClient) {
		token = yield put(getCookie('token'))
	} else {
		token = yield select( state => state.Common.token )
	}

  data.body = prepareRequestBody(data.body, jsonRequest)
  data.headers = prepareRequestHeaders(data, jsonRequest, url, token)
  const response = yield fetch(config.api.baseUrl + url, data)
  // if user is unauthorized - try to refresh token
  if (response.status === 401) {
    yield put(refreshToken())
  }
  // TODO response can be empty (backend, tell me why), so json parsing fails, @arek said that he'll fix it
  try {
    const responseBody = yield response.json()
    return response.ok ? {res: responseBody} : {err: responseBody}
  } catch (error) {
    return response.ok ? {res: {}} : {err: {}}
  }
}
