import {take, call, put} from 'redux-saga/effects'
import request from 'sagas/api'
import config from 'config.json'
import {SIGN_UP_FACEBOOK_ATTEMPT} from '../redux/modules/User'

export default () => {
  function * worker({values, responseSuccess, responseFailure}) {
    const body = {
      method: 'POST',
      body: {
        client_id: config.client_id,
        client_secret: config.client_secret,
        grant_type: 'convert_token',
        backend: 'facebook',
        token: values.accessToken
      }
    }

    try {
      const {res, err} = yield call(request, 'auth/convert_token', body, false)

      if (res) {
        yield put(responseSuccess(res))
      } else {
        yield put(responseFailure(err))
      }
    } catch (error) {
      yield put(responseFailure())
    }
  }

  function * watcher() {
    while (true) {
      const data = yield take(SIGN_UP_FACEBOOK_ATTEMPT)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
