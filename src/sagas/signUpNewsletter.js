import {take, call, put} from 'redux-saga/effects'
import request from 'sagas/api'
import {SubmissionError} from 'redux-form'
import {I18n} from 'react-redux-i18n'
import {hide} from 'redux-modal'
import {SIGN_UP_NEWSLETTER_ATTEMPT} from '../redux/modules/User'
import {errorNotification, successNotification} from '../utils/notifications'

export default () => {
  function * watcher() {
    while (true) {
      const {values, resolve, reject} = yield take(SIGN_UP_NEWSLETTER_ATTEMPT)
      try {
        const {res, err} = yield call(request, 'newsletter/subscribers/', {
          method: 'POST',
          body: values
        })

        if (res) {
          resolve && resolve()
          yield [
          	put(successNotification(I18n.t('Notify.successNewsletter'))),
						values.fromLanding && put(hide('paybackUnavailableModal'))
					]
        } else {
          reject && reject(new SubmissionError({email: I18n.t('Notify.subscriberExist')}))
        }
      } catch (error) {
        yield put(errorNotification(I18n.t('Notify.smthGoWrong')))
      }
    }
  }

  return {
    watcher
  }
}
