import {take, call, put} from 'redux-saga/effects'
import request from 'sagas/api'
import {setCookie} from 'redux-cookie'
import {SubmissionError} from 'redux-form'
import {SIGN_UP_ATTEMPT, signInAttempt} from '../redux/modules/User'

export default () => {
  function * worker({values, responseSuccess, responseFailure, resolve, reject}) {
    const body = {
      method: 'POST',
      body: values
    }

    try {
      const {res, err} = yield call(request, 'users/user', body)

      if (res) {
        yield[
          put(responseSuccess(values)),
          put(setCookie('token', res.access_token, {}))
        ]
        resolve && resolve()
      } else {
        yield put(responseFailure(err))
        reject && reject(new SubmissionError(err))
      }
    } catch (error) {
      yield put(responseFailure())
    }
  }

  function * watcher() {
    while (true) {
      const data = yield take(SIGN_UP_ATTEMPT)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
