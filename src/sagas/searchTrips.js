import { take, call } from 'redux-saga/effects'
import { ws } from '../services/api'

import {
  SEARCH_TRIPS_CONNECT,
  SEARCH_TRIPS_DISCONNECT,
  SEARCH_TRIPS_SEND,
  searchTripsEmit,
  searchTripsDisconnect,
  onSearchTripConnect,
  searchTripReconnect
} from '../redux/modules/Trips'

export default () => {
  function connect(dispatch) {
    const socket = ws('flights/trips/search')

    socket.onopen = () => dispatch(onSearchTripConnect())

    socket.onerror = () => dispatch(searchTripReconnect())

    socket.onclose = () => dispatch(searchTripsDisconnect())

    socket.onmessage = response => dispatch(searchTripsEmit(response))

    return socket
  }

  function* send(socket, values) {
    if (socket.readyState === socket.OPEN) {
      socket.send(JSON.stringify(values))
      yield take(SEARCH_TRIPS_DISCONNECT)
      socket.close()
    }
  }

  function* handleIO(socket) {
    const { values } = yield take(SEARCH_TRIPS_SEND)
    yield call(send, socket, values)
  }

  function* watcher() {
    while (true) {
      const { dispatch } = yield take(SEARCH_TRIPS_CONNECT)
      const socket = yield call(connect, dispatch)
      yield* handleIO(socket)
    }
  }

  return {
    watcher
  }
}
