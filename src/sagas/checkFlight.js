import { call, cancel, fork, take } from 'redux-saga/effects'
import { ws } from '../services/api'

import {
  CHECK_FLIGHT_CONNECT,
  CHECK_FLIGHT_DISCONNECT,
  CHECK_FLIGHT_SEND,
  checkFlightEmit,
  onCheckFlightOpen,
  checkFlightDisconnect
} from '../redux/modules/Ticket'

export default () => {
  function connect(dispatch, submit) {
    const socket = ws('flights/trips/check')

    socket.onopen = () => {
      dispatch(onCheckFlightOpen())
    }

    socket.onerror = () => {
      dispatch(checkFlightDisconnect())
    }

    socket.onclose = () => {
      // dispatch(checkFlightDisconnect())
    }

    socket.onmessage = (response) => {
      dispatch(checkFlightEmit(response, submit))
    }

    return socket
  }

  function* send(socket, { booking_token, passengersNumber, baggageNumber, paybackSelected }) {
    if (socket.readyState === socket.OPEN) {
      socket.send(JSON.stringify({
        data: {
          booking_token,
          number_of_passengers: passengersNumber,
          number_of_baggages: baggageNumber,
          number_of_months: paybackSelected,
        }
      }))
      yield take(CHECK_FLIGHT_DISCONNECT)
      socket.close()
    }
  }

  function* watcher() {
    while (true) {
      const { dispatch, submit } = yield take(CHECK_FLIGHT_CONNECT)
      const socket = yield call(connect, dispatch, submit)
      const { values: { data } } = yield take(CHECK_FLIGHT_SEND)
      yield call(send, socket, data)
    }
  }

  return {
    watcher
  }
}
