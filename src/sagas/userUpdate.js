import {take, call, put} from 'redux-saga/effects'
import request from 'sagas/api'
import {SubmissionError} from 'redux-form'
import {USER_UPDATE_ATTEMPT} from '../redux/modules/User'

export default () => {
  function * worker({values, responseSuccess, responseFailure, resolve, reject}) {
    const body = {
      method: 'PATCH',
      // TODO we should send only changed fields
      body: values
    }

    try {
      const {res, err} = yield call(request, 'users/user', body)

      if (res) {
        yield put(responseSuccess(res))
        resolve && resolve()
      } else {
        yield put(responseFailure(err))
        reject && reject(new SubmissionError(err))
      }
    } catch (error) {
      yield put(responseFailure())
    }
  }

  function * watcher() {
    while (true) {
      const data = yield take(USER_UPDATE_ATTEMPT)
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
