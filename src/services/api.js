import config from 'config.json'

export const ws = url => __CLIENT__ ? new WebSocket(config.ws.baseUrl + url) : {}
