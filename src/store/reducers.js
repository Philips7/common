import {combineReducers} from 'redux'
import {routerReducer as routing} from 'react-router-redux'
import {reducer as modal} from 'redux-modal'
import {reducer as form} from 'redux-form'
import {reducer as notifs} from 'redux-notifications'
import {i18nReducer} from 'react-redux-i18n'
import User from '../redux/modules/User'
import ResetPassword from '../redux/modules/ResetPassword'
import Ticket from '../redux/modules/Ticket'
import Common from '../redux/modules/Common'
import Trips from '../redux/modules/Trips'

import Landing from 'routes/Landing/modules/Landing'
import searchAirports from 'routes/Landing/modules/searchAirports'
import Support from 'routes/Support/modules/Support'
import Dashboard from 'routes/Dashboard/modules/Dashboard'
import ThankYou from 'routes/ThankYou/modules/ThankYou'
import EmailConfirm from 'routes/EmailConfirm/modules/EmailConfirm'
import EditInfo from 'routes/EditInfo/modules/EditInfo'
import ConfirmResetPassword from 'routes/ConfirmResetPassword/modules/ConfirmResetPassword'
import Checkout from 'routes/Checkout/modules/Checkout'
import ActivateUser from 'routes/ActivateUser/modules/ActivateUser'

export const makeRootReducer = (asyncReducers = {}) => {
  return combineReducers({
    // Add sync reducers here
    routing,
    modal,
    form,
    notifs,
    i18n: i18nReducer,
    User,
    ResetPassword,
    Ticket,
    Trips,
    Common,
		Landing,
		searchAirports,
		Support,
		Dashboard,
		ThankYou,
		EmailConfirm,
		EditInfo,
		ConfirmResetPassword,
		Checkout,
		ActivateUser,
    ...asyncReducers
  })
}

// export const injectReducer = (store, {key, reducer}) => {
//   if (store.asyncReducers[key]) {
//     return
//   }
//   store.asyncReducers[key] = reducer
//   store.replaceReducer(makeRootReducer(store.asyncReducers))
// }

export default makeRootReducer
