import {applyMiddleware, compose, createStore} from 'redux'
import {routerMiddleware} from 'react-router-redux'
import thunk from 'redux-thunk'
import makeRootReducer from './reducers'
import makeRootSaga from './sagas'
import createSagaMiddleware from 'redux-saga'
import Cookies from 'js-cookie'
import {syncTranslationWithStore} from 'react-redux-i18n'
import { createCookieMiddleware } from 'redux-cookie'
import config from 'config.json'
import productConfig from '../../config'

const debug = require('debug')('app:createStore')

const { __CLIENT__, _STAG_ } = productConfig

export default (initialState = {}, history) => {


  // ======================================================
  // Middleware Configuration
  // ======================================================
  const sagaMiddleware = createSagaMiddleware()
  const middleware = [thunk, routerMiddleware(history), sagaMiddleware]
	const clientMiddleware = typeof window !== 'undefined' ?  [createCookieMiddleware(Cookies)] : []
	//use to connect Remote DevTools in case of iOS development. Change compose to composeEnhancers
	//const composeEnhancers = composeWithDevTools({ realtime: true})
	// ======================================================
	// Store Instantiation and HMR Setup
	// ======================================================
	const store = createStore(
		makeRootReducer(),
		initialState,
		compose(
			applyMiddleware(
				...middleware,
				...clientMiddleware
			),
			(!__PROD__ && !_STAG_ &&__CLIENT__ && typeof window !== 'undefined' && window.devToolsExtension) ? window.devToolsExtension() : f => f
		)
	)
	store.asyncReducers = {}
  store.asyncSagas = {}
  store.runSaga = (saga) => {
    sagaMiddleware.run(saga)
  }

  syncTranslationWithStore(store)

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const reducers = require('./reducers').default
      store.replaceReducer(reducers(store.asyncReducers))
    })
  }

  store.runSaga(makeRootSaga())

  return store
}
