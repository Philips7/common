import {fork} from 'redux-saga/effects'
import signUpFacebook from 'sagas/signUpFacebook'
import signUp from 'sagas/signUp'
import signIn from 'sagas/signIn'
import userGet from 'sagas/userGet'
import resetPassword from 'sagas/resetPassword'
import userUpdate from 'sagas/userUpdate'
import refreshToken from 'sagas/refreshToken'
import watchStartup from 'sagas/startup'
import checkFlight from 'sagas/checkFlight'
import searchTrips from 'sagas/searchTrips'
import signUpNewsletter from 'sagas/signUpNewsletter'
import checkIP from 'sagas/checkIP'

export const makeRootSaga = () =>
  function *() {
    yield fork(watchStartup)
    yield fork(signUpFacebook().watcher)
    yield fork(signUp().watcher)
    yield fork(signIn().watcher)
    yield fork(userGet().watcher)
    yield fork(userUpdate().watcher)
    yield fork(resetPassword().watcher)
    yield fork(refreshToken().watcher)
    yield fork(checkFlight().watcher)
    yield fork(searchTrips().watcher)
    yield fork(signUpNewsletter().watcher)
    yield fork(checkIP().watcher)
  }

export const injectSagas = (store, {key, sagas}) => {
  if (store.asyncSagas[key]) {
    return
  }
  store.asyncSagas[key] = sagas
  store.runSaga(function *() {
    yield sagas.map(saga => fork(saga().watcher))
  })
}

export default makeRootSaga
