export const isSmallScreen = __CLIENT__ ? window && window.matchMedia('(max-width: 767px)').matches : {}
export const isTabletScreen = __CLIENT__ ? window && window.matchMedia('(max-width: 992px)').matches : {}
export const isNotHighScreen = __CLIENT__ ? window && window.matchMedia('(max-height: 799px)').matches : {}

const iOSRegex = new RegExp('\\biPhone.*Mobile|\\biPod|\\biPad', 'i')
export const isiOS = __CLIENT__ ? window && iOSRegex.test(window.navigator.userAgent) : {}

export default isSmallScreen
