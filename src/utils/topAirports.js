import { anywhereObj } from './helpers'
// Top departures: please change object properties in array if you want to have different options in departure search.
const topDepartures = [
  {
    name: 'London',
    country: 'United Kingdom',
    type: 'city',
    custom: true,
    first: true,
    code: [
      'LTN',
      'SEN',
      'LCY',
      'STN',
      'LGW',
      'LHR',
    ]
  },
  {
    name: 'Manchester',
    country: 'United Kingdom',
    type: 'city',
    custom: true,
    code: 'MAN'
  },
  {
    name: 'Edinburgh',
    country: 'United Kingdom',
    type: 'city',
    custom: true,
    code: [
      'EDI',
    ]
  },
  {
    name: 'Birmingham',
    country: 'United Kingdom',
    type: 'city',
    custom: true,
    code: [
      'BHX',
    ]
  },
  {
    name: 'Glasgow',
    country: 'United Kingdom',
    type: 'city',
    custom: true,
    code: [
      'PIK',
      'GLA',
    ]
  },
]

const topDestination = [
  anywhereObj,
  {
    name: 'Bogota',
    country: 'Colombia',
    type: 'city',
    custom: true,
    first: true,
    code: [
      'BOG',
    ]
  },
  {
    name: 'San Francisco',
    country: 'USA',
    type: 'city',
    custom: true,
    code: [
      'OAK',
      'SJC',
      'SFO',
    ]
  },
  {
    name: 'New York',
    country: 'USA',
    type: 'city',
    custom: true,
    code: [
      'EWR',
      'LGA',
      'JFK',
    ]
  },
  {
    name: 'Los Angeles',
    country: 'USA',
    type: 'city',
    custom: true,
    code: [
      'LAX',
    ]
  },
]

// Top destinations: please change object properties in array if you want to have different options in destination search.
const topDestinationsLondon = [
  anywhereObj,
  {
    name: 'Copenhagen',
    country: 'Denmark',
    type: 'city',
    custom: true,
    first: true,
    code: [
      'RKE',
      'CPH',
    ]
  },
  {
    name: 'Dublin',
    country: 'Ireland',
    type: 'city',
    custom: true,
    code: [
      'DUB',
    ]
  },
  {
    name: 'Oslo',
    country: 'Norway',
    type: 'city',
    custom: true,
    code: [
      'RYG',
      'OSL',
    ]
  },
  {
    name: 'Prague',
    country: 'Czech Republic',
    type: 'city',
    custom: true,
    code: [
      'PRG',
      'VOD',
    ]
  },
  {
    name: 'Los Angeles',
    country: 'USA',
    type: 'city',
    custom: true,
    code: [
      'LAX',
    ]
  },
]

const topDestinationsManchester = [
  anywhereObj,
  {
    name: 'Dublin',
    country: 'Denmark',
    type: 'city',
    custom: true,
    first: true,
    code: [
      'DUB',
    ]
  },
  {
    name: 'Hamburg',
    country: 'Germany',
    type: 'city',
    custom: true,
    code: [
      'XFW',
      'HAM',
    ]
  },
  {
    name: 'Amsterdam',
    country: 'Netherlands',
    type: 'city',
    custom: true,
    code: [
      'AMS',
    ]
  },
  {
    name: 'Barcelona',
    country: 'Spain',
    type: 'city',
    custom: true,
    code: [
      'BCN',
      'GRO',
    ]
  },
  {
    name: 'Berlin',
    country: 'Germany',
    type: 'city',
    custom: true,
    code: [
      'THF',
      'BER',
      'TXL',
      'SXF',
    ]
  },
]

const topDestinationsEdinburgh = [
  anywhereObj,
  {
    name: 'London',
    country: 'United Kingdom',
    type: 'city',
    custom: true,
    first: true,
    code: [
      'LTN',
      'SEN',
      'LCY',
      'STN',
      'LGW',
      'LHR',
    ]
  },
  {
    name: 'Copenhagen',
    country: 'Denmark',
    type: 'city',
    custom: true,
    code: [
      'RKE',
      'CPH',
    ]
  },
  {
    name: 'Dublin',
    country: 'Ireland',
    type: 'city',
    custom: true,
    code: [
      'DUB',
    ]
  },
  {
    name: 'Reykjavik',
    country: 'Iceland',
    type: 'city',
    custom: true,
    code: [
      'RKV',
      'KEF',
    ]
  },
  {
    name: 'Amsterdam',
    country: 'Netherlands',
    type: 'city',
    custom: true,
    code: [
      'AMS',
    ]
  },
]

const topDestinationsBirmingham = [
  anywhereObj,
  {
    name: 'Dublin',
    country: 'Ireland',
    type: 'city',
    custom: true,
    first: true,
    code: [
      'DUB',
    ]
  },
  {
    name: 'Amsterdam',
    country: 'Netherlands',
    type: 'city',
    custom: true,
    code: [
      'AMS',
    ]
  },
  {
    name: 'Sofia',
    country: 'Bulgaria',
    type: 'city',
    custom: true,
    code: [
      'SOF',
    ]
  },
  {
    name: 'Barcelona',
    country: 'Spain',
    type: 'city',
    custom: true,
    code: [
      'BCN',
      'GRO',
    ]
  },
  {
    name: 'Málaga',
    country: 'Spain',
    type: 'city',
    custom: true,
    code: [
      'AGP',
    ]
  },
]

const topDestinationsGlasgow = [
  anywhereObj,
  {
    name: 'London',
    country: 'United Kingdom',
    type: 'city',
    custom: true,
    first: true,
    code: [
      'LTN',
      'SEN',
      'LCY',
      'STN',
      'LGW',
      'LHR',
    ]
  },
  {
    name: 'Dublin',
    country: 'Ireland',
    type: 'city',
    custom: true,
    code: [
      'DUB',
    ]
  },
  {
    name: 'Amsterdam',
    country: 'Netherlands',
    type: 'city',
    custom: true,
    code: [
      'AMS',
    ]
  },
  {
    name: 'Bucharest',
    country: 'Romania',
    type: 'city',
    custom: true,
    code: [
      'BBU',
      'OTP',
    ]
  },
  {
    name: 'Berlin',
    country: 'Germany',
    type: 'city',
    custom: true,
    code: [
      'THF',
      'BER',
      'TXL',
      'SXF',
    ]
  },
]

const getDestSuggestions = (type) => {
  const suggestions = {
    'London': topDestinationsLondon,
    'Manchester': topDestinationsManchester,
    'Edinburgh': topDestinationsEdinburgh,
    'Birmingham': topDestinationsBirmingham,
    'Glasgow': topDestinationsGlasgow,
  }

  //Will be used later
  //return type ? suggestions[type] : [anywhereObj]
  return type ? topDestination : [anywhereObj]
}

export {
  topDepartures,
  getDestSuggestions
}
