export const sortByTypes = [
  {
    name: 'Price',
    val: 0
  },
  {
    name: 'Shortest',
    val: 1
  },
  {
    name: 'Recommended',
    val: 2
  }
]


export const sortByStops = [
  {
    name: 'Any',
    val: null
  },
  {
    name: 'DirectFlights',
    val: 0
  },
  {
    name: 'UpTo1Stop',
    val: 1
  },
  {
    name: 'UpTo2Stops',
    val: 2
  },
]

export const sortByMonth = [
	{
    "value": 3,
    "rate": 10.50,
    "apr": 95.8
  },
	{
		"value": 4,
		"rate": 11.99,
    "apr": 86.1
	},
	{
		"value": 5,
		"rate": 13.99,
    "apr": 84.6
	},
	{
		"value": 6,
		"rate": 14.99,
    "apr": 76.5
	},
  {
    "value": 7,
    "rate": 14.99,
    "apr": 64.5
  },
  {
    "value": 8,
    "rate": 14.99,
    "apr": 55.7
  },
  {
    "value": 9,
    "rate": 14.99,
    "apr": 49
  },
	{
		"value": 10,
		"rate": 17,
    "apr": 51.8
	},
]
