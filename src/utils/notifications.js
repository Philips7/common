import {I18n} from 'react-redux-i18n'
import { isClient } from './helpers'

let notifSend = null
if (isClient) {
	const notifActions = require('redux-notifications').actions
	notifSend = notifActions.notifSend
}

export const errorNotification = (message = I18n.t('Notify.smthGoWrong'), dismissAfter = 5000) =>
  notifSend({
    message,
    kind: 'danger',
    dismissAfter,
  })

export const successNotification = (message = I18n.t('Notify.saveChanges'), dismissAfter = 5000) =>
  notifSend({
    message,
    kind: 'success',
    dismissAfter,
  })
