import Scroll from 'react-scroll'
import moment from 'moment'
import { I18n } from 'react-redux-i18n'
import { defaultFilters } from '../redux/modules/Trips.js'
const scroller = Scroll.animateScroll
const scrollerElement = Scroll.scroller
const scrollOptions = {
	duration: 1000,
	smooth: true
}

export const isClient = __CLIENT__ && typeof window !== 'undefined'

export const anywhere = I18n.t('form.anywhere')
export const anywhereObj = {
	type: 'country',
	name: anywhere,
	flag: ''
}

export const anywhereCheck = x => x === anywhere

export const isDefaultFilter = (x, y) => {
	if (typeof x === 'boolean') {
		return x
	}
	else if (typeof x === 'object') {
		return (x && (x.min === defaultFilters[y].min && x.max === defaultFilters[y].max))
	}
	else if (Number.isInteger(x)) {
		return (x === defaultFilters[y])
	}
}

export const isEmpty = obj => obj ? !Object.keys(obj).length : true

export const convertTitle = title => typeof title === 'object' ? null : `${title}`

export const oneWay = way => way == '1'

export const serializeUser = values => ({
	...values,
	birth_date: serializeDateAccordingToBackend(values.birth_date),
	title: JSON.parse(values.title),
	phone: values.phone.replace(/[\s()-]/gi, ''), //Remove whitespace ( ) and
	document_expiration: !!values.document_expiration ? serializeDateAccordingToBackend(values.document_expiration) : undefined
})

export const serializeDateAccordingToBackend = date => {
	const splitDate = date.split('-')
	return `${splitDate[2]}-${splitDate[1]}-${splitDate[0]}`
}

export const scrollToTop = () => scroller.scrollToTop(scrollOptions)

export const scrollTo = (backPosition, customScrollOptions) => scroller.scrollTo(backPosition, customScrollOptions || scrollOptions)
export const scrollToElement = (element, customScrollOptions) =>
	scrollerElement.scrollTo(element, customScrollOptions || scrollOptions)

export const addCompaniesMap = trip => {
	let companiesMap = new Map()
	trip.outbound.segments.forEach(segment => {
		companiesMap.set(segment.carrier_iata, {
			carrier_icon: segment.carrier_icon,
			carrier: segment.carrier
		})
	})
	trip.outbound.companiesMap = Array.from(companiesMap)

	if (trip.type === 2) {
		companiesMap.clear()
		trip.returnn.segments.forEach(segment => {
			companiesMap.set(segment.carrier_iata, {
				carrier_icon: segment.carrier_icon,
				carrier: segment.carrier
			})
		})
		trip.returnn.companiesMap = Array.from(companiesMap)
	}
	return trip
}

export const parseTimeToHHMM = minutes => {
	const hour = Math.floor(minutes / 60)
	const minute = (minutes - hour * 60)
	return moment({ hour, minute }).format('HH:mm')
}

export const iOSHandlers = () => {
	document.addEventListener('gesturestart', e => e.preventDefault())
	document.documentElement.addEventListener('touchstart', e => {
		(e.touches.length > 1) && e.preventDefault()
	}, false)
}

export const globalBodyStyles = (className, addStyle) => {
	const body = document.getElementsByTagName('body')[0]
	if (addStyle) {
		body.classList.add(className)
	} else {
		body.classList.remove(className)
	}
}

export const getDeposit = (departure, totalPrice) => ((moment(departure).diff(moment(new Date()), 'days') > 29 ? 0.05 : 0.1) * totalPrice).toFixed(2)

export const showFeaturedPrice = (featureFlights, code, code2) => {
	let price = null
	if (featureFlights && featureFlights.length) {
		const flightExist = featureFlights.find(val => val.destination_place.code === code || code2)
		if (flightExist) price = flightExist.price
	}
	return price
}

export const scrollToFirstError = (errors) => {
	if (typeof errors === 'string') {
		const element = document.querySelector(`[name="${errors}"]`)
		if (element) {
			scroller.scrollTo(element.offsetTop - 50)
		}
	} else {
		const errorFields = getErrorFieldNames(errors)
		// Using breakable for loop
		for (let i = 0; i < errorFields.length; i++) {
			const fieldName = `${errorFields[i]}`
			// Checking if the marker exists in DOM
			const element = document.querySelector(`[name="${fieldName}"]`)
			if (element) {
				if (fieldName.indexOf('birth_date') >= 0) {
					scroller.scrollTo(element.offsetParent.offsetTop - 50)
				} else {
					scroller.scrollTo(element.offsetTop - 50)
				}
				break
			}
		}
	}
}

export const getErrorFieldNames = (obj, name = '') => {
	const errorArr = []
	errorArr.push(Object.keys(obj).map((key) => {
		const next = obj[key]
		if (next) {
			if (typeof next === 'string') {
				return name + key
			}
			// Keep looking
			if (next.map) {
				errorArr.push(next.map((item, index) => getErrorFieldNames(item, `${name}${key}[${index}].`)).filter(o => o))
			}
		}
		return null
	}).filter(o => o))
	return flatten(errorArr)
}

const flatten = (arr) => {
	return arr.reduce((flat, toFlatten) => flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten), [])
}

export const getValues = (trip, all_months = {}, paybackSelected, checkout = false) => {
  if (checkout) {
    const { per_month, total_price, price_per_person, service_fee_percentage, paid_today } = trip
    return { per_month, total_price, price_per_person, service_fee_percentage, paid_today }
  } else {
    const { per_month, total_price, price_per_person, service_fee_percentage, paid_today } = all_months && all_months[paybackSelected]
    return { per_month, total_price, price_per_person, service_fee_percentage, paid_today }
  }
}
