export default {
  employment_status: [
    {
      value: 29,
      label: 'Full Time Employed'
    },
    {
      value: 30,
      label: 'Part Time Employed'
    },
    {
      value: 31,
      label: 'Self Employed'
    },
    {
      value: 32,
      label: 'Not Employed'
    },
    {
      value: 33,
      label: 'Student'
    },
    {
      value: 34,
      label: 'Homemaker'
    },
    {
      value: 35,
      label: 'Retired'
    },
    {
      value: 36,
      label: 'Other'
    },
  ],
  residental_status: [
    {
      value: 22,
      label: 'Owner'
    },
    {
      value: 23,
      label: 'Tenant Unfurnished'
    },
    {
      value: 24,
      label: 'Tenant Furnished'
    },
    {
      value: 25,
      label: 'Council Tenant'
    },
    {
      value: 26,
      label: 'House Share'
    },
    {
      value: 27,
      label: 'Living With Parents'
    },
    {
      value: 28,
      label: 'Other Tenant'
    },
  ],
  title: [
    {
      value: 17,
      title: 'Male'
    },
    {
      value: 20,
      title: 'Female'
    }
  ],
  permanent_residence: [
    {
      value: 1,
      title: 'More than a year'
    },
    {
      value: 2,
      title: 'Less than a year'
    }
  ]
}
