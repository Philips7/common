import { getDeposit } from './helpers'

export const priceParser = (str) => {
  const num = Number.parseFloat(Number.parseFloat(str).toFixed(2))
  return Number.isInteger(num) ? num.toFixed(0) : num.toFixed(2)
}
export const countTotalPrice = (airfare, fee = 0,
  rate = 0) => priceParser((airfare + fee) * (1 + rate / 100))

export const countPerMonth = (totalPrice, value) => priceParser((totalPrice / value))

export const countFeatureTripPrice = (airfare, fee,
  rate, months) => countPerMonth(countTotalPrice(airfare, fee, rate), months)

export const breakDownPrices = ({ airfare, fake_booking_fee_percent }, { rate, value }) => {
  const totalPrice = countTotalPrice(airfare, fake_booking_fee_percent, rate)
  const perMonth = countPerMonth(totalPrice, value)
  return {
    interestRate: rate,
    totalPrice,
    perMonth,
    creditPoint: value
  }
}

export const countDeposit = (departure, totalPrice) => getDeposit(departure, totalPrice)
