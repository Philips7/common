import {connect} from 'react-redux'
import ResetPasswordModal from '../components/modals/ResetPasswordModal/ResetPasswordModal'
import {resetPasswordAttempt} from '../redux/modules/ResetPassword'

const mapActionCreators = {
  onSubmit: resetPasswordAttempt
}

const mapStateToProps = (state) => ({
  isVerifiedMessage: state.User.isVerifiedMessage,
  signUpLoading: state.User.signUpLoading,
  user: state.User.user
})

export default connect(mapStateToProps, mapActionCreators)(ResetPasswordModal)
