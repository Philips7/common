import {connect} from 'react-redux'
import {show, hide} from 'redux-modal'
import UKOnlyModal from 'components/modals/UKOnlyModal/UKOnlyModal'

const mapActionCreators = {
  show
}

const mapStateToProps = (state) => ({
  isVerifiedMessage: state.User.isVerifiedMessage,
  signInLoading: state.User.signInLoading,
  userGetLoading: state.User.userGetLoading,
  verificationLoading: state.User.verificationLoading,
  user: state.User.user,
  emailRegistered: state.Checkout ? state.Checkout.emailRegistered : false
})

export default connect(mapStateToProps, mapActionCreators)(UKOnlyModal)
