import {connect} from 'react-redux'
import {show, hide} from 'redux-modal'
import LoginModal from 'components/modals/LoginModal/LoginModal'
import {signInAttempt, logout, signUpFacebookAttempt} from '../redux/modules/User'

const mapActionCreators = {
  show,
  signInAttempt,
  logout,
  signUpFacebookAttempt,
  onSubmit: signInAttempt,
  showRegisterModal: () => dispatch => {
    dispatch(hide('loginModal'))
    dispatch(show('registerModal'))
  },
  showResetPasswordModal: () => dispatch => {
    dispatch(hide('loginModal'))
    dispatch(show('resetPasswordModal'))
  }
}

const mapStateToProps = (state) => ({
  isVerifiedMessage: state.User.isVerifiedMessage,
  signInLoading: state.User.signInLoading,
  userGetLoading: state.User.userGetLoading,
  verificationLoading: state.User.verificationLoading,
  user: state.User.user,
  LoginModal: state.form.LoginModal,
  emailRegistered: state.Checkout ? state.Checkout.emailRegistered : false
})

export default connect(mapStateToProps, mapActionCreators)(LoginModal)
