import {connect} from 'react-redux'
import {show, hide} from 'redux-modal'
import Header from '../components/Header/Header'
import {changeLanguage, checkIpCookie} from '../redux/modules/Common'
import {signUpFacebookAttempt, signUpAttempt, logout, scrollToHowItWorks} from '../redux/modules/User'

const mapActionCreators = {
  show,
  hide,
  signUpAttempt,
  signUpFacebookAttempt,
  logout,
  checkIpCookie,
  changeLanguage,
  scrollToHowItWorks
}

const mapStateToProps = ({
  User: {
    loggedIn
  },
	Common: {
		ipUK,
	},
	routing: {
  	locationBeforeTransitions: {
  		pathname,
		}
	}
  // i18n: {
  //   locale
  // },
}) => ({
  loggedIn,
	ipUK,
	fromLanding: pathname === '/',
  // locale
})

export default connect(mapStateToProps, mapActionCreators)(Header)
