import {connect} from 'react-redux'
import {
  searchAirports,
  clearSuggestions
} from 'routes/Landing/modules/Landing'

import Autosuggest from 'routes/Landing/components/SearchField/Autosuggest/Autosuggest'

const mapActionCreators = {
  searchAirports,
  clearSuggestions
}

const mapStateToProps = ({Landing: {suggestions}}) => ({suggestions})

export default connect(mapStateToProps, mapActionCreators)(Autosuggest)
