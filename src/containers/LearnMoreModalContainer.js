import { connect } from 'react-redux'
import { show, hide } from 'redux-modal'
import LearnMoreModal from 'components/modals/LearnMoreModal/LearnMoreModal'

const mapActionCreators = {
  show,
  hide,
}

const mapStateToProps = () => ({})

export default connect(mapStateToProps, mapActionCreators)(LearnMoreModal)
