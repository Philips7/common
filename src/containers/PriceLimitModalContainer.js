import { connect } from 'react-redux'
import { show, hide } from 'redux-modal'
import PriceLimitModal from 'components/modals/PriceLimitModal/PriceLimitModal'

const mapActionCreators = {
  show,
  hide,
}

const mapStateToProps = () => ({})

export default connect(mapStateToProps, mapActionCreators)(PriceLimitModal)
