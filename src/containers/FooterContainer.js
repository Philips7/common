import {connect} from 'react-redux'
import {signUpNewsletterAttempt} from '../redux/modules/User'
import Footer from '../components/Footer'

const mapActionCreators = {
	signUpNewsletterAttempt
}

export default connect(null, mapActionCreators)(Footer)
