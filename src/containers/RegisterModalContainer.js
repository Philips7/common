import {connect} from 'react-redux'
import {show, hide} from 'redux-modal'
import RegisterModal from 'components/modals/RegisterModal/RegisterModal'
import {signUpFacebookAttempt, signUpAttempt, signInAttempt} from '../redux/modules/User'
import {forgetAboutTicket} from 'routes/Landing/modules/Landing'

const mapActionCreators = {
  signUpFacebookAttempt,
  signInAttempt,
  forgetAboutTicket,
  onSubmit: signUpAttempt,
  showLoginModal: () => dispatch => {
    dispatch(hide('registerModal'))
    dispatch(show('loginModal'))
  },
}

const mapStateToProps = ({User: {isVerifiedMessage, signUpLoading, userGetLoading}}) => ({
  isVerifiedMessage,
  signUpLoading,
  userGetLoading
})

export default connect(mapStateToProps, mapActionCreators)(RegisterModal)
